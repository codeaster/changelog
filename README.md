# Release Notes & Changelogs

This repository contains code_aster Release Notes and Changelog files.

## Release Notes

- [Release Note for code_aster 15.4](./release_notes/15.4.md)

## Changelogs

- [Changelogs for code_aster version 17](./histor/v17), the version under development (new features and bugfixes).
- [Changelogs for code_aster version 16](./histor/v16), the current stable version (bugfixes only).
- Browse [histor directory](./histor) to read changelogs for older versions.
