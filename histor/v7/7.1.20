========================================================================
Version 7.1.20 du : 15/10/2003
========================================================================


-----------------------------------------------------------------------
--- AUTEUR acbhhcd G.DEVESA   DATE  le 14/10/2003 a 16:51:50

------------------------------------------------------------------------
CORRECTION AL 2003-250
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   TITRE  "MULT_APPUI dans DYNA_*"
   FONCTIONNALITE
   On permet la valeur d'entree NON par defaut a MULT_APPUI sous EXCIT
   des commandes dynamiques pour eviter un plantage eventuel dependant
   de la position sous EXCIT des charges multi-appui avant les autres.
   On corrige en V6 et V7 dans le catalogue des operateurs
   DYNA_LINE_TRAN DYNA_NON_LINE plus DYNA_TRAN_EXPLI en V7 seulement,
   en remplacant :
   MULT_APPUI=SIMP(statut='f',typ='TXM',into=("OUI",))
   par :
   MULT_APPUI=SIMP(statut='f',typ='TXM',defaut="NON",into=("OUI","NON"))

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION

------------------------------------------------------------------------
CORRECTION AL 2003-255
   NB_JOURS_TRAV  : 0.2
   INTERET_UTILISATEUR : NON
   TITRE  "cas tests miss02a et miss04a NOOK en 7.1.19"
   FONCTIONNALITE
   Suite a l'implantation de la version 6.3 de MISS3D dans
   l'environnement Aster-Miss, on reajuste les tolerances du cas MISS02A
   a 3.E-3 et celles du cas MISS04A a 0.1

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION


-----------------------------------------------------------------------
--- AUTEUR d6bhhjp J.P.LEFEBVRE   DATE  le 10/10/2003 a 14:04:46

------------------------------------------------------------------------------
REALISATION EL 2003-187
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : OUI
   TITRE  DEFI_FICHIER : longueur de chaine
   FONCTIONNALITE
   DETAIL :
   Le nom de fichier pass� derri�re le mot cl� NOM_SYTEME pouvait
   se r�v�ler de longueur insuffisante, on le fait passer de 80 � 255
   caract�res.
   Une v�rification dans le catalogue de la commande validators=LongStr(1,255)
   est ajout�e.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U4.12.03 (commande DEFI_FICHIER)
      EXPL_ : le nom de fichier derri�re NOM_SYTEME est limit� � 255 caract�res
   VALIDATION
     test personnel

------------------------------------------------------------------------------
CORRECTION AL 2003-247
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : OUI
   TITRE bases au format hdf
   FONCTIONNALITE
   DETAILS
   Le traitement par jeimhd des objets de collection dispers�e de longueur variable
   �tait mal r�alis�, ce qui avait pour effet de pointer sur 0 ou n'importe quoi pour
   la longueur lors de l'allocation des segments de valeurs associ�s. D'o� un arret
   brutal en erreur <F>.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
   test zzzz119a
   On ajoute le fichier de commande zzzz119a.com1 qui fait une poursuite
   sur la base cr��e par les commandes pr�c�dentes et un FIN avec �criture
   au format hdf. Cette modification avait mis en �vidence le bug.


-----------------------------------------------------------------------
--- AUTEUR durand C.DURAND   DATE  le 13/10/2003 a 11:42:22

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV : 1
   INTERET_UTILISATEUR : OUI
   TITRE  "Adaptation de maillage par HOMARD : raccourcissement des noms de
           fichiers"
   FONCTIONNALITE
     Les noms des fichiers d'�change entre ASTER et HOMARD �tait exprim�s avec
     leur chemin absolu. Cela pouvait conduire dans certains cas � d�passer la
     limite des 80 caract�res impos�e par la commande DEFI_FICHIER. Il y a
     avait alors arret brutal du calcul. Ces noms sont d�sormais les noms
     relatifs au r�pertoire d'ex�cution d'ASTER.
   DETAILS :
     Modifications du python : macr_adap_mail_ops, la macro en python qui pilote
     MACR_ADAP_MAIL et MACR_INFO_MAIL
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION : cas-tests zzzz121a et zzzz121c

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE superviseur
   DETAILS
     modification du visiteur genpy.py pour filtrer sur les mots cl�s cach�s
     Dans l'affichage de l'�cho de la commande dans le fichier de messages,
     ces mots cl�s cach�s polluaient surtout STAT_NON_LINE.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION


-----------------------------------------------------------------------
--- AUTEUR jmbhh01 J.M.PROIX   DATE  le 13/10/2003 a 10:51:55

------------------------------------------------------------------------------
CORRECTION AL 2003-207
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   POUR_LE_COMPTE_DE : Aimery Assire
   TITRE   cas test SSNV164B (cables de precontraintes) sur linux
   FONCTIONNALITE
    En version 7.1.15 sur linux, le cas test ssnv164b plante en erreur_F
    avec le message :
     <F> <STAT_NON_LINE> <TE0248 COMP1D> INITIALISATION DE  LA DICHOTOMIE
          IMPOSSIBLE     - DEPLACEMENT  EN Y NUL

     En fait cela provient de l'algorithme utilis� pour int�grer les
     comportements 1D. Pour que tous les comportements en contraintes planes
     soient utilisables en 1D, il faut pour cela annuler SIGMA YY.
     On proc�de par dichotomie pour trouver DEPSYY tel que SIGYY=0.
     Pour initialiser la dichotomie, on prend DEPSYY=-DEPSXX. Mais si DEPSXX
     etait nul (� une precision pr�s), on �mettait le message ci-dessus.
     on corrige pour ce cas pr�cis en faisant appel � la routine d'int�gration
     du comportement, avec DEPS=0, ce qui permet de sortir DSIG=0, mais aussi
     le comportement tangent ; et on ne fait pas de dichotomie dans ce cas.
   DETAILS
     La routine modifi�e est COMP1D. Elle est appel�e par les BARRES (TE0148)
     et les poutres multi-fibres (pmfcom).
     On valide donc la correction en faisant passer les tests correspondants.

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
    passage de tous les tests non lin�aires de barres et des poutres multi-fibres


-----------------------------------------------------------------------
--- AUTEUR pbadel P.BADEL   DATE  le 13/10/2003 a 08:41:07

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV : 5.
   INTERET_UTILISATEUR : OUI
   TITRE debuggage en sensibilite en non-lineaire en 2D et 3D
   FONCTIONNALITE  STAT_NON_LINE  -  Mot-cle SENSIBILITE
     En corrigeant l'AL 2002-111, Pierre Badel a modifie par
     erreur un signe dans la section CHARGEMENTS SENSIBILITE/DE-
     RIVATION CHARGEMENTS MECANIQUES DONNES de la routine NMCHAR.
     Je rectifie cette erreur et j'en profite pour corriger ce
     qui fait que cela ne marchait pas en 3D: dans NSPL3D, il
     fallait multiplier par RAC2 (racine de 2) les composantes
     numero 4 a 6 des contraintes issues du calcul direct a
     l'instant + et a l'instant - avant l'appel a NSCOMP. La
     meme erreur existait dans NSPL2D avec la 4eme composante des
     contraintes et je la corrige egalement.
   DETAILS
     Deverrouillage du cas 3D qui maintenant fonctionne.
     Modification des routines FORTRAN :
        nmchar,nscomp,nspl2d,nspl3d

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI  DEPUIS : 7.01.18
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
     perso (de nouveaux tests seront ajout�s dans le cadre de la
     realisation de l'EL 2003-174)


-----------------------------------------------------------------------
--- AUTEUR romeo R.FERNANDES   DATE  le 13/10/2003 a 15:13:43

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 0.1
   INTERET_UTILISATEUR : OUI
   TITRE   Probl�me avec la loi de couplage de type LIQU_VAPE_GAZ
   FONCTIONNALITE
     LIQU_VAPE_GAZ avec des fonctions d�pendantes
     de la pression de gaz non constantes.
   DETAILS
     Dans le cas de la loi de couplage de type LIQU_VAPE_GAZ,
     on remplace NOMPAR(2) = 'TEMP' par NOMPAR(3) = 'TEMP'
     pour �viter l'�crasement de NOMPAR(2) = 'PGAZ'.

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION : cas tests THM de la base aster


========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

 CASTEST MODIF miss02a                       acbhhcd G.DEVESA          1272      9      1
 CASTEST MODIF miss04a                       acbhhcd G.DEVESA           485      7      1
 CASTEST MODIF zzzz119a                      d6bhhjp J.P.LEFEBVRE        83      1      2
CATALOPY MODIF commande/defi_fichier         d6bhhjp J.P.LEFEBVRE        40      3      3
CATALOPY MODIF commande/dyna_line_tran       acbhhcd G.DEVESA           128      4      3
CATALOPY MODIF commande/dyna_non_line        acbhhcd G.DEVESA           272      4      3
CATALOPY MODIF commande/dyna_tran_expli      acbhhcd G.DEVESA           263      4      3
 FORTRAN MODIF algorith/comp1d               jmbhh01 J.M.PROIX          267     11      4
 FORTRAN MODIF algorith/nmchar                pbadel P.BADEL            394      2      2
 FORTRAN MODIF algorith/nscomp                pbadel P.BADEL             78      1      6
 FORTRAN MODIF algorith/nspl2d                pbadel P.BADEL            196     14      3
 FORTRAN MODIF algorith/nspl3d                pbadel P.BADEL            199     18      3
 FORTRAN MODIF algorith/thmrcp                 romeo R.FERNANDES       1066      2      2
 FORTRAN MODIF jeveux/jeimhd                 d6bhhjp J.P.LEFEBVRE       326      2      1
 FORTRAN MODIF jeveux/jxecro                 d6bhhjp J.P.LEFEBVRE       274      4      2
 FORTRAN MODIF utilifor/iunifi               d6bhhjp J.P.LEFEBVRE        63      2      2
 FORTRAN MODIF utilifor/ulisop               d6bhhjp J.P.LEFEBVRE        66      2      2
 FORTRAN MODIF utilitai/op0026               d6bhhjp J.P.LEFEBVRE        60      2      2
 FORTRAN MODIF utilitai/ulclos               d6bhhjp J.P.LEFEBVRE        51      2      2
 FORTRAN MODIF utilitai/uldefi               d6bhhjp J.P.LEFEBVRE       166      3      3
 FORTRAN MODIF utilitai/ulimpr               d6bhhjp J.P.LEFEBVRE        67      2      2
 FORTRAN MODIF utilitai/ulinit               d6bhhjp J.P.LEFEBVRE        43      2      2
 FORTRAN MODIF utilitai/ulnume               d6bhhjp J.P.LEFEBVRE        56      2      2
 FORTRAN MODIF utilitai/ulopen               d6bhhjp J.P.LEFEBVRE       199     15     15
  PYTHON MODIF Execution/genpy                durand C.DURAND           305      3      2
  PYTHON MODIF Macro/macr_adap_mail_ops       durand C.DURAND           650     27     19


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :    0           0         0                +0
 MODIF :   26        7069       148      92       +56
 SUPPR :    0           0                 0        +0
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :   26        7069       148      92       +56 
