========================================================================
Version 7.0.16 du : 26/03/2003
========================================================================


-----------------------------------------------------------------------
--- AUTEUR assire A.ASSIRE   DATE  le 21/03/2003 a 14:23:48

------------------------------------------------------------------------------
REALISATION EL 2002-094
   NB_JOURS_TRAV  : 40.
   INTERET_UTILISATEUR : OUI
   TITRE  DEFI_CABLE_BP : Traitement des noeuds d'ancrages

   FONCTIONNALITE
   La modelisation d'un cable de precontraint se traduit generalement par
   des niveaux de contraintes trop elevees au niveau des noeuds d'ancrage.
   Ceci se traduit par un ecrasement du beton, et par consequant
   l'impossibilite de realiser des calculs nonlineaires sans precautions
   particulieres. Dans la r�alit�, l'effort induit par la tension du cable
   n'est pas port� par un seul noeud mais pas une zone de matiere plus dure.

   On ajoute dans l'operateur DEFI_CABLE_BP un mot-cl� CONE qui permet la
   d�finition d'une zone autour de l'ancrage, par la syntaxe suivante :

     CONE=_F(RAYON=rayon,
             LONGUEUR=longueur,
             PRESENT=('OUI','OUI',),),

   Cette zone est en fait une succession de cylindres d'axes les differents
   segments du cable, de rayon 'rayon' et de longueur (au sens abscisse
   curviligne le long du cable) 'longueur'.

   Les noeuds du maillage b�ton et du maillage cable qui sont dans cette zone
   formeront alors un bloc rigide (equivalent � un LAISON_SOLIDE) en sortie de
   AFFE_CHAR_MECA mot-cl� RELA_CINE_BP.


   !!!!!!! ATTENTION UTILISATEURS DE DEFI_CABLE_BP !!!!!!!

   Modifications effectu�es dans DEFI_CABLE_BP (ancienne version et nouvelle
   version) :

   - le positionnement du mot-cl� TYPE_ANCRAGE est chang� (il est sorti de
   DEFI_CABLE et devient global � tous les cables d'un meme DEFI_CABLE_BP).

   - lorsque l'on utilise un CONE, on retire la possibilit� de d�finir des
   cables par des MAILLES. Il faut faire un DEFI_GROUP / CREA_GROUP_MA option
   MAILLE pour g�n�rer un cable � partir d'une succession de mailles.

   DETAILS
     Avec ce d�veloppement, la commande DEFI_CABLE_BP devient une macrocommande
   DEFI_CABLE_BP et l'ancienne commande DEFI_CABLE_BP devient DEFI_CABLE_OP et
   est "cach�" aux utilisateurs (elle reste appel�e par la macro en
   sous-terrain).

     Concernant la syntaxe, et afin d'harmoniser le catalogue, on deplace le
   mot-cl� TYPE_ANCRAGE (qui etait sous DEFI_CABLE) pour le mettre au meme
   niveau que les autres mot-cl�s servant � d�finir un groupe de cable :
   TENSION_INIT, RECUL_ANCRAGE, CONE. En consequence, on ne peut definir via
   DEFI_CABLE que des cables qui ont les memes caracteristiques. Sinon, il faut
   declarer plusieurs DEFI_CABLE_BP.

     Concernant le fonctionnement de cette macro : on utilise la commande
   DEFI_GROUP option TUNNEL pour generer le groupno (avec un nom generique) et
   qui sera stock� dans la structure de donn�es maillage. L'affectation des
   relations cin�matiques est toujours effectu�e par AFFE_CHAR_MECA mot-cl�
   RELA_CINE_BP. La prise en compte du CONE dans AFFE_CHAR_MECA s'effectue sans
   action de l'utilisateur.

     Modification de la structure de donn�e cablepr pour rajouter le nom du ou
   des eventuels groupes de noeuds correspondant aux blocs rigides.

     Ajout du mot-cl� INFO dans DEFI_CABLE_BP : celui-ci devient bavard.

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
     Tous les cas-tests utilisant DEFI_CABLE_BP ont �t� modifi�s pour prendre
   en compte la nouvelle position du mot-cl� TYPE_ANCRAGE : sslv115a, ssnp109a,
   ssnv164a, yyyy117a, ssnp108a, ssnv137a, ssnv164b  zzzz111a (sans utilisation
   de CONE).
     Cas-test suppl�mentaire pour le mot-cl� CONE : ssnv164b.

   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.42.04  Macro-commande DEFI_CABLE_BP
     DOC_V : V6.04.164 V6.04.164
     DOC_D : D4.06.16  Structure de donn�es cablepr
     DOC_R : R7.01.02  Mod�lisation des cables de pr�contrainte

------------------------------------------------------------------------------
REALISATION EL 2003-063
   NB_JOURS_TRAV  : 1.
   INTERET_UTILISATEUR : OUI
   TITRE  DEFI_GROUP : possibilit� de ne pas alarmer.

   FONCTIONNALITE
     Avec les possibilit�s des Macro-commandes en python, il peut arriver que
   des messages d'Alarmes emis par DEFI_GROUP ne soient pas reellement grave
   (s'il n'a pas gener� le groupe en question par exemple), dans le cas ou la
   macro-commande gere ce cas.

     On ajoute un nouveau mot-cl� facultatif ALARME dans DEFI_GROUP, qui prend
   une valeur parmi 'OUI' ou 'NON' avec un defaut a 'OUI', et qui a pour but de
   ne pas emettre d'<A>larmes si ALARME='NON' est renseign�.

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.22.01  Commande DEFI_GROUP


-----------------------------------------------------------------------
--- AUTEUR boiteau O.BOITEAU   DATE  le 21/03/2003 a 15:43:31

------------------------------------------------------------------------------
CORRECTION AL 2002-553
  NB_JOURS_TRAV  : 0.1
   INTERET_UTILISATEUR : NON
   TITRE    Cas-test FORMA04B NOOK sur linux

   FONCTIONNALITE/CONTEXTE
   Rien d'inquietant a l'horizon, il s'agit de cas-test formalisant le TP n�1
   de la formation "indicateur et adaptation de maillage". Le NOOK se produit
   sur une erreur relative de quantite qui est deja, elle-meme, l'erreur
   relative de quantite mecanique (energie de deformation). Erreur
   relative calculee, via PYTHON, avant et apres un processus de remaillage
   uniforme.
   Donc, un delta sur l'energie a la 10ieme decimale produit un delta sur
   l'erreur relative, dont l'ordre de grandeur est 10-6, de 0.1%.
   On modifie le dernier TEST_FONCTION de forma04b, de 1% a 2%

   SUR COMPACT
   REFERENCE: NON_REGRESSION
 OK  BIDON        RELA    -0.136 % VALE: 7.0833235172460D-06
      0.00000E+00 TOLE     2.000 % REFE: 7.0929655135255D-06

  SUR LINUX
   REFERENCE: NON_REGRESSION
 OK  BIDON        RELA    -1.252 % VALE: 7.0833235172460D-06
      0.00000E+00 TOLE     2.000 % REFE: 7.0929655135255D-06

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

  VALIDATION/MODE OPERATOIRE
     tests sur V7.00.15
  IMPACT_DOCUMENTAIRE : OUI
     DOC_V :  V6.03.120 (DOC DE FORMA04)

------------------------------------------------------------------------------
CLASSEMENT SANS SUITE AL 2002-515
   NB_JOURS_TRAV  : 0.1
   INTERET_UTILISATEUR : NON
   TITRE  Planton sous LINUX des cas tests FORMA04C/FORMA05AB
   FONCTIONNALITE
      Pb gestion des noms de machines du cluster par les shells utilises
      par HOMARD via la commande EXEC_LOGICIEL (cf. AL2002-531 de G.NICOLAS)

   VALIDATION
   IMPACT_DOCUMENTAIRE : NON


-----------------------------------------------------------------------
--- AUTEUR boyere E.BOYERE   DATE  le 24/03/2003 a 14:30:04

------------------------------------------------------------------------------
REALISATION EL 2003-021
   NB_JOURS_TRAV  : 3.0
   INTERET_UTILISATEUR : NON
   TITRE EVOLUTION DE LA TABLE PRODUITE PAR POST_DYNA_MODA_T

   FONCTIONNALITE
   On a ajoute l'intitule du choc dans la table produite par
   POST_DYNA_MODA_T dans les analyses d'impact et d'usure.
   Il est maintenant plus simple d'identifier le choc auquel on s'interesse.
   DETAILS
   Dans POCHOC et POCHPV on a remis en service le
      CALL JEVEUO(NOMK19//'.INTI','L', IDNINT)
   qui permet d'acceder a l'intitule dans la structure de donnees
   puis on l'a passe en parametre de STATIM, STATCH et STCHPV.

   DANS STATIM, STATCH et STCHPV on a ajoute INTITULE aux listes
   de parametres de la table et des lignes de donnees
   puis on a elargi la variable VALEK pour y mettre en premier
   l'intitule du choc, en deuxieme le nom du noeud de choc
   et en troisieme les diverses statistiques calculees.

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
      astout + passage de l'etude de utilisateur demandeur
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
CORRECTION AL 2002-552
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   TITRE RECUPERATION DU MOT CLEF MULT_APPUI

   FONCTIONNALITE
   G. devesa souhaitait modifier la methode de recuperation du mot clef mult_appui,
   i.e. tester MULT_APPUI='OUI' plutot que la presence du mot clef. C'est fait.
   Par ailleurs pour completer la validation de ce type d'analyse sismique,
   on emet une fiche EL a propos de ce chantier.
   DETAILS
   Dans DLNEWI, MDRECF, NMMUAP, RFRGEN et TRAN75, on a mis
          IF (K8B.EQ.'OUI') THEN
   a la place de
          IF (ND.NEQ.0) THEN
   ou
        IF (MONMOT(1).EQ.'OUI'.OR.MONMOT(2).EQ.'OUI') NOMMOT = 'OUI'
   a la place de
        IF (N2.NE.0 .OR. N3.NE.0) NOMMOT = 'OUI'
   comme c'etait realise en v5 !!! ???

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
      astout sur MULT_APPUI
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
CORRECTION AL 2003-058
   NB_JOURS_TRAV  : 2.
   INTERET_UTILISATEUR : NON
   TITRE NORM_MODE : PLUS D'ARRET FATAL EN CAS DE BANDE DE FREQUENCE VIDE
   FONCTIONNALITE
   On a restaure la fonctionnement de NORM_MODE d'avant la 6.2.13
   qui laissait passer des bandes de frequence vide dans NORM_MODE.
   Ce fonctionnement avait ete altere lors de la suppression de la premiere
   passe du superviseur. Or, pour un bon fonctionnement de NORM_MODE,
   il est necessaire de ne pas arreter l'execution dans ce cas.
   DETAILS
   dans op0037 (NORM_MODE) on passe direct a la fin si la bande de frequence
   est vide. Un petit message dans le .mess est largement suffisant.
      CALL JELIRA ( MODEIN//'           .ORDR', 'LONUTI', IRET, K8B )
   C SI LA BANDE DE FREQUENCE EST VIDE, ON NE FAIT RIEN
   C  => DIRECT A LA FIN APRES UN PETIT MESSAGE D'INFO
      IF ( IRET .EQ. 0 ) THEN
            IF ( NIV .GE. 1 ) THEN
               WRITE(IFM,1000) MODEIN
               WRITE(IFM,1030)
            ENDIF
            GOTO 9999
      ENDIF

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
   IMPACT_DOCUMENTAIRE : NON


-----------------------------------------------------------------------
--- AUTEUR cibhhpd D.NUNEZ   DATE  le 24/03/2003 a 15:35:46

------------------------------------------------------------------------------
CORRECTION AL 2003-036
   NB_JOURS_TRAV  : 1.
   POUR_LE_COMPTE_DE : A.M.DONORE
   INTERET_UTILISATEUR : OUI
   TITRE  un format unique pour IMPR_RESU

   FONCTIONNALITE
   On utilise maintenant un LOGICAL qui vaut true d�s que FORMAT= 'GMSH',
   pour ecrire l'entete une et une seule fois. Celui-ci ne depend donc plus de
   l'occurrence o� FORMAT='GMSH' est demande. Toutefois, il faudra mettre
   toutes les impressions au format GMSH dans la meme commande IMPR_RESU.

   Attention : en Version 6, il faudra faire un IMPR_RESU par format.

   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI

   VALIDATION
   IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U7.05.32


-----------------------------------------------------------------------
--- AUTEUR jmbhh01 J.M.PROIX   DATE  le 25/03/2003 a 09:52:14

------------------------------------------------------------------------------
REALISATION EL 2001-226
   NB_JOURS_TRAV  : 6
   INTERET_UTILISATEUR : OUI
   POUR_LE_COMPTE_DE : Pierre Badel
   TITRE : nouvelle methode de test de convergence
   FONCTIONNALITE :
      Les tests de convergence en residus actuels souffrent de deux defauts :
         1. ils melangent allegrement des grandeurs de nature differente
         2. le residu relatif est sensible a la taille des elements
      On introduit une nouvelle generation de test de convergence base sur
      l'idee suivante : element par element, les forces nodales obtenues quand
      le "vecteur contrainte" de l'elements (de dimension N_point_de_Gauss x
      NDIMSI) parcourt la sphere de rayon sig_refe (ou sig_refe est une
      precision de contrainte) est une bonne indication de la precision que
      l'on peut exiger sur l'equilibre
      Le grand interet est qu'on peut generaliser � tout type d'element, et
      tout type de modelisation (remplacer "contrainte" par flux_thermique ou
      flux_hydraulique pour respectivement la thermique et l'hydromecanique)
   DETAILS
      Pour le moment seuls les elements isoparametriques "standard" (i.e. pas
      les quasi-incompressibles et les a gradients de deformations) sont
      developpes. En revanche tout le vocabulaire est introduit.
      Utilisateur :
      -------------
        ajout des mots cles :
           SIGM_REFE
           EPSI_REFE
           FLUX_THER_REFE
           FLUX_HYD1_REFE
           FLUX_HYD2_REFE
           sous le mot-cle convergence de STAT_NON_LINE
        du mot-cle RESI_REFE_RELA sous le mot-cle convergence de STAT_NON_LINE

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
     ssnv108a
     ssnp102a
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.51.03
     DOC_R : R5.03.01

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 0.4
   INTERET_UTILISATEUR : NON
   TITRE : TESTS DU CATALOGUE MATERIAU
   FONCTIONNALITE
    Pour tester encore mieux le catalogue materiau, on modifie 2 tests :
    ZZZZ118C et D : ajout de test_resu sur toutes les mailles
    + ajout de la courbe de fatigue dans le materiau 30M5 (oubli)

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
REALISATION EL 2002-171
   NB_JOURS_TRAV  : 2
   POUR_LE_COMPTE_DE   : J.M.PROIX
   REALISE PAR L.VIVAN
   INTERET_UTILISATEUR : OUI
   TITRE  POST_RCCM/FATIGUE_B3200 et B3600: am�liorations � faire

   FONCTIONNALITE
   am�liorations r�alis�es dans la commande POST_RCCM, pour les options
   FATIGUE_B3200 et FATIGUE_B3600.

   Petite correction dans le calcul de Sn avec seisme, et dans l'�limination
   des lignes et colonnes de la matrice Salt
   => variations sur les facteurs d'usage de 2% sur certains tests
   de non r�gression

   DETAIL
   - Algorithme de calcul des Salt :
     la matrice des Salt est remplie compl�tement.
   - fichier messages (INFO 2)
     Matrice des Salt: on a ajout� les num�ros (utilisateur) des
     situations, � la fois sur les lignes et sur les colonnes.
     Calcul des SN et SP : on a ajout� les num�ros (utilisateur) des
     situations et on indique les ETATs qui sont combin�s.
     P et Q => num�ros de situation
     I et J => ETAT_A et ETAT_B
   - Calcul du facteur d'usage :
     dans RC36FU, �limination plus explicite en testant nocc=0
     on ne peut plus avoir nocc=0 et une colonne ou ligne non nulle

   - Pour TYPE_RESU='VALE_INST', le developpement sera r�alis�e en 7.2

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
     RCCM02,RCCM03,RCCM04
   IMPACT_DOCUMENTAIRE : OUI
     DOC_V : V3.01.113
     DOC_V : V3.01.115

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 2
   INTERET_UTILISATEUR : NON
   TITRE : KE MIXTE dans POST_RCCM
   FONCTIONNALITE
   Introduction du Ke mixte selon le RCCM. L'utilisateur de POST_RCCM
   choisir (pour les options FATGIUE_B3200 et B3600) le type de Ke par :
   TYPE_KE= 'KE_MECA' (d�faut) ou 'KE_MIXTE'
      Test dans une nouvelle modelisation RCCM01B (analytique) pour B3200
   + test dans RCCM03A (non r�gression) pour B3600

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
      RCCM01B
   IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U4.83.11
      DOC_V : V1.01.107

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 3
   INTERET_UTILISATEUR : NON
   TITRE : Accord sur RCCM04A
   FONCTIONNALITE
   Dans le test RCCM04A,
    correction d'une erreur de donn�es => r�sultats
    maintenant conformes � la feuille de calcul MMC
    avec transitoire 6 partout.
    On fait maintenant  une boucle pour tester les 9 ligaments
    Ecarts sur les facteurs d'usage inf�rieurs � 1%

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
      RCCM04
   IMPACT_DOCUMENTAIRE : OUI
      DOC_V : V3.04.136


-----------------------------------------------------------------------
--- AUTEUR mcourtoi M.COURTOIS   DATE  le 24/03/2003 a 13:53:27

------------------------------------------------------------------------------
REALISATION EL 2002-022
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : OUI
   TITRE  Ajout d'un titre dans IMPR_TABLE
   FONCTIONNALITE
      Mot cl� TITRE_TABLE facultatif : permet d'afficher un titre en d�but
      d'impression (avant le titre �ventuel de la sdd table)
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
   IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U4.91.03
   MODIF_SYNTAXE : OUI  (�quivalent � impact doc U4.02.01)
      TITRE_TABLE : Ce mot cl� permet d'afficher un titre en d�but
      d'impression (avant le titre �ventuel de la table)


-----------------------------------------------------------------------
--- AUTEUR pabhhhh N.TARDIEU   DATE  le 24/03/2003 a 14:53:44

------------------------------------------------------------------------------
CLASSEMENT SANS SUITE AL 2002-235
   NB_JOURS_TRAV : 0.2
   INTERET_UTILISATEUR : NON
   TITRE  Contact Methode Continue : Pb lors de la projection
   FONCTIONNALITE
     Je classe SANS_SUITE cette fiche dont l'�tude ne peut
     tourner : des mailles n'appartiennent pas au mod�le
     alors qu'elles voient un chargement par PRESS_REP.

   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
CLASSEMENT SANS SUITE AL 2001-498
   NB_JOURS_TRAV : 0.2
   INTERET_UTILISATEUR : NON
   TITRE  Pb DYNA_NON_LINE et Methode Continue
   FONCTIONNALITE
     Je classe SANS_SUITE cette fiche car le meme
     probl�me est soulev� par la fiche 2002-290.

   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
CLASSEMENT SANS SUITE AL 2000-434
   NB_JOURS_TRAV : 0.2
   INTERET_UTILISATEUR : NON
   TITRE  Contact et Metis demandent bcp de m�moire
   FONCTIONNALITE
     Je classe SANS_SUITE cette fiche dans la mesure o�
     o� j'�mets une fiche EL pour am�liorer la gestion
     m�moire du contact (stockage Morse).

   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
REALISATION EL 2002-076
   NB_JOURS_TRAV : 0.2
   INTERET_UTILISATEUR : NON
   TITRE  Performance CPU et M�moire du CONTACT

   FONCTIONNALITE
   Cette fiche soul�ve le pb du temps calcul et d'occupation
   m�moire pour le contact.
   Je classe cette fiche corrig�e car en terme de performance
   en temps, un effort a d�j� �t� r�alis� en amenant un gain
   int�ressant. En ce qui concerne la place m�moire, une fiche
   �volution est �mise.

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
CORRECTION AL 2002-473  AL 2002-474
   NB_JOURS_TRAV  : 2.
   POUR_LE_COMPTE_DE : N. Tardieu
   INTERET_UTILISATEUR : OUI
   TITRE  Contact - Frottement avec METIS, avec LDLT

   FONCTIONNALITE
   Probl�me dans le calcul de At.A avec la renum�rotation Metis,
   avec le solveur LDLT.
   Quand on faisait un calcul de contact + frottement avec la
   m�thode PENALISATION en 2D ou en 3D ou avec la m�thode LAGRANGIEN
   en 3D, on pouvait obtenir des r�sultats converg�s mais faux.

   DETAIL
   Lors du calcul du tableau de travail .VALE
   et lors de l'affectation du tableau .HCOL de la MATR_ASSE ATA,
   la variable ICUM donne le cumul du nombre de termes non nuls
   pour les NEQ colonnes de la matrice A. L'initialisation de cette variable
   � z�ro est incorrecte puisque la premi�re colonne de A a un traitement
   simplifi� particulier. Il faut donc initialiser la variable ICUM � ZI(ID2-1+1)
   qui d�signe le nombre de termes non nuls pour la premi�re colonne de A.

   routine modifi�e : - atasmo pour METIS
                      - atalc2 pour LDLT

   RESU_FAUX_VERSION_EXPLOITATION    :  OUI  DEPUIS : 6.0.7
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI  DEPUIS : 7.0.0
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
     ssnv128
   IMPACT_DOCUMENTAIRE : NON

--------------------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 0.2
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
   Dans l'archivage du contact, on d�truisait syst�matiquement
   une structure de donn�es temporaire meme si elle n'avait pas
   �t� cr��e. Cela faisait planter le code sous LINUX.

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
   IMPACT_DOCUMENTAIRE : NON


-----------------------------------------------------------------------
--- AUTEUR vabhhts J.PELLET   DATE  le 25/03/2003 a 10:22:56

------------------------------------------------------------------------------
CORRECTION AL 2003-061
   NB_JOURS_TRAV : 0.3
   INTERET_UTILISATEUR : NON
   TITRE "7.0.15 : ttll100b et ttlv100b s'arretent en erreur <F>"
   FONCTIONNALITE
   L'�volution des ELREFE de la semaine derni�re avait cass� les �l�ments de bord
   (SEG3) des mod�lisations THERMIQUE / AXIS_DIAG et PLAN_DIAG.
   On r�pare ces 2 �l�ments finis.
   DETAILS
   Correction des catalogues des 2 �l�ments et des fortran te0071 et te0073

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
REALISATION EL 2003-054
   NB_JOURS_TRAV : 2.5
   INTERET_UTILISATEUR : OUI
   TITRE "V�rification de la coh�rence du comportement en poursuite de calcul
          (STAT_NON_LINE/DYNA_NON_LINE)"

   FONCTIONNALITE
   Lorsque l'on initialise un calcul non-lin�aire m�canique (STAT_NON_LINE/
   DYNA_NON_LINE) par autre chose qu'un �tat nul, (par exemple lorsque l'on
   "poursuit" un calcul STAT_NON_LINE(reuse=U,...), Il faut r�cup�rer le
   champ de variables internes initiales pour l'associer � l'instant "-"
   du 1er incr�ment de charge.
   Il faut �videmment que ce champ soit coh�rent vis-�-vis du comportement
   choisi par l'utilisateur (COMP_INCR/RELATION).
   Par exemple, on ne peut pas se servir des variables internes d'un 1er
   calcul fait avec RELATION='LAIGLE'  pour initialiser un nouveau calcul
   avec RELATION='VMIS_ISOT_LINE' !

   Avant la correction :
   --------------------
   Jusqu'� pr�sent, la seule v�rification faite consistait � comparer le
   nombre de variables internes des 2 relations de comportement
   (VMIS_ISOT_LINE:2 et LAIGLE:4)

   La logique �tait la suivante :
     si nb_vari(compor_initial) == nb_vari(compor_suite)   : OK
     si nb_vari(compor_initial) < nb_vari(compor_suite)    : erreur <F>
                              (cas : VMIS_ISOT_LINE puis LAIGLE)
     si nb_vari(compor_initial) > nb_vari(compor_suite)    : alarme <A>
                              (cas : LAIGLE puis VMIS_ISOT_LINE)

     Les r�sultats FAUX provenaient de ce dernier cas (alarme) : On
     avertissait l'utilisateur que le nouveau comportement utilisait moins
     de variables internes que l'ancien mais on ne redimensionnait pas
     le champ.
     Les routines de calcul �l�mentaires imprudentes (au moins celles de 2D,
     3D, coques DKT) se m�langeaient alors les variables internes entre les
     points de gauss et les r�sulats �taient faux.

   Apr�s la correction :
   ----------------------
   2 cas de figure se pr�sentent selon la facon de donner l'�tat initial pour
   les variables internes:
     / via le mot cl� ETAT_INIT/EVOL_NOLI=U1  (cas le plus fr�quent)
     / via le mot cl� ETAT_INIT/VARI=chv1

   Dans le 2eme cas, on ne peut pas faire autre chose que comparer les nombres
   de variables (comme avant).
   En revanche, si l'utilisateur donne le nom de l'evol_noli initial, on peut
   comparer le nom des 2 relations de comportement ce qui est mieux.

   La logique est maintenant la suivante :

   Si evol_noli est donn� :
     si nom(compor_initial) == nom(compor_suite)   : OK
     sinon :
       si nom(compor_initial) == 'ELAS'            : OK
       si nom(compor_initial) == 'SANS'            : OK
       si nom(compor_initial) et nom(compor_suite)
         sont tous les 2 dans la liste:
         (LEMAITRE,VMIS_ISOT_TRAC,VMIS_ISOT_LINE)  : OK
       sinon :                                     : erreur <F>

   Sinon (champ de variables isol�) :
     si nb_vari(compor_initial) == nb_vari(compor_suite)   : OK
     si nb_vari(compor_initial) != nb_vari(compor_suite)    : erreur <F>

   Par ailleurs, pour �viter les r�sultats FAUX, la routine qui v�rifie la
   coh�rence, modifie parfois le champ de variables initiales :
      - si  nom(compor_initial) == 'ELAS' ou 'SANS' : on g�n�re des variables
        initiales nulles.
      - si  nom(compor_suite) == 'ELAS' ou 'SANS' : on remet les variables
        initiales � z�ro.


   R�sum� concernant les r�sultats FAUX :
   ======================================
   Le code donnait des r�sulats faux si :
     - on faisait une poursuite de calcul lin�aire en changeant de relation
       de comportement
     - le nouveau comportement avait moins de variables internes que
       l'ancien comportement

   Exception :
     les r�sultats devaient etre justes si le nouveau comportement est 'ELAS'
     car alors, on ne se sert pas des variables internes !

   RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 6.1.16
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 7.0.0
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION  divers essais pour tester plusieurs types d'erreurs possibles
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
CORRECTION AL 2003-065
   NB_JOURS_TRAV : 0.6 (JP:0.1, JPL:0.5)
   INTERET_UTILISATEUR : OUI
   TITRE  "PROJ_CHAMP : transmettre le prof_chno"
   FONCTIONNALITE
   On am�liore les performances "disque" de la commande PROJ_CHAMP.

   Avant : on cr�ait une SD "profil de champ aux noeuds" pour chaque champ
           projet� (i.e. pour tous les pas de temps).
   Maintenant: on ne cr�e qu'1 seul profil pour tous les num�ros d'ordre.
              (tous les champs partagent le m�me profil)

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI

   VALIDATION
   IMPACT_DOCUMENTAIRE : NON


========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

 CASTEST AJOUT rccm01b                       jmbhh01 J.M.PROIX          371    371      0
 CASTEST AJOUT ssnv164b                       assire A.ASSIRE           588    588      0
 CASTEST MODIF forma04b                      boiteau O.BOITEAU          370      3      3
 CASTEST MODIF rccm02a                       jmbhh01 J.M.PROIX          352      7      7
 CASTEST MODIF rccm02b                       jmbhh01 J.M.PROIX          353      8      8
 CASTEST MODIF rccm03a                       jmbhh01 J.M.PROIX         4278   2125      1
 CASTEST MODIF rccm04a                       jmbhh01 J.M.PROIX          322     80   1151
 CASTEST MODIF sslv115a                       assire A.ASSIRE           341      5      4
 CASTEST MODIF ssnp102a                      jmbhh01 J.M.PROIX          507      7      3
 CASTEST MODIF ssnp108a                       assire A.ASSIRE           335      5      4
 CASTEST MODIF ssnp109a                       assire A.ASSIRE           545      5      4
 CASTEST MODIF ssnv108a                      jmbhh01 J.M.PROIX          250      4      2
 CASTEST MODIF ssnv137a                       assire A.ASSIRE           771      5      4
 CASTEST MODIF ssnv164a                       assire A.ASSIRE           576     17     11
 CASTEST MODIF yyyy117a                       assire A.ASSIRE           680     94    171
 CASTEST MODIF zzzz111a                       assire A.ASSIRE          2047     10      9
 CASTEST MODIF zzzz118c                      jmbhh01 J.M.PROIX           37      1      1
 CASTEST MODIF zzzz118d                      jmbhh01 J.M.PROIX           35      1      2
CATALOGU AJOUT options/refe_forc_noda        jmbhh01 J.M.PROIX           54     54      0
CATALOGU MODIF compelem/grandeur_simple__    jmbhh01 J.M.PROIX          390      6      5
CATALOGU MODIF typelem/gener_me3d_3          jmbhh01 J.M.PROIX          415      4      1
CATALOGU MODIF typelem/gener_meax_2          jmbhh01 J.M.PROIX          481      4      1
CATALOGU MODIF typelem/gener_meaxs2          jmbhh01 J.M.PROIX          484      4      1
CATALOGU MODIF typelem/gener_mecpl2          jmbhh01 J.M.PROIX          479      4      1
CATALOGU MODIF typelem/gener_medpl2          jmbhh01 J.M.PROIX          486      4      1
CATALOGU MODIF typelem/gener_thaxd1          vabhhts J.PELLET           147      2      2
CATALOGU MODIF typelem/gener_thpld1          vabhhts J.PELLET           148      2      5
CATALOGU MODIF typelem/mecptr3               jmbhh01 J.M.PROIX          474      4      1
CATALOGU MODIF typelem/mecptr6               jmbhh01 J.M.PROIX          470      4      1
CATALOGU MODIF typelem/medpqs8               jmbhh01 J.M.PROIX          473      4      1
CATALOGU MODIF typelem/medptr6               jmbhh01 J.M.PROIX          476      4      1
CATALOPY AJOUT commande/defi_cable_bp         assire A.ASSIRE            85     85      0
CATALOPY AJOUT commande/defi_cable_op         assire A.ASSIRE            52     52      0
CATALOPY MODIF commande/defi_group            assire A.ASSIRE           174      2      1
CATALOPY MODIF commande/dyna_non_line        jmbhh01 J.M.PROIX          267      8      1
CATALOPY MODIF commande/impr_table          mcourtoi M.COURTOIS          57      2      1
CATALOPY MODIF commande/post_rccm            jmbhh01 J.M.PROIX          206      5      1
CATALOPY MODIF commande/stat_non_line        jmbhh01 J.M.PROIX          231      8      1
    CMAT MODIF 30M5_REF_A.NOMI             J.M.PROIX 169                 39      1      0
 FORTRAN AJOUT assembla/assmiv               jmbhh01 J.M.PROIX          647    647      0
 FORTRAN AJOUT calculel/vrcom2               vabhhts J.PELLET           202    202      0
 FORTRAN MODIF algeline/op0037                boyere E.BOYERE           544     15      3
 FORTRAN MODIF algeline/op0093               cibhhpd D.NUNEZ            494      3      3
 FORTRAN MODIF algorith/dlnewi                boyere E.BOYERE           718      2      2
 FORTRAN MODIF algorith/mdrecf                boyere E.BOYERE           221      2      2
 FORTRAN MODIF algorith/nddoet               vabhhts J.PELLET           444      9      6
 FORTRAN MODIF algorith/nmarch               pabhhhh N.TARDIEU          270      2      2
 FORTRAN MODIF algorith/nmconv               jmbhh01 J.M.PROIX          388     35     15
 FORTRAN MODIF algorith/nmdocn               jmbhh01 J.M.PROIX          174     31      6
 FORTRAN MODIF algorith/nmdoet               vabhhts J.PELLET           340     10      7
 FORTRAN MODIF algorith/nmimpr               jmbhh01 J.M.PROIX          600      9      7
 FORTRAN MODIF algorith/nminit               jmbhh01 J.M.PROIX          284     57     11
 FORTRAN MODIF algorith/nmlect               jmbhh01 J.M.PROIX          158     15      5
 FORTRAN MODIF algorith/nmmuap                boyere E.BOYERE            88      2      2
 FORTRAN MODIF algorith/op0046               jmbhh01 J.M.PROIX          358      3      3
 FORTRAN MODIF algorith/op0070               jmbhh01 J.M.PROIX          768     11      8
 FORTRAN MODIF algorith/statch                boyere E.BOYERE           431     28     25
 FORTRAN MODIF algorith/statim                boyere E.BOYERE           190     22     19
 FORTRAN MODIF algorith/stchpv                boyere E.BOYERE           495     28     36
 FORTRAN MODIF algorith/tran75                boyere E.BOYERE           701      2      2
 FORTRAN MODIF calculel/cesexi               vabhhts J.PELLET           130      3      3
 FORTRAN MODIF calculel/cesimp               vabhhts J.PELLET           207      2      2
 FORTRAN MODIF calculel/cesred               vabhhts J.PELLET           237      9      1
 FORTRAN MODIF calculel/cnscno               vabhhts J.PELLET           224      1      1
 FORTRAN MODIF calculel/mecalc               vabhhts J.PELLET           497      2      2
 FORTRAN MODIF calculel/pjefpr               vabhhts J.PELLET           145      4      3
 FORTRAN MODIF calculel/vrcomp               vabhhts J.PELLET           230    136     50
 FORTRAN MODIF elements/te0008               jmbhh01 J.M.PROIX          151     41      9
 FORTRAN MODIF elements/te0009               jmbhh01 J.M.PROIX          144     41     14
 FORTRAN MODIF elements/te0071               vabhhts J.PELLET           130      2      1
 FORTRAN MODIF elements/te0073               vabhhts J.PELLET           465      2      1
 FORTRAN MODIF modelisa/atalc2               pabhhhh N.TARDIEU          424      5      2
 FORTRAN MODIF modelisa/atasmo               pabhhhh N.TARDIEU          340      4      2
 FORTRAN MODIF modelisa/caprec                assire A.ASSIRE           526    418    150
 FORTRAN MODIF modelisa/immeca                assire A.ASSIRE           463    172      6
 FORTRAN MODIF modelisa/op0180                assire A.ASSIRE           388     12      8
 FORTRAN MODIF modelisa/topoca                assire A.ASSIRE           498     41     12
 FORTRAN MODIF postrele/rc3201               jmbhh01 J.M.PROIX          491    377    257
 FORTRAN MODIF postrele/rc3203               jmbhh01 J.M.PROIX          313     91     34
 FORTRAN MODIF postrele/rc32ac               jmbhh01 J.M.PROIX          300     11      6
 FORTRAN MODIF postrele/rc32fs               jmbhh01 J.M.PROIX          178     38     13
 FORTRAN MODIF postrele/rc32ma               jmbhh01 J.M.PROIX          162     18      8
 FORTRAN MODIF postrele/rc32sa               jmbhh01 J.M.PROIX           79     23      3
 FORTRAN MODIF postrele/rc32sp               jmbhh01 J.M.PROIX          248    150     76
 FORTRAN MODIF postrele/rc3601               jmbhh01 J.M.PROIX          410    291    230
 FORTRAN MODIF postrele/rc3603               jmbhh01 J.M.PROIX          319     63     36
 FORTRAN MODIF postrele/rc36ac               jmbhh01 J.M.PROIX          449     10      4
 FORTRAN MODIF postrele/rc36fs               jmbhh01 J.M.PROIX          186     45     14
 FORTRAN MODIF postrele/rc36fu               jmbhh01 J.M.PROIX          225     73     43
 FORTRAN MODIF postrele/rc36ma               jmbhh01 J.M.PROIX          291     14      4
 FORTRAN MODIF postrele/rc36sa               jmbhh01 J.M.PROIX           77     19      3
 FORTRAN MODIF postrele/rc36sp               jmbhh01 J.M.PROIX          165     54      3
 FORTRAN MODIF postrele/rcma01               jmbhh01 J.M.PROIX          255     15      1
 FORTRAN MODIF postrele/rcma02               jmbhh01 J.M.PROIX           68      2      2
 FORTRAN MODIF prepost/irecri                cibhhpd D.NUNEZ            476      4      4
 FORTRAN MODIF prepost/irgmsh                cibhhpd D.NUNEZ            225      5      4
 FORTRAN MODIF prepost/op0039                cibhhpd D.NUNEZ            829      4      3
 FORTRAN MODIF prepost/op0176                cibhhpd D.NUNEZ            219      3      3
 FORTRAN MODIF prepost/pochoc                 boyere E.BOYERE           103      4      2
 FORTRAN MODIF prepost/pochpv                 boyere E.BOYERE           102      4      2
 FORTRAN MODIF soustruc/sscgma                assire A.ASSIRE           502     13      1
 FORTRAN MODIF soustruc/sscgno                assire A.ASSIRE           526     19      6
 FORTRAN MODIF utilitai/iredpl               cibhhpd D.NUNEZ            353      4      4
 FORTRAN MODIF utilitai/iredsu               cibhhpd D.NUNEZ            352      4      4
 FORTRAN MODIF utilitai/op0155              mcourtoi M.COURTOIS         149      6      2
 FORTRAN MODIF utilitai/rfrgen                boyere E.BOYERE           308      2      2
  PYTHON AJOUT Macro/defi_cable_bp_ops        assire A.ASSIRE           317    317      0
  PYTHON MODIF Macro/calc_precont_ops         assire A.ASSIRE           468      4      7


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :    8        2316      2316             +2316
 MODIF :  100       39389      5000    2623     +2377
 SUPPR :    0           0                 0        +0
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :  108       41705      7316    2623     +4693 
