========================================================================
Version 7.2.14 du : 05/02/2004
========================================================================


-----------------------------------------------------------------------
--- AUTEUR acbhhcd G.DEVESA   DATE  le 04/02/2004 a 11:23:13

------------------------------------------------------------------------
REALISATION EL 2003-236
   NB_JOURS_TRAV  : 1
   INTERET_UTILISATEUR : NON
   TITRE  "IMPR_MACR_ELEM et amortissement au format MISS3D"
   FONCTIONNALITE
     Il est desormais possible de rentrer une liste d'amortissements de
     type LISTR8 dans IMPR_MACR_ELEM derriere le nouveau mot clef
     LIST_AMOR en alternance avec AMOR_REDUIT qui attend lui une suite de
     reels L_R. On est maintenant en conformite avec DYNA_TRAN_MODAL.
   DETAILS
     On modifie le catalogue de la commande IMPR_MACR_ELEM.
     On modifie en consequence la routine IREDMI.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
     Le test ZZZZ108A passe avec les 2 options.
   IMPACT_DOCUMENTAIRE : OUI
   DOC_U : U7.04.33
      EXPL_: ajouter le mot clef LIST_AMOR avec le format 'MISS3D'


-----------------------------------------------------------------------
--- AUTEUR cambier S.CAMBIER   DATE  le 04/02/2004 a 11:07:46

------------------------------------------------------------------------------
REALISATION EL 2004-027
   NB_JOURS_TRAV  : 20
   INTERET_UTILISATEUR : OUI
   TITRE : Sensibilit� en dynamique non-lin�aire
   FONCTIONNALITE  DYNA_NON_LINE  -  Mot-cle SENSIBILITE
     Ceci est une contribution � l'EL 2001-146. La possibilit� de faire des
     calculs de sensibilit� avec l'op�rateur DYNA_NON_LINE a �t� r�alis�e.
     Cela fonnctionne en elastoplasticite a ecrouissage Von-Mises isotropep
     en 2D et 3D pour les sensibilit�s par rapport aux proprietes materiaux (masse
     et amortissement exclu) et au chargement (Dirichlet exclu avec verouillage,
     le Fortran modifi� pr�parant en partie l'�volution correspondante).
   DETAILS
     Routine impact�e :
       op070.f : architecture conserv�e, quelques changements de passage
                 d'arguments
       segico.f : ajout n�cessaire de champs d�riv�s suppl�mentaires dans
                  la structure qui contient toutes les infos relatives
                  aux sensibilit�s
       nmchar.f : ajout au second membre des termes dues aux champs
                  de vitesse et d'acc�l�ration d�riv�es
       nmsens.f : passage de champs suppl�mentaires et de donn�es
                  suppl�mentaires (amortissement,...) resolution
                  du nouveau pb
       nmcrch.f et nmnoli : creation des champs absolus deriv�s
       nddoet.f, nminit.f et dinit : inialisation des nouveaux champs d�riv�s
                                      n�cessaires
       op0069.f : mise en coherence appel dinit et nmsens + suppression
                  des appels inutiles � psnsle
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION : SENSD07A
     R�alisation d'un nouveau test : sensd07a
     Calcul des derives des champs resulat par rapport a E, NU, D_SIGM_EPSI (=ET=Pente
     de la courbe de traction), SY (limite d'elasticite), et le chargement (FORCE_NODALE).
     Le modele integre de l'amortissement modal et et de l'amortissement proportionnel.
     Deux dyna_non_line s'enchaine pour tester la fonctionalite de reprise avec
     sensibilite. La solution de reference des sensibilites est calculee par differences
     finies, puis en non regression.
     On teste ensuite avec un chargement tel que le regime est lineaire, et on compare
     avec les resultats de DYNA_LINE_TRAN.
   IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U4.53.01
       EXPL_ : ajout du mot cl� SENSIBILITE
      DOC_U : U4.50.02
       EXPL_ : ajout paragraphe DYNA_NON_LINE
      DOC_R : R4.03.04
       EXPL_ : mise � jour du tableau des fonctionnalit� disponible
      DOC_V : V1.01.184
       EXPL_ : nouveau cas test sensd07a

------------------------------------------------------------------------------
CORRECTION AL 2004-020
   NB_JOURS_TRAV  : 3
   INTERET_UTILISATEUR : OUI
   TITRE : SENSIBILITE DANS DEUX STAT_NON_LINE SUCCESSIFS
   FONCTIONNALITE
     Il y avait deux plantages dans l'enchainement de deux STAT_NON_LINE avec
     calcul des deriv�es.
     Le premier, corrig� par G. NICOLAS, etait du a un mauvais d�codage des
     sensibilit�s dans le cas de l'enchainement de deux fois le m�me op�rateur
     avec sensibilit�. Je restititue cette correction.
     Je corrige le second plantage de fa�on que le second STAT_NON_LINE
     initialise correctement les champs deriv�es issus du premier.
     Je modifie le cas test sensm09a pour tester cette fonctionalit�.
   DETAILS
     - 1er plantage : La corrrection consiste � ce que dans B_SENSIBILITE_JDC
     la pr�-existence d'un concept d�riv� a l'entr�e de l'op�rateur ne provoque
     pas une erreur (differentiation de deux codes retour 1 et 2). On ne plante
     plus lorsqu'il s'agit d'enrichir un structure d�riv�e (code retour 2).
     - 2eme plantage : La routine nmdoet.f est modifi�e (notamment les champs
     d�riv�es �taient initialis�es par la valeur des champs principaux).
     Au moment du developpement initial (par N. Tardieu) , il avait �t�
     choisi de ne pas permettre l'initialisation a partir d'une �tat intial
     qui ne serait pas donn� par un evol_noli. On ne revient pas sur ce
     choix pour l'instant, mais il serait int�ressant de le reconsid�rer.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION SENSD06A
     On d�coupe le stat_non_line en deux stat_non_line successifs.
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
CORRECTION AL 2004-025
   NB_JOURS_TRAV : 1
   POUR_LE_COMPTE_DE : G. NICOLAS
   INTERET_UTILISATEUR : OUI
   TITRE : Pb dans B_SENSIBILITE
   FONCTIONNALITE : Dans le cas de calcul des sensibilit�s, le superviseur op�re
   un pr�-traitement du jeu de commandes. Pour cela, il explore la cha�ne de
   d�pendance des concepts en fonction du param�tre sensible. L'anomalie
   apparaissait quand le traitement de d�rivation de mot-cl� simple avait � faire
   avec un tuple ou une liste au lieu d'une valeur isol�e. On corrige en appliquant
   le traitement de d�rivation � tous les �l�ments de cette liste ou de ce tuple.
   DETAILS :
     L'essentiel de la modification a lieu dans la fonction derive_mcsimp de
     B_SENSIBILITE_DERIVATION.py. On en profite pour am�liorer les messages
     d'erreur du pr�-traitement des sensibilit�s.
     L'�criture du jeu de commandes cas-test sensm06a a �t� l�g�rement modifi� :
        CHMAT=AFFE_MATERIAU(MAILLAGE=MA,
                            AFFE=(_F(GROUP_MA=('MA',),MATER=MAT,),),);
     au lieu de :
        CHMAT=AFFE_MATERIAU(MAILLAGE=MA,
                            AFFE=(_F(GROUP_MA='MA',MATER=MAT,),),);
     Cela ne change strictement rien au cas-test, mais fait emprunter le
     chemin de d�codage d'un tuple ('MA',) qui plantait.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION : SENSM06A
   IMPACT_DOCUMENTAIRE:  NON

--------------------------------------------------------------------------
REALISATION EL 2004-015
   NB_JOURS_TRAV : 2
   INTERET_UTILISATEUR : OUI
   TITRE resorption des macro MACR_GOUJ2E_MAIL et MACR_GOUJ2E_CALC
   FONCTIONNALITE MACR_GOUJ2E_MAIL et MACR_GOUJ2E_CALC
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   DETAILS :
     on r�sorbe les deux macro-commandes fortran MACR_GOUJ2E_MAIL et
     MACR_GOUJ2E_CALC et les routines qui vont avec : ops022.f ops023.f
     goujdo.f goujp1.f goujp2.f
     La m�thodologie de calcul est conserv�e dans les fichiers de commande
     zzzz120a et zzzz120b qui validaient ces fonctionnalit�s.
     Dans chacun, la m�thode macr_gouj2e_mail est cr��e, analogue � la macro
     pr�c�dente. Pour la phase calcul, les commandes g�n�r�es autrefois par
     la macro sont directement �crites dans le fichier de commandes.
     Bien que la n�cessit� d'un post-traitement d�di� ne soit pas vraiment
     �vidente, on conserve la commande POST_GOUJ2
   IMPACT_DOCUMENTAIRE : OUI
   VALIDATION : zzzz120a et zzzz120b
     DOC_U : U4.GJ.10
        EXPL_ : R�sorption de la commande, donc suppression de cette doc.
                Penser � int�grer les (maigres) explications de ce document
                dans la doc V de zzzz120a (V1.01.120)
     DOC_U : U4.GJ.20
        EXPL_ : idem que pour U4.GJ.10
     DOC_V : V1.01.120
        EXPL_ : int�grer les infos des docs U de MACR_GOUJ* dans cette doc V

--------------------------------------------------------------------------
CORRECTION AL 2004-006
   NB_JOURS_TRAV  : 5
   INTERET_UTILISATEUR : NON
   TITRE : erreur d'�valuation des conditions quand blocs imbriqu�s
   FONCTIONNALITE : SUPERVISEUR catalogues
   DETAILS :
   Probl�me
   =========
   Quand un catalogue comporte des blocs imbriqu�s comme ci-dessous ::

    TATA  =SIMP(statut='f',typ='TXM',defaut='BBB',into=('AAA','BBB'),),
    b_unit3  =BLOC(condition = "TATA =='BBB'",
                   TOTO3  =SIMP(statut='f',typ='TXM',defaut='AAA',into=('AAA','BBB'),),
                   c_unit3       =BLOC(condition = "TOTO3 == 'BBB'",
                                       UNITE3   =SIMP(statut='f',typ='I',defaut=25),
                                      ),
                              ),

   et que le mot cl� simple TOTO3 vaut 'BBB', il n'est pas possible, en version
   7.2.10, de r�cup�rer la valeur du mot-cl� simple UNITE3 dans une fonction
   sd_prod d'un OPER ou dans une fonction ops d'une MACRO.

   Analyse
   ==========
   Le probl�me vient des m�thodes get_valeur de la classe MCBLOC et
   cree_dict_valeurs de la classe MCCOMPO.

   La m�thode cree_dict_valeurs de la classe MCCOMPO a en charge  de
   reconstruire un dictionnaire des mots cl�s en eliminant les niveaux fictifs
   de blocs. Le dictionnaire produit est utilis� soit pour �valuer les
   conditions de pr�sence des blocs soit pour passer des arguments aux
   fonctions sd_prod d'un OPER et ops d'une MACRO. Elle utilise la m�thode
   get_valeur de la classe MCBLOC pour atteindre cet objectif.

   La m�thode get_valeur de la classe MCBLOC doit retourner un dictionnaire
   contenant les mots cl�s pr�sents dans le bloc.  Ce traitement n'est r�alis�
   correctement que sur un niveau ce qui explique que les blocs imbriqu�s ne
   soient pas correctement trait�s. Dans notre exemple, TOTO3 est pris en
   compte. En revanche, le bloc c_unit3 et donc le mot-cl� UNITE3 sont ignor�s.

   Le principe de fonctionnement de cree_dict_valeurs est le suivant. On
   parcourt la liste des mots cl�s pass�e en argument (liste). Pour chaque mot
   cl�, on appelle sa m�thode get_valeur. Quand on rencontre un bloc, la
   m�thode get_valeur retourne un dictionnaire qui est remont� d'un niveau
   par utilisation de la m�thode update du dictionnaire en cours de
   construction. Ensuite, on rajoute les mots cl�s par d�faut, puis les mots
   cl�s globaux. Enfin, on r��value les conditions de pr�sence des blocs et on
   recr�e des blocs temporaires avec valeurs par d�faut. Dans notre exemple,
   on recr�e le bloc b_unit3, alors qu'il existe d�j�, avec les mots cl�s par
   d�faut (TOTO3 = 'AAA') ce qui ne cr�e pas pour autant le bloc c_unit3.

   Le probl�me vient, principalement, de la programmation incorrecte de la
   m�thode get_valeur pour la classe MCBLOC. Cependant, � la lecture des deux
   m�thodes concern�es et des utilisations dans le superviseur et dans EFICAS,
   on constate deux autres probl�mes.

   Premier probl�me : la derni�re �tape de la m�thode cree_dict_valeurs est
   inutile et dangereuse, car les blocs qui doivent �tre pr�sents ont d�j� �t�
   cr��s par la m�thode build_mc de la classe MCCOMPO. D'ailleurs, les autres
   m�thodes d'acc�s aux informations sur les mots cl�s ne proc�dent pas de
   cette fa�on (get_mocle, get_child, genpy).

   Deuxi�me probl�me : la m�thode cree_dict_valeurs est utilis�e dans 2 buts
   assez diff�rents. Le premier but est de construire un contexte pour �valuer
   les conditions de pr�sence des blocs. Le deuxi�me est de construire un
   dictionnaire pour passer des arguments aux fonctions sd_prod et ops.
   Dans le deuxi�me cas, il est normal de faire remonter les mots cl�s
   contenus dans des blocs au niveau le plus haut en �liminant les niveaux
   de blocs pour passer les mots cl�s en arguments de sd_prod et ops.
   Par contre, dans le premier cas, il est dangereux de faire remonter les
   mots cl�s pour �valuer les conditions de pr�sence des blocs voisins. On ne
   contr�le pas l'ordre dans lequel ces �valuations sont faites et des
   bouclages sans fin sont possibles.

   R�solution du probl�me
   ==========================
   La m�thode get_valeur de MCBLOC a �t� corrig�e pour qu'elle tienne compte
   de tous les niveaux de blocs et des mots cl�s avec d�faut.

   Ce n'est pas suffisant, car il faut rendre coh�rentes les diff�rentes
   m�thodes d'acc�s. La derni�re �tape de cree_dict_valeurs a �t� supprim�e
   car inutile et dangereuse. Ceci rend les diff�rentes m�thodes d'acc�s
   semblables.

   Enfin, pour distinguer les cas d'�valuation de pr�sence de blocs et le
   passage d'arguments aux fonctions ops et sd_prod, une nouvelle m�thode de
   la classe MCCOMPO a �t� introduite : cree_dict_condition(liste).
   Elle a pour but de calculer un dictionnaire qui servira de contexte pour
   �valuer les conditions de pr�sence des blocs.
   Les arguments � passer aux fonctions ops et sd_prod continueront d'etre
   calcul�s par appel � cree_dict_valeur.
   En interne cree_dict_condition appelle cree_dict_valeur(liste,condition=1).
   La m�thode cree_dict_valeur a un nouvel argument : condition qui peut valoir
   0 (valeur par d�faut qui indique que l'on veut une remont�e des mots cl�s
   contenus dans les blocs) ou 1 (qui indique que l'on ne veut pas de remont�e
   des mots cl�s contenus dans les blocs).

   La m�thode build_mc de MCCOMPO a �t� modifi�e pour tenir compte de ces
   changements. Elle appelle maintenant cree_dict_condition et le contexte
   d'�valuation des conditions de pr�sence des blocs est calcul� une fois
   pour toutes et n'est pas remis � jour lorsqu'un nouveau bloc apparait.
   Ceci permettait de conditionner des blocs par des mots cl�s contenus dans
   des blocs fr�res ce qui n'est pas d�terministe car d�pendant de l'ordre
   d'�valuation des conditions. Ce changement ne produit aucun probl�me sur tous
   les cas tests pass�s.

   EFICAS doit etre modifi� car il utilise lui aussi cree_dict_valeurs pour
   evaluer les conditions de pr�sence des blocs.

   Pr�sentation des m�thodes d'acc�s dans Accas
   =================================================
   On acc�de aux valeurs des mots cl�s par diff�rents moyens selon le contexte:

     - acc�s depuis les fonctions ops et sd_prod
     - acc�s depuis le FORTRAN
     - acc�s depuis les conditions de pr�sence de blocs
     - acc�s depuis la trace d'ex�cution

   Suite aux modifications pr�sent�es ci-dessus, les 4 acc�s sont maintenant
   homog�nes.

   Acc�s depuis les fonctions ops et sd_prod
   -----------------------------------------------
   Les mots cl�s sont transmis aux fonctions ops et sd_prod en construisant un
   dictionnaire avec la m�thode cree_dict_valeurs.

   Ce dictionnaire contient les mots cl�s provenant des blocs, les mots cl�s
   non pr�sents avec valeur par d�faut et les mots cl�s globaux. Les mots cl�s
   absents sans valeur par d�faut sont ajout�s au dictionnaire avec comme
   valeur None.

   Ce dictionnaire est pass� en argument de la fonction comme suit::

   ops(**dico).

   Les mots cl�s simples sont transmis � la fonction en tant que valeur (entier,
   flottant, ...). Les mots cl�s facteurs sont transmis sous forme de MCFACT ou
   de liste de MCFACT. Il faut donc utiliser une m�thode d'acc�s pour retrouver
   la valeur d'un mot cl� simple appartenant � un mot cl� facteur. Cette m�thode est
   get_mocle ou l'op�rateur [] (m�thode sp�ciale __getitem__). Elle �limine les
   niveaux de blocs et retourne les valeurs par d�faut des mots cl�s absents.

   Exemples::

    valeur=mcfact["nom_mot_cle_simple"]
    valeur=mcfact[occurence]["nom_mot_cle_simple"]
    valeur=mcfact[occurence].get_mocle("nom_mot_cle_simple" )

   ATTENTION : certaines macros utilisent la m�thode get_child suivie d'un appel
   � get_valeur pour obtenir le meme resultat. Ce type d'utilisation est d�conseill�.
   La m�thode get_child n'�limine pas les blocs et est r�serv�e aux d�veloppeurs d'Accas.

   Acc�s depuis le FORTRAN
   ----------------------------
   Les d�veloppeurs fortran acc�dent aux valeurs des mots cl�s simples au moyen de
   l'interface GETXXX. Cette interface utilise en interne la m�thode get_mocle.

   Acc�s depuis les conditions de pr�sence de blocs
   ---------------------------------------------------
   Apr�s les modifications pr�sent�es ci-dessus, les mots cl�s accessibles dans le
   contexte d'�valuation d'une condition de pr�sence sont ceux pr�sents dans le
   dictionnaire  construit par appel � la m�thode cree_dict_condition.

   Ce dictionnaire ne contient pas les mots cl�s contenus dans les blocs fr�res.
   Il contient les mots cl�s non pr�sents avec valeur par d�faut et les mots cl�s
   globaux. Les mots cl�s absents sans valeur par d�faut sont ajout�s au dictionnaire
   avec comme valeur None. Un mot cl� sp�cial a �t� ajout� : reuse qui indique si la
   commande concern�e a �t� appel�e avec un concept r�utilis� ou pas.

   Acc�s depuis la trace d'ex�cution
   --------------------------------------
   Pendant l'ex�cution, un �cho des commandes est produit sur la sortie standard.
   Cet �cho des commandes est produit au moyen de la classe genpy.genpy qui est
   un visiteur de l'objet ETAPE.
   Il permet d'imprimer l'�cho d'une commande avec les valeurs de tous les mots
   cl�s pr�sents et les valeurs des mots cl�s par d�faut absents en �liminant les
   niveaux de blocs.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION : d�veloppement priv� (cf texte de la fiche)
   IMPACT_DOCUMENTAIRE : OUI
      DOC_D : D6.03.01
       EXPL_ : int�grer les d�tails du corps de l'histor dans la doc D du superviseur


-----------------------------------------------------------------------
--- AUTEUR cibhhpd D.NUNEZ   DATE  le 04/02/2004 a 09:04:14

--------------------------------------------------------------------------
CORRECTION AL 2004-005
   NB_JOURS_TRAV  : 4
   POUR_LE_COMPTE_DE   : J-M Proix
   INTERET_UTILISATEUR : OUI
   TITRE : Correction de CRIT_ELNO_RUPT
   FONCTIONNALITE : CALC_ELEM OPTION CRIT_ELNO_RUPT
       - les criteres sont maintenant calcules a partir des contraintes
          dans le repere de la couche et non de la coque
       - le critere de Tsai-Hill utilise les limites en compression ou en traction
          suivant le signe des contraintes
   DETAILS :
      Modifications du te0040.f de CRIT_ELNO_RUPT:
      Desormais il y 7 valeurs: SIGL, SIGT, SIGLT, CRIL, CRIT, CRILT, CRITH qui
      sont stockees dans la grandeur PCRITER: les trois premieres sont les contraintes
      dans le repere de la couche et les quatre dernieres sont les criteres.
      ECRITER  = CRRU_R   ELNO__ IDEN__  (SIGL SIGT SIGLT CRIL CRIT CRILT CRITH    )
      Ajout du nouveau cas-test ssls128a qui teste CRIT_ELNO_RUPT
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION :
     test calculant CRIT_ELNO_RUPT
   IMPACT_DOCUMENTAIRE : OUI
     DOC_V : V3.03.128
        EXPL_ : Ajout d'un test sur le calcul du critere de Tsai_Hill


-----------------------------------------------------------------------
--- AUTEUR vabhhts J.PELLET   DATE  le 04/02/2004 a 10:58:25

------------------------------------------------------------------------------
CORRECTION AL 2004-023
   NB_JOURS_TRAV : 0.5
   INTERET_UTILISATEUR : OUI
   TITRE "Plantage dans CREA_RESU : assertion non v�rifi�e"
   FONCTIONNALITE
     La commande CREA_RESU lorsqu'elle cr�e un evol_ther en �valuant un champ
   de fonctions pour une liste d'instants, se plantait avec le message
   "assertion non v�rifi�e ..." lorsque le nombre d'instants �tait sup�rieur
   � 9998.
     De plus, la commande pr�sentait l'inconv�nient de cr�er autant de "profils"
   de champs aux noeuds que d'instants alors que tous ces profils sont identiques.
   Il y avait donc une perte d'espace disque.
   Enfin, cette commande ne lib�rait pas les objets ramen�s en m�moire dans la
   boucle en temps. La m�moire finissait pas etre encombr�e par tous les champs
   de temp�rature calcul�s pour tous les instants. C'est pourquoi l'utilisateur
   avait du "charger la mule (en taille m�moire)" pour pouvoir faire son calcul.
     Ce dernier probl�me a �t� corrig� par Lionel Vivan � l'occasion de la
   correction de l'AL 2003-289
     Ces 3 probl�mes sont maintenant corrig�s.
   DETAILS
     Liste des fichiers modifi�s:  crtype.f
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
CORRECTION AL 2004-030
   NB_JOURS_TRAV : 0.5
   INTERET_UTILISATEUR : NON
   TITRE  "Corrections AL suite ELREFA sur segments"
   FONCTIONNALITE
   DETAILS
   1) j'avais laiss� train� 'SEG2' (au lieu de 'SE2') dans te0071 et te0073
   2) je n'avais pas vu que la variable NPG de te0469 �tait un PARAMETER et que
      l'on ne pouvait donc pas s'en servir d'argument de sortie de ELREF5
   Liste des fichiers modifi�s:
     elref4.f  elref5.f  te0071.f  te0073.f  te0469.f
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
     passage des tests : ttll100b, ttlv100b, ssll102e, sslv200a, sslv200b,
                         sdls505a, ssnl502a, ssnl502b
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
RESTITUTION HORS AREX   (H1)
   NB_JOURS_TRAV : 0.1
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
   On supprime le catalogue de la commande PRE_CHAR_IDEAS
   (le fortran avait d�j� �t� supprim�)
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
RESTITUTION HORS AREX   (H2)
   NB_JOURS_TRAV : 0.1
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
     On corrige le catalogue de AFFE_CHAR_MECA / FORCE_ELEC qui faisait
     s'exclure les mots cl�s POSITION et FX (FY, FZ) et donnait une valeur
     par d�faut � FX,FY,FZ
   DETAILS
     Liste des fichiers modifi�s: affe_char_meca.capy
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
RESTITUTION HORS AREX   (H3)
   NB_JOURS_TRAV : 0.
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
     On corrige une ligne de astermodule.c pour �viter un warning de compilation :
      static PyObject*aster_impers(self,args)
   DETAILS
     Liste des fichiers modifi�s:  astermodule.c
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
RESTITUTION HORS AREX   (H4)
   NB_JOURS_TRAV : 0.1
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
     On am�liore la routine CESCEL qui transforme un CHAM_ELEM_S en
     CHAM_ELEM en permettant d'utiliser l'argument NOMPAZ=' ' m�me
     si le param�tre cherch� est un param�tre "IN" pour l'option.
     Jusqu'� pr�sent, on se contentait d'examiner les champs "OUT".
   DETAILS
     Liste des fichiers modifi�s: cescel.f  nopar2.f
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
RESTITUTION HORS AREX   (H5)
   NB_JOURS_TRAV : 0.5
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
     Dans la commande PRE_IDEAS, on �crit syst�matiquement dans le titre
     du fichier maillage produit que celui-ci vient d'IDEAS.
     Jusqu'� pr�sent cette information n'�tait �crite que si le fichier
     universel contenait un DATASET 151.
     Ce DATASET n'est pas toujours pr�sent lorsque ce sont d'autres logiciels
     qu'IDEAS qui �crivent le fichier "universel".
     Au passage, avec l'accord d'A.M Donore on supprime les autres informations
     �crites dans le titre (sauf la date et l'heure de la lecture)
   DETAILS
     Liste des fichiers modifi�s:  presup.f
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION : un petit essai pour v�rifier que le mot cl� AUTEUR=INTERFACE_IDEAS
     est bien pris en compte dans IMPR_RESU
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
  derni�re minute :
  -----------------
  Mon asrest s'est plant� par manque de temps CPU pour le test ssnv128b.
  J'ai regard� avec Jean-Pierre le temps consomm� par ce test dans le pass�.
  On a constat� que ce test est un peu singulier :
    - il utilise beaucoup de temps "syst�me" (plus que du temps "user")
    - ces derniers temps il utilisait presque les 200s allou�s (3 minutes ?)
  Je modifie donc son ".para" : 200s -> 300s


========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

       C MODIF supervis/astermodule          vabhhts J.PELLET          4153      2      2
 CASTEST AJOUT sensd07a                      cambier S.CAMBIER          502    502      0
 CASTEST AJOUT ssls128a                      cibhhpd D.NUNEZ            321    321      0
 CASTEST MODIF sdll403a                      cambier S.CAMBIER          254    172    194
 CASTEST MODIF sensm06a                      cambier S.CAMBIER          181      2      2
 CASTEST MODIF sensm09a                      cambier S.CAMBIER          410     36      5
 CASTEST MODIF ssls118n                      cambier S.CAMBIER          585    318    340
 CASTEST MODIF ssls121a                      cibhhpd D.NUNEZ            483      2     14
 CASTEST MODIF zzzz120a                      cambier S.CAMBIER          619    545    383
 CASTEST MODIF zzzz120b                      cambier S.CAMBIER          617    544    384
CATALOGU MODIF compelem/grandeur_simple__    cibhhpd D.NUNEZ            501      7      6
CATALOGU MODIF typelem/gener_medkg1          cibhhpd D.NUNEZ            223      2      2
CATALOGU MODIF typelem/gener_medkt2          cibhhpd D.NUNEZ            284      2      2
CATALOGU SUPPR commande/macr_gouj2e_calc     cambier S.CAMBIER           49      0     49
CATALOGU SUPPR commande/macr_gouj2e_mail     cambier S.CAMBIER           51      0     51
CATALOGU SUPPR commande/pre_char_ideas       vabhhts J.PELLET            24      0     24
CATALOPY MODIF commande/affe_char_meca       vabhhts J.PELLET           754      6      8
CATALOPY MODIF commande/calc_fonction        cambier S.CAMBIER          205      2      2
CATALOPY MODIF commande/comb_cham_elem       cambier S.CAMBIER           68      4      4
CATALOPY MODIF commande/comb_cham_no         cambier S.CAMBIER           66      4      4
CATALOPY MODIF commande/comb_matr_asse       cambier S.CAMBIER           62      3      3
CATALOPY MODIF commande/impr_macr_elem       acbhhcd G.DEVESA            54      3      2
 FORTRAN MODIF algorith/crtype               vabhhts J.PELLET           320     31     21
 FORTRAN MODIF algorith/dinit                cambier S.CAMBIER          168     27      4
 FORTRAN MODIF algorith/nddoet               cambier S.CAMBIER          495    133    102
 FORTRAN MODIF algorith/nmchar               cambier S.CAMBIER          429     31      3
 FORTRAN MODIF algorith/nmcrch               cambier S.CAMBIER          140     10      4
 FORTRAN MODIF algorith/nmdoet               cambier S.CAMBIER          388     47     31
 FORTRAN MODIF algorith/nminit               cambier S.CAMBIER          340      8      4
 FORTRAN MODIF algorith/nmnoli               cambier S.CAMBIER          138     14      3
 FORTRAN MODIF algorith/nmsens               cambier S.CAMBIER          215     59     11
 FORTRAN MODIF algorith/op0069               cambier S.CAMBIER          662     17     10
 FORTRAN MODIF algorith/op0070               cambier S.CAMBIER          800     20      6
 FORTRAN MODIF calculel/cescel               vabhhts J.PELLET           474      8      4
 FORTRAN MODIF calculel/nopar2               vabhhts J.PELLET           134     31      5
 FORTRAN MODIF elements/te0040               cibhhpd D.NUNEZ            205     67     38
 FORTRAN MODIF elements/te0071               vabhhts J.PELLET           125      2      2
 FORTRAN MODIF elements/te0073               vabhhts J.PELLET           469      2      2
 FORTRAN MODIF elements/te0469               vabhhts J.PELLET           233      3      7
 FORTRAN MODIF stbtrias/presup               vabhhts J.PELLET           192     23     12
 FORTRAN MODIF utilitai/iredmi               acbhhcd G.DEVESA           212     13      6
 FORTRAN MODIF utilitai/segico               cambier S.CAMBIER          324     33      2
 FORTRAN SUPPR stbtrias/sletit               vabhhts J.PELLET           141      0    141
 FORTRAN SUPPR supervis/ops023               cambier S.CAMBIER          763      0    763
  PYTHON MODIF Accas/__init__                cambier S.CAMBIER           70      2      2
  PYTHON MODIF Build/B_ETAPE                 cambier S.CAMBIER          652      4     14
  PYTHON MODIF Build/B_SENSIBILITE_DERIVATION    cambier S.CAMBIER          415     47      8
  PYTHON MODIF Build/B_SENSIBILITE_JDC       cambier S.CAMBIER          479     88     52
  PYTHON MODIF Build/B_SENSIBILITE_MEMO_NOM_SENSI    cambier S.CAMBIER          237     16      4
  PYTHON MODIF Execution/E_JDC               cambier S.CAMBIER          126      1     11
  PYTHON MODIF Noyau/N_ETAPE                 cambier S.CAMBIER          372     10      1
  PYTHON MODIF Noyau/N_JDC                   cambier S.CAMBIER          426     21      7
  PYTHON MODIF Noyau/N_MACRO_ETAPE           cambier S.CAMBIER          548     12      1
  PYTHON MODIF Noyau/N_MCBLOC                cambier S.CAMBIER          130     31      8
  PYTHON MODIF Noyau/N_MCCOMPO               cambier S.CAMBIER          357     58     57
  PYTHON MODIF Noyau/N_MCLIST                cambier S.CAMBIER          157     12      1
  PYTHON MODIF Noyau/N_MCSIMP                cambier S.CAMBIER          137      1      1
  PYTHON MODIF Noyau/N_OBJECT                cambier S.CAMBIER           98      8      1
  PYTHON MODIF Noyau/N_VALIDATOR             cambier S.CAMBIER          889    474     58
  PYTHON MODIF Validation/V_MCSIMP           cambier S.CAMBIER          404     71    107


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :    2         823       823              +823
 MODIF :   53       21479      3089    1967     +1122
 SUPPR :    5        1028              1028     -1028
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :   60       23330      3912    2995      +917 
