==================================================================
Version 17.0.15 (révision 62f8913e0a) du 2024-05-03 11:03:35 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 33553 SELLENET Nicolas         printMedFile + fichier unique plante
 33773 SELLENET Nicolas         Modification abusive de table_graph.py
 33590 LE CORVEC Véronique      En version 17.0.3, les cas test htna100a et mtlp200a sont en erreur NOOK_TEST...
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 33553 DU 09/01/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : printMedFile + fichier unique plante
- FONCTIONNALITE :
  | Problème :
  | ----------
  | L'utilisateur rencontre un cas où printMedFile(local=False) sur un résultat distribué plante alors que, si l'on fait le defi_fichier + impr_resu/fichier_unique
  | fonctionne.
  | 
  | 
  | Solution :
  | ----------
  | En fait printMedFile fait un DEFI_FICHIER en souterrain. En parallèle, chaque DEFI_FICHIER créé le fichier. Mais si avant l'ouverture du fichier par MED, le
  | dernier DEFI_FICHIER n'est pas terminé, ça peut mal se passer.
  | 
  | Je n'ai pas constaté le problème sur mon poste mais Nicolas T m'a dit qu'ajouter une barrière dans printMedFile résout le problème.
  | 
  | Je propose donc d'ajouter cette barrière.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : unitaire
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 33773 DU 29/04/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Modification abusive de table_graph.py
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Avec des versions temporaires des prérequis, j'avais des problèmes au build d'aster avec table_graph.py, la ligne 42 du fichier empêchait la construction. Je
  | l'avais donc commentée.
  | 
  | Le problème, c'est que j'ai poussé cette modif.
  | 
  | 
  | Solution :
  | ----------
  | Je décommente la ligne.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : build
- NB_JOURS_TRAV  : 0.01


================================================================================
                RESTITUTION FICHE 33590 DU 22/01/2024
================================================================================
- AUTEUR : LE CORVEC Véronique
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 17.0.3, les cas test htna100a et mtlp200a sont en erreur NOOK_TEST_RESU sur Cronos et Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les tests htna100a et mtlp200a sont NOOK suite au passage en python de THER_NON_LINE. Les 
  | variations des valeurs ( de non-regression) sont très faibles (htna100a <5e-4% et mtlp200a 
  | <0.5%).
  | 
  | Correction
  | ----------
  | 
  | On met à jour les valeurs de non-regression.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : mtlp200a, htna100a
- NB_JOURS_TRAV  : 0.5

