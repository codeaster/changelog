==================================================================
Version 17.0.18 (révision 84cee355e7) du 2024-05-27 09:05:11 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 33719 ABBAS Mickael            REST_ECRO : erreur fatale en présence d'élément 0D non affecté
 33799 COURTOIS Mathieu         Erreur de lancement avec comm multiples
 33750 SELLENET Nicolas         MECA_NON_LINE et recherche linéaire
 33749 SELLENET Nicolas         MECA_NON_LINE : problème de critères de convergence et résidus
 33564 SELLENET Nicolas         IMPR_RESU FICHIER_UNIQUE et zzzz503o
 33778 SELLENET Nicolas         IMPR_RESU fichier unique : freeze
 33563 SELLENET Nicolas         IMPR_RESU FICHIER_UNIQUE
 33677 FLEJOU Jean Luc          Calcul de EPEQ_ELGA incohérent par rapport à la formule dans la documentation
 33428 FLEJOU Jean Luc          DIS_CHOC : Différence de résultats entre version de code_aster 14.6 et 15.6
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 33719 DU 18/03/2024
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 16.5)
- TITRE : REST_ECRO : erreur fatale en présence d'élément 0D non affecté
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | On affecte le comportement VMIS_ISOT_LINE avec REST_ECRO seulement sur un groupe de maille qui contient une maille QUAD4. Il existe un autre groupe de mailles
  | qui contient une maille POI1. 
  | L'exécution s'arrête en erreur fatale avec le message d'erreur suivant :
  | 
  | 
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <F> <CALCUL_37>                                                                                ║
  |  ║                                                                                                ║
  |  ║ Erreur utilisateur :                                                                           ║
  |  ║   -> Le TYPE_ELEMENT MECA_2D_DIS_T_N  ne sait pas encore calculer l'option:  REST_ECRO.        ║
  |  ║                                                                                                ║
  |  ║   -> Risques & Conseils :                                                                      ║
  |  ║    * Si vous utilisez une commande de "calcul" (THER_LINEAIRE, STAT_NON_LINE, ...), il n'y a   ║
  |  ║ pas                                                                                            ║
  |  ║      moyen de contourner ce problème. Il faut changer de modélisation ou émettre une demande   ║
  |  ║ d'évolution.                                                                                   ║
  |  ║                                                                                                ║
  |  ║    * Si c'est un calcul de post-traitement, vous pouvez sans doute "éviter" le problème        ║
  |  ║      en ne faisant le post-traitement que sur les mailles qui savent le faire.                 ║
  |  ║      Pour cela, il faut sans doute utiliser un mot clé de type "GROUP_MA".                     ║
  |  ║      S'il n'y en a pas, il faut faire une demande d'évolution.                                 ║
  |  ║                                                                                                ║
  |  ║                                                                                                ║
  |  ║ Cette erreur est fatale. Le code s'arrête.                                                     ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | Correction/Développement
  | ------------------------
  | 
  | REST_ECRO est à la fois une option de calcul (dépendant de l'EF) et une loi de comportement (dépendant de la loi de comportement).
  | En particulier, elle ajoute une variable interne (pour gérer l'état initial).
  | On ne peut donc pas simplement exclure les discrets de l'option rest_ecro (par la méthode cond_calcul), car alors le dimensionnement des variables internes
  | n'est pas fait et on plante salement dans calcul.F90
  | 
  | On met les variables internes à zéro (en passant par le te0099 dans les catalogues des discrets).
  | 
  | Remarques: 
  | - les discrets ont besoin d'un matériau (même s'ils ne l'utilisent pas puisque la rigidité est donnée par AFFE_CARA_ELEM), il faut donc modifier le
  | AFFE_MATERIAU en mettant TOUT="OUI" et pas simplement "GROUP_MA="FACE".
  | - le test est NOOK car il vérifie la valeur VARI_NOEU. Or, sur le noeud testé, nous avons deux contributions: celle de la face avec la restauration
  | d'écrouissage (on doit trouver V1=0.0004518137101797803) et celle du noeud correspond au discret (on a V1=0). EN moyenne on trouve donc VARI_NOEU/V1 =
  | 0.00022590685508989025 (la moitié donc). C'est normal. Si on regarde les autres points de la face (ceux pas partagés avec le discret) ou VARI_ELGA (sur la
  | quatre points de Gauss) la valeur est OK. Attention donc à l'interprétation de VARI_NOEU ! Les vrais valeurs sont VARi_ELGA !

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33799 DU 13/05/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Erreur de lancement avec comm multiples
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Suite à issue33748, il y a une erreur au lancement des cas-tests qui ont plusieurs fichiers de commandes.
  | 
  | 
  | Analyse
  | -------
  | 
  | Quand on lance un calcul avec plusieurs fichiers de commandes avec run_aster, il découpe automatiquement
  | en autant de fichiers '.export' qu'il lance séparément.
  | Si on lance 'mpiexec -n N run_aster' + un fichier '.export' qui utilise plusieurs fichiers de commandes,
  | il s'arrête en erreur.
  | 
  | issue33748 proposait de récupérer l'id du processus quelque soit le lanceur (mpiexec ou srun).
  | 
  | Or les cas-tests sont soumis sur Cronos et Gaia dans un seul job avec sbatch, donc SLURM_JOBID est
  | défini (=0) au niveau de run_ctest et donc le lancement des '.export' avec plusieurs fichiers de commandes
  | sont en erreur car run_aster pense qu'il est déjà sous mpiexec et donc ne découpe pas.
  | 
  | 
  | Correction
  | ----------
  | 
  | Déjà intégré:
  | 
  | commit cafbbe1a00e821eb21a2ffb0b8a7601e3e5e283a
  | Author: Mathieu Courtois <mathieu.courtois@edf.fr>
  | Date:   Tue May 14 14:17:43 2024 +0200
  | 
  |     [hotfix] temporarly disable srun support
  | 
  | => On ne regarde plus SLURM_PROCID.
  | 
  | 
  | L'option '--no-mpi' signifiait : ne pas relancer avec mpiexec (ou srun) même si nécessaire.
  | On ajoute '--mpi' pour relancer run_aster avec mpiexec (ou srun si la version est configurée ainsi)
  | même si cela ne semble pas nécessaire.
  | Par défaut (ni '--mpi', ni '--no-mpi'), c'est automatique, on relance avec mpiexec (ou srun) si mpi_nbcpu > 1
  | ou si require_mpiexec est activé.
  | 
  | L'erreur qui dit qu'on ne peut pas lancer `mpiexec -n N run_aster export` si plusieurs fichiers de
  | commandes est changée en warning si ni '--mpi', ni '--no-mpi' est passé au cas où.
  | Mais c'est la norme MPI qui dit qu'après MPI_Finalize d'un 1er .comm, on ne peut appeler aucune
  | fonction MPI, même MPI_Init.
  | 
  | On ajoute aussi un warning si on fait `mpiexec -n N run_aster export` (mode 'auto') et mpi_nbcpu > 1
  | car on va alors relancer avec mpiexec et ça devrait échouer (mpiexec ou srun).
  | 
  | On détecte si run_aster est 'sous' mpiexec en regardant la variable OMPI_COMM_WORLD_RANK pour OpenMPI.
  | 
  | Pour srun, SLURM_JOBID est défini si on est sous srun ou sbatch. Mais il semble que SLURM_STEPID
  | ne soit défini que sous srun.
  | On doit donc avoir le même comportement avec mpiexec et srun.
  | 
  | 
  | Cas hybrides
  | ------------
  | 
  | On ne peut pas laisser run_aster choisir la commande de lancement et il ne peut pas détecter
  | correctement s'il est déjà 'sous' mpiexec/srun.
  | 
  | - Lancer avec srun une version configurée avec mpiexec :
  | 
  | srun -n N --cpus-per-task=1 --threads-per-core=1 --label run_aster --no-mpi export
  | 
  | - Lancer avec mpiexec une version configurée avec srun :
  | 
  | mpiexec -n N --bind-to none --tag-output run_aster --no-mpi export

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : divers lancements
- NB_JOURS_TRAV  : 3.5


================================================================================
                RESTITUTION FICHE 33750 DU 15/04/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : MECA_NON_LINE et recherche linéaire
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Lors du passage des tests STAT_NON_LINE à MECA_NON_LINE, je constate que de nombreux tests cassés utilisent la recherche linéaire.
  | 
  | Correction :
  | ==========
  | 
  | On remarque que la recherche linéaire avec STAT_NON_LINE n'est active qu'en phase de correction, c'est à dire après l'itération 0,
  | alors que dans MECA_NON_LINE, elle est présente à chaque itération.
  | 
  | J'ai modifié l'algo pour ne lancer la recherche linéaire qu'en phase de prédiction.
  | Cela corrige plusieurs tests mais d'autres sont encore en erreur avec un mystérieux WARNING ().
  | 
  | En regardant de plus l'algo de recherche linéaire je constate qu'il peut se terminer sur un ConvergenceError().
  | Cela ne devrait pas être la cas, car au pire la recherche linéaire n'apporte juste aucune amélioration.
  | 
  | En fait, quand le critère (RESI_LINE_RELA) n'était pas atteint au bout du nombre d'itérations maximum permis, on s'arrêtait sur cette ConvergenceError()?
  | J'ai corrigé l'algo pour conserver l'itération de recherche linéaire ayant le plus petit résidu.
  | Avec cela les tests : 
  | - ssns116a,b et sont OK
  | - les autres tombent sur le problème du critère du RESI_GLOB_MAXI inférieur à 1E-12 manquant.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test fiche


================================================================================
                RESTITUTION FICHE 33749 DU 15/04/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : MECA_NON_LINE : problème de critères de convergence et résidus
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Plusieurs cas tests utilisant STAT_NON_LINE sont NOOK si on utilise MECA_NON_LINE à la place.
  | Il apparaît un problème de convergence avec des valeurs de résidu étrange.
  | 
  | Correction :
  | ==========
  | 
  | A chaque début de pas de temps, un premier calcul des résidus est fait sans qu'aucune itération de newton n'ait été effectuée.
  | J'imagine que seuls des chargements sont mis à jour et que l'on calcule le résidu avec l'état de déplacement du pas précédent (??).
  | Toujours est-il que dans les cas en question, le résidu obtenu est inférieur au résidu demandé. MECA_NON_LINE considère alors le critère de
  | convergence atteint pour le pas en cours.
  | Pourtant l'algo prévoit bien une valeur minimal pour ITER_GLOB_MAXI à 1 mais cela est vérifier plus tard et ça ne semble donc pas pertinent.
  | 
  | J'ai donc ajouté le critère sur le nombre d’itération dans la fonction isConverged de convergence_manager.py.
  | 
  | Avec cette correction, tous les tests indiqués dans cette fiche sont OK sauf 3 dont l'un avec des NOOK très faible, je le considère donc OK
  | à ce stade.
  | 
  | Les deux NOOK sont :
  | ssnp108a     NOOK_TEST_RESU   !! PROBLEME RESIDU : residu vaut -1
  | ssnp108b     NOOK_TEST_RESU   !! PROBLEME RESIDU : residu vaut -1
  | 
  | Ce problème sera traité dans une autre fiche car il y a d'autres tests dans ce cas.
  | 
  | La correction à aussi corriger 3 tests qui étaient en S_ERROR et 4 qui étaient en F_ERROR.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests fiche


================================================================================
                RESTITUTION FICHE 33564 DU 11/01/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : IMPR_RESU FICHIER_UNIQUE et zzzz503o
- FONCTIONNALITE :
  | Problème :
  | ----------
  | En essayant de tester les perfs d'IMPR_RESU FICHIER_UNIQUE sur zzzz503o, je suis tombé sur un plantage.
  | 
  | Il intervient sur 8 procs (alors que ça fonctionne sur 4) et c'est un seg fault.
  | 
  | 
  | Analyse :
  | ---------
  | Le travail en cours sur issue33778 corrige le problème.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz503o
- NB_JOURS_TRAV  : 0.01


================================================================================
                RESTITUTION FICHE 33778 DU 30/04/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : IMPR_RESU fichier unique : freeze
- FONCTIONNALITE :
  | Problème :
  | ----------
  | L'étude jointe freeze dans le impr_resu fichier unique : à 1 processus ça tourne, à 4, ça freeze.
  | 
  | 
  | Solution :
  | ----------
  | issue33746 corrige partiellement le problème de cette fiche.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : etude jointe
- NB_JOURS_TRAV  : 0.01


================================================================================
                RESTITUTION FICHE 33563 DU 11/01/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : IMPR_RESU FICHIER_UNIQUE
- FONCTIONNALITE :
  | Problème :
  | ----------
  | IMPR_RESU FICHIER_UNIQUE ne fonctionne pas sur la ligne PZR sur 30 procs.
  | 
  | 
  | Solution :
  | ----------
  | Cette fiche est corrigée par issue33746.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : étude jointe
- NB_JOURS_TRAV  : 0.01


================================================================================
                RESTITUTION FICHE 33677 DU 29/02/2024
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : aide utilisation concernant code_aster (VERSION 16.2)
- TITRE : Calcul de EPEQ_ELGA incohérent par rapport à la formule dans la documentation
- FONCTIONNALITE :
  | Je me suis basé sur le cas test zzzz336a, avec des matériaux/comportement différents MAIS qui restent dans
  | leur domaine élastique. Les relations testées : "ELAS", "VMIS_ISOT_NL", "MAZARS_UNIL", "VMIS_CINE_GC"
  | 
  | Il y a également un calcul thermique linéaire sur du 3D + projection sur DKT (aux sous-points), comme dans
  | l'étude initiale.
  | 
  | Pourquoi tester plusieurs relations :
  | -------------------------------------
  | * le calcul de structure c'est des DKT ==> on est donc en contraintes planes
  | * les comportements testés ne passent pas par les mêmes algorithmes/subroutines :
  | .    + ELAS : programmation nativement en C_PLAN, avec deform_ldc="OLD"
  | .    + VMIS_ISOT_NL : programmation en 3D ==> on passe par DEBORST pour résoudre les C_PLAN. Ce comportement
  |                       est écrit dans le 'nouveau' cadre des Ldc avec deform_ldc="MECANIQUE".
  | .    + VMIS_CINE_GC : programmation nativement en C_PLAN, avec deform_ldc="OLD"
  | .    + MAZARS_UNIL" : programmation nativement en C_PLAN, avec deform_ldc="OLD"
  | 
  | 
  | "Phase n°1" Analyse des Résultats avec plusieurs LdC :
  | ======================================================
  | Toutes les relations donnent les MÊMES résultats pour les champs :
  |   'SIEF_ELGA', "SIEQ_ELGA", 'EPSI_ELGA', 'EPEQ_ELGA', 'EPME_ELGA', 'EPVC_ELGA', 'EPSP_ELGA'
  | Comparaison réalisée sur UNE maille QUA4 proche de l'encastrement.
  | 
  | ==> On peut donc avoir confiance dans les CALCULS.
  | 
  | 
  | "Phase n°2" Analyse des CALC_CHAMP en post-traitement : 
  | =======================================================
  | Lorsque l'on fait le CALC_CHAMP
  |    CRITERES=('SIEQ_ELGA', 'EPEQ_ELGA'),
  |    DEFORMATION=('EPSI_ELGA', 'EPME_ELGA', 'EPVC_ELGA', 'EPSP_ELGA'),
  | 
  | On a le message suivant :
  | ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  | ║ <A> <ELEMENTS3_13>                                                                             ║
  | ║                                                                                                ║
  | ║ Les composantes SIXZ et SIYZ du champs de contraintes sont nulles pour les                     ║
  | ║   éléments DKT et TUYAU. Le calcul des composantes EPXZ et EPYZ du champs de déformations      ║
  | ║   anélastiques donnerait des valeurs fausses. Ces valeurs sont donc mises                      ║
  | ║   à zéro et ne doivent pas être prises en compte.                                              ║
  | ║                                                                                                ║
  | ║                                                                                                ║
  | ║ Ceci est une alarme. Si vous ne comprenez pas le sens de cette                                 ║
  | ║ alarme, vous pouvez obtenir des résultats inattendus !                                         ║
  | ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | Cela veut dire que certaines composantes sont mises à ZERO ==>  il ne faut donc pas utiliser ce champ
  | car certaines des composantes ne sont pas cohérentes avec les SIGMA.
  | 
  | 
  | Dans la documentation on précise que (CALC_CHAMP, §2.6.5 Options de calcul de critères):
  | ----------------------------------------------------------------------------------------
  | On note que les DÉFORMATIONS ÉQUIVALENTES obtenues à partir de EPSI_* et EPME_* sont identiques.
  | En effet, la différence entre les deux tenseurs est un tenseur sphérique (déformation thermique).
  | Comme la déformation équivalente est obtenue à partir du second invariant du déviateur, le
  | tenseur sphérique «disparaît» lorsque l'on prend le déviateur.
  | 
  | ==> On a donc EPEQ_ELGA identique à EPMQ_ELGA (égalité venant des propriétés des tenseurs)
  | 
  | Sur les DKT l'option EPMQ_ELGA n'EXISTE PAS. Si on veut calculer ce critère, il faut donc le faire
  | "à la main" en prenant les composantes du champ MAIS "<A> <ELEMENTS3_13>"
  | ==> ON NE DOIT DONC PAS LE CALCULER COMME CELA.
  | 
  | Si on veut calculer EPMQ_ELGA, il faut le faire d'une autre façon :
  | On a les champs SIEF_ELGA et EPSI_ELGA
  | On calcule
  | .   EPSI = (1+ν)/E.SIEF_ELGA - ν/E.trace(SIEF_ELGA).IId + α.ΔT.IId   (relation de l'élasticité)
  | .   EPME_ELGA = EPSI_ELGA - EPSI
  | .   Deviateur = EPME_ELGA - trace(EPME_ELGA).IId/3
  | A partir de ce EPME_ELGA, on calcule EPMQ_ELGA en appliquant la formule (2/3.Deviateur:Deviateur)^0.5
  | et on retrouve bien EPEQ_ELGA calculé par code_aster.
  | 
  | MAIS CELA NE FONCTIONNE QU'EN THERMO-ÉLASTICITÉ. S'il existe des déformations anélastiques,
  | la formule EPME_ELGA = EPSI_ELGA - EPSI est potentiellement incomplète.
  | Cela va dépendre de ce que l'on souhaite calculer.
  | 
  | En remarques : 
  | * Le tenseur lié à la température est "sphérique". Que l'on mette ou pas ce tenseur dans les 
  | équations de l'élasticité, cela n'aura aucun effet sur le déviateur.
  | * Si on met EPSI(zz)=0 (comme cela est fait dans code_aster) le tenseur lié à la
  | thermique n'est plus sphérique et donc la température "vient" jouer un rôle dans le calcul
  | du déviateur, d'où les différences. MAIS ce calcul est FAUX ==> ET c'est pour cela que code_aster
  | ne propose pas cette option sur les DKT.
  | 
  | 
  | Donc en conclusion :
  | ====================
  | * code_aster ne propose pas EPMQ_ELGA sur les DKT et c'est NORMAL. Le champ EPME est incomplet du
  |   fait des contraintes planes, code_aster ne propose donc pas de calculer ce critère.
  | * les champs SIEF_EGA, EPSI_ELGA, SIEQ_ELGA, EPEQ_ELGA sont correctement calculés.
  | * Le fait que le critère soit calculé à partir du déviateur et que le tenseur lié à la température
  |   soit sphérique fait que ce tenseur "disparaît".
  | * Il n'y a pas de raison de calculer EPMQ_ELGA "à la main" car c'est la même chose que EPEQ_ELGA.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 33428 DU 22/11/2023
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 17.1)
- TITRE : DIS_CHOC : Différence de résultats entre version de code_aster 14.6 et 15.6
- FONCTIONNALITE :
  | Entre les versions 14.6 et 15.6, l'orientation des DISCRETS de CHOC sur des NOEUDS a été mis au carré.
  | 
  | Pour que le contact fonctionne il est nécessaire de donner une orientation au discret de type POI1. Pour les
  | SEG2 ce n'est pas nécessaire la direction est donnée par l'axe du discret.
  | 
  | En 14.6, rien n'était indiqué et cela dans aucune documentation sur l'orientation des discrets de choc sur
  | des noeuds, POI1.
  | 
  | Maintenant c'est précisé dans R5.03.17 : Relations de comportement des éléments discrets.
  | L'orientation du discret doit être la normale SORTANTE au plan de contact. Maintenant cela correspond
  | à la convention adoptée en MMC pour la définition de la normale.
  | 
  | Les cas tests qui faisaient du contact sur des POI1, ont été modifiés en 'inversant' la normale.
  | La mise au carré a été faite lors de la fiche issue29468, et celles citées dans cette fiche.
  | 
  | 
  | Correction à faire dans l'étude :
  | ---------------------------------
  | L'orientation des POI1 n'est la bonne
  | 'VECT_X_Y', VALE =(0.0,0.0,-1.0,0.0,1.0,0.0) ==> le xlocal c'est -Z
  | 
  | La bonne orientation 
  | 'VECT_X_Y', VALE =(0.0,0.0,1.0, 0.0,1.0,0.0) ==> le xlocal c'est Z
  | 
  | 
  | Résultat FAUX jusqu'à la version 15.6 :
  | ---------------------------------------
  | Il n'y avait aucune explication avant cette mise au carré du sens de la normale au plan de choc.
  | L'utilisateur n'avait aucun moyen de savoir comment orienter la normale.
  | Comme pour l'étude jointe, l'analyse des efforts de contacts permet de s'en rendre compte.
  | Les résultats pouvaient potentiellement être faux si l'utilisateur ne mettait pas le bon 'sens'.

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 13.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 13.0
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 2.0

