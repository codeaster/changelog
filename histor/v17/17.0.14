==================================================================
Version 17.0.14 (révision 8b30a9e475) du 2024-04-25 17:15:03 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 33506 SELLENET Nicolas         Plantage DVP_1 CALC_CHAMP/CHAM_UTIL
 33609 SELLENET Nicolas         Ajouter de l'équilibrage dynamique
 33464 SELLENET Nicolas         Utiliser parmetis dans le découpeur de maillage
 33622 SELLENET Nicolas         Découpage de maillage et de groupes
 33763 BETTONTE Francesco       Simplification de l'appariement nodal pour LIAISON_GROUP
 33700 ABBAS Mickael            Mise à jour des slides Formation Intermédiaire
 33664 ABBAS Mickael            Documentation de MECA_NON_LINE
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 33506 DU 15/12/2023
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Plantage DVP_1 CALC_CHAMP/CHAM_UTIL
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Quand il y a une erreur sur le nom du champ à traiter dans CALC_CHAMP/CHAM_UTIL dans le fichier de commande, le code plante en erreur de programmation DVP_1.
  | 
  | 
  | Solution :
  | ----------
  | Il faut ajouter un message d'erreur pour préciser à l'utilisateur que le champ demandé n'existe pas. J'ajoute donc le message suivant :
  | """
  | Le champ demandé NEUT_ELGA n'existe pas.
  | """

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : unitaire
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33609 DU 26/01/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Ajouter de l'équilibrage dynamique
- FONCTIONNALITE :
  | Demande :
  | ---------
  | Dans le cadre des études de stockage où des non-linéarités sont très localisées puis se propagent, il est nécessaire d'équilibrer la charge au cours du calcul.
  | On peut imaginer fournir un champ aux nœuds scalaire qui sera utilisé comme poids par le partitionneur.
  | 
  | 
  | Solution :
  | ----------
  | Le premier dev ajouté dans cette fiche, c'est la possibilité de redécouper un ParallelMesh. Jusqu'à présent, on découpait soit des Mesh ou des IncompleteMesh.
  | Cette modification m'a permis de trouver un bug dans le découpage des familles de nœuds et de mailles. Lorsque une entité du maillage était dans 2 familles
  | différentes, le découpage était impossible. Je corrige donc et j'ajoute un test.
  | 
  | Le but final est d'ajouter une fonction applyBalancingStrategy qui à partir d'une sd_resultat et d'un nouveau découpage produise une nouvelle sd_resultat avec
  | le modèle, le cham_mater, etc redécoupés.
  | 
  | Pour ce faire, j'ajoute un objet ArrayWrapper qui est une surcouche à un std::vector ou un JeveuxVector afin qu'ils puissent être considérés comme des tableaux
  | à 2 dimensions (par exemple : nb_no*nb_cmp) et qu'ils puissent être découpés par ObjectBalancer.
  | 
  | Contrairement à ce qui avait été dit en RTA, je ne diffère pas l'instanciation du Model, etc car on peut en fait s'en passer.
  | 
  | Pour le modèle et le champs de matériau, il est possible d'écrire simplement un constructeur de ces objets qui par exemple pour le modèle, avec un modèle déjà
  | instancié et un nouveau maillage redécoupé produise un nouveau modèle sur le nouveau maillage.
  | 
  | En effet dans les classes Model et MaterialField, il y a les données permettant de refaire un appel à l'OP donc en recopiant ces données et en refaisant un
  | appel à l'OP, on produit un nouveau modèle ou un nouveau cham_mater.
  | 
  | Pour les chargements, c'est plus compliqué. L'idée est toujours la même : produire un nouveau chargement à partir d'un nouveau maillage et de l'ancien
  | chargement (calculé plutôt que communiquer).
  | 
  | Pour cela, j'ajoute un objet SyntaxSaver dont le but est de sauver la syntaxe pour pouvoir rappeler l'OP. Avec cette syntaxe et un nouveau maillage (ou un
  | modèle), on peut reconstruire l'objet.
  | 
  | Avec tout ça, j'ai donc écrit applyBalancingStrategy qui prend un sd_resu et produit une nouvelle sd_resu avec :
  | - un nouveau maillage redécoupé,
  | - un nouveau modèle,
  | - un nouveau cham_mater,
  | - des nouveaux chargements (DirichletBC, ParallelMechanicalLoad et ParallelThermalLoad uniquement),
  | - et pour le dernier pas de temps, les champs aux nœuds, aux éléments et aux points de Gauss redécoupés.
  | 
  | Pour le moment, on peut redécouper des ElasticResult, des ThermalResult et des NonLinearResult comportant des champs réels (pas complexes) aux nœuds, aux
  | éléments et aux points de Gauss.
  | 
  | J'ajoute aussi la possibilité de donner à PtScotch un poids sur les nœuds.
  | 
  | Ce qui manquera pour faire vraiment de l'équilibrage dynamique, ce sera :
  | - Le calcul du poids.
  | - La prise en compte dans un opérateur de calcul (MECA_NON_LINE).
  | 
  | 
  | Validation :
  | ------------
  | J'ajoute un test zzzz155h qui réalise un calcul MECA_STATIQUE et redécoupe le résultat suivant une commande donnée par l'utilisateur. Le calcul utilise un
  | AFFE_CHAR_CINE et un AFFE_CHAR_MECA.
  | 
  | Je teste ensuite les champs redécoupés (DEPL et SIEF_ELGA) en les complétant sur tous les procs. Ils sont identiques avant et après redécoupage.
  | 
  | Je relance aussi un MECA_STATIQUE avec les objets redécoupés (modèle, chargements, etc.) et je vérifie que les champs sont bien identiques (à 1e-15 pour DEPL et
  | 5e-10 pour SIEF_ELGA en absolu) par rapport à la référence.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz155h
- NB_JOURS_TRAV  : 20.0


================================================================================
                RESTITUTION FICHE 33464 DU 05/12/2023
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Utiliser parmetis dans le découpeur de maillage
- FONCTIONNALITE :
  | Demande :
  | ---------
  | Ajouter ParMetis comme partitionneur de maillage.
  | 
  | 
  | Solution :
  | ----------
  | J'ajoute les classes nécessaires pour l'appel à ParMetis et je le rends accessible dans LIRE_MAILLAGE.
  | 
  | Cet ajout a permis de constater que l'installation de ParMetis ne fonctionnait pas correctement dans les prérequis de code_aster, que ce soit dans le conteneur
  | ou dans les prérequis sur cronos.
  | 
  | J'ajoute un découpage supplémentaire qui utilise ParMetis dans zzzz155a.
  | 
  | Cette fiche sera restituée avec l'arrivée des nouveaux prérequis. De mon point de vue, il n'y a pas d'urgence comme le demandeur a été muté.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz155a
- NB_JOURS_TRAV  : 2.5


================================================================================
                RESTITUTION FICHE 33622 DU 31/01/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Découpage de maillage et de groupes
- FONCTIONNALITE :
  | Problème :
  | ----------
  | En travaillant sur issue33609, j'ai trouvé un bug dans le découpeur de maillage et plus précisément dans la répartition des groupes des mailles et de nœuds.
  | 
  | Lorsqu'on démarre d'un maillage séquentiel qu'on répartit sur plusieurs procs, le découpage plante au moment de la découpe des groupes.
  | 
  | 
  | Solution :
  | ----------
  | Ce plantage vient du fait que le découpeur (version maillage initial séquentiel) ne sait pas découper les maillages contenant des groupes de mailles (resp.
  | nœuds) qui se chevauchent.
  | 
  | Ce problème n'est pas présent lorsqu'on part d'un IncompleteMesh car dans ce cas, ce ne sont pas les groupes qu'on découpe mais les familles (au sens MED).
  | 
  | Pour corriger ce problème de manière efficaces, j'utilise des entiers codés portés par les mailles et/ou les nœuds. Chaque entier codé porte l'information du ou
  | des groupes portés par l'entité considérée (nœud ou maille).
  | 
  | Cette façon de faire n'est sans doute pas la plus efficace en terme de mémoire car on a à chaque fois n fois des tableaux portant sur les nœuds et les mailles à
  | répartir.
  | 
  | Mais ce sera plus efficace en terme de temps de retour et de possibilité de bug.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz155a
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 33763 DU 19/04/2024
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Simplification de l'appariement nodal pour LIAISON_GROUP
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | L'appariement nodal effectué dans LIAISON_GROUP (pacoap.F90) n'est pas optimal. Il y a beaucoup trop de vérifications sans utilité et l'ordre des boucles peut
  | être inversé pour gagner en efficacité.
  | 
  | 
  | Développement
  | -------------
  | 
  | Réécriture de pacoap.F90 :
  | - Suppression des vérifications redondantes
  | - Inversion de l'ordre des operations pour réduire le nombre de changements de repère (rotation+translation)
  | - Test de la distance sur la distance**2 
  | 
  | Tous les tests de vérification sont OK, et le temps de calcul de la liaison est divisé par 4.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests verification
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 33700 DU 11/03/2024
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant Documentation (VERSION )
- TITRE : Mise à jour des slides Formation Intermédiaire
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Mettre à jour les slides de la formation intermédiaire
  | - Formation intermédiaire - Non-Linear basis
  | - Formation intermédiaire - Kinematic/incompressissility
  | 
  | 
  | 
  | Correction/Développement
  | ------------------------
  | Ré-écrit en LaTex/Beamer et poussé dans le dépot formation

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 33664 DU 26/02/2024
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant Documentation (VERSION )
- TITRE : Documentation de MECA_NON_LINE
- FONCTIONNALITE :
  | Première version de la documentation de la nouvelle commande MECA_NON_LINE
  | 
  | Choses pas faites:
  | - mot-clef contact (va dépendre de l'avancée du chantier)
  | - mot-clef SCHEMA_TEMPS (apparemment, on n'est pas très sûrs de nous)
  | 
  | Je me suis beaucoup inspiré de la documentation de STAT_NON_LINE sur le fond mais j'ai changé sur la forme: au lieu de faire un catalogue des "opérandes" et
  | leur description syntaxique, il y a un seul paragraphe syntaxe global (généré automatiquement) et plusieurs chapitres
  | 
  |     1. Syntaxe
  |     2. Contenu de la structure de données EVOL_NOLI
  |     3. Paramètres généraux
  |     4. Paramétrisation du calcul
  |     5. Réglages des solveurs non-linéaires
  |     6. Réglages de la convergence
  |     7. Réglages du solveur linéaire
  |     8. Comportements non-linéaires
  |     9. Reprises, archivage et gestion de l’état initial
  |     10. Chargements
  |     11. Contact
  |     12. Gestion de la verbosité
  | 
  | Je pense que c'est beaucoup plus lisible et utile. Dans l'état actuel de la doc, si tu connais pas le mot-clef, tu as du mal à trouver la fonctionnalité !

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.51.04
- VALIDATION : visu
- NB_JOURS_TRAV  : 2.0

