==================================================================
Version 13.3.14 (révision aaffe992c10e) du 2017-04-14 09:29 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 26354 DU 06/04/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Macro de compilation _GLIBCXX_USE_CXX11_ABI désactivée dans linux.py
FONCTIONNALITE
   Problème
   --------
   
   Dans issue25589, on a mis la macro _GLIBCXX_USE_CXX11_ABI=0 dans calibre9.py.
   
   Cette macro permet de compiler un code avec GCC >= 5.1 en utilisant une bibliothèque compilée avec une version plus ancienne. Ici,
   le problème se produit avec MFront s'il est fourni par le paquet des prérequis construit sur Calibre9.
   
   Or comme linux.py, qui se veut plus "générique", est un lien vers calibre9.py, si on construit MFront et code_aster avec GCC>=5.1,
   on force l'utilisation du nommage de l'ancienne ABI pour code_aster et donc on ne peut plus linker.
   
   
   Correction
   ----------
   
   On n'a pas besoin de cette macro sur calibre9 (GCC 4.9), donc on peut l'enlever.
   
   De plus, lors de l'étape de configuration, on essaie de voir si on n'a besoin de cette macro ou pas.
   On fait le test avec la bibliothèque TFELSystem de MFront. Si on trouve un symbole avec l'ancien nommage, on met
   -D_GLIBCXX_USE_CXX11_ABI=0 dans CXXFLAGS, sinon on met -D_GLIBCXX_USE_CXX11_ABI=1.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    configure & build
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26364 DU 07/04/2017
AUTEUR : DROUET Guillaume
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    Les mots-clés GROUP_MA_ESCL et GROUP_MA_MAIT doivent être obligatoires pour la méthode LAC
FONCTIONNALITE
   Problème:
   =========
   
   Il y a une erreur dans le catalogue de DEFI_CONTACT pour la méthode LAC. Les mots-clés GROUP_MA_ESCL et GROUP_MA_MAIT doivent être
   obligatoires et pas facultatif pour la méthode LAC.
   
   Action:
   =======
   On corrige le catalogue.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u4.44.11
VALIDATION
    submit
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25955 DU 17/01/2017
AUTEUR : DROUET Guillaume
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    D124.17 - Lever la limite de 9 zones de contact pour LAC
FONCTIONNALITE
   Problème:
   =========
   
   On est actuellement limité à 9 zones de contact car on code certaines structures de données liées aux zones avec l'indice de la zone
   de 1 à 9. En effet, la convention de nommage de ces objets impose d'utiliser un seul chiffre pour indexer les occurrences dans la
   structure de données.
   
   Solution:
   =========
   
   Une solution simple consiste à revoir le nommage des objets de la structure de donnée sd_appa concernés, on propose :
   
   .MAS => .MS
   .ESC => .EC
   .MAN => .MN
   .CIM => .CM
   .LNM => .LM
   
   
   Cela permet de disposer de deux digits pour coder les objets par rapport aux zones de contact et on peut donc autoriser 100 zones de
   contact.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    submit + test perso
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26270 DU 15/03/2017
AUTEUR : BILLON Astrid
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    A204.17 - [MFRONT] Suppression de lois officielles Mfront
FONCTIONNALITE
   Contexte
   =======================
   
   Suite à la note de David (6125-1716-2016-16161-FR), on propose la suppression de toutes les lois "officielles" MFront sauf Iwan et
   MetaAcierEPIL_PT.
   
   
   Résolution
   =======================
   
   L'appel aux lois MFront en mode officiel a été remplacé par un appel en mode prototype pour tous les cas tests concernés (à l'exception de 
   ssnv205bs snv207b mfron06a mfron06b comp012d qui appellent Iwan et MetaAcierEPIL_PT en officiel).
   
   Les lois MFront ont été déplacées de /mfront à /astest en conservant les mêmes noms (sauf Iwan et MetaAcierEPIL_PT qui restent dans /mfront).
   
   Pour éviter les doublons, 
   zzzz387b.mfront (=Chaboche.mfront)
   ssnl117b.mfront (=Plasticity.mfront)
   mfron06c.mfront (=MetaAcierEPIL_PT.mfront)
   mfron02i.mfront (=DruckPragEcroLin.mfront)
   mfron02c.mfront (=BurgerAgeing.mfront)
   ont été supprimés de /astest.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    astest
NB_JOURS_TRAV  : 4.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25954 DU 17/01/2017
AUTEUR : DROUET Guillaume
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    D124.17 - Nouveau type de decoupe des hexaedres pour la LAC
FONCTIONNALITE
   On introduit un nouveau type de découpe pour les éléments HEXA 8 et HEXA 20 (les HEXA27 ne sont pas découpés) pour le pré-traitement
   de la méthode de contact LAC (CREA_MAILLAGE/DECOUPE_LAC). Au lieu de découper un élément HEXA en 6 HEXA, on le découpe en 5 éléments
   PYRA. Cette découpe ne conserve pas le type d'éléments générés par l'utilisateur mais elle permet de contenir le nombre de noeuds et
   donc de DDL ajoutés :
   
   HEXA8 => HEXA8 = 8 noeuds => 24 ddl supplémentaires par élément de contact
   HEXA8 => PYRA5 = 1 noeuds => 3 ddl supplémentaires par élément de contact 
   
   HEXA20 => HEXA20 = 28 noeuds => 72 ddl supplémentaires par élément de contact
   HEXA20 => PYRA13 = 9 noeuds => 27 ddl supplémentaires par élément de contact
   
   De plus, on ne génère plus de maille QUAD 8 trapézoïdale sur la zone de contact ce qui facilite les opérations d'appariement. 
   
   Cette découpe apporte donc un gain de performance significatif pour la méthode LAC avec des éléments HEXA8 et HEXA20. On propose
   d'utiliser ce type de découpe par défaut. Cependant, il peut être pénalisant pour l'utilisateur d'introduire des éléments de type
   PYRA alors qu'il avait volontairement maillé en hexaèdre. On propose donc de conserver la découpe HEXA => HEXA à l'aide du mot-clé
   DECOUPE_HEXA sous le mot-clé DECOUPE_LAC dans CREA_MAILLAGE: 
   
   DECOUPE_HEXA = / 'PYRA' [défaut]
                  / 'HEXA'
   
   Pour continuer à couvrir la découpe en HEXA on impact le cas test ssnp170m et ssnp170n, patch test de Taylor avec HEXA8 et HEXA20.
   
   Impact doc:
   
   u4.23.02 CREA_MAILLAGE
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : u4.23.02
VALIDATION
    submit
NB_JOURS_TRAV  : 5.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26355 DU 06/04/2017
AUTEUR : FLEJOU Jean Luc
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    MACR_CARA_POUTRE avec ANGLE <0
FONCTIONNALITE
   Lors de "issue26297 : Blindage de MACR_CARA_POUTRE", si l'angle du repère principal est != 0 un message d'alarme est déclenché.
   
   Le test est
    . if ( alpha > 1.0E-08 ):
   
   Ce n'est pas bon, cela devrait être
   . if ( abs(alpha) > PetitAngle ):
   
   Alpha est en d°
   ==> cos(0.001) = 0.9999999998
   ==> sin(0.001) = 0.0000174533
   
   ==> PetitAngle = 0.001
   
   Un autre oubli : Lorsque l'on utilise ANGLE dans DEFI_GEOM_FIBRE, les sous-points sont correctement tournés, mais pas le maillage
   sous-jacent. Ce maillage peut être réutilisé pour visualisation ou post-traitement, il faut donc également le tourner.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tests MACR_CARA_POUTRE
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26299 DU 21/03/2017
AUTEUR : PLESSIS Sarah
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    POST_RCCM : évolutions 2017 partie 1
FONCTIONNALITE
   A l'approche des VD4 900, le SEPTEN souhaite utiliser POST_RCCM pour réaliser des calculs industriels (échéance au début du second
   semestre 2017). 
   Dans le cadre du projet MODERN, des évolutions sont donc apportées à l'opérateur. Elles font suite aux développements de 2015
   (issue23842 et issue23869) et 2016 (issue24957, issue25577 et issue25702).
   
   Dans cette fiche, plusieurs points sont traités :
   
   - lorsqu'une situation n'a pas de transitoire, des adaptations étaient réalisées lors de la combinaison avec une autre situation, ce
   qui est contraire au RCC-M. Désormais, si une situation n'a pas de transitoire alors la contrainte est tout simplement considérée
   nulle. Ce changement de stratégie casse les cas-tests rccm01b, rccm07a, rccm10a, rccm10b, rccm11a.
   
   - on ajoute en méthode B3200 une nouvelle forme pour prendre en compte les moments :  ceux-ci sont interpolés sur la température. De
   nouveaux mots-clés sont introduits (TEMP_A et TEMP_B), ceux-ci avaient déjà été discutés lors de la RTA du 3/11/16 (cf. issue25577).
   
   - on permet de combiner les trois formes de chargements en B3200 entre elles (ajout du cas-test rccm01f).
   
   - j'augmente le nombre de combinaisons de situations affichées dans le .resu (il passe de 50 à 200).
   
   - je modifie la prise en compte du séisme en B3200 (on ajoute un facteur 2 lors d'un chargement de type instantané).
   
   - j'enrichis le cas-test rccm01e
   
   - le calcul de la grandeur Sn* et du critère sur le rochet thermique est étendu aux trois variantes de la méthode B3200 et aux deux
   variantes ZE200
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 13.0
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u4.83.11, R7.04.03
VALIDATION
    passage de cas tests rccm*
NB_JOURS_TRAV  : 15.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25611 DU 23/09/2016
AUTEUR : ABBAS Mickael
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    PQUASI.17 - Reduction de modèle en vibroacoustique
FONCTIONNALITE
   Proposition
   ===========
   
   Intégration des travaux issus de la collaboration EDF/DCNS sur la réduction de modèle en vibroacoustique
   
   On introduit la possibilité de faire de la réduction de modèle sur les problèmes multi-paramétrique avec une méthode de type
   "GLOUTON" (greedy) qui ne s'arrête pas spécifiquement à la vibroacoustique.
   
   L'idée est de décrire le système linéaire du problème multi-paramétrique de manière explicite, ce qui implique de savoir fabriquer
   matrices et vecteur second membre en mode "commandes éclatées".
   
   1/ Décrire le problème multi-paramétrique: DEFI_BASE_REDUITE
   2/ Calculer les modes empiriques: DEFI_BASE_REDUITE
   
   
   Pour l'instant, on ne rend pas la nouvelle commande CALC_REDUIT qui utilise les modes empiriques pour calculer le problème réduit: 
   
   1/ Description du problème multi-paramétrique
   ---------------------------------------------
   
   La matrice du problème est une combinaison linéaire de matrices assemblées (au maximum 8 matrices) avec un coefficient variable
   devant chaque matrice.
   Le second membre est aussi assorti d'un coefficient variable
   
   
   OPERATION='GLOUTON'
   
   MATR_ASSE   = FACT( statut='f', min= 1, max=8,
    regles=(UN_PARMI('COEF_R','COEF_C','FONC_R','FONC_C' ),),
    MATRICE         =SIMP(statut='o',typ=(matr_asse_depl_r,matr_asse_depl_c,
                                  matr_asse_temp_r,matr_asse_temp_c,
                                  matr_asse_pres_r,matr_asse_pres_c, ) ),
    COEF_R          =SIMP(statut='f',typ='R', max = 1),
    COEF_C          =SIMP(statut='f',typ='C', max = 1),
    FONC_R          =SIMP(statut='f',typ=(fonction_sdaster, formule), max = 1),
    FONC_C          =SIMP(statut='f',typ=(fonction_c, formule_c), max = 1),
                             ),
    regles=(UN_PARMI('COEF_R','COEF_C',),),
    VECTEUR         =SIMP(statut='f',typ=cham_no_sdaster),
    COEF_C          =SIMP(statut='f',typ='C', max = 1),
    COEF_R          =SIMP(statut='f',typ='R', max = 1),
   
   Matrices, vecteur et coefficients peuvent être réels ou complexes. Les coefficients peuvent être des constantes ou des fonctions.
   Les fonctions sont décrites à l'aide de paramètres (fréquence par exemple)
   
   Ensuite, on décrit la variation des coefficients par l'intermédiaire du mot-clef suivant:
    NB_VARI_COEF    =SIMP(statut='o',typ='I', max = 1, val_min = 1),
    VARI_PARA   = FACT( statut='f', min= 1, max=5,
     NOM_PARA        =SIMP(statut='o',typ='TXM',into=C_PARA_FONCTION(), max=1 ),
     VALE_PARA       =SIMP(statut='o',typ='R', max = '**'),
     VALE_INIT       =SIMP(statut='o',typ='R', max = 1),
                                 ),
   
   Pour l'instant, c'est à l’utilisateur de donner la variation des paramètres (en pratique, aléatoire par NUMPY).
   
   2/ Calculer les modes empiriques
   --------------------------------
   
   Ensuite, DEFI_BASE_REDUITE va calculer la base empirique par une méthode GLOUTON. On aura besoin de résoudre des systèmes complets
   => on introduit le mot-clef SOLVEUR (avec option DYNA_LINE_HARM pour l'instant). On a aussi besoin de résoudre des problèmes réduits
   => on utilise les BLAS/LAPACK (il a été nécessaire d'introduire zgauss pour les problèmes complexes)
   
   Pour des raisons d'efficacité computationnelle, on pré-calcule aussi les produts par mode que l'on sauvegarde en même temps que la
   base proprement dite dans ls SD MODE_EMPIRIQUE ('PROD_BASE_MATR_1' à 'PROD_BASE_MATR_8')
   
   Du point de vue implémentation, on réutilsie les structures de données déjà présentes dans DEFI_BASE_REDUITE à laquelle on ajoute
   des nouveaux types dérivés.
   
   Validation
   ==========
   
   On introduit un nouveau test fdlv115 qui simule une tranche de sous-marin 2D visco-élastique. Le problème est un couplage IFS avec
   condition d'impédance (donc
   complexe)
   fdlv115a fait un calcul de trois modes avec 10 fréquences. On compare les modes avec ceux obtenus par le fichier en full-Python
   C'est OK à 10-12%
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.67.04;R5.01.05;U4.67.01;V8.01.115
VALIDATION
    fdlv115a
NB_JOURS_TRAV  : 60.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26024 DU 02/02/2017
AUTEUR : ABBAS Mickael
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    SPAR.17 - Enrichissement de la commande DEFI_DOMAINE_REDUIT
FONCTIONNALITE
   Proposition
   ===========
   
   Cette fiche vise à enrichir la commande DEFI_DOMAINE_REDUIT qui crée les GROUP_MA/GROUP_NO pour la réduction de modèle
   
   Les premières améliorations sont générales:
   - possibilité de donner le nombre d'éléments voisins à pendre en plus des éléments repérés par les points magiques: l'idée est de
   pouvoir étendre un peu le RID pour améliorer la précision car c'est parfois nécessaire
   - possibilité d'avoir un groupe de mailles/nœuds au minimum dans le RID. C'est important si on veut prendre en compte les mailles
   correspondant aux CL qui ne seront pas forcément sélectionnées par la procédure de  points magiques
   
   Les secondes sont liées à la fiche issue26005 sur la correction EF pendant le calcul hyper-réduit:
   - il est nécessaire de créer un groupe de noeuds à l'intérieur du RID qui servira à imposer la rigidité à l'interface entre RID et
   le reste du domaine
   
   Développement
   =============
   
   Pour ajouter des couches d'éléments: NB_COUCHE_SUPPL avec une valeur par défaut à 1
   Pour avoir des éléments initialement dans le RID: DOMAINE_INCLUS
   Pour activer la construction de la couche de noeuds supplémentaires en correction EF: 
   Si CORR_COMPLET = 'OUI'
      NOM_ENCASTRE obligatoire
   
   Le développement est très léger car il ré-utilise la programmation actuelle et la routine cncinv qui construit la connectivité inverse
   
   Validation
   ==========
   
   La validation est purement informatique: on teste le nombre de mailles/noeuds dans les groupes créés (zzzz395m)
   TEST_RESU(MAILLAGE = _F(CARA        = 'NB_MA_GROUP_MA',)) par exemple
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.67.02
VALIDATION
    zzzz395m
NB_JOURS_TRAV  : 5.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25263 DU 01/06/2016
AUTEUR : GEOFFROY Dominique
TYPE evolution concernant Code_Aster (VERSION 13.4)
TITRE
    RUPT : Ajout opérateur POST_T_Q
FONCTIONNALITE
   Evolution
   ***********
   En mécanique de la rupture, on décrit l'état mécanique en pointe de fissure à l'aide des coefficients d'intensité des 
   contraintes. Dans Code_Aster, ces quantités peuvent actuellement être déterminées à l'aide de l'opérateur CALC_G (à partir 
   d'une formation intégrale) ou de l'opérateur POST_K1_K2_K3 (à partir du champ de déplacement asymptotique).
   
   Si l'on considère l'approche asymptotique, le champ utilisé dans POST_K1_K2_K3 est équivalent à une série de Williams 
   tronquée au premier ordre. Néanmoins, il est possible d'enrichir la formulation et d'employer un terme d'ordre supérieur, 
   sommé T-stress. Cette approche peut être utilisée dans le code R6 - Annexe 14.
   
   L'objet de cette fiche est de créer un nouvel opérateur, nommé POST_T_Q, permettant de  calculer la contrainte T en mécanique de la
   rupture. C'est une macro en PYTHON largement inspirée de POST_K1_K2_K3, mais indépendante dans son utilisation.
   
   Impact
   *********
   Nouvelle routine dans CONTRIB et donc hors AQ. Vocabulaire est le même que pour POST_K1_K2_K3.
   
   Validation
   **********
   Cas-test d'un prisme à base rectangulaire avec une fissure semi-elliptique sur lequel on vient déterminer la contrainte T. Le calcul
   est comparé  à une solution analytique
   
   Impact Doc :
   ***********
   
   Doc U :U4.82.40
   Doc R : R7.02.20
   Doc V : V3.04.324 / sslv324
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : v3.04.324, U4.82.40,R7.02.20
VALIDATION
    Comparaison avec calcul analytique
NB_JOURS_TRAV  : 2.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25581 DU 14/09/2016
AUTEUR : BÉREUX Natacha
TYPE evolution concernant Code_Aster (VERSION 13.3)
TITRE
    A301.xx - Methode de NEWTON_KRYLOV pour THER_NON_LINE
FONCTIONNALITE
   Problème 
   ========
   La méthode 'NEWTON_KRYLOV' permet d'obtenir des gains de temps intéressants dans STAT_NON_LINE. 
   L'idée est  d'utiliser un solveur linéaire itératif (METHODE='PETSC'), et d'adapter la précision de résolution souhaitée au fur et à
   mesure des itérations de Newton. Au début (lorsqu'on est loin de la solution), on résout grossièrement. Plus on se rapproche de la
   solution, plus on résout précisément.  
   On souhaite rendre disponible cette méthode dans THER_NON_LINE. 
   Travail effectué
   ================
   On peut désormais utiliser METHODE='NEWTON_KRYLOV' dans THER_NON_LINE. Attention! pour espérer bénéficier de la méthode, il faut
   également choisir un solveur itératif, typiquement METHODE='PETSC'. 
   Sur hsna100b, on passe ainsi de 15638 itérations à 5843 itérations pour les résolutions de systèmes linéaires.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u4.54.02
VALIDATION
    hsna* htna
NB_JOURS_TRAV  : 3.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24629 DU 15/12/2015
AUTEUR : BÉREUX Natacha
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    A301.xx - Utiliser le préconditionneur simple précision en mode GESTION_MEMOIRE='AUTO' de MUMPS
FONCTIONNALITE
   Problème
   ========
   Quand on utilise un solveur itératif préconditionné par LDLT_SP, on tombe de temps en temps sur des messages d'arrêt du calcul
   concernant le mot-clef pcent_pivot (exemple :
   petsc_15 : La création du préconditionneur 'LDLT_SP' a échoué car on manque de mémoire. Conseil : augmentez la valeur du mot clé
   SOLVEUR/PCENT_PIVOT.)
   L'utilisateur souhaiterait qu'une correction de PCENT_PIVOT soit faite de façon transparente.
   
   Analyse
   =======
   Quand on utilise MUMPS comme solveur direct, il existe une option GESTION_MEMOIRE='AUTO'. Cette option permet de confier au code le
   choix de la meilleure "stratégie mémoire" à utiliser ( dans IN_CORE, OUT_OF_CORE) et effectue des essais pour adapter la valeur de
   PCENT_PIVOT
   
   Développement
   =============
   On rend accessible le paramètre GESTION_MEMOIRE pour LDLT_SP. 
   On limite (dans le catalogue) les valeurs utilisables à 'IN_CORE' (comportement actuel de LDLT_SP) et 'AUTO' (nouveau).
   
   Test
   ====
   On modifie petsc01d en imposant la valeur GESTION_MEMOIRE='AUTO'
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u4.55.01 u4.50.01
VALIDATION
    tests perso + petsc01d
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26327 DU 29/03/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    [perform] Plantage contact méthode LAC
FONCTIONNALITE
   Objet de la fiche : 
   =================
   On utilise un mélange de deux méthodes de contact dans DEFI_CONTACT qui n'est pas permise ALGO_CONT='LAC'/'STANDARD'. L'utilisateur
   n'est pas arrêté après DEFI_CONTACT et il y a un message d'erreur FPE dans STAT_NON_LINE qui n'est pas explicite. 
   
   Analyse :
   =========
   C'est  la routine "cazouu" qui devrait normalement stopper l'utilisateur. 
   
   Entrée utilisateur dans DEFI_CONTACT : 
   """
                       ZONE=(_F(GROUP_MA_MAIT='SMARKCV',
                                GROUP_MA_ESCL='SKCVMAR',
                                ALGO_CONT='LAC',),
                              _F(GROUP_MA_MAIT='SSUPKCV',
                                 GROUP_MA_ESCL='SKCVSUP',),
                                ),
   """
   
   
   On doit tester si la valeur du mot-clef ALGO_CONT est bien le même dans chaque zone dans le cas où il y a la méthode LAC.
   
   Dans cazouu, on fait :
   1. On fait une boucle sur les zones de contact : Z_i
   2. Pour chaque Z_i, on parcourt une liste de 10 mots-clefs du mot-clef facteur ZONE. Pour ce faire on s'appuie sur une routine
   "getmjm" qui retourne les 10 premiers mots clefs du mot clef facteur du catalogue de la commande en cours
   3. Pour chaque mot-clef trouve=mclf_tmp, si mclf_tmp = 'ALGO_CONT' alors on vérifie si la valeur entrée par l'utilisateur est le
   même que celui de référence. 
   
   C'est l'étape 2&3 qui ne fonctionne pas dans cazouu. Pour la deuxième zone, on ne trouve pas de mot-clef ALGO_CONT parmi les 10
   premiers mot-clefs, donc cazouu n'arrête pas l'utilisateur.
   
   Test de l'algorithme de cazouu : 
   ==============================
   """
    CAZOUU BCLE SUR LA ZONE =                     1
    GETMJM : LISTE DES                   10 MOT-CLEFS DE LA ZONE                      1
    MCLF_TMP : GROUP_MA_MAIT   MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : TYPE_APPA       MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : TYPE_JACOBIEN   MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : GROUP_MA_ESCL   MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : APPARIEMENT     MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : INTEGRATION     MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : ALGO_CONT       MCLF_A_TESTER : ALGO_CONT       --> OK
    MCLF_TMP : CONTACT_INIT    MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : ADAPTATION      MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : RESI_APPA       MCLF_A_TESTER : ALGO_CONT
   """
   On a trouvé ALGO_CONT parmi la liste des dix mot-clefs renvoyés par getmjm. On stocke vale(ALGO_CONT_ZONE1)= 'LAC'--> vale_refe_k.
   
   """      
    CAZOUU BCLE SUR LA ZONE =                     2
    GETMJM : LISTE DES                   10 MOT-CLEFS DE LA ZONE                      2
    MCLF_TMP : NORMALE         MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : GROUP_MA_MAIT   MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : APPARIEMENT     MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : INTEGRATION     MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : VECT_ESCL       MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : DIST_POUTRE     MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : GLISSIERE       MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : CONTACT_INIT    MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : DIST_COQUE      MCLF_A_TESTER : ALGO_CONT       
    MCLF_TMP : TOLE_PROJ_EXT   MCLF_A_TESTER : ALGO_CONT
   """
   
   On n'a pas trouvé ALGO_CONT parmi la liste des mot-clefs renvoyés par getmjm. On ne peut donc pas tester si la vale(ALGO_CONT_ZONE2)
   est égale à vale_refe_k. Dans ce cas on ne s'arrête pas.
   
   Solution :
   =========
   
   Rajouter un blindage dans cazouu. Si on ne trouve pas le mot-clef à tester parmi la liste des mot-clefs renvoyés par getmjm alors on
   plante avec un message d'erreur fatale. 
   
   On peut aller plus loin en simplifiant l'algorithme de cazouu avec les modifications suivantes :
   
   1. Désormais cazouu prend un argument supplémentaire qui indique le type du mot-clef à tester (entier, réel, texte) : 
   """
    cazouu(keywf, nb_cont_zone, keyw_,keyw_type)
   """
   2. En fonction du type de mot-clef à tester et pour chaque zone de contact, on va récupérer la valeur entrée par l'utilisateur avec
   getvtx, getvr8,...
   3. On teste la cohérence entre les zones.
   
   
   Je propose de retenir la dernière solution. 
   
   Tests de vérification = OK
   =====================
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    submit
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26341 DU 03/04/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Problème de détection des erreurs de syntaxe
FONCTIONNALITE
   Problème
   --------
   
   Un mot-clé obligatoire n'est pas renseigné. Or ce mot-clé est utilisé dans la fonction de typage de la commande.
   Exemple : TYPE_CHAM dans CREA_CHAMP.
   
   Dans cette foncton, on fait TYPE_CHAM[0:2], si TYPE_CHAM n'est pas fourni, il est mis à None par défaut sur lequel on ne peut pas
   appeler l'opérateur [].
   L'erreur est donc :
   
   ERREUR A L'INTERPRETATION DANS ACCAS - INTERRUPTION
   >> JDC.py : DEBUT RAPPORT
   CR phase d'initialisation
    . !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . ! Etape .CREA_CHAMP ligne : .184 fichier : .fort.1 Impossible d'affecter un type !
    . ! au résultat: 'NoneType' object has no attribute '__getitem__' . . . . . . . . !
    . !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   fin CR phase d'initialisation
   
   
   
   Correction
   ----------
   
   Dans le cas où une erreur se produit dans la fonction de typage, on n'émet pas de message immédiatement. On affecte None au type
   retourné, ainsi l'erreur sera rapportée en même temps que la vérification de la syntaxe.
   
   
   ERREUR A LA VERIFICATION SYNTAXIQUE - INTERRUPTION
   >> JDC.py : DEBUT RAPPORT
   DEBUT CR validation : fort.1
    . Etape : CREA_CHAMP . .ligne : 89 . .fichier : u'fort.1'
    . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . .! Concept retourné non défini !
    . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . .Mot-clé simple : TYPE_CHAM
    . . . . !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . ! Mot-clé : TYPE_CHAM obligatoire non valorisé !
    . . . . !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . ! None n'est pas une valeur autorisée !
    . . . . !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . .Fin Mot-clé simple : TYPE_CHAM
    . Fin Etape : CREA_CHAMP
    . Etape : TEST_RESU . .ligne : 98 . .fichier : u'fort.1'
    . . .CHAM_ELEM
    . . . . Mot-clé simple : CHAM_GD
    . . . . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . . .! Mot-clé : CHAM_GD obligatoire non valorisé !
    . . . . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . . .! None n'est pas une valeur autorisée !
    . . . . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . Fin Mot-clé simple : CHAM_GD
    . . .Fin CHAM_ELEM
    . Fin Etape : TEST_RESU
   FIN CR validation :fort.1
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    erreur dans zzzz403a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26366 DU 07/04/2017
AUTEUR : COURTOIS Mathieu
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    A101.17 - Utiliser directement les catalogues sans passer par cata.py
FONCTIONNALITE
   Objectif
   --------
   
   Utiliser les mêmes catalogues dans une exécution code_aster et dans AsterStudy directement depuis le répertoire d'installation.
   
   
   Réalisation
   -----------
   
   - On ne génère plus le gros fichiers `cata.py`.
   
   - Les fichiers de définition des catalogues sont importés individuellement : extension `.py`.
   
   - Les catalogues de commandes et catalogues communs sont déplacés dans `code_aster/Cata/Commands`  et `code_aster/Cata/Commons`.
   
   - Les objets du langage de commandes sont distincts pour les deux usages, d'où un aiguillage entre `Legacy` (pour le superviseur
   actuel) et `Language` pour la description du langage de commandes pour AsterStudy (et AsterHPC). Par exemple
   `code_aster/Cata/DataStructure.py` importe les objets de `Legacy` dans une exécution de code_aster ou de `Language` dans AsterStudy.
   Idem pour `Syntax.py`, `SyntaxObjects.py`...
   
   - Modification des imports dans les macro-commandes :
   
   from Cata.cata import *
   from Accas import _F
   
    devient:
   
   from code_aster.Cata.Commands import LIRE_MAILLAGE
   from code_aster.Cata.DataStructure import maillage_sdaster
   from code_aster.Cata.Syntax import _F
   
   
   - L'arborescence est ainsi :
   
   code_aster/
   ├── Cata
   │ . ├── aster_version.py
   │ . ├── Commands
   │ . │ . ├── affe_cara_elem.py
   │ . │ . ├── ...
   │ . │ . └── ther_non_line.py
   │ . ├── Commons
   │ . │ . ├── c_affichage.py
   │ . │ . ├── ...
   │ . ├── DataStructure.py
   │ . ├── __init__.py
   │ . ├── Language
   │ . │ . ├── DataStructure.py
   │ . │ . ├── __init__.py
   │ . │ . ├── Rules.py
   │ . │ . ├── SyntaxChecker.py
   │ . │ . ├── SyntaxObjects.py
   │ . │ . ├── Syntax.py
   │ . │ . ├── SyntaxUtils.py
   │ . │ . └── Validators.py
   │ . ├── Legacy
   │ . │ . ├── cata.py
   │ . │ . ├── DataStructure.py
   │ . │ . ├── DS
   │ . │ . │ . ├── co_cabl_precont.py
   │ . │ . │ . ├── ...
   │ . │ . │ . ├── co_vect_elem.py
   │ . │ . │ . └── __init__.py
   │ . │ . ├── __init__.py
   │ . │ . ├── ops.py
   │ . │ . ├── SyntaxObjects.py
   │ . │ . └── Syntax.py
   │ . ├── ops.py
   │ . ├── SyntaxObjects.py
   │ . └── Syntax.py
   └── __init__.py
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : d2.03.01, d5.01.01, d5.01.02, d5.01.03, d6.03.01, d9.02.01
VALIDATION
    verification
NB_JOURS_TRAV  : 4.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26339 DU 02/04/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TMA : Necs
TITRE
    En version 13.3.12-e97001fe427b,le cas test htna100a échoue sur AthosDevValid
FONCTIONNALITE
   Analyse :
   =======
   Le format est passé de CASTEM à MED mais MODELE est toujours présent alors qu'il n'est pas disponible avec MED.
   
   Correction :
   ==========
   On supprime MODELE.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    htna100a (syntax)

--------------------------------------------------------------------------------
RESTITUTION FICHE 26319 DU 27/03/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.3.11-108e419153d0, les cas tests sdnv142a, sdnv142b et sdnl106a échoue sur AthosDevValid
FONCTIONNALITE
   Problème :
   ----------
   Suite au changement de format par défaut dans IMPR_RESU et LIRE_MAILLAGE, les tests sdnl106a, sdnv142a et sdnv142b s'arrêtent en
   erreur fatale.
   
   
   Solution :
   ----------
   Il faut modifier modal_analysis.py pour y changer l'IMPR_RESU s'y trouvant ainsi que le .comm de sdnv142a et sdnv142b.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdnl106a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 21696 DU 30/10/2013
AUTEUR : ABBAS Mickael
TYPE evolution concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    CALC_PRECONT ne prend pas en compte la gestion des ECHEC
FONCTIONNALITE
   Problème
   ========
   
   Si on donne à CALC_PRECONT une liste d'instant créée par DEFI_LIST_INST à laquelle on a
   indiqué le mot-clé ECHEC, il ne sera pas pris en compte.
   En effet CALC_PRECONT crée une nouvelle liste d'instant à partir des valeurs de celle
   fournie pour ajouter des pas de temps.
   
   Solution
   ========
   
   Cette proposition est liée au bug constaté dans issue25720
   Dans cette dernière fiche, on a proposé de reconstruire un DEFI_LSIT_ISNT dans la macro au lieu de faire un DEFi_LIST_REEL, ce qui
   permet de gérer les échecs.
   C'est donc une fiche sans suite
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    rien de particulier

