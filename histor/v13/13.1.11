==================================================================
Version 13.1.11 (révision 2d46b0f01209) du 2016-03-17 17:10 +0100
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 24515 DU 19/11/2015
AUTEUR : COURTOIS Mathieu
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    Nouveaux catalogues d'éléments : pouvoir  mutualiser "calculs" entre des éléments voisins
FONCTIONNALITE
   Demande
   -------
   
   Lors de issue19176, on a mis dans le même fichier (tuyaux.py) les 3 éléments finis
   de tuyaux.
   Les modes locaux communs aux 3 éléments ont été partagés mais la définition des calculs élémentaires qui utilisent les modes locaux
   dont les composantes varient d'un élément à l'autre était répétées à l'identique pour les 3 éléments.
   
   
   Évolution
   ---------
   
   On permet dans la définition d'un élément d'utiliser une méthode `postInit()` qui, comme
   son nom l'indique, sera appelée après l'initialisation de l'élément.
   On introduit une méthode qui permet de modifier (=changer les composantes) des LocatedComponents (modes locaux).
   
   Ainsi, pour les tuyaux, les éléments MET3SEG4 et MET6SEG3 sont identiques au MET3SEG3 sauf que les composantes sont redéfinies pour
   4 LocatedComponents, ce qui représente environ 25 lignes par éléments.
   
   
   Détails
   -------
   
   Pour parcourir l'arbre des éléments, on utilise le pattern Visitor.
   Ceci permet de définir cette modification de manière isolée
   (voir cataelem/Tools/modifier.py) dans une classe ChangeComponentsVisitor
   contenant 3 méthodes d'une dizaine de lignes.
   Le système pourra être étendu pour simplifier les écritures répétitives de
   certains éléments.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : d5.02.02, d5.02.03
VALIDATION
    49 tests TUYAU_XX
NB_JOURS_TRAV  : 2.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 18059 DU 13/12/2011
AUTEUR : ABBAS Mickael
TYPE evolution concernant Code_Aster (VERSION )
TITRE
    A203.16 - Resorption contact methode discretes lagrangiennes - Phase 2
FONCTIONNALITE
   Proposition
   ===========
   
   
   Cette fiche est la seconde partie du chantier visant à résorber toutes les méthodes de contact/frottement "discrètes" de type LAGRANGIEN
   
   
   Réalisation
   ===========
   
   Il s'agit de supprimer sources (Fortran, capy et Python) correspondant au contact discret LAGANGIEN (contact Et frottement):
   - Suppression dans DEFI_CONTACT (catalogue et Fortran)
   - Suppression dans STAT_NON_LINE (Fortran), y compris l'évenement "attente point fixe" qui y est associé
   
   Documentation
   =============
   
   Doc R, doc U2 et doc U4
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : R5.03.50;U4.44.11;D4.06.14;U2.04.04
VALIDATION
    submit
NB_JOURS_TRAV  : 3.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 23763 DU 04/05/2015
AUTEUR : ABBAS Mickael
TYPE evolution concernant Code_Aster (VERSION )
TITRE
    A203.16 - Resorption DEPL_CALCULE
FONCTIONNALITE
   Proposition
   ===========
   
   Suite à l'issue23326, l'EDA a proposé la résorption de la fonctionnalité PREDICTION='DEPL_CALCULE'
   
   Une alarme de type "deprecation warning" est émise (SUPERVIS_9).
   
   
   Réponse
   =======
   
   Cette fonctionnalité est importante car c'est la seule qui permette actuellement de lancer un calcul à partir d'un champ de déplacement.
   
   La prédiction dite "DEPL_CALCULE" de l'algo de newton global de SNL est une fonctionnalité utilisée par des contributeurs au projet
   "stockages" 
   C'est une fonctionnalité utilisée principalement pour suivre des solutions bifurquées :
   lorsqu'une instabilité est détectée (si la plus petite valeur propre de la matrice
   tangente est nulle ou négative), on utilise DEPL_CALCULE pour alimenter l'algo de Newton
   avec des prédictions perturbées (la construction de ces vecteurs perturbés d'incrément de
   déplacement restant à la charge de l'utilisateur). 
   
   Champ d'application : modèles de comportement géomécanique adoucissants (d'où projet "stockages").
   
   C'est également une méthode très utilisée en calcul d'endommagement (projet CIWAP, entre autres)
   
   Et me projet ASSEMBLAGES (MAC3) compte aussi l'utiliser
   
   Compte-tenu des différents retours, il apparaît que cette fonctionnalité est utile et importante.
   
   Cette fiche est donc classée sans suite: la fonctionnalité est conservée. On supprime le "deprecation warning".
   Néanmoins, les problèmes soulevés par la fiche issue23326 demeurent. On propose donc d'ouvrir une nouvelle fiche (issue24914) pour
   investiguer ces problèmes et proposer des solutions.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24668 DU 06/01/2016
AUTEUR : BERRO Hassan
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    [BB0070] Issues with REST_SPEC_PHYS still not resolved
FONCTIONNALITE
   Problème
   ========
   
   Erreur persistante dans l'opérateur REST_SPEC_PHYS dans le cas d'usage des modes statiques (MODE_STAT = xxx) pour la modélisation de
   l'excitation en multi-appuis.
   
   Correction
   ==========
   
   A l'image de la correction de issue23584, lors du traitement du mode statique, on fait la différence entre champ aux nœuds (DEPL par
   ex.) et champ aux éléments (SIPO_ELNO par ex.). La programmation initiale dans speph0 s’apprêtait bien pour la première famille de
   problèmes.
   
   Je rajoute un test dans sdlx302a (vérification) de ce cas de figure, la restitution de issue23584 avait été uniquement validée par
   un test dans la repo validation.
   
   La correction fait passer le cas initial du forum / tracker - bitbucket.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdlx302a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24641 DU 18/12/2015
AUTEUR : BERRO Hassan
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.1.1, sdld102a, sdld102e, sdll113a, sdll113b s'arrêtent en debug_jeveux
FONCTIONNALITE
   Analyse
   =======
   
   Dans un cas de sous structuration, on utilise l'utilitaire "ajlagr" dans dtmprep pour ajouter les lagranges en créant des copies
   temporaires (de travail) des matrices M et A.
   
   On récupère ensuite les descriptifs de ces matrices pour les traitements suivants (intégration temporelle)
   
   Le problème vient du refactoring qui a été fait avec le chantier DYNA_VIBRA, ces *descriptifs* créés dans dtmprep ne valent plus
   rien à la sortie de la routine et passage par jedema.
   
   
   Correction
   ==========
   
   Pour le cas précis, on fait un jeveut plutôt qu'un jeveuo pour récupérer le descriptif de ces matrices. 
   
   L'autre solution aurait consisté de faire le jeveuo dans dtmcalc, la routine de la quelle décline les étapes d'intégration mais ça
   ne serait pas très carré dans la vision initiale qui consistait à grouper toutes les étapes de préparation de calcul dans dtmprep et
   dtmprep_*
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdld102a, sdld102e, sdll113a, sdll113b en dbgjeveux
NB_JOURS_TRAV  : 0.25

--------------------------------------------------------------------------------
RESTITUTION FICHE 24881 DU 25/02/2016
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Problème d'affichage dans ther_non_line (si PETSC +  LDLT_INC)
FONCTIONNALITE
   Problèmes :
   ===========
   PB_1) L'usage d'un solveur itératif avec le preconditionneur LDLT_INC dans la commande THER_NON_LINE (par exemple) conduit à de
   nombreuses impressions "parasites" qui se mêlent au tableau de convergence.
   PB_2) Dans le fichier .mess, on voit : SOLVEUR/RENUM='SANS' (la valeur par défaut) et pourtant les impressions parasites parlent
   toutes de la numérotation 'RCMK'.
   PB_3) Pour l'étude jointe, le preconditionneur LDLT_SP est beaucoup plus efficace que LDLT_INC. Est-ce normal ?
   
   
   Analyse :
   =========
   Les problèmes PB_1 et PB_2 sont des conséquences (négatives) de issue23618.
   
   Lors de cette évolution, on a supprimé le mot clé RENUM='RCMK' de la commande NUME_DDL (et de la routine numero.F90). La
   numérotation des matrices est manitenent toujours la numérotation "naturelle" (l'ordre des noeuds du maillage).
   
   Quand on utilise le solveur LDLT (ou un solveur itératif avec PREC_COND='LDLT_INC'), on a décidé d'utiliser systématiquement la
   numérotation 'RCMK' (car c'est la plus efficace le plus souvent).
   Il faut donc, à chaque factorisation (et à chaque résolution si le solveur est itératif), construire une nouvelle matrice avec la
   numérotation RCMK. Pour cela, on appelle la routine nueffe.F90 et cette routine (qui est habituellement appelée un seule fois au
   début d'un calcul transitoire) imprime des informations concernant la numérotation et le stockage de la matrice : nombre
   d'équations, nombre de termes non nuls, gains apportés par RCMK, ...
   
   C'est pour cela que ces informations viennent poluer le tableau de convergence de la commande THER_NON_LINE (PB_1).
   
   Nous avons vu que, pour les solveurs utilisant LDLT, nous avons décidé d'utiliser systématiquement la numérotation RCMK
   (l'utilisateur n'a pas le choix pour le mot clé RENUM). Malheureusement, dans le catalogue du mot clé SOLVEUR, pour les solveurs
   utilisant LDLT, j'ai choisi le vocabulaire RENUM='SANS' (au lieu de RENUM='RCMK'). Ce n'est pas malin, car l'écho des commandes fait
   apparaitre RENUM='SANS', ce qui trouble l'utilisateur quand il voit les messages concernant la numérotation RCMK (PB_2).
   
   
   Corrections :
   =============
   1) On modifie la routine ldlt_renum.F90 :
      Les impressions de numérotation ne sont écrites qu'un seule fois par commande.
      Cela résout PB_1.
   
   2) On modifie le catalogue du mot clé SOLVEUR :
   -   _BlocLD['RENUM'] = SIMP(statut='f', typ='TXM', defaut="SANS", into=("SANS",), )
   +   _BlocLD['RENUM'] = SIMP(statut='f', typ='TXM', defaut="RCMK", into=("RCMK",), )
   
   -   _BlocGC_INC['RENUM'] = SIMP(statut='f', typ='TXM', defaut="SANS", into=("SANS",), )
   -   _BlocPE_INC['RENUM'] = SIMP(statut='f', typ='TXM', defaut="SANS", into=("SANS",), )
   +   _BlocGC_INC['RENUM'] = SIMP(statut='f', typ='TXM', defaut="RCMK", into=("RCMK",), )
   +   _BlocPE_INC['RENUM'] = SIMP(statut='f', typ='TXM', defaut="RCMK", into=("RCMK",), )
      Cela résout PB_2.
   
   
   Remarque :
   ==========
   Pour PB_3, on ne fait rien. 
   Il n'est pas anormal que LDLT_SP soit plus performant que LDLT_INC. L'expérience montre que c'est en général le préconditionneur le
   plus efficace.
   Lors de l'évolution issue23618, on a montré que la renumérotation systématique de la matrice à chaque résolution (qui est uen étape
   nécessaire) n'engendrait pas de sur-coût CPU important.
   Je pense donc que issue23618 n'est pas responsable du sur-coût CPU de LDLT_INC par rapport à LDLT_SP.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u4.50.01 u4.53.02 u4.55.01 u4.61.11
VALIDATION
    essai perso
NB_JOURS_TRAV  : 0.4

--------------------------------------------------------------------------------
RESTITUTION FICHE 24917 DU 08/03/2016
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    EVOL_CHAR exagérement gros (pour les champs T_EXT et COEF_H)
FONCTIONNALITE
   Problème:
   =========
   Dans le cadre de isse24169, S. Meunier a remarqué que la solution proposée conduisait à créer un evol_char dont la taille sur la
   base globale est de 14Go (pour 17 pas de temps).
   Cette taille est anormale pour stocker uniquement la température et le coefficient d'échange sur la peau d'un maillage 3D.
   
   Analyse :
   =========
   En reproduisant le même problème (en plus petit !), je me suis aperçu que les objets les plus gros ne sont pas les vecteurs de réels
   (objets .CELV) mais les vecteurs d'entiers (.CELD).
   En imprimant ces objets, on voit que, contrairement à ce qui est écrit dans la doc D4.06.05 (sd_cham_elem), on stocke des "zéros"
   inutilement pour tous les éléments qui ne sont pas concernés par les champs T_EXT et COEF_H (les tétraèdres).
   
   Correction :
   ============
   C'est la routine alchml.F90 qui est coupable.
   Pour allouer (et remplir) l'objet .CELD, il faut regarder si le mode local de chaque GREL est > 0 (ou non).
   
   Validation :
   ============
   Sur un test ressemblant à l'étude fournie, le calcul donne le même résultat et la place disque de l'evol_char est fortement réduite.
   J'estime que l'evol_char qui faisait 14Go avant la correction ne devrait pas dépasser 1Go après la correction.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    essai perso
NB_JOURS_TRAV  : 0.3

--------------------------------------------------------------------------------
RESTITUTION FICHE 24930 DU 11/03/2016
AUTEUR : PELLET Jacques
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    Améliorer la maintenabilité de bibfor/calcul/*.F90
FONCTIONNALITE
   Problème :
   ==========
   Dans le cadre de la transmission de responsabilité de la programmation de la routine calcul.F90, je souhaite améliorer la
   maintenabilité des sources de bibfor/calcul/*.F90
   
   Evolutions :
   ============
   1) On ajoute dans calcul_module.F90 quelques variables globales utilisées pour la mise en oeuvre du parallélisme MPI:
         * ca_nbelmx_  : nombre maximum d'elements dans un grel du ligrel
         * ca_nbproc_  : nombre de processeurs
         * ca_rang_    : numero du processeur (0:nbproc-1)
         * ca_ldist_   : .true. => le calcul est "distribue" :
            Un element n'est calcule que par un processeur.
         * ca_ldgrel_   : .true.  => le calcul est "distribue par grel"
            Tous les elements d'un grel sont traites par le meme processeur.
             Le processeur "rang" calcule le grel de numero igrel si :
             mod(igrel,nbproc) .eq. rang
         * ca_lparal_   : .true.  => le calcul est "distribue par element"
         * ca_paral_    : pointeur sur l'objet '&&CALCUL.PARALLELE'
             Le processeur "rang" calcule l'element iel si :
             ca_paral_(iel)=.true.
         * ca_numsd_   : ca_numsd_(ima) -> rang
             rang est le numero du processeur qui doit traiter l'element porte
             par la maille ima. 
   
   Cela permet mutualiser dans 7 routines, les lignes qui gèrent la prise en compte du parallélisme.
   
   2) On supprime certains arguments (de quelques utilitaires) qui sont en réalité des variables "globales" du module.
   
   3) On déplace la préparation du calcul MPI de la routine calcul.F90 vers debca1.F90 (pour rendre calcul.F90 plus lisible).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.7

--------------------------------------------------------------------------------
RESTITUTION FICHE 24863 DU 22/02/2016
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    routine barych.F90 dangereuse (résultats potentiellement faux)
FONCTIONNALITE
   Problème :
   ==========
   Suite à l'eda de ce matin, j'ai regardé la programmation de la routine barych.F90 (qui réalise l'interpolation entre 2 champs).
   
   La programmation pour les cham_elem est "limitée" : on ne traite que le cas (très simple) où les champs ont la même organisation
   (une simple boucle sur les vecteurs .CELV).
   La "justesse" de la programmation repose entièrement sur la bonne vérification de la cohérence des 2 champs.
   
   Pour vérifier que les 2 champs ont la même organisation, on vérifie :
   1) Que les "références" du champ sont les mêmes (routine VRREFE).
   2) Que les objets .CELV ont la même longueur (routine BARYCH).
   
   La programmation initiale de VRREFE vérifiait l'identité de 3 quantités associées au champ: le ligrel, l'option de calcul et la
   grandeur. Mais depuis (quand ?), un commentaire a été ajouté pour signaler que l'on ne vérifie plus le nom de l'option (à cause de
   la métallurgie).
   Remarque : cette "levée" de vérification est déjà presque un "crime" !
   
   Mais le problème est encore plus complexe si l'on tient compte de la possibilité des cham_elem "étendus" (de la grandeur VARI_R
   et/ou avec sous-points).
   La seule vérification de la longueur des objets .CELD, même si elle a de fortes chances de nous arrêter n'est pas "imparable" : un
   champ de VARI_R ayant n sous-points et deux composantes (V1,V2) a la même longueur que le champ ayant 2n sous-points et une seule
   composante (V1).
   
   
   Correction :
   ============
   1) Dans la routine vrrefe.F90, on rétablit la vérification du nom de l'option.
    . Pour les besoins de la métallurgie, on laisse "passer" si les 2 noms d'option commencent par 'META_'
    . On vérifie également d'autres "cases" de l'objet .CELK : nom du paramètre, caractère MPI_COMPLET.
   
   2) Dans la routine barych.F90, on vérifie également le contenu des objets .CELD : nombre de GRELS, modes locaux, nombres de
   sous-points, de CMPS pour VARI_R
   
   
   Validation :
   ============
   1) Les tests actuels restent OK.
   2) Si on modifie la routine extdch.F90 pour utiliser barych, le test ssnp15f s'arrête avec un message d'erreur :
    !-----------------------------------------------------------------------------------!
    ! <F> <CALCULEL_27> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
    ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
    ! Erreur d'utilisation (ou de programmation) : . . . . . . . . . . . . . . . . . . .!
    ! . .On cherche à combiner 2 champs par éléments qui n'ont pas la même "structure". !
    ! . .La programmation ne le permet pas actuellement. . . . . . . . . . . . . . . . .!
    !-----------------------------------------------------------------------------------!
   
   Le problème vient du fait que l'on fait la différence entre 2 champs de variables internes, mais l'un des champs a été obtenu par
   l'option RAPH_MECA et l'autre par l'option TOU_INI_ELGA.
   Ce qui prouve que E. Lorentz a eu raison de re-programmer la combinaison des champs dans extdch.F90 (issue24829).
   
   
   Résultats faux :
   ================
   Cette anomalie a été détectée par E. Lorentz dans le cadre du développement de issue24869.
   Les résultats "faux" (dus à l'utilisation du mot clé DELTA_GRANDEUR) ont été signalés dans cette fiche.
   
   Il n'a pas été signalé que le manque de vérification de barych ait pu provoquer des résultats faux dans d'autres routines que
   extdch.F90.
   C'est pourquoi, je ne "coche" pas "résultats faux" pour cette fiche.
   En revanche, par prudence, je propose de reporter cette correction en version 12.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.4

--------------------------------------------------------------------------------
RESTITUTION FICHE 24839 DU 17/02/2016
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    moyenne et resultante dans POST_RELEVE_T
FONCTIONNALITE
   Problème :
   ==========
   Pour la commande POST_RELEVE / ACTION, il existe des mots clés (RESULTANTE, MOMENT et POINT) qui permettent de calculer le torseur
   équivalent à un champ de forces nodales.
   Ces mots clés peuvent être utilisés avec OPERATION='EXTRACTION' et OPERATION='MOYENNE'.
   Avec ces 2 opérations, on obtient la même table résultat, ce qui peut perturber l'utilisateur.
   
   En réalité, quand on utilise RESULTANTE, on fait la somme des forces nodales (et de leurs moments) et on ne fait aucune "moyenne".
   Il est donc plus logique d'utiliser OPERATION='EXTRACTION'.
   
   
   Analyse supplémentaire :
   ========================
   Dans la doc U4.81.21, on explique également qu'informatiquement, on peut utiliser RESULTANTE avec n'importe quel champ, mais que
   l'opération n'a de sens "physique" que pour des forces nodales (champs FORC_NODA et REAC_NODA).
   On explique également que derrière le mot clé MOMENT, il faut indiquer les noms des ddls de rotation (DRX, DRY et DRZ) et que si on
   utilise (DX, DY et DZ) on obtient des résultats faux.
   
   
   Evolution :
   ===========
   On propose de restreindre l'usage des mots clés RESULTANTE et MOMENT :
    * On n'autorise ces mots clés que pour OPERATION='EXTRACTION'
    * ces mots cés ne sont autorisés que pour les champs de DEPL_R (utilisés pour les forces nodales).
    * derrière RESULTANTE, on n'autorise que DX, DY et DZ
    * derrière MOEMENT, on n'autorise que DRX, DRY et DRZ
   
   Plus précisément :
   
   1) On modifie le catalogue de POST_RELEVE_T :
   -              RESULTANTE      =SIMP(statut='f',typ='TXM',max='**'),
   +              RESULTANTE      =SIMP(statut='f',typ='TXM',max='**',into=("DX","DY","DZ")),
   -              MOMENT          =SIMP(statut='f',typ='TXM',max='**'),
   +              MOMENT          =SIMP(statut='f',typ='TXM',max='**',into=("DRX","DRY","DRZ")),  
   
   2) On introduit une vérification dans le fortran (car je ne sais pas comment le faire dans le catalogue):
      RESULTANTE (et MOMENT) ne sont autorisés que pour OPERATION='EXTRACTION'
   
   3) Dans les tests qui utilisaient OPERATION='MOYENNE' + RESULTANTE, on remplace :
      OPERATION= 'MOYENNE' -> 'EXTRACTION'
   
   4) Pour le test forma21a qui utilisait RESULTANTE=('TEMP',), on remplace RESULTANTE= par NOM_CMP= pour calculer la température moyenne.
      Cela modifie (beaucoup) 2 valeurs de non-régression. Une fiche (issue24928) a été émise car je pense que ce test est mal fait.
   
   5) Pour le test ssll14a, le calcul du moment était fait avec MOMENT=('DX','DY','DZ') ce qui donne des résultats faux.
      Je corrige le test.
   
   
   Impact documentaire :
   =====================
   U4.81.21 : POST_RELEVE_T : expliquer les nouvelles limitations pour les mots clés RESULTANTE et MOMENT.
   Doc V : Rien : les valeurs modifiées dans les tests ne sont pas écrites dans les doc V.
   
   Résultats faux ?
   ----------------
   Non : la doc indique déjà les risques de se tromper.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.81.21
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.8

--------------------------------------------------------------------------------
RESTITUTION FICHE 24916 DU 08/03/2016
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Contact continu et matr_distribuee='oui' => plantage jeveux_10
FONCTIONNALITE
   Problème (S. Meunier) :
   ~~~~~~~~~~~~~~~~~~~~~~~
   Suite à la correction de la fiche issue24612, j'ai voulu utiliser matr_distribuee='oui' pour un calcul de contact en robinetterie en
   version unstable. J'ai obtenu un message d'erreur jeveux_10 (voir pj).
   
   Analyse :
   ~~~~~~~~~
   Il s'agit d'une mauvaise correction de issue24612 :
   
   J'ai introduit un bloc dans la routine detrsd.F90 pour détruire tous les objets de la sd NUML_EQUA.
   Parmi ces objets il existe des objets nommés :
   XXXX.NUML.1
   XXXX.NUML.2
   XXXX.NUML.3
   ...
   XXXX.NUML.N
   
   J'ai cru que N était le nombre de processeurs, mais il s'agit du nombre de joints.
   
   Correction :
   ~~~~~~~~~~~~~
   Je corrige detrsd.F90
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    essai perso
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24949 DU 15/03/2016
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Un bug dans bibfor/nonlinear/ReadMeasure.F90
FONCTIONNALITE
   Problème :
   ==========
   Dans bibfor/nonlinear/ReadMeasure.F90, on récupère le mot clé MESURE/TABLE='OUI'/'NON' avec un getvtx. Ce mot clé a la valeur 'NON'
   par défaut, mais comme le mot clé facteur (MESURE) n'a pas le statut 'd' dans le catalogue, le getvtx ne retourne pas la valeur
   'NON' par défaut.
   
   Valgrind signale alors l'usage d'une variable non initialisée (test hpla100k).
   
   Correction :
   ============
   On ajoute le statut "d" au mot clé MESURE :
   -def C_MESURE() : return FACT(statut='f',max=1,
   +def C_MESURE() : return FACT(statut='d',max=1,
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24948 DU 15/03/2016
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    3 tests cassés sur clap0f0q en version 13.1.10
FONCTIONNALITE
   Problème :
   ==========
   En version 13.1.10-3234fe8c79ff - 14/03/2016 06:45:29 sur clap0f0q, 3 tests sont cassés :
   ssls118u
   ssls118v
   ssns106c
   
   Analyse :
   =========
   Le problème vient de la routine te0409 utilisée par les coques "globales" (DKTG, Q4GG, ...).
   
   Quand on est en linéaire (option RIGI_MECA), on "saute" de grands blocs de la routine (puisqu'on ne dispose d'aucun champ : DEPL-,
   DEPL+, SIEF-, ...).
   Malheureusement, on calcule dans ces blocs des variables qui sont utilisées plus tard.
   
   Les premières variables de ce type que j'ai identifiées sont ul et dul.
   Je les ai initialisées à zéro mais le code se plante alors plus loin.
   
   Correction :
   ============
   J'ai finalement initialisé à zéro 11 tableaux de réels :
   +    real(kind=8) :: ul(6, 4)=0.d0, dul(6, 4)=0.d0, angmas(3)
   +    real(kind=8) :: bf(3, 3*4)=0.d0, bm(3, 2*4)=0.d0, bmq(2, 3)=0.d0, bc(2, 3*4)=0.d0
   +    real(kind=8) :: flex(3*4, 3*4)=0.d0, memb(2*4, 2*4)=0.d0, flexi(3*4, 3*4)=0.d0
   +    real(kind=8) :: mefl(2*4, 3*4)=0.d0, work(3, 3*4)=0.d0
   
   Mais je ne sais pas si c'était nécessaire pour ces 11 tableaux ...
   
   Validation :
   ============
   Les 3 tests cassés sont maintenant OK
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssls118u,v et ssns106c sur clap0f0q
NB_JOURS_TRAV  : 0.7

--------------------------------------------------------------------------------
RESTITUTION FICHE 24884 DU 26/02/2016
AUTEUR : FERTE Guilhem
TYPE aide utilisation concernant Code_Aster (VERSION 12.5)
TITRE
    Pb MACR_RECAL et fichier esclave
FONCTIONNALITE
   Anomalie (ou pas?):
   -------------------
   
   Un recalage avec la commande MACR_RECAL implique un fichier de commande maître, réalisant l'optimisation et un fichier de commande
   esclave, définissant le calcul Aster à réaliser à chaque itération. Si on spécifie "comm" au lieu de "libr" comme type de fichier
   dans ASTK pour le fichier de commande esclave, le calcul plante sur erreur NO_COMM_FILE.
   
   Le problème a été remonté à l'occasion de cette fiche AOM. On a conseillé à l'utilisateur de renseigner "libr" au lieu de "comm"
   comme type pour le fichier de commande esclave dans ASTK.
   
   Explication :
   -------------
   
   L'explication se trouve lors de la création du fichier .export esclave (fonction Creation_Fichier_Export_Esclave dans le fichier
   reca_calcul_aster.py). Lignes 529-541, on y lit :
   
                       # Ancien .comm non pris en compte
                       # Fichier d'unite logique UNITE_RESU (rapport de
                       # MACR_RECAL) non pris en compte
                       if dico['type'] == 'comm' or (dico['ul'] == self.UNITE_RESU and lab == 'resu'):
                           l_fr.remove(dico)
   
                       # Fichier d'unite logique UL devient le nouveau .comm
                       elif dico['ul'] == self.UNITE_ESCL:
                           self.fichier_esclave = dico['path']
                           dico['type'] = 'comm'
                           dico['ul'] = 1
                           dico['path'] = user_mach + os.path.join(
                               os.getcwd(), 'fort.%d' % self.UNITE_ESCL)
   
   On voit qu'on supprime donc n'importe quel fichier d'extension .comm AVANT de se demander s'il porte l'unité logique identifiant le
   fichier esclave.
   
   Actions possibles:
   -----------------
   
   - On ne fait rien car MACR_RECAL a vocation à être remplacé par ADAO
   
   - On met un message dans la documentation U de MACR_RECAL pour mentionner ce point.
   
   - On inverse les deux conditions dans reca_calcul_aster.py.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    Réalisation de l'étude soumise par l'utilisateur (sur athosdev)
NB_JOURS_TRAV  : 2.0

