==================================================================
Version 13.3.19 (révision ff3ea3930955) du 2017-05-17 13:22 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 26422 DU 04/05/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Erreur avec IMPR_FONCTION format Tableau
FONCTIONNALITE
   Problème
   --------
   
   On imprime une fonction et une nappe dans un même IMPR_FONCTION.
   
   Au FORMAT='XMGRACE', le tracé est correct.
   Au FORMAT='TABLEAU', les valeurs de la 3ème colonne (=ordonnée de la 1ère fonction de la nappe) ne sont pas correctes.
   
   
   Correction
   ----------
   
   Au FORMAT='TABLEAU', toutes les colonnes ont le même nombre de lignes.
   Toutes les fonctions sont donc interpolées sur les abscisses de la 1ère fonction fournie dans IMPR_FONCTION.
   
   Or en cas de présence d'une nappe, on supposait que seule la nappe était à tracer et donc que les abscisses d'interpolation étaient
   fournies par la 1ère fonction de la nappe. Donc, on n'interpolait pas la 1ère fonction de la nappe (puisqu'il n'y avait rien à faire).
   Il faut interpoler cette 1ère fonction car les abscisses sont fournies par la fonction précédente.
   
   Remarque : Une alarme était émise pour préciser que la deuxième fonction n'a pas le même nombre de points.
   
   Avec l'erreur précédente, si N est le nombre de points de la fonction, on prenait les N premières ordonnées de la 1ère fonction de
   la nappe sans interpolation. S'il y avait moins de points, on mettait None.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    exemple joint
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25168 DU 28/04/2016
AUTEUR : FLEJOU Jean Luc
TYPE anomalie concernant Code_Aster (VERSION 11.6)
TITRE
    [FAUX] Effort sur des 'K_T_L'  impossible à postraiter dans le repere global
FONCTIONNALITE
   Description du problème :
   -------------------------
   Une étude avec un modèle contient des DISCRET de type 'K_T_L', 
   Lorsque l'on post-traite le champ SIEF_NOEU avec la commande POST_RELEVE_T et REPERE=GLOBAL
   
   test_eff=POST_RELEVE_T(
      ACTION=_F(OPERATION='EXTRACTION', RESULTAT=lin, NOM_CHAM='SIEF_NOEU',NOEUD=('N1',), REPERE='GLOBAL',),
   )
   
   La valeur imprimée dans le ".resu" contient les entêtes N VY VZ qui ne correspondent pas à des grandeurs dans le repère GLOBAL mais
   à des grandeurs exprimées dans les repères locaux des éléments.
   
   
   Analyse du problème :
   ---------------------
   Il y a en fait 2 problèmes:
   a) La commande CALC_CHAMP qui permet de calculer les SIEF_NOEU à partir des SIEF_ELNO ne faisait pas bien le travail pour les
   DISCRETS. Pour les poutres et coques la commande vérifie bien que les repères locaux des éléments connectés à un même nœud ont les
   mêmes repères locaux (à 5°dg près). Si ce n'est pas le cas le message <A> <UTILITAI_4> est émis. Si les repères ne sont pas
   identiques le cumul des efforts locaux n'a aucun sens.
   
      ! <A> <UTILITAI_4>                                                       !
      !  Vous avez demandé le calcul d'un champ aux nœuds sur des éléments     !
      !  de structure. Mais les repères locaux de certaines mailles entourant  !
      !  des noeuds sur lesquels vous avez demandés le calcul ne sont pas      !
      !  compatibles (Au maximum, on a 90.0456 degrés d'écart entre les angles !
      !  nautiques définissant ces repères).                                   !
      !                                                                        !
      !  Risque & Conseil :                                                    !
      !    Il se peut que vous obteniez des résultats incohérents.             !
      !    Il est donc recommandé de passer en repère global les champs        !
      !    utiles au calcul du champ aux nœuds.                                !
   
   
   b) Lorsque l'on post-traite avec POST_RELEVE_T, des champs d'éléments de structure qui n'ont de signification que dans les repères
   locaux le mot clef REPERE=GLOBAL n'est pas pris en compte, et ne peut pas l'être car les champs n'ont pas de signification dans le
   repère global. Le seul post-traitement possible est dans le repère local des éléments. L'utilisateur n'est pas avertit de la non
   prise en compte de REPERE=GLOBAL. 
   
   
   Corrections :
   -------------
   a) Ajout des discrets dans la vérification des repères locaux des éléments connectés à un même nœud.
   
   b) Dans le cas où le modèle contient des éléments de structure et que l'on demande un post-traitement d'un champ aux _NOEU ailleurs
   que dans le repère LOCAL des éléments, il y a émission du message <A> <POSTRELE_70>.
      !--------------------------------------------------------------------------------------------------!
      ! <A> <POSTRELE_70>                                                                                !
      !                                                                                                  !
      ! Commande POST_RELEVE_T :                                                                         !
      ! Votre modèle contient des éléments de structures et :                                            !
      !     - vous avez demandé le post-traitement du champ SIEF_NOEU, qui n'a de signification que dans !
      !       le repère LOCAL des éléments.                                                              !
      !     - vous avez demandé le post-traitement dans un repère différent de LOCAL.                    !
      !                                                                                                  !
      ! Le mot clef REPERE est ignoré.                                                                   !
      ! Le post-traitement sera réalisé dans le repère LOCAL des éléments.                               !
      !                                                                                                  !
      ! Conseil :                                                                                        !
      !     Si vous avez précédemment utilisé la commande CALC_CHAMP et que les repères locaux des       !
      !     éléments connectés à un même noeud sont différents, une alarme a été émise.                  !
      !     Dans ce cas les résultats obtenus aux noeuds ne sont pas pertinents.                         !
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 11.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 11.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage cas étude+test perso
NB_JOURS_TRAV  : 2.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26433 DU 05/05/2017
AUTEUR : FLEJOU Jean Luc
TYPE aide utilisation concernant Code_Aster (VERSION 11.4)
TITRE
    MASS_REP : masse attribuée ou pas ?
FONCTIONNALITE
   Description du problème :
   -------------------------
   L'étude concerne la modélisation d'une conduite forcée.
   Dans le modèle il y a des masses qui sont ajoutées pour prendre en compte la présence de 'pilettes' et de 'massifs'. Lorsque l'on
   pèse le modèle à l'aide de POST_ELEM les masses ne sont pas prises en compte.
   
   
   Analyse :
   ---------
   Les masses sont des POI1 qui sont sur un noeud commun soit à une pilette soit à un massif. Mais la maille POI1 est distincte des
   mailles 'tuyau'.
   Donc, si l'on souhaite "peser" la structure en tenant compte des masses, il faut le préciser. Il faut donner les mailles qui
   correspondent aux masses dans la commande POST_ELEM:
   MAS=POST_ELEM(
       MODELE=MODELE, CARA_ELEM=CARAC, CHAM_MATER=CHMAT,
       MASS_INER = ( _F(GROUP_MA='Tuyau',), _F(GROUP_MA=G_pilettes,), _F(GROUP_MA=G_PO_ml,), ),
   )
   
   De même lorsque l'on définit le chargement de pesanteur il faut également indiquer les mailles qui correspondent aux masses.
   
   
   Remarques :
   -----------
   Au cours de l'analyse AFFE_CARA_ELEM émet le message d'information suivant, avec des affectations de masse nulle sur certaines
   mailles. Le problème aurait pu venir de là, mais c'était volontaire et maîtrisé.
   "
   AFFE_CARA_ELEM. Il y a 38 occurrences du mot clef facteur <MASS_REP>.
   Entre ces différentes occurrences les GROUP_MA_POI1 ont 240 mailles en communs.
   La règle de surcharge est donc appliquée 240 fois.
   "
   
   Cela n'est pas très facile à débugger notamment quand les groupes de mailles se superposent. Il faut revenir au maillage et repérer
   une à une les mailles communes au 38 groupes.
   ==> Développement fait pour afficher toutes les mailles surchargées lorsque INFO=2.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage cas étude+test perso
NB_JOURS_TRAV  : 2.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26435 DU 09/05/2017
AUTEUR : FLEJOU Jean Luc
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    DEFI_GEOM_FIBRE : maillage sans TRI ou QUAD
FONCTIONNALITE
   Suite à formation GC :
   ----------------------
   Lorsque l'on donne à DEFI_GEOM_FIBRE un maillage ne contenant aucun TRI3 ou QUAD4, aucun maillage de fibre n'est construit (c'est
   normal). Par contre, on construit par la suite un maillage de fibre, avec le nombre de noeud et d'élément de surface qui va bien. On
   utilise les commandes régaliennes et ça plante sur un jeveux avec un vecteur de taille nulle. Comme il n'y a aucun éléments de
   surface c'est normal, mais pas propre.
   
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! <S> Exception utilisateur levee mais pas interceptee.                      !
      ! Les bases sont fermees.                                                    !
      ! Type de l'exception : error                                                !
      !                                                                            !
      ! Erreur de programmation.                                                   !
      !                                                                            !
      ! Condition non respectée:                                                   !
      !     lso.ne.0                                                               !
      ! Fichier /home/B50238/dev/codeaster/src/bibfor/jeveux/jjalls.F90, ligne 111 !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   
   Correction :
   ------------
   Quand l'utilisateur donne un maillage pour lequel aucun éléments n'est valide ==> message <F> (avant de faire un plantage par appel
   à jeveux)
   Pas de risque de résultats faux car ça plante en <F> sur erreur jeveux.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage cas étude+test perso
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25795 DU 22/11/2016
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    B105.16 : en version 13.2.15, le test ssns115b ne converge pas dans STAT_NON_LINE sur athosdev mpi avec np=3
FONCTIONNALITE
   Objet de la fiche :
   ==================
   Problème de convergence en calcul parallèle (ncpu=3). Il s'agit d'un test de membrane en grandes transformations avec une loi de
   comportement élastique non linéaire. Ce test utilise la recherche linéaire.
   
   Diagnostique :
   =============
   Le partitionneur SCOTCH ne pose pas de problème. Le partionneur METIS pose des soucis de convergence.   La décomposition en sous
   domaine a une influence sur la recherche linéaire. 
   
   Par exemple, avec SCOTCH on obtient pas les mêmes rho (np=3) qu'avec METIS. Avec le même fichier de commande, la valeur de rho
   commence par diverger à la 8 itération de Newton :
   
   """
   rho = 3.21852 --> SCOTCH
   rho = 3.21851 --> METIS
   """
   
   Dans ce cas, METIS a besoin de plus d'itérations pour faire converger le calcul. En basculant ITER_GLOB_MAXI=500 dans le cas-test
   posant problème ssns115b, on obtient la convergence et aucun test_resu n'est cassé
   (np=3, PARITITIONNEUR='METIS').
   
   Restitution :
   ============
   - Conseils dans la doc d'utilisation de stat_non_line : [U2.04.01]
   - Restitution de ssns115b avec des commentaires dans le cas-test.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : U2.04.01
VALIDATION
    submit
NB_JOURS_TRAV  : 3.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26405 DU 24/04/2017
AUTEUR : DROUET Guillaume
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    [perform] Essai de résilience
FONCTIONNALITE
   Analyse du problème:
   ===================
   
   Sur une modélisation d'un essai de résilience, je tombe sur l'erreur développeur JEVEUX1_67
   
   [1]    !                                                        !
   [1]    ! La valeur 0 affectée à l'attribut LONMAX est invalide. !
   [1]    ! Ce message est un message d'erreur développeur.        !
   [1]    ! Contactez le support technique.                        !
   [1]    !--------------------------------------------------------!
   
   pour un calcul qui a du mal à converger.
   
   Lors de ce calcul qui a du mal à converger, les zones de contact "s'éloignent" de sorte que l'opération d'appariement par
   projection-intersection renvoie une liste de mailles de contact vide. Ce cas de figure n'est actuellement pas pris en compte dans la
   méthode LAC, on suppose, à tort que l'utilisateur peut toujours définir des zones de contact où des mailles de contact sont
   détectées par projection intersection.
   
   Correction:
   ==========
   
   Pour corriger ce problème, on modifie les sources de sorte à pouvoir gérer un ligrel de contact absent. Si l'on détecte aucune
   maille de contact on ne crée pas de maille de ligrel de contact et tous les lagrangiens de contact sont associés au statut -1. La
   matrice et le second membre sont alors automatiquement corrigés après assemblage.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    submit + test perso
NB_JOURS_TRAV  : 2.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26413 DU 27/04/2017
AUTEUR : PLESSIS Sarah
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Erreur dans POST_RCCM : calcul de déformation progressive
FONCTIONNALITE
   Dans la fiche issue26299, les critères SN* et rochet thermique ont été étendus à toutes les options de POST_RCCM. Ces critères
   permettent de vérifier la tenue d'un composant à la déformation progressive.
   Après plusieurs échanges avec le SEPTEN, les équations intégrées comportaient des erreurs qui ont été rectifiées.
   Un cas-test est impacté : rccm01e.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : v1.01.107, r7.04.03
VALIDATION
    passage de cas tests rccm*
NB_JOURS_TRAV  : 5.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26454 DU 11/05/2017
AUTEUR : PLESSIS Sarah
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Erreur dans POST_RCCM détectées grâce à la création de cas-tests
FONCTIONNALITE
   Dans le cadre de la création de nouveaux cas-tests pour l'opérateur POST_RCCM (issue24734 et issue25030), j'ai détecté quelques
   erreurs dans les routines de l'opérateur, certaines datant d'avant mes développements 2015/2016/2017 et d'autres non. Je ferai le
   report dans la branche v12 et default. 
   
   Depuis la V12, lors de la prise en compte du séisme en B3200, (mot clé 'SEISME' et TYPE_RESU_MECA='B3200'ou'UNITAIRE') deux
   grandeurs dans le .resu sont fausses car elles ne tiennent pas compte du séisme 
   - la grandeur SALTMAX  : contrainte alternée maximale lors du calcul en fatigue
   - la grandeur FU_PARTIEL (pour TYPE='SITU'): valeur du facteur d'usage partiel en fatigue par situation 
   
   Depuis la V13, plusieurs erreurs subsistent : 
   
   - pour TYPE_RESU_MECA ='B3200' + TYPE_KE ='KE_MIXTE' + METHODE='TOUT_INST',  les grandeurs calculées avec OPTION='FATIGUE' sont
   fausses : la partie mécanique de la contrainte est prise en compte 2 fois 
   
   - pour TYPE_RESU_MECA ='B3200' + 'RESU_MECA_UNIT' + METHODE='TOUT_INST', les grandeurs calculées avec OPTION='FATIGUE' sont fausses
   pour les combinaisons de situations (TYPE='COMBI' dans le .resu)
   
   - pour TYPE_RESU_MECA ='B3200' + 'RESU_MECA_UNIT' +TEMP_A/TEMP_B/TABL_TEMP, les grandeurs calculées avec toutes les options
   (OPTION='SN', 'FATIGUE' ou 'EFAT') sont fausses : facteur +/- sur la contrainte due aux moments interpolés sur la température peut
   être ignorée. 
   
   PAs d'impact doc vu qu'elle n'est pas encore à jour
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 11.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 13.0
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage de cas tests rccm*
NB_JOURS_TRAV  : 7.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24734 DU 18/01/2016
AUTEUR : PLESSIS Sarah
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    Courverture de POST_RCCM en version 13.1.3
FONCTIONNALITE
   Certaines fonctionnalités de POST_RCCM n'étaient sont plus testées en 13.1.3 depuis le transfert des tests 
   du dossier src/validation.
   
   Elles sont désormais testées grâce à la création de trois nouveaux cas-tests (rccm13, rccm14, rccm15) et aussi à la
   modification de l'ancien cas-test rccm01. 
   
   - Le cas-test rccm13 teste la méthode ZE200 : V3.04.161
   - Le cas-test rccm14 teste la méthode B3200 avec les groupes de fonctionnement : V3.04.162
   - Le cas-test rccm15 est une copie light du cas-test validation rccm02 qui est confidentiel, j'ai enlevé du fichier de commande les
   commentaires qui mentionnent l'application visée, la doc sera rédigée en conséquence : V3.04.163
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : V1.01.107, V3.04.161, V3.04.162, V3.04.163
VALIDATION
    passage de cas tests rccm*
NB_JOURS_TRAV  : 7.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25030 DU 06/04/2016
AUTEUR : PLESSIS Sarah
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Couverture routines RCCM
FONCTIONNALITE
   Nouveaux cas-tests créés, voir issue24734
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage de cas tests rccm*
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25035 DU 06/04/2016
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    Couverture de mefgri, tfvegr, etc.
FONCTIONNALITE
   On créé une nouvelle modélisation de sdll152 pour couvrir les fonctions: mefgri, tfvegr:
   
   On créé un nouveau cas test zzzz405a pour couvrir : motubn, pusur2, stapu2, usuban, usubis, usufon, usukwu, usunew, usupu2 et usuvu2
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V2.02.152, V1.01.405
VALIDATION
    zzzz405a, sdll125b

--------------------------------------------------------------------------------
RESTITUTION FICHE 24728 DU 18/01/2016
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TMA : Necs
TITRE
    Couverture de DEFI_FLUI_STRU en version 13.1.3
FONCTIONNALITE
   On crée la modélisation sdll152 afin de couvrir les routines :
   deffen, discax, discff, fenexc, lexseg, scaldf, scalff, specff, veriff.
   Plusieurs cas sont effectués afin de couvrir DEFI_FLUI_STRU/GRAPPE/COUPLAGE=NON et tous les mots-clés non couverts de
   DEFI_SPEC_TURB/SPEC_LONG_COR_1 et DEFI_SPEC_TURB/SPEC_LONG_COR_2 pointés par issue24708 et issue24729. 
   
   
   L'impact doc v2.02.152 sera fait dans une fiche dédié.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : v2.02.152
VALIDATION
    sdll152*

--------------------------------------------------------------------------------
RESTITUTION FICHE 25031 DU 06/04/2016
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    Couverture de coesp1
FONCTIONNALITE
   Problème :
   ========
   
   La routine coesp1 n'est validée que par des tests de la liste validation (sdll504b).
   
   Correction :
   ==========
   
   On crée la modélisation C à sdll152 (ajouté par Hassan dans issue25032). On y reporte les commandes de sdll504b permettant de
   couvrir la routine coesp1.
   
   Impact doc:
   ==========
   
   V2.02.152
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V2.02.152
VALIDATION
    nouveau test sdll152c

--------------------------------------------------------------------------------
RESTITUTION FICHE 24729 DU 18/01/2016
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TMA : Necs
TITRE
    Couverture de DEFI_SPEC_TURB en version 13.1.3
FONCTIONNALITE
   On crée la modélisation e à sdll152 afin de couvrir les routines :deffen, discax, discff, fenexc, lexseg, scaldf, scalff, specff,
   veriff.
   Plusieurs cas sont effectués afin de couvrir DEFI_FLUI_STRU/GRAPPE/COUPLAGE=NON et tous les mots-clés non couverts de
   DEFI_SPEC_TURB/SPEC_LONG_COR_1 et DEFI_SPEC_TURB/SPEC_LONG_COR_2 pointés par issue24708 et issue24729. 
   
   L'impact doc v2.02.152 sera fait dans une fiche dédié.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : v2.02.152
VALIDATION
    sdll152*

--------------------------------------------------------------------------------
RESTITUTION FICHE 25029 DU 06/04/2016
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    Couverture de axdipo, cajgr2, etc.
FONCTIONNALITE
   Fiche chapeau issue24708 et issue24729
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdll152*

--------------------------------------------------------------------------------
RESTITUTION FICHE 26412 DU 27/04/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TMA : Necs
TITRE
    Interpolation dans LISS_ENVELOP
FONCTIONNALITE
   Lors de la première restitution de LISS_ENVELOPPE, la fonction interpolate de scipy était utilisée.
   Or scipy n'est pas un prérequis de code_aster.
   
   A la place, une fonction d'interpolation avait été écrite en Python.
   Il est préférable d'utiliser la fonction interp de numpy dans liss_enveloppe.py, numpy étant un prérequis de code_aster.
   
   -        ynew = interpolate(spec.listFreq, spec.dataVal,l_freq)
   +        ynew = N.interp(l_freq, spec.listFreq, spec.dataVal)
   
   Les résultats de zzzz100e ne sont pas modifiés.
   
   Fiche SAV pour le projet risque sismique.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz100e

--------------------------------------------------------------------------------
RESTITUTION FICHE 26440 DU 09/05/2017
AUTEUR : GUILLOUX Adrien
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TMA : Necs
TITRE
    Valeurs tabeau dans IMPR_FONCTION/LISS_ENVELOP pour plusieurs NAPPE_LISSEE
FONCTIONNALITE
   Problème
   ========
   Dans IMPR_FONCTION au format LISS_ENVELOP, il y a un tableau avec les valeurs de NAPPE_LISSEE. Comme 
   indiqué dans la documentation, s'il y a plusieurs occurrences de NAPPE_LISSEE alors on doit avoir 
   les données de la dernière table.
   
   En pièce jointe, un exemple où ce n'est pas le cas. Je n'ai pas compris d'où venaient les valeurs 
   affichées pour la "deuxième" nappe lissée.
   
   Analyse
   =======
   Dans l'exemple proposé, on donne deux nappes lissées avec des valeurs d'amortissement différentes. 
   Dans ce cas, les données de ces deux nappes lissées ont été mises dans le tableau.
   Pour résoudre ce soucis, on modifie la lecture des nappes et on ne considère que la première nappe 
   lissée pour le tableau des valeurs. La documentation U4.33.01 doit être modifiée en conséquence.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.33.01
VALIDATION
    zzzz100e

--------------------------------------------------------------------------------
RESTITUTION FICHE 26125 DU 24/02/2017
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Erreur JEVEUX si l'on fournit un NUME_DDL_GENE existant dans PROJ_BASE
FONCTIONNALITE
   Objet de la fiche :
   ===================
   Si on fournit un NUME_DDL_GENE existant dans PROJ_BASE pour la projection de matrices sur base modale, on obtient une erreur JEVEUX
   (objet déjà existant).
   
   Diagnostique :
   ==============
   Dans la programmation actuelle de PROJ_BASE, on crée systématiquement un nume_ddl_gene avec l'opérateur NUME_DDL_GENE.D'où plantage
   jeveux avec "objet déjà existant" si l'utilisateur essaie d'imposer un nume_ddl_gene calculé au préalable.
   Apparemment, la gestion de l'argument du mot-clé NUME_DDL_GENE dans PROJ_BASE ne se fait pas correctement.
   
   Restitution :
   =============
   Dans PROJ_BASE on fait le test :
   ...
       numgen = NUME_DDL_GENE
       NUME_DDL_GENE = self.get_cmd('NUME_DDL_GENE')
       if numgen is None:
           _num = NUME_DDL_GENE(BASE=BASE, NB_VECT=NB_VECT, STOCKAGE=STOCKAGE)
       else:
           if numgen.is_typco():
               self.DeclareOut('_num', numgen)
               _num = NUME_DDL_GENE(BASE=BASE, NB_VECT=NB_VECT, STOCKAGE=STOCKAGE)
           else:
               _num = numgen
   ...
   
   à la place de (version actuelle du code) :
   ...
       numgen = NUME_DDL_GENE
       NUME_DDL_GENE = self.get_cmd('NUME_DDL_GENE')
       if numgen is not None:
           self.DeclareOut('_num', numgen)
       _num = NUME_DDL_GENE(BASE=BASE, NB_VECT=NB_VECT, STOCKAGE=STOCKAGE)
   
   Validation :
   ============
   On modifie le cas test sdnl32a. On rajoute l'opérande NUME_DDL_GENE à l'opérateur PROJ_BASE en faisant au préalable un calcul
   NUME_DDL_GENE sur la base.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.63.11
VALIDATION
    cas test sdnl32a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26390 DU 14/04/2017
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    Définition de l'excitation dans l'opérateur REST_SPEC_PHYS plusieurs noeuds et composantes
FONCTIONNALITE
   Objet de la fiche :
   ===================
   On obtient le message d'erreur "Le groupe de noeuds  n'existe pas" lorsqu'on utilise l'opérande GROUP_NO de REST_SPEC_PHYS.
   
   Disgnostique :
   ==============
   Ce message d'erreur apparaît précisément lorsqu'on utilise REST_SPEC_PHYS sans l'opérande "BASE_ELAS_FLUI" et avec une seule
   composante "NOM_CMP". Ceci vient de l'appel à "getvem" dans la subroutine speph0.F90 pour obtenir la liste des GROUP_NO. Dans la
   version actuelle cet appel retourne comme nom de groupe de noeuds le caractère vide (" ").
   
   Restitution :
   ============
   Dans speph0.F90 on remplace :
   call getvem(maillage, 'NOEUD', 'EXCIT', 'GROUP_NO', i,iarg, 0, group, iret) 
   par
   call getvem(maillage, 'GROUP_NO', 'EXCIT', 'GROUP_NO', 1, iarg, nbgrno, group_noeud, iret)
   
   et ensuite on vient parcourir la liste des groupes de nœuds obtenue pour extraire les nœuds associés.
   
   C'est le même problème pour EXCIt. La correction est faite pour les deux cas.
   
   Validation :
   ============
   Pour valider l'implémentation, on modifie le cas test "sdld101a" en modifiant le premier REST_SPEC_PHYS. L'opérande "NOEUD" est
   remplacé par "GROUP_NO" avec des group_no à un seul noeud.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdld101a; test associé à la fiche
NB_JOURS_TRAV  : 3.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26420 DU 03/05/2017
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    dyna vibra en harmonique
FONCTIONNALITE
   Objet de la fiche :
   ==================
   La commande DYNA_VIBRA nécessite toujours un chargement  EXCIT même quand il y a déjà un EXCIT_RESU en harmonique
   
   
   Restitution :
   ============
   Effectivement, l'erreur vient du catalogue de DYNA_VIBRA où dans le cas harmonique le mot clé "EXCIT" est obligatoire. 
   
   On rend "EXCIT" facultatif comme dans le cas transitoire
   
   Ensuite, on blinde les mots clés EXCIT et EXCIT_RESU en rajoutant la règle : 
   regles=(AU_MOINS_UN('EXCIT', 'EXCIT_RESU',),)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    Les cas tests DYNA_VIBRA
NB_JOURS_TRAV  : 0.2

--------------------------------------------------------------------------------
RESTITUTION FICHE 26469 DU 16/05/2017
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    mbrigi.F90 : "common" JEVEUX au lieu de #include "jeveux.h"
FONCTIONNALITE
   Nettoyage de routines contenant encore le common JEVEUX en dur ou des commentaires associés (mbrigi.F90, virhol.F90).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25535 DU 01/09/2016
AUTEUR : BILLON Astrid
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    A203.xx - Probleme de convergence UMAT avec variables de commandes
FONCTIONNALITE
   L'interface UMAT a été vérifiée à l'aide d'une petite étude mettant en œuvre une loi de comportement "maison" (écrouissage linéaire 
   isotrope) et une résolution du type SIMU_POINT_MAT. Cette étude est valorisée dans le nouveau cas test zzzz409.
   Le comportement du code pendant la convergence est bien cohérent avec celui obtenu pour un script Python produisant la même 
   résolution de manière totalement indépendante : mêmes nombres d'itérations et mêmes matrices tangentes.
   L'interface UMAT semble donc satisfaisante.
   
   En revanche, certaines corrections ont été apportées à l'interface.
   Un bug provenait notamment du fait que, même en l'absence de thermique, il était nécessaire d'indiquer une valeur de alpha, au risque 
   d'avoir des valeurs NaN de déformation mécanique transmises à UMAT.
   
   Corrections apportées :
   	1 Sans thermique, le mot-clé facteur 'ELAS' n'est plus requis. Avec thermique, le mot-clé facteur 'ELAS' et le mot-clé 
   simple 'ALPHA' sont requis.
   	2 Le numéro de la maille est maintenant bien transmis dans la variable NOEL (sauf si appel par CALC_POINT_MAT, auquel cas 
   NOEL = 1)
   	3 Le numéro de l'itération courante est maintenant bien transmis dans la variable KINC, avec KINC = 0 en phase de prédiction 
   (sauf si appel par CALC_POINT_MAT, auquel cas KINC = 0)
   	4 Les coordonnées du point de Gauss courant sont maintenant bien transmises dans la variable COORDS si la variable de 
   commande GEOM est déclarée.
   
   Les itérations suspectes observées par UKC dans l'exemple joint à la fiche proviennent de la LDC elle-même qui ne respecte pas tout à 
   fait le standard -> à voir directement avec eux.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U2.10.01, V1.01.409
VALIDATION
    cas tests
NB_JOURS_TRAV  : 22.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26461 DU 14/05/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En En version 13.3.18-bf89094912f0, les tests icare01a, icare01b, icare02a,icare03a,icare04a échouent sur AthosDev_ Valid
FONCTIONNALITE
   Lors de la réalisation de issue26366, les tests icare* avaient été oubliés.
   Les 'import *' ont été corrigées pour importer les commandes.
   
   Or 'MAIL_PY' a été mis dans les commandes à importer...
   
   MAIL_PY doit être importé depuis 'Utilitai.partition'.
   S'il n'y avait pas eu ce 2ème 'import *', il n'y aurait pas eu de confusion. Encore une bonne raison d'interdire 'import *'.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    icare*
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26039 DU 08/02/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE aide utilisation concernant Code_Aster (VERSION 12.4)
TITRE
    Aide à la modélisation - Compensateur A Ondes - Modèle élastoplastique
FONCTIONNALITE
   Objet de la demande
   ===================
   1. Analyser  la  bonne mise  en données sans se  préoccuper des aspects lois de comportement. Si possible optimiser.
   2. Analyse de convergence en maillage
   
   Résolution :
   ===========
   - Vérification des conditions aux limites (remplacement de la LIAISON_SOLIDE par une liaison COQ_POU)
   - Vérification de la mise en donnée de STAT_NON_LINE(activation de REAC_ITER)
   - Conseils de mise en oeuvre du calcul parallèle.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 1.0

