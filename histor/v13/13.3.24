==================================================================
Version 13.3.24 (révision 44d1982fb110) du 2017-06-23 13:14 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 26106 DU 22/02/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    ELAS_COQUE : raideur membranaire et de flexion
FONCTIONNALITE
   Problème :
   ==========
   Un utilisateur observe des résultats difficiles à interpréter sur les forces nodales en utilisant ELAS_COQUE.
   
   Pour rappel, ELAS_COQUE permet de prendre en compte des raideurs de flexion et de membrane non classiques de coque. Les valeurs sont
   rentrées à la main par l'utilisateur dans le fichier de commande. Dans tous les cas, l'épaisseur renseigné dans AFFE_CARA_ELEM ne
   sert qu'à calculer la masse en dynamique. 
   
   - question 1 : l'utilisateur applique une raideur membranaire équivalent à une épaisseur=1.8 et une raideur de flexion équivalent à
   une épaisseur de 0.5m. On constate que les valeurs de FORC_NODA sont proportionnelles de l'épaisseur rentrée dans AFFE_CARA_ELEM
   alors que cette valeur ne joue pas sur la raideur normalement. 
   
   - question 2 : on réalise un second test où on compare les résultats ELAS_COQUE versus ELAS. Le test est tel que les raideurs de
   flexion sont les mêmes mais pas les raideurs de membrane. Normalement, les valeurs de moments doivent être égales. Ce n'est pas ce
   qui est observé en utilisant FORC_NODA. 
   
   Réponses : Il n y'a pas d'anomalie
   =========
   - réponse à la question 1 : 
   Dans tous les cas le déplacement ne devrait pas être influencé par la valeur entrée dans AFFE_CARA_ELEM. C'est bien le cas en
   considérant le cas-test ssls27a. 
   
   Pour ELAS_COQUE, FORC_NODA qui fait une intégration dans l'épaisseur des contraintes calculées aux sous points n'a pas de sens
   puisqu'il s'agit d'un comportement homogéineisé. Il faut plutôt EFGE_NOEU (=[Kutilisateur]{U}) qui n'est pas dépendant de la valeur
   de l'épaisseur et doit être le champ à privilégier dans ce cas précis. 
   
   Attention : FORC_NODA est calculé dans le repère global tandis que EFGE_NOEU est calculé dans le repère intrinsèque de l'élément. Il
   y a aussi une possible source d'erreurs avec EFGE_NOEU lorsqu'on utilise ELAS_COQUE. code_aster ne contrôle pas que les valeurs de
   raideurs de membrane et de flexion rentrées par l'utilisateur. 
   
   Rappel du calcul de FORC_NODA :
   1. Bcle sur les PDG
      2. bcle sur les couches :
   !
   !         -- CALCUL DES EFFORTS GENERALISES DANS L'EPAISSEUR (N, M ET T)
   !         --------------------------------------------------------------
                   hic = epaisseur/nbcou
                   zic = position du sous-point dan l'épaisseur
                   coef = poids du point d'intégration
                   coehsd = coef*hic/2.d0
                   n(1) = n(1) + coehsd*cont(icpg+1)
                   n(2) = n(2) + coehsd*cont(icpg+2)
                   n(3) = n(3) + coehsd*cont(icpg+4)
                   m(1) = m(1) + coehsd*zic*cont(icpg+1)
                   m(2) = m(2) + coehsd*zic*cont(icpg+2)
                   m(3) = m(3) + coehsd*zic*cont(icpg+4)
                   t(1) = t(1) + coehsd*cont(icpg+5)
                   t(2) = t(2) + coehsd*cont(icpg+6)
   !
   FORC_NODA n'est pas bugé à proprement parlé. Il n'est juste pas exploitable dans le cas des matrices de raideurs entrées à la main
   par l'utilisateur. 
   
   --> Action documentaire. 
   
   - réponse à la question 2 : 
   Pour être plus rigoureux, on fait un calcul comparatif ELAS_COQUE Vs ELAS. Les matrices de rigidité membrane+flexion sont identiques
   dans les deux cas. Et les valeurs de l'épaisseur dans AFFE_CARA_ELEM sont identiques. On observe que les valeurs de DEPL, FORC_NODA
   et EFGE_NOEU sont identiques dans les deux cas. Cela montre bien qu'ELAS_COQUE n'est pas buggé. 
   
   - Il y a une alarme incompréhensible qu'il faut  supprimer :
   
      !------------------------------------------------------------------------------------------------------------------------!
      ! <A> <ELEMENTS_93>                                                                                                      !
      !                                                                                                                        !
      !   Avertissement :                                                                                                      !
      !   Le comportement  ELAS_COQUE que vous utilisez les propriétés matériaux (membrane, flexion) dans le repère            !
      !   intrinsèque de la coque. Vous devez donc vous assurez des valeurs entrées sinon utilisez ELAS_ORTH et DEFI_COMPOSITE !
      !   s'il s'agit d'une coque multi couche.                                                                                !
      !                                                                                                                        !
      !                                                                                                                        !
      ! Ceci est une alarme. Si vous ne comprenez pas le sens de cette                                                         !
      ! alarme, vous pouvez obtenir des résultats inattendus !                                                                 !
      !------------------------------------------------------------------------------------------------------------------------!
      
   Restituion :
   ============
   
   Enrichir les docs d'utilisation : 
   - AFFE_CARA_ELEM : préciser que la valeur d'épaisseur ne contribue pas à la raideur.[U4.42.01]
   - CALC_CHAMP : précisions supplémentaires sur le sens de FORC_NODA/ELAS_COQUE. [U4.81.04]
   - Supprimer l'alarme ELEMENTS_93.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.81.04, U4.42.01
VALIDATION
    submit
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26463 DU 14/05/2017
AUTEUR : BERRO Hassan
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.3.18-bf89094912f0, le cas test sdll134a est NOOK sur AthosDev_Valid et Eole_valid
FONCTIONNALITE
   Problème
   ========
   
   Cas test GeviBus (base validation) sdll134a en NOOK pour un calcul de la vitesse d'entrainement 
   et le rapport d'instabilité selon la méthode Connors.
   
   Analyse et correction
   =====================
   
   Il s'agit bien d'une erreur de "re"programmation de la routine connor.F90 suite à issue26380.
   
   Il y a une parenthèse qui est oubliée dans une boucle d’intégration pour le calcul de la 
   vitesse d'entrainement du tube (méthode Connors Gevibus)
   
   La correction est simple :
   
   - . a = 0.5d0*r8pi()*(di**2) * drho_i + &
   - . . . correl*(de**2)*drho_e/(2*dx)
   
   + . a = (0.5d0*r8pi()*(di**2) * drho_i + &
   + . . . correl*(de**2)*drho_e)/(2*dx)
   
   Le test sdll134a redevient OK.
   
   J'en profite pour supprimer des /print/ qui persistaient dans la routine
   
   *************************************
   NOTE : correction à reporter en v12 
   *************************************
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdll134a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26604 DU 14/06/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Confusion des répertoires outils entre plusieurs versions
FONCTIONNALITE
   Problème
   --------
   
   Le répertoire 'outils' devient compliqué à gérer de manière automatique.
   Si on veut utiliser le même répertoire pour plusieurs versions, les scripts de lancement doivent être retouchés à la main pour
   aiguiller entre les différentes versions.
   
   
   Correction
   ----------
   
   On propose ici de récupérer le chemin vers les outils lors de l'étape de 'waf configure' et de les appeler ensuite directement sans
   passer par 'outils'.
   
   - Le script waf (data/wscript) cherche dans le $PATH. Pour la plupart des outils, on cherche en priorité dans le répertoire
   "attendu" dans les prérequis. Exemple: on cherche 'gpmetis' en priorité dans '$METISDIR/bin'.
   Afin d'éviter le répertoire outils existant actuellement, on fait une première recherche en retirant outils du $PATH.
   
   - L'utilitaire qui permet de récupérer le chemin d'un outil est 'get_option()' qui permet déjà d'obtenir la valeur d'une option ou
   une information générale (version, machine...).
   Par exemple, en Python:
   
       aster_core.get_option("prog:gmsh")
   
   En fortran:
   
       call gtoptk('prog:gpmetis', jnom(1), iret)
   
   - Il est possible de forcer le support d'un programme en faisant, par exemple :
       waf configure --with-prog-metis
   
   Dans ce cas, si le(s) programme(s) associé(s) à metis (ici uniquement gpmetis en v13) ne sont pas trouvés, le configure s'arrête en
   erreur. Voir 'waf --help' pour la liste des options '--with-prog-xxx'.
   
   La syntaxe en Python dans les fichiers de configuration est: self.options.with_prog_metis = True
   Pour désactiver cette exigence, on peut mettre: self.options.with_prog_metis = False
   
   On ajoute ces options dans les fichiers de configurations des machines officielles (via un script unique wafcfg/official_programs.py).
   SALOME et Europlexus ne sont exigés que sur les serveurs aster5, athosdev et eole.
   
   
   Détails à régler
   ----------------
   
   Actuellement, Ecrevisse 3.2.2 n'est pas dans les prérequis. On trouve donc la version 3.2.1. Les tests vont échouer.
   
   Les scripts salome et europlexus sont actuellement trouvés dans l'ancien répertoire 'outils'.
   
   
   Choix du chemin
   ---------------
   
   Exemple pour homard : Il est cherché par défaut dans $HOMARD_ASTER_ROOT_DIR/ASTER_HOMARD + $PATH.
   
   Remarque : Le répertoire d'installation de Homard fournit un script homard au plus haut niveau dédié au fonctionnement dans outils.
   C'est pour cela qu'on ajoute ASTER_HOMARD/.
   
   Si HOMARD_ASTER_ROOT_DIR n'est pas défini ou pointe vers un répertoire inexistant, il suffit d'enrichir la variable d'environnement
   PATH pour choisir un autre script 'homard'.
   Si HOMARD_ASTER_ROOT_DIR est défini et que $HOMARD_ASTER_ROOT_DIR/ASTER_HOMARD/homard existe, il faut écraser la valeur de
   HOMARD_ASTER_ROOT_DIR pour choisir un autre script.
   
   Pour les développeurs, voir data/wscript pour savoir quelles variables sont utilisées en priorité.
   
   C'est le chemin complet qui est stocké. L'éventuelle modification de variable d'environnement n'est nécessaire que le temps de 'waf
   configure'.
   
   
   
   Fichier contenant les informations
   ----------------------------------
   
   Le chemin des programmes trouvés par 'waf configure' sont stockés dans un fichier au format JSON: share/aster/external_programs.js
   Ce fichier peut-être modifié en post-installation.
   
   
   Impact Salome-Meca
   ------------------
   
   L'ajout de outils dans codeaster-frontend ne sert plus.
   
   En post-installation, modifier le chemin des outils pour ceux pris dans l'installation dans le fichier
   `share/aster/external_programs.js`.
   
   
   Report en branche v12.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tous les tests
NB_JOURS_TRAV  : 2.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26565 DU 08/06/2017
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Correction des impressions MED aux sous-points pour compatibilité avec SALOME 8.3.0
FONCTIONNALITE
   Problème
   ========
   
   La fiche PLEIADE 7075 a permis la visualisation aux sous-points des éléments de structure dans PARAVIS via le format MED.
   
   Dans le cadre d'une réunion organisée le 23/02 (cf. issue26562), une proposition de modification de la spécification de ce
   développement a été faite.
   Celle-ci n'a pas pu être intégrée dans SALOME 8.3.0 mais a toutefois fait l'objet de la restitution idoine dans code_aster
   (issue26107, issue26296, issue26361).
   
   Réalisation
   ===========
   
   Afin que la visualisation aux sous-points fonctionne dans Salome-Meca 2017 s'appuyant sur SALOME 8.3.0, il faut faire un backout sur
   les révisions incriminées.
   
   On retire donc les révisions :
   - 254da52cb0aa correspondant à issue26361
   - 7e7f8baf42ec correspondant à issue26107
   
   Vérification
   ============
   
   On vérifie dans la recette graphique la bonne lecture dans ParaViS des résultats MED aux sous-points pour :
   - poutres : OK
   - plaques : OK
   - tuyaux : NOOK, erreur à la lecture
   
   ==> on bloque l'impression aux sous-points pour les tuyaux dans l'attente de la correction côté SALOME (issue26562, PLEIADE 14873).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u7.05.21
VALIDATION
    visuelle
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26517 DU 01/06/2017
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Définitions de TRIAX incohérentes dans la doc de CALC_CHAMP
FONCTIONNALITE
   Problème
   ========
   
   '''
   Dans la doc sur les critères SIEQ_XXXX, page 23, il est indiqué : "TRIAX = TRSIG / VMIS". 
   Il manque un facteur 1/3.
   Ce n'est pas cohérent avec la définition de la page 21...
   
   De plus, en en observant les champs et en bricolant avec la calculette PARAVIS, il semble que ce n'est pas cohérent non plus avec ce
   que fait le code, et que ce facteur 1/3 est bien pris en compte dans le calcul de TRIAX.
   
   La doc est peut-être à faire évoluer sur ce point.
   '''
   
   Analyse
   =======
   
   * Lors du calcul de SIEQ_ELGA/SIEQ_ELNO, le taux de tri-axialité est bien calculé par le code comme :
   TRIAX = TRSIG / 3 / VMIS
   
   ==> on corrige la documentation de CALC_CHAMP sur ce point
   
   * En relisant la routine qui calcule les composantes de SIEQ_ELGA/SIEQ_ELNO, j'avais cru par erreur que la trace des contraintes
   était mal calculée pour un tenseur des contraintes 1D (éléments multifibres POU_D_EM et POU_D_TGM). Il n'en est finalement rien.
   
   ==> on fait toutefois du refactoring dans les routines de calcul des équivalents et on teste le taux de triaxialité pour les poutres
   multifibres, qui sont un cas particulier, dans SSLL11F.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u4.81.04,v3.01.011
VALIDATION
    ssll11f
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26522 DU 01/06/2017
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    En version 13.3.21-88f6cd1ead31, le cas test ssnv206e est nook sur Eole
FONCTIONNALITE
   Problème
   ========
   
   En version 13.3.21-88f6cd1ead31, le cas test ssnv206e est nook sur Eole :
   
    
    ---- RESULTAT         NUME_ORDRE       NOM_CHAM         NOM_CMP          NOEUD
         U2               52               DEPL             DX               N4
         REFERENCE        LEGENDE          VALE_REFE          VALE_CALC          ERREUR           TOLE            
   NOOK  NON_REGRESSION   XXXX             0.0299427586642    0.0299421383808     2.071564E-03%   0.002%
   
   Analyse
   =======
   
   Ce cas-test utilisant 'LETK' est instable : 
   - il plante en "debug" (floating point exception) ;
   - il ne convergence pas quand il est exécuté sous Valgrind.
   
   Il présente déjà des TOLE_MACHINE pour les valeurs NOOK.
   
   Action
   ======
   
   Compte-tenu des éléments ci-dessus, on augmente TOLE_MACHINE pour assurer le passage sur EOLE.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssnv206e
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26588 DU 12/06/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    Utilisation des versions stabilisées d'Europlexus dans les versions stabilisées de code_aster
FONCTIONNALITE
   Avant stabilisation, on bascule la version d'EUROPLEXUS utilisée par défaut :
   
   En v13, le catalogue est modifié pour positionner la valeur à EPX2017. 
   
   Le test plexu13a qui utilise un développement plus récent et qui fait appel à la version DEV (VERSION_EUROPLEXUS = 'DEV') ne pourra
   pas fonctionner correctement. On positionne le mot clé VERSION_EUROPLEXUS à 'DEV' dans les 2 autres appels à CALC_EUROPLEXUS
   Avec VERSION_EUROPLEXUS = 'EPX2017', les tests suivants sont NOOK 
   2 ('NOOK', 'XXXX', '0.00250230296487', '0.00124294734011', '50.3278636697%', '0.0001%')
   3 ('NOOK', 'XXXX', '-16.46051998', '-7.88579129622', '52.0926963074%', '0.0001%')
   
   
   En v12, le catalogue actuel utilise par défaut la version EPX2016p1, il n'y a donc pas de modification à effectuer.
   
   Le script /home/rd-ap-simumeca/outils/europlexus est modifié pour prendre en compte les versions disponibles sur Athosdev (et Eole).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 13.3.5 plexu13a utilise VERSION_EUROPLEXUS = 'DEV'
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    Tests Europlexus
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26621 DU 20/06/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Construire code_aster dans Salome-Meca sans avoir à modifier le répertoire des sources
FONCTIONNALITE
   Problème
   --------
   
   Aujourd'hui, il est nécessaire de modifier le répertoire des sources pour construire code_aster dans Salome-Meca.
   
   - datg et materiau pour aller pointer vers un dépôt qui n'est pas un niveau au dessus,
   
   - alarme à cause des traces de construction laissées par yamm.
   
   
   Correction
   ----------
   
   Le chemin vers les dépôts data et validation sont maintenant paramétrables en ligne de commande en utilisant respectivement les
   options --with-data=xxxx et --with-validation=yyyy.
   Par défaut, les chemins sont ../data et ../validation. Il n'y a donc aucun impact pour les constructions existantes.
   
   Pour la construction dans Salome-Meca par Yamm, il suffit donc d'ajouter lors du configure quelque chose comme
   --with-data=${CODE_ASTER_DATA_TESTING_INSTALL_DIR}.
   
   Le répertoire des logs de Yamm est ignoré.
   
   
   Complément : Ajout de ops.py pour AsterStudy dans cette fiche (corrige le problème d'import du catalogue avec la dernière unstable,
   depuis issue26553).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    build
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26609 DU 19/06/2017
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    En version 13.3.23-2aa19caee3cc le cas test fdlv112a est nook sur clap0f0q
FONCTIONNALITE
   Problème
   ========
   
   En version 13.3.23-2aa19caee3cc le cas test fdlv112a est NOOK sur clap0f0q.
   
   Analyse
   =======
   
   Il s'avère que ce cas-test est instable (OK en debug, NOOK en nodebug sur cette plateforme).
   
   Correction
   ==========
   
   Les valeurs calculées de non-régression ne sont plus celles du serveur de référence : en mettant à jour les VALE_CALC avec les
   valeurs calculées sur Athosdev (où le test est OK), on retrouve une variabilité machine inférieure à 1.0E-6.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    fdlv112a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26485 DU 22/05/2017
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Projection de matrice non symétrique sur base modale
FONCTIONNALITE
   Problème
   ========
   
   '''
   On n'obtient pas la même réponse vibratoire lorsqu'on réalise un calcul harmonique sur base physique et sur base modale même lorsque
   celle-ci contient un nombre de modes égal au nombre de degrés de liberté actifs.
   
   Il est apparu après analyse que :
    - ce problème est lié à la présence de la gyroscopie ou de matrices non symétriques ;
    - pour avoir le même résultat, il faut mettre (dans DYNA_VIBRA) la phase en +j et en non en -j ;
    - le calcul modal sur base modale inverse les modes directs et rétrogrades (par rapport à un calcul sur base physique) ;
    - d'autres analyses suggèrent que le problème vient de la projection d'une matrice non symétrique sur base modale.
   '''
   
   Analyse
   =======
   
   1. Erreur dans la projection des matrices non symétriques
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   La vérification de la programmation de PROJ_MATR_BASE pour les matrices non symétriques montre que le produit tP.A.P (P
   matrice rectangle des modes, A matrice à projeter) est erroné : les termes triangulaires supérieurs sont stockés dans le bloc 2 de
   la matrice - au format morse - au lieu du bloc 1 (et inversement).
   
   ==> c'est une conséquence de issue23534. La correction revient à en annuler les modifications, ce qui casse les trois cas-tests que
   cette fiche a corrigé à l'époque : sdll126b, sdll144a, sdll144b (dynamique transitoire sur base modale avec gyroscopie).
   
   2. Erreur dans le stockage des matrices pleines dans DYNA_VIBRA sur base modale
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   Il existe une autre erreur dans le code dans le stockage des matrices pleines utilisées pour les calculs sur base modale :
   - lorsque l'on extrait ces matrices à partir des concepts Aster, on les stocke dans un vecteur, **en ligne** ("row-major order"),
   cf. copmat.F90 ;
   - dans les routines d'intégration temporelle, ces matrices sont passées à des routines utilitaires (triangulation, produit
   matrice-vecteur) qui font l'hypothèse qu'elles sont stockées suivant la convention Fortran, c'est à dire en **en colonne**
   ("column-major order).
   
   ==> c'est une conséquence de issue23499. La correction permet de corriger les trois cas-tests cassés pré-cités.
   
   3. Homogénéisation des accès aux matrices pleins dans DYNA_VIBRA sur base modale
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   Historiquement, DYNA_VIBRA - et avant DYNA_TRAN_MODAL - ont toujours stocké les matrices pleines en ligne et y ont accédé ainsi. La
   correction précédente a donc pour conséquence d'introduire une disparité dans le stockage et l'accès au sein de DYNA_VIBRA.
   Cela n'a a priori pas de conséquence sur les résultats car les manipulations purement internes à DYNA_VIBRA (hors utilitaires de
   triangulation et produit matrice-vecteur) ne font que des combinaisons linéaires de matrices pour lesquelles l'accès n'a pas
   d'importance, seules les opérations comptent.
   
   ==> à des fins de lisibilité, on homogénéise toutefois l'accès à toutes les matrices dans DYNA_VIBRA en préférant le format en
   colonne comme en Fortran ("column-major order").
   
   Vérification
   ============
   
   Matrice non symétrique sur base modale en dynamique harmonique
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   L'équipe machines tournantes m'a gentiment construit un cas-test basé sur sdll126d (réponse harmonique avec prise en compte de la
   gyroscopie sur base physique). Ce nouveau cas-test - sdll126e - calcule la réponse harmonique avec prise en compte de la gyroscopie
   sur base modale. Les résultats testés sont les mêmes que ceux de sdll126d.
   Ce cas-test permet de vérifier que la projection de la matrice gyroscopique sur base modale est correcte, ce qui n'était fait dans
   aucun des cas-tests existants. Les résultats de sdll126e sont OK à partir des modifications apportées par 1. plus haut et NOOK avant.
   
   Le cas-test présente toutefois une variabilité machine non négligeable qui nécessite d'ajouter des TOLE_MACHINE. Je choisis de le
   faire néanmoins et je propose que la variabilité de ce cas-test soit analysée dans une fiche dédiée.
   
   Matrice non symétrique sur base modale en dynamique transitoire
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   Les cas-tests sdll126b, sdll144a et sdll144b dont la mise en données a été contrôlée par l'équipe machines tournantes restent OK
   avec toutes les corrections.
   
   Changement du mode d'accès des matrices
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   Tous les cas-tests utilisant DYNA_VIBRA sont OK après les modifications.
   
   Performances
   ============
   
   Les modifications apportées dans cette fiche pourraient avoir un léger impact sur les performances, à confirmer par le suivi
   hebdomadaire. En effet dans le cas de matrices pleines (présence d'amortissement), l'accès aux matrices n'est désormais plus optimal
   lors de l'intégration temporelle (boucle sur lignes puis colonnes alors que les matrices sont stockées en colonne).
   
   Résultats faux
   ==============
   
   Il est difficile d'estimer s'il y a pu avoir des résultats faux avant issue23499/issue23534 : on accédait aux matrices pleines avec
   la mauvaise convention (stockage en colonnes mais accès en lignes) mais compte-tenu des opérations effectuées (combinaisons
   linéaires), il se peut que cela n'ait pas eu d'influence.
   
   En tout état de cause, il y a des résultats faux pour les calculs sur base modale en dynamique harmonique
   (DYNA_VIBRA/TYPE_CALCUL='HARM'/BASE_CALCUL='GENE') avec présence de gyroscopie ou d'une matrice d'amortissement non symétrique et ce
   dans les versions 11.8/12.4/13.0 et ultérieures.
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 12.4.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 13.0.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : v2.02.126
VALIDATION
    sdll126e,tous tests DYNA_VIBRA
NB_JOURS_TRAV  : 5.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26608 DU 19/06/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    En version 13.3.23-2aa19caee3cc le cas test erreu09b échoue sur AthosDev
FONCTIONNALITE
   Problème
   --------
   
   Le test erreu09b s'arrête parfois en CPU Limit sur Athosdev.
   
   
   Correction
   ----------
   
   Ce test sert à vérifier que STAT_NON_LINE détecte le manque de temps et lève dans ce cas l'exception `aster.ArretCPUError`.
   
   STAT_NON_LINE s'arrête quand il estime qu'il n'y a pas assez de temps pour faire un pas de plus. Or comme les pas de temps sont très
   courts dans ce test, il se peut qu'il n'y ait plus assez de temps pour exécuter les deux commandes suivantes CREA_TABLE et TEST_TABLE !
   
   Pour éviter de tomber aléatoirement sur ce problème, juste avant l'appel à STAT_NON_LINE, on diminue le temps maxi à 7 secondes.
   STAT_NON_LINE lève l'exception ArretCPUError au bout de 7 secondes. Puis, on remet la limite en temps à sa valeur initiale. On a
   donc assez de temps pour faire terminer le test correctement.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    erreu09b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26537 DU 02/06/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    A101.17 - Fichier obligatoire non justifié dans la commande FIN
FONCTIONNALITE
   On bascule le mot-clé UNITE de FIN en statut 'C' (caché) ce qui a pour effet de ne plus le faire apparaître dans AsterStudy.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    adlv100a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26372 DU 11/04/2017
AUTEUR : BILLON Astrid
TYPE aide utilisation concernant Code_Aster (VERSION 13.2)
TITRE
    Utilisation loi anisotrope cylindrique dans un repère cartésien
FONCTIONNALITE
   Bonjour,
   
   Voici quelques éléments de réponse : 
   
   J'ai essayé de reproduire votre problème à l'aide d'une loi de comportement anisotrope plus simple (ELAS_ORTH). Dans un 
   premier temps, on introduit de l'anisotropie seulement en prenant un module d'Young différent dans l'une des directions.
   On trouve alors bien une ovalisation du tube et des contraintes uniformes dans l'épaisseur du tube [erratum].
   Ensuite, j'applique votre procédure de réorientation, qui fonctionne bien puisqu'elle permet de conserver l'axisymétrie du 
   tube et des contraintes uniformes dans la section.
   Attention cependant à l'ordre des composantes des vecteurs de la base locale calculées dans la fonction ProjAxe. J'ai dû faire 
   une permutation des composantes pour obtenir le résultat recherché.
   
   Dans un second temps, on introduit également de l'anisotropie au niveau des coefficients de Poisson, (comme c'est le cas dans 
   votre matrice de Hill il me semble). On constate qu'il n'est alors plus possible de retrouver des contraintes uniformes avec 
   votre procédure.
   Je dirais donc que ces contraintes sont physiques. 
   
   En PJ l'étude avec le matériau ELAS_ORTH, afin que vous puissiez visualiser ces phénomènes (avec un maillage modifié car celui 
   que vous aviez joint n'était pas bon (axe du cylindre selon z).
   
   Par précaution, je vous suggèrerais enfin de vérifier la définition de la matrice de Hill de votre loi de comportement afin de 
   s'assurer qu'elle est bien en cohérence avec la géométrie que vous cherchez à modéliser. 
   
   Cordialement,
   AB
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    src
NB_JOURS_TRAV  : 4.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26580 DU 11/06/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.3.22, les tests zzzz218*, zzzz354* et zzzz355 échouent sur Calibre9
FONCTIONNALITE
   Le script permettant d'aiguiller sur la bonne version d'Ecrevisse a été rétabli. Les tests fonctionnent de nouveau. Il est envisagé
   de modifier la procédure d'installation pour détecter directement la version à utiliser dans le fichier de configuration wafcfg et
   de pointer sur l'exécutable nécessaire.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz218*, zzzz354* et zzzz355
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26583 DU 11/06/2017
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 12.7.10-f3be860c2484, le cas test sdll134b est NOOK sur Athosdev
FONCTIONNALITE
   Résolue par issue26463.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdll134b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26567 DU 08/06/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version : version 13.3.21-88f6cd1ead31 les cas tests zzzz151a et icare01b échouent sur Eole et Eole_valid
FONCTIONNALITE
   L'installation de SalomeMeca 2016 version Station Calibre9 sur EOLE permet de faire fonctionner correctement l'appel à salome dans
   les tests zzzz151a et icare01b.
   
   Salome est installé sous /projets/simumeca/salome_meca/appli_V2016/salome, un lien est créé dans
   /projets/simumeca/public/default/tools/Code_aster_frontend-salomemeca/outils.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz151a et icare01b
NB_JOURS_TRAV  : 0.5

