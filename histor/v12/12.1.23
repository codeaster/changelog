==================================================================
Version 12.1.23 (révision 40344b84f7bb) du 2014-06-10 16:19 +0200
==================================================================

--------------------------------------------------------------------------------
--- AUTEUR geniaut      GENIAUT Samuel      

--------------------------------------------------------------------------------
RESTITUTION FICHE 021670 DU 2013-10-22 15:33:32
TYPE evolution concernant Code_Aster (VERSION )
TMA : Phimeca
TITRE
    MAC3COEUR : outil-mxc3xa9tier de creation de maillage
FONCTIONNALITE
   Contexte :
   ----------
   
   L’outil MAC3 est un ensemble de macro-commandes de Code_Aster permettant la simulation de
   la déformation des assemblages combustibles dans un cœur complet, lors des cycles
   d’irradiation.
   Le maillage du cœur est une donnée d’entrée des macro-commandes et doit être généré au
   préalable. Pour cela, un script python pour Salomé a été créé. Ce script s’appuie sur une
   procédure standardisé et des données géométriques propres à chaque cas d’application
   (palier 900, 1300, N4, ou autre type de cœur). 
   
   
   Problématique actuelle :
   ------------------------
   
   Les problèmes engendrés par cette technique de création de maillage sont doubles :
   - il est difficile de gérer en configuration le script python de maillage, qui doit
   évoluer avec les versions de Salomé, et dont l’archivage/stockage n’est pas assuré 
   - certaines données géométriques demandées par le script sont redondantes avec des données
   disponibles dans les sources de MAC3 (répertoire data/datg)
   
   
   Objectif de la fiche :
   ----------------------
   
   Pour résoudre ces problèmes, il est envisagé la création d’un outil-métier de création
   d’un maillage d’un cœur complet. 
   
   Détails :
   L’outil-métier sera intégré au sein de Salome-Meca, sous la forme d’un plugin (comme
   bride, CT). Il s’appuiera sur le script python de maillage actuel. Les données utilisateur
   seront simplifiées au maximum en exploitant celles disponibles dans le catalogue datg
   d’Aster. Les fichiers correspondant au script actuel seront fournis en début de prestation.
   
   
   Réalisation :
   -------------
   
   Un plugin de Salome-Meca a été créé. Il dispose d'une IHM très simple permettant :
   - de choisir le type de Coeur : 900, 1300, N4, MONO, TEST (3x3)
   - de renseigner le nom du fichier qui contient la description des types de conceptions
   d'assemblages en chaque position (comme c'est le cas actuellement). Ce fichier python doit
   contenir un dictionnaire 'dico' dont :
   ➢ Les clés sont les noms des positions d'assemblage
   ➢ Les valeurs sont les noms des types de conception ('AFA3GL'ou'AFAXL').
   
   Un certain nombre de vérifications sont alors faites (compatibilité entre le type de Coeur
   et le fichier fourni...).
   
   Le maillage est ensuite automatique créé (cette étape nécessite environ 2 minutes sur
   poste Calibre) et il apparaît dans arborescence Salomé.
   
   Informatiquement, le script de maillage a été repris, de manière à aller chercher dans les
   répertoires sources d'Aster (répertoire datg) les données géométriques nécessaires au
   maillage.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : sv1.03.01
VALIDATION
    perso
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR courtois     COURTOIS Mathieu    

--------------------------------------------------------------------------------
RESTITUTION FICHE 022709 DU 2014-06-05 07:51:31
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Erreur dans le calcul de la masse d'un xc3xa9lxc3xa9ment de poutre tronconique creux
FONCTIONNALITE
   Problème
   ~~~~~~~~
   
   Quand on définit une section de poutre avec le mot-clé VARI_SECT='HOMOTHETIQUE', on peut
   renseigner le rayon et l'épaisseur à l'entrée (R1, EP1) et le rayon et l'épaisseur à la
   sortie (R2, EP2).
   Cependant, il n'y a pas de vérification de ces paramètres. Or le modèle n'est valable
   uniquement si le rapport d'homothétie est respecté : R2/R1=EP2/EP1.
   
   
   Analyse et correction
   ~~~~~~~~~~~~~~~~~~~~~
   
   - Le modèle actuel pour les poutres à section variable creuse est valide uniquement pour
   une variation de section purement homothétique (rapport R2/R1 = EP2/EP1).
   
   - La documentation r3.08.01 est claire. Le paragraphe 5.2 précise comment la section varie
   le long de l'abscisse curviligne (équation 5.2-1). Elle pourrait néanmoins mieux préciser
   que "homothétique" doit être pris au sens strict. La programmation est cohérente avec
   cette documentation.
   
   - En revanche, la doc u4.42.01 n'est pas assez claire au paragraphe 9.4.5. Il faut
   préciser que le rapport R2/R1 soit être égal au rapport EP2/EP1.
   Dans le cas contraire, on obtient une réponse approchée qui respecte la variation décrite
   dans la documentation r3.08.01.
   On peut voir que la variation de section programmée conduit à un "amincissement"
   artificiel au centre (voir les 2 images jointes).
   
   - Cette hypothèse impacte le calcul de la masse en elle-même (MASS_INER) mais également le
   calcul des matrices de masse, de rigidité (traction, compression, flexion, torsion...), du
   second membre (pesanteur, charges réparties...), de la matrice de gyroscopie...
   
   - Il faut donc ajouter une alarme dans AFFE_CARA_ELEM si R2/R1 != EP2/EP1 (à un epsilon)
   pour prévenir que l'on sort du cadre du modèle.
   
   - On pourrait utiliser un modèle 3D pour ces éléments et un raccord 3D-poutre.
   Dans le cadre des calculs réalisés par l'outil MT, cela risque d'être coûteux.
   
   - Dans l'outil MT, il faut utiliser le raffinement pour définir plusieurs éléments poutre
   pour ces éléments à section variable (voir données chiffrées plus bas).
   
   
   Dans le cadre de cette fiche, on ajoute l'alarme dans AFFE_CARA_ELEM lors de la saisie des
   données.
   
   !----------------------------------------------------------------------------------!
   ! <A> <POUTRE0_4> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! Poutre circulaire à variation de section homothétique. . . . . . . . . . . . . . !
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! Le rapport d'homothétie est assez différent entre les rayons et les épaisseurs : !
   ! . . - rayon 1 = . . . . .0.09625 . . . . . . . . . . . . . . . . . . . . . . . . !
   ! . . - rayon 2 = . . . . . .0.095 . . . . . . . . . . . . . . . . . . . . . . . . !
   ! . . . . `- rapport d'homothétie = . . .0.987012987 . . . . . . . . . . . . . . . !
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! . . - épaisseur 1 = . . . . . . 0.02 . . . . . . . . . . . . . . . . . . . . . . !
   ! . . - épaisseur 2 = . . . . . . 0.02 . . . . . . . . . . . . . . . . . . . . . . !
   ! . . . . `- rapport d'homothétie = . . . . . . . .1 . . . . . . . . . . . . . . . !
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! La différence entre les rapports d'homothétie est supérieure à 1%. . . . . . . . !
   ! Les hypothèses du modèle de poutre à variation homothétique ne sont donc pas . . !
   ! respectées (consultez la documentation de référence des éléments poutre). . . . .!
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! Risques et conseil: . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! . . - Les calculs seront inexacts . . . . . . . . . . . . . . . . . . . . . . . .!
   ! . . - Raffiner le maillage permet de minimiser les écarts. . . . . . . . . . . . !
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! Ceci est une alarme. Si vous ne comprenez pas le sens de cette . . . . . . . . . !
   ! alarme, vous pouvez obtenir des résultats inattendus ! . . . . . . . . . . . . . !
   !----------------------------------------------------------------------------------!
   
   
   
   On ajoutera après la stabilisation un cas-test qui montre les différences obtenues entre
   1, 2 et 5 éléments de poutre (cas choisi: E57_HP).
   Les résultats obtenus par Code_Aster seront confrontés à une structure équivalente
   dans Cadyro.
   
   Le cas où les caractéristiques géométriques de la poutre varient linéairement selon
   l'abscisse curviligne est différent des cas existants actuellement ('AFFINE' et
   'HOMOTHETIQUE'). Il pourra être traité par le projet DECOLLE dans le cas de issue19864 ou
   d'une autre fiche à émettre.
   Ce n'est pas immédiat car il faut établir l'expression mathématique des matrices citées
   précédemment, et le valider par rapport à des solutions analytiques en statique (flèche,
   moment, réaction...), dynamique (modes et fréquences propres), rotation (gyroscopie).
   
   
   Exemple numérique sur l'élément E57_HP
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   - dimensions :
     - longueur = 15,5 cm,
     - diamètre externe = 1,5 m, 
     - diamètres externes = 0,98 m et 1,235 m
     - masse = 966,66 kg
   
   - avec 1 élément, on obtient une masse de 943,58 kg (2,4 % d'écart)
   
   - avec 2 éléments, on obtient une masse de 960,84 kg (0,6 % d'écart)
   
   - avec 5 éléments, on obtient une masse de 965,73 kg (moins de 0,1 % d'écart)
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 10.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 12.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : r3.08.01, u4.42.01, v2.02.127
VALIDATION
    sdll127a.com1
NB_JOURS_TRAV  : 3.0
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR bereux       BEREUX Natacha      

--------------------------------------------------------------------------------
RESTITUTION FICHE 022724 DU 2014-06-06 14:24:30
TYPE express concernant Code_Aster (VERSION 12.2)
TITRE
    Cas-tests petsc01d et petsc01f NOOK sur aster5_mpi
FONCTIONNALITE
   Problème
   ========
   Les cas-tests petsc01d et petsc01f sont NOOK sur aster5_mpi. 
   Les NOOK sont les valeurs de non-régression sur le déplacement DZ aux noeuds 2974 et 2958
   (voir issue22652 qui trace le problème).
   
   Analyse
   =======
   On renvoie aussi à issue22456 qui avait commencé à analyser le problème. 
   
   Ces cas-tests sont variables pour les valeurs incriminées (DZ aux noeuds 2974 et 2958),
   c'est à dire que des exécutions successives du cas-test donnent des résultats toujours
   proches de la valeur de référence mais pas suffisamment proches de la valeur de
   non-régression. 
   Voilà un exemple de valeurs obtenues pour DZ sur noeud 2974 (petsc01f):
   OK   NON_REGRESSION   XXXX             -2.9173573919E-05    -2.91743796954E-05  
   0.0027620079657% 0.021%
   OK   NON_REGRESSION   XXXX             -2.9173573919E-05    -2.91755910996E-05   
   6.914410E-03%   0.021%
   Il y a un écart relatif en 1.E-04. 
   
   Pour cet exemple, on a vérifié que les valeurs passées en entrée au solveur PETSc (appel
   de KSPSolve) sont bien strictement identiques d'une exécution à l'autre (matrice et second
   membre).
   En revanche, la solution calculée par PETSc est légèrement variable : écart relatif entre
   les deux solutions de 1.E-7 (toutes les deux vérifient le critère de convergence).
   
   Explication
   ===========
   On est dans un cas limite d'utilisation du solveur CR: en effet, une partie des
   chargements est définie via AFFE_CHAR_MECA (pour petsc01f comme pour petsc01d) et la
   matrice n'est pas définie positive. Or c'est le bon cadre d'emploi pour le gradient
   conjugué (CG). La méthode CR appartient à la même famille même si elle tolère des matrices
   indéfinies. 
   Si l'on change CR par GMRES, on n'observe plus cette variabilité. 
   
   Solution
   ========
   Les TEST_RESU qui conduisent aux résultats variables sont commentés (+ 1 commentaire
   explicatif)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    petsc01f petsc01d
NB_JOURS_TRAV  : 2.0
--------------------------------------------------------------------------------


