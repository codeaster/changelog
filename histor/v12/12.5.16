==================================================================
Version 12.5.16 (révision 8eaafa7d9f51) du 2016-06-03 08:15 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 24927 DU 11/03/2016
AUTEUR : BADEL Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    MAC3COEUR : erreur lecture THYC coeur_900
FONCTIONNALITE
   objectif
   ========
   une erreur s'est glissée dans la définition des coeurs 900, concernant la conversion du repère 
   THYC en repere MAC3 dans la routine position_fromthyc
   
   realisation
   ===========
   
   modification du Coeur_900.datg pour prise en compte du bon repère
   
   validation en attente de restitution des tests de cœur complet
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 12.4
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 12.4
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    perso
DEJA RESTITUE DANS : 13.1.15
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25254 DU 26/05/2016
AUTEUR : BADEL Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    MAC3 : ecart dans données assemblage
FONCTIONNALITE
   probleme
   ========
   écart relevé sur largeur de grilles intermédiaires dans RFA1300.datg
   
   correction
   ==========
   on corrige la liste de largeur de grille dans RFA1300.datg
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 12.4
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 13.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    perso
DEJA RESTITUE DANS : 13.1.21
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25189 DU 09/05/2016
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 12.6)
TITRE
    En version 12.5, le cas test ssnv194c échoue sur Calibre9
FONCTIONNALITE
   Problème :
   ----------
   En version 12.5, le cas test ssnv194c échoue sur Calibre9 avec un CORE_DUMP.
   
   
   Solution :
   ----------
   On reporte le correctif issue24686.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssnv194c
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 24695 DU 12/01/2016
AUTEUR : BERRO Hassan
TYPE anomalie concernant Code_Aster (VERSION 12.5)
TITRE
    C105.16 - En version 12.5.0-89460f50fb85, le cas test sdld325b est NOOK sur Calibre 9
FONCTIONNALITE
   Ce test compare différents schémas explicites d'intégration avec DYNA_VIBRA et se casse en version 12 sur calibre 9. 
   
   Les évolutions dans DYNA_VIBRA dans la version 13 ont permis de stabiliser les calculs transitoires par :
   - ré-écriture complète des algorithmes d'intégration des schémas disponibles
   - restructuration du code
   - améliorations sur la partie archivage pour les schémas adaptatifs.
   
   Il est donc pas envisageable d'apporter de solution stabilisant en v12 sans un gros effort qui serait la ré-écriture de l'opérateur.
   
   En conséquence, je propose de simplement rajouter un TOLE_MACHINE au test sdld325b.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdld325b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25248 DU 25/05/2016
AUTEUR : LEFEBVRE Jean-Pierre
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    Modification des fichiers de configuration v12 pour pointer sur Mfront_stable
FONCTIONNALITE
   Pour construire la version 2016 de SalomeMeca il faut pouvoir disposer des deux versions 2.0.2 et 2.0.3 de Mfront (stable et
   testing). YAMM ne pouvant gérer plusieurs versions d'un même produit, on introduit le "software" Mfront_stable pour pointer sur la
   version 2.0.2.
   Les pré-requis ont été installés sur les différentes plates-formes, on modifie les fichiers de configuration dans le répertoire
   wafcfg de la branche v12 des sources pour pointer sur la version renommée de Mfront 2.0.2.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tests mfront
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25236 DU 20/05/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 12.5)
TITRE
    Stanley ne contacte salome
FONCTIONNALITE
   Problème
   --------
   
   Au lancement de Stanley, la session SALOME n'est pas trouvée.
   Le mode GMSH est activé par défaut.
   
   
   Correction
   ----------
   
   À l'occasion de issue24556, on fait le ménage dans les options de la ligne de
   commande... un peu trop car ORBInitRef est encore nécessaire pour savoir sur
   quelle machine Stanley doit effectué la visualisation.
   
   Il y a donc deux modifications à annuler dans astk :
   - lire l'adresse de la session SALOME (valeur fournie au lancement par :
     `astk --from_salome NameService=corbaname::host:port`),
   - lors du passage d'argument à l'exécutable de code_aster :
     ne pas ignorer ORBInitRef.
   
   Dans code_aster, faire de ORBInitRef une "vraie" option, stockée dans l'objet
   CoreOptions, pour être interrogée proprement plus tard par Stanley (et non en
   allant lire `sys.argv`).
   On en profite pour indiquer clairement (avec des UTMESS) ce qu'on fait de
   la valeur de ORBInitRef. On ne dit pas "Salome détecté" mais :
   """
   Démarrage en mode SALOME sur la machine %(k1)s sur le port %(k2)s.
   Remarque : il est nécessaire que cette session soit en cours d'exécution.
   """
   
   Ou bien :
   """
   La session SALOME est inconnue (non disponible dans les arguments).
   Le mode GMSH va être activé.
   Utilisez le menu si vous souhaitez vous connecter à une session SALOME existante.
   """
   
   au lieu de "Salome non détecté" car il n'y a pas de détection d'existence de la
   session SALOME.
   
   De plus, le passage des options à l'exécutable code_aster a changé (on utilise
   les noms longs: --ORBInitRef=value).
   Il ne faut donc pas passer cette option aux anciennes versions (<12.6-13.2).
   Conséquence : avec astk de Salome-Meca 2016, on pourra exécuter les anciennes
   versions mais sans ORBInitRef, donc sans post-traitement avec Stanley dans
   SALOME.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    stanley
DEJA RESTITUE DANS : 13.1.21
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25253 DU 26/05/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 12.4)
TITRE
    Champ SIPM_ELNO non-récupérable dans CREA_CHAMP
FONCTIONNALITE
   Problème
   --------
   
   On ne peut pas extraire le champ SIPM_ELNO avec CREA_CHAMP.
   On tombe sur l'erreur :
   
    !  le mot-clé type_champ =  ELNO_SIEF_R n'est pas cohérent avec le type !
    !  du champ extrait :  ELNO_SIEFMX_R                                    !
   
   Si on renseigne TYPE_CHAMP='ELNO_SIEFMX_R', on a :
   
    ! La valeur : 'ELNO_SIEFMX_R'  ne fait pas partie des choix possibles !
   
   
   Correction
   ----------
   
   On ajoute la grandeur SIEFMX_R dans c_nom_grandeur.
   Cependant, le test sur la grandeur est mal fait dans chprec.
   On testait typchlu(6:12), ce qui donne pour ELNO_SIEFMX_R: 'ELNO_SIEFMX_'.
   
   On homogénéise tous les tests de ce type dans chprec.
   
   
   Validation
   ----------
   
   Dans ssll11g, on ajoute l'extraction du champ SIPM_ELNO.
   On teste SIXXMAX en A et SIXXMIN en B comme cela est déjà fait d'une autre manière.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssll11g
DEJA RESTITUE DANS : 13.1.21
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25192 DU 09/05/2016
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant Code_Aster (VERSION 12.6)
TITRE
    En version 12.5, le cas test zzzz268b est NOOK sur Calibre9
FONCTIONNALITE
   Problème
   ========
   
   '''
   En version 12.5.12, le cas test zzzz268b est NOOK sur Calibre9.
   Le cas test a été réparé 'tout seul' en version 13.1.14.
   Il est instable en version 12: NOOK en version 12.5.11, pas en version 12.5.9
   '''
   
   Analyse
   =======
   
   1. La comparaison des tableaux de convergence entre Athosdev et Calibre9 sur ce test montre que c'est le nombre d'itérations de
   Newton (plus précisément le résidu atteint à convergence) qui induit les très légers NOOK observés sur les grandeurs calculées par
   la commande POST_BORDET.
   
   2. La comparaison des calculs en mode "debug" de CALCUL montre que la différence provient de l'intégration de la loi de comportement
   et, après quelques impressions, plus précisément de la détection du franchissement du seuil plastique pour les lois à écrouissage
   isotrope.
   
   Quelques impressions montrent en effet que : 
   - sur Calibre9, le seuil est franchi dès la première itération (sigma_eq - rp ~= +5.0E-14)
   - sur Athosdev, ce n'est pas le cas (sigma_eq - rp ~= -5.0E-14)
   
   Correction
   ==========
   
   On modifie le test du franchissement du seuil plastique en traitant explicitement la quasi-égalité de la contrainte équivalente et
   du seuil plastique courant.
   Si les deux sont égaux en relatif à la précision machine près, on impose l'égalité.
   
   Validation
   ==========
   
   Le test est à nouveau OK sur toutes les plateformes en debug/nodebug et les tableaux de convergence sont strictement identiques.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz268b
DEJA RESTITUE DANS : 13.1.21
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24934 DU 14/03/2016
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    Problème d'utilisation d'un écrouissage linéaire avec Drucker-Prager non associé
FONCTIONNALITE
   Problème
   ========
   
   Dans la doc R de la loi Dricker-Prager en version non associée il est précisé que seul un écrouissage parabolique peut-être utilisé. Or 
   rien n'interdit l'utilisation de la version linéaire. Dans ce cas c'est un appel à la version associée de la loi qui se fait.
   
   Il faudrait bloquer du coup l'appel à la version linéaire dans le cas non-associé.
   
   Travail effectué :
   ================
   
   Dans la routine lcdrpr.F90, dans le cas de l'écrouissage linéaire (typedp = 1) on émet un message d'erreur (COMPOR5_7) si 
   compor(1).eq.'DRUCK_PRAG_N_A'.
   
   La validation de la bonne émission du message a été faite en modifiant le test ssnd104a.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    submit
DEJA RESTITUE DANS : 13.1.21

--------------------------------------------------------------------------------
RESTITUTION FICHE 25111 DU 18/04/2016
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.1.14-faf2e4504821, le cas test ssnp140c est NOOK sur Calibre9 et clap0f0q
FONCTIONNALITE
   Problème
   ========
   
   En version 13.1.14-faf2e4504821, le cas test ssnp140c est NOOK sur Calibre9 et clap0f0q
   apparemment suite à [#17384] Upgrade of METIS: from 4.0 to 5.1.0
   
   Solution
   ========
   
   Ce test vérifie ENDO_FRAGILE en mode IMPLEX. Le premier SNL fait du ENDO_FRAGILE en mode standard (Newton-Raphson), puis un SNL en
   mode IMPLEX.
   
   ENDO_FRAGILE est peu robuste (il doit être remplacé par ENDO_SCALAIRE, voir issue14675). En plus, il s'agit du mode local (D_PLAN)
   et pas de ses versions régularisées (GRAD_EPSI, GRAD_VARI). Le premier SNL est donc instable: quand on change la numérotation, le
   solveur, on a des différences entre machines.
   Ce n'est pas la peine de s'acharner: on propose de récupérer les résultats (des fonctions) du premier SNL pour faire la comparaison
   avec IMPLEX
   mais de ne pas refaire le calcul systématique pour comparer (on ne garde que le calcul IMPLEX)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssnp140c
DEJA RESTITUE DANS : 13.1.20
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25063 DU 08/04/2016
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Plantage dans Calc_Champ
FONCTIONNALITE
   Problème :
   ----------
   Cette fiche est liée à issue24882. Après un calcul avec STAT_NON_LINE (9min), on plante dans CALC_CHAMP.
   
   
   Solution :
   ----------
   Le problème vient de la structure de données charge. Grosso modo, on a 4 charges stockées dans la sd_resu mais quand on regarde la
   liste, on obtient 3 noms et une chaîne vide.
   
   Du coup quand on fait le dismoi('TYPE_CHARGE') avec la chaîne vide, on plante.
   
   Je propose d'ignorer cette chaîne vide. Ainsi le dismoi se passera correctement.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test unitaire
DEJA RESTITUE DANS : 13.1.20
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25264 DU 02/06/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Le report de issue24211 n'a pas été fait en v12
FONCTIONNALITE
   Il s'agit du report de issue24211 en branche v12.
   
   
   Problème
   --------
   
   Lorsque l'on passe l'étude jointe, ca plante avec le message :
     . ! Etape  AFFE_CARA_ELEM ligne :  146 fichier :  fort.1 impossible d affecter un
     . ! type au resultat: Erreur construction de la commande
   
   La syntaxe de AFFE_CARA_ELEM n'est pas bonne, il y a :
    CARA=('E1','R1','E2','R2') affecté à un GROUP_MA/SECTION=CERCLE,
    VARI_SECT=HOMOTHETIQUE
   alors que cela devrait être
    CARA=('EP_DEBUT','R_DEBUT','EP_FIN','R_FIN')
   
   Le problème est que la "sd_prod" est appelée avant de vérifier la syntaxe de la
   commande. Comme la syntaxe n'est pas bonne et que la "sd_prod" va chercher EP_DEBUT
   qui n'existe pas ça plante avec une message qui ne devrait jamais être émis en
   utilisation normale.
   
   Si on remplace dans la "sd_prod" les "raise" par des "print" (le temps de tester),
   cela plante dans l'analyse de la commande, comme il faut :
    POUTRE
    . .b_cercle
    . . . b_homothetique
    . . . . .b_grma
    . . . . . . Mot-clé simple : CARA
    . . . . . . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . . . . .! La valeur : 'R1' .ne fait pas partie des choix possibles (u'R_DEBUT', u'R_FIN', !
    . . . . . . . .! u'EP_DEBUT', u'EP_FIN') . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
    . . . . . . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . . . Fin Mot-clé simple : CARA
    . . . . .Fin b_grma
    . . . Fin b_homothetique
    . .Fin b_cercle
    Fin POUTRE
   
   
   Analyse & Correction
   --------------------
   
   Le superviseur fait deux appels à ``get_sd_prod()`` et donc à la fonction ``sd_prod()``
   de la commande.
   Le premier appel doit permettre de typer le résultat pour construire le JDC complet
   dans le cas PAR_LOT='OUI', ou bien juste l'étape avant son exécution en PAR_LOT='NON'.
   Le second appel intervient plus tard au moment de la validation de la commande
   (vérification syntaxique).
   
   On ajoute un argument ``__only_type__`` lors de l'appel à ``sd_prod()`` qui
   correspond au premier appel. ``sd_prod()`` peut alors ensuite prendre la responsabilité
   de retourner un type sans savoir si la syntaxe est correcte.
   Lors de la validation de la commande, cet argument n'est pas ajouté. En cas d'erreur,
   il y aura un rapport standard concernant la syntaxe.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test joint à issue24211
NB_JOURS_TRAV  : 0.25

--------------------------------------------------------------------------------
RESTITUTION FICHE 25010 DU 04/04/2016
AUTEUR : MARTIN Alexandre
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    sslv315g arrêt pour non convergence sur Clap0f0q dès qu'on change un paramétrage du solveur linéaire
FONCTIONNALITE
   Problème :
   ----------
   
   Suite au passage à METIS 5, le cas-test sslv315g ne converge plus sur calp0f0q.
   
   Analyse :
   ---------
   
   Le cas-test sslv315g valide la propagation d'une fissure X-FEM, en utilisant des éléments cohésifs.
   
   Le mode debug de la routine calcul est activé les runs debug et nodebug sont comparés, clap0f0q. Les sommes de contrôle du champ IN
   PDEPL1R de l'option PILO_PRED_ELAS_M sont différentes dès le premier calcul de cette option, pour la première itération du processus
   de Newton :
      - debug :   SOMMR=  -0.33002613491E+10
      - nodebug : SOMMR=   0.69964439147E+09
   
   Les sommes de contrôle des matrices et des seconds membres élémentaires calculés en amont sont identiques en debug et nodebug. La
   résolution du système linéaire pour la prédiction élastique donne donc deux résultats différents en debug et nodebug, alors que les
   systèmes assemblés sont identiques.
   
   Ce comportement est caractéristique d'un système mal conditionné.
   
   Dans l'appel à MECA_STATIQUE initial, le solveur MULT_FRONT est utilisé avec NPREC=12 et dans les appels à STAT_NON_LINE, le solveur
   MUMPS est utilisé avec NPREC=-1. En ramenant NPREC à sa valeur par défaut, le calcul s'arrête en matrice non factorisable lors de la
   résolution du système linéaire pour la prédiction élastique. Cela confirme que ce système linéaire est très mal conditionné.
   
   L'utilisation du solveur MUMPS dans l'appel MECA_STATIQUE initial et d'autres choix du paramètre de stabilisation de l'algorithme de
   lagrangien augmenté utilisé par le modèle cohésif n'ont pas permis de retrouver une matrice factorisable.
   
   L'option ELIM_ARETE='ELIM' est utilisé dans l'opérateur DEFI_CONTACT. Cette fonctionnalité permet de ne pas utiliser de double
   multiplicateurs de Lagrange pour imposer les relations d'égalité entre les multiplicateurs de Lagrange de contact-frottement,
   utilisés en X-FEM pour satisfaire la LBB. Cependant, il a été signalé lors de sa restitution, dans le cadre de la fiche issue22914,
   que cette fonctionnalité pouvait poser des problèmes de non convergence lors de l'utilisation d'éléments cohésifs et le pilotage.
   Or, le cas-test sslv315g utilise justement des éléments cohésifs et le pilotage.
   
   La suppression de ELIM_ARETE='ELIM' de l'opérateur DEFI_CONTACT a permis de faire converger le calcul.
   
   Validation :
   ------------
   
   Avec les modifications proposées, le cas-test est OK en debug et nodebug sur clap0f0q, calibre9, athosdev et aster5. De plus, les
   sommes de contrôle du champ IN PDEPL1R de l'option PILO_PRED_ELAS_M sont maintenant identiques en debug et nodebug sur clap0f0q et
   calibre9.
   
   Remarque :
   ----------
   
   La comparaison des runs debug et nodebug sur clap0f0q aussi mis en lumière un comportement étrange. Les sommes de contrôle du champ
   IN correspondant à la structure de données de contact PDONCO sont différentes :
      - debug : SOMMR=   0.21000000000E+05
      - nodebug : SOMMR=   0.21120549693E+96
   Bien que la différence soit frappante, les termes calculés semblent être les mêmes, puisque les sommes de contrôle des champs de
   sortie sont identiques. Par exemple, les sommes de contrôle pour le champ PMATUUR de l'option RIGI_CONT_M sont :
      - debug : SOMMR=   0.13539189528E+15
      - nodebug : SOMMR=   0.13539189528E+15
   Il est à noter que le champ PDONCO n'est pas lu dans le cas de la formulation mortar pour les éléments cohésifs dont le cas-test
   considéré fait partie.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    cas-test
DEJA RESTITUE DANS : 13.1.21
NB_JOURS_TRAV  : 4.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25258 DU 30/05/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 12.1)
TITRE
    En version 12.5.15, cas tests NO_TEST_RESU sur AthosDev_Valid
FONCTIONNALITE
   Problème
   --------
   
   En 12.5.15, les tests suivants ne font pas de TEST_xxx :
   perf014c     NO_TEST_RESU
   perf014a     NO_TEST_RESU
   perf015d     NO_TEST_RESU
   perf014b     NO_TEST_RESU
   perf015c     NO_TEST_RESU
   perf015b     NO_TEST_RESU
   perf011b     NO_TEST_RESU
   perf002c     NO_TEST_RESU
   
   
   Correction
   ----------
   
   On reporte les modifications de la révision oubliée.
   Les fichiers perf002c.comm et perf015d.comm ne sont pas modifiés dans la
   révision oubliée. Cela s'explique car en version 13 ils ont été supprimés,
   perf015d.export appelle perf015c.comm et perf002c.export appelle perf002a.comm.
   
   perf002c : on supprime perf002c.comm et on utilise perf002a.comm.
   perf015d : utilise déjà perf015c.comm que l'on corrige.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tests mentionnés
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25239 DU 23/05/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    En version 13.1.19-bc7c294adc01, le cas test sdnv142a est NO_TEST_RESU sur AthosDevValid
FONCTIONNALITE
   Problème
   --------
   
   En version 13.1.19-bc7c294adc01, le cas test sdnv142a est NO_TEST_RESU sur AthosDevValid.
   
   
   Correction
   ----------
   
   Il faut appliquer le correctif de issue25190 à la modélisation A.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdnv142a
DEJA RESTITUE DANS : 13.1.20
NB_JOURS_TRAV  : 0.25

