==================================================================
Version 12.5.12 (révision 3ba9da81e784) du 2016-05-05 10:51 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 24997 DU 01/04/2016
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    AFFE_MODELE ne gère pas bien le cas des macro-éléments
FONCTIONNALITE
   Problème
   ========
   
   Suite à la fiche issue24918, on s'est aperçu qu'AFFE_MODELE ne sait pas gérer le cas où l'on a UNIQUEMENT des macro-éléments:
   
   MOMACR = AFFE_MODELE(MAILLAGE=MAG,
                          AFFE_SOUS_STRUC=_F(PHENOMENE='MECANIQUE',
                                             SUPER_MAILLE=('MAILLE1', ),),)
   
      
      !-----------------------------------------------------------------------------------!
      ! <F> <JEVEUX_26>                                                                   !
      !                                                                                   !
      !      Objet JEVEUX inexistant dans les bases ouvertes : >MOMACR  .MODELE    .LIEL< !
      !      l'objet n'a pas été créé ou il a été détruit                                 !
      !                                                                                   !
      !                                                                                   !
      ! Cette erreur est fatale. Le code s'arrête.                                        !
      !-----------------------------------------------------------------------------------!
   
   Il faut vérifier qu'il est possible de n'utiliser que des macro-éléments ou blinder le cas avec un message d'erreur propre 
   
   Analyse
   =======
   
   La routine cormgi plante sur un objet inexistant. Cet objet LIEL correspond à la liste des éléments finis du modèle.
   Néanmoins, ça ne veut pas dire qu'on ne peut pas faire un AFFE_MODELE avec uniquement des macro-éléments, mais qu'on ne peut pas
   faire un MODELE sur un MAILLAGE avec uniquement des macro-éléments.
   
   On peut faire:
   
   mesh = DEFI_MAILLAGE(DEFI_SUPER_MAILLE= ...)
   AFFE_MODELE(AFFE_SOUS_STRUCT=_F(), MAILLAGE= mesh)
   
   
   Mais pas:
   
   mesh = LIRE_MAILLAGE()
   AFFE_MODELE(AFFE_SOUS_STRUCT=_F(), MAILLAGE= mesh)
   
   Dans cormgi, le premier cas fonctionne car on vérifie la présence d'une connectivité. Or dans le cas
   DEFI_MAILLAGE(DEFI_SUPER_MAILLE= ...), il n'y a pas de connectivité et on sort directement de la routine (sans vérifier l'objet LIEL).
   
   Le deuxième cas ne fonctionne pas, car LIRE_MAILLAGE produit une connectivité, et cormgi vérifie ensuite la présence de l'objet LIEL
   Après quelques essais, il semblerait que le second cas (LIRE_MAILLAGE/AFFE_MODELE(AFFE_SOUS_STRUCT)) pose problème dans d'autres
   opérateurs
   (erreurs en cascade, dont CALC_MATR_ELEM).
   
   Solution
   ========
   
   Il est _possible_ de n'avoir que des macro-éléments. D'ailleurs, dans la doc D4 et la SDVERI de la SD MODELE, l'objet LIEL (qui
   décrit les EF) est facultatif. Mais seulement si les macro-éléments sont définis par DEFI_MAILLAGE et pas a psoteriori sur un
   maillage lu par LIRE_MAILLAGE.
   
   Blinder le catalogue en rendant obligatoire AFFE est une mauvaise idée car on ne peut pas alors traiter le cas 1
   (DEFI_MAILLAGE/AFFE_MODELE(AFFE_SOUS_STRUCT)). Par contre, on peut vérifier dans le Fortran que l'on n'est pas dans le cas 1 et
   émettre un message d'erreur dédié:
   
   '''
   Votre modèle doit contenir au moins un élément fini car il n'est pas possible de n'avoir que des macro-éléments si le maillage est
   lu par LIRE_MAILLAGE.
     -> Risque & Conseil :
   Si vous voulez définir entièrement un modèle avec des macro-éléments, il faut définir ces derniers avec DEFI_MAILLAGE.
   '''
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.41.01
VALIDATION
    à la main
DEJA RESTITUE DANS : 13.1.16
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25120 DU 19/04/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    TEST_FONCTION fait des erreurs d'interpolations
FONCTIONNALITE
   Problème
   --------
   
   Une fonction est définie en lisant une table que l'on peut simplifier à :
   
   2.6474531374E+01  -3.9192369365E+02  -9.7873723830E-01   1.9502029066E+03
   2.6475024180E+01  -3.9194152346E+02  -9.7878181282E-01   1.9502029066E+03
   
   On fait un TEST_FONCTION et on obtient :
   
    .---- FONCTION . . . . INST
    . . . Forc . . . . . . 26.475
    . . . REFERENCE . . . .LEGENDE . . . . .VALE_REFE . . . .VALE_CALC . . . .ERREUR . . . . . TOLE
    NOOK .NON_REGRESSION . XXXX . . . . . . -391.9406486200 .-391.9415234600 .2.2E-04% . . . . 1.0E-04%
   
    .---- FONCTION . . . . INST
    . . . Depl . . . . . . 26.475
    . . . REFERENCE . . . .LEGENDE . . . . .VALE_REFE . . . . . . . .VALE_CALC . . . . . . . .ERREUR . . . . . TOLE
    NOOK .NON_REGRESSION . XXXX . . . . . . -0.97877962573000 . . . .-0.97878181282000 . . . .2.2E-04% . . . . 1.0E-04%
   
   L'interpolation manuelle donne :
   26.4750000000  -391.9406486232 -0.9787796257 1950.2029066000
   
   
   Donc TEST_FONCTION est cassé !
   
   
   Correction
   ----------
   
   En fait, TEST_FONCTION évalue la fonction à INST=26.475, c'est l'évaluation
   de la fonction qui pose problème !
   
   D'ailleurs, si dans le jeu de commandes, on imprime Forc(26.475), on trouve :
       -391.94152345999998
   soit la valeur qui apparaît dans VALE_CALC.
   
   C'est la valeur, pour INST=2.6475024180E+01 (borne supérieure de l'intervalle).
   
   En effet, suite à issue16014 en 10.3.19, on commence par regarder si on se trouve
   à 1.e-6 près d'une borne de l'intervalle d'interpolation et si c'est le cas, on
   retourne la valeur *sur* la borne. Avant cette fiche, on ne le faisait que si
   l'interpolation était interdite.
   Malgré cela, ce serait encore passé...
   ...mais pour issue21451, on vérifie la proximité à la borne en relatif et
   là, ça ne passe plus !
   
   Le problème est que la vérification de proximité avec une borne de l'intervalle
   est fait en relatif par rapport à l'abscisse. Il faut le faire par rapport à
   l'intervalle lui-même.
   
   Avec cette prise en compte de l'intervalle, l'interpolation de la fonction à
   INST=26.475 retourne :
       -391.94064862323717.
   
   TEST_FONCTION peut alors fonctionner normalement.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    fichiers joints
DEJA RESTITUE DANS : 13.1.16
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25134 DU 22/04/2016
AUTEUR : FLEJOU Jean Luc
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    DIS_CONTACT/DIS_CHOC ne respecte pas toujours le seuil de coulomb
FONCTIONNALITE
   Dans certain cas l'utilisation de DIS_CONTACT/DIS_CHOC peut conduire à des résultats faux ou à des problèmes de convergence. L'objet
   de cette fiche est d'avertir l'utilisateur en version 13.2.0 et 12 sur la possibilité de résultat faux : en dynamique avec du
   frottement de COULOMB et (AMOR_NOR ou AMOR_TAN) différents de 0.
   
   La résolution des anomalies tracées dans issue24736, issue24593, issue24420 nécessite de faire des évolutions et des cas tests de
   couverture.
   Par prudence, ces évolutions assez invasives juste avant une stabilisation, ne seront pas versées dans la version stabilisée 13.2.0.
   
   La proposition est d'émettre un message d'alarme, lors de la définition du matériau DIS_CONTACT, lorsque les mots clefs COULOMB et
   (AMOR_NOR ou AMOR_TAN) sont différents de 0. Si l'utilisateur donne AMOR_NOR ou AMOR_TAN on peut considérer qu'il va faire un calcul
   dynamique.
   
   Impact sur le code :
   --------------------
   A la fin de op0005 (DEFI_MATERIAU) on fait un "call verif_loi_mater(matout)"
   Pour DIS_CONTACT si COULOMB et (AMOR_NOR ou AMOR_TAN) > r8prem ==> <A>
   
   
   Impact sur les Fiches "en cours" devant être taguée 13.3 :
   ----------------------------------------------------------
   issue24736 Couverture de DEFI_MATERIAU/DIS_CONTACT/AMOR_*
   issue24593 A203.xx - Défaut de couverture DEFI_MATERIAU / DIS_CONTACT / AMOR_NOR ou AMOR_TAN
   issue24420 Problème de convergence avec l’utilisation de AMOR_NOR (ou AMOR_TAN) dans DEFI_MATERIAU
   
   
   Pour Informations : fiches "en cours" un peu plus vieilles, évoquant ce problème
   --------------------------------------------------------------------------------
   issue13461 (2009-05-27) Contact penalise dans DTM et DNL : vers une modélisation identique
   issue13328 (2009-04-21) Traitement des forces d'amortissement en dynamique non-linéaire
   issue12763 (2008-11-03) DYNA_NON_LINE : contact pénalisé + frottement
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage cas test perso
DEJA RESTITUE DANS : 13.1.16
NB_JOURS_TRAV  : 0.3

--------------------------------------------------------------------------------
RESTITUTION FICHE 24577 DU 02/12/2015
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    [FORMA] Champ DEPL demandé pour calculer SIEQ_ELNO sur un modèle poutre
FONCTIONNALITE
   Problème :
   ----------
   Dans le calcul joint, on se plante dans CALC_CHAMP lorsque l'on essaye de calculer SIEQ_ELNO sur un modèle mixte contenant des
   éléments DKT et POU_D_T à partir d'une sd_resultat produite par POST_CHAMP contenant uniquement SIGM_ELGA.
   
   
   Solution :
   ----------
   Il y a un bug dans ccpoux.F90. Dans cette routine, dans tous les cas, on fait un rsexch sur le champ DEPL y compris dans le cas où
   cela ne sert à rien.
   
   On est dans ce cas. Ici, on ne se sert pas du résultat du rsexch mais on le fait quand alors même que le champ est absent de la
   sd_resultat. D'où le plantage.
   
   Je corrige donc ccpoux.F90 pour que le rsexch ne soit fait que lorsqu'on en a besoin.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test joint
DEJA RESTITUE DANS : 13.1.16
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25080 DU 12/04/2016
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 12.7)
TITRE
    En version 12.5.9-75b0decc0c9e, le cas test wdnp101a est NOOK sur Calibre7 et clap0f0q
FONCTIONNALITE
   Problème
   ========
   
   En version 12.5.9-75b0decc0c9e, le cas test wdnp101a est NOOK sur Calibre7 et clap0f0q
   
   Solution
   ========
   
   ---- TABLE            NOM_PARA         TYPE_TEST
         TAB_SIXY         SIXY             MAX
         REFERENCE        LEGENDE          VALE_REFE          VALE_CALC          ERREUR           TOLE            
   NOOK  NON_REGRESSION   XXXX             0.214709990447     0.214721626981      1.163653E-05    6E-06        
   
   On augmente le TOLE_MACHINE à 5.E-5
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    wdnp101a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25141 DU 25/04/2016
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 12.5)
TITRE
    En version 12.5.11-3e406e760e90, les cas tests ssns106f et ssns106g échouent sur Calibre9
FONCTIONNALITE
   Le problème vient de te0409.F90. On initialise, puis on calcule la variable "effint" (calcul des efforts internes) seulement dans
   les cas où on a l'option RAPH_MECA et FULL_MECA et pas dans les autres cas. Or l'option qui fait planter le calcul ssns106g,f est
   RIGI_MECA_TANG qui a besoin de "effint" (valeurs autour de 1.E+308, 1.E-308) pour calculer zr(icontp+icpg-1).
   
   Ça ne coûte pas cher d'initialiser dans tous les cas de figure où on en a besoin "effint" (tous les cas sauf RIGI_MECA).
   
   Le calcul est OK après correction. On profite pour initialiser correctement d'autres quantités élémentaires dans le te.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssns106f,g
DEJA RESTITUE DANS : 13.1.17
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25088 DU 13/04/2016
AUTEUR : DELMAS Josselin
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    Issue24880 casse les tests rccm02a, b et c
FONCTIONNALITE
   Problème :
   ========
   Les tests rccm02a, b et c sont cassés par issue24880. FREQ n'est pas présents dans CREA_RESU/MODE_MECA.
   
   Correction :
   ==========
   On tombe sur une erreur de COMB_SISM_MODAL : il y a un problème car les valeurs des fréquences ne correspondent pas entre les champs
   à "combiner".
   En effet dans ces tests CREA_RESU est utilisé en reuse, il possède déjà des modes avec des fréquences définies. En imposant FREQ à
   CREA_RESU/MODE_MECA, on force l'utilisateur à donner une fréquence bidon dans la plupart des cas alors qu'en ne donnant rien, les
   nouveaux champs ajoutés (ici EFGE_ELNO) ont directement la fréquence du NUME_MODE correspondant.
   
   Il faut donc revenir sur une partie de issue24880 : FREQ ne doit pas être obligatoire.
   
   On remet facultatif le mot-clé FREQ dans CREA_RESU/MODE_MECA.
   On reprend les modifications faites dans U4.44.12 :
   
   - FREQ n'est pas obligatoire mais NUME_MODE l'est
   - explication pour le cas CREA_RESU/MODE_MECA en reuse.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.44.12
VALIDATION
    lancement rccm02a, b et c
DEJA RESTITUE DANS : 13.1.17

--------------------------------------------------------------------------------
RESTITUTION FICHE 24856 DU 19/02/2016
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    En version 13.1.8 le cas test ssls131a et en version 12.5.6 le cas test demo006a échouent sur Calibre 9
FONCTIONNALITE
   L'appel à gmsh depuis calibre9 échoue. 
   
   La version 32bits de gmsh 2.5 issue du dépôt http://aster-repo.der.edf.fr/scm/hg/aster-prerequisites/gmsh ne tourne pas sous
   Calibre9 dans l'environnement de compatibilité. Je propose de faire évoluer la version de gmsh (2.12) et de stoker dans le dépôt les
   binaires 64 bits (aster5/athosdev/calibre9) et 32bits (clap0f0q) qui eux fonctionnent correctement. 
   
   On fait évoluer YAMM pour tirer la bonne version du binaire suivant la plate-forme. 
   
   Il est nécessaire de modifier la valeur de non-régression de TEST_TABLE dans ssls131a car le maillage gmsh obtenu avec la nouvelle
   version est légèrement différent.
   
   TEST_TABLE(
   -           VALE_CALC=1.9669824189084001E9,
   +           VALE_CALC=1959874356.84 ,
               NOM_PARA='VMIS',
               TABLE=RELV_5,
               FILTRE=_F(NOM_PARA='QUANTITE',
   I
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssls131a et demo06a
DEJA RESTITUE DANS : 13.1.17
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24505 DU 16/11/2015
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Ecrasement mémoire dans lcidbg.F90
FONCTIONNALITE
   Problème :
   ----------
   Dans le cadre d'une étude avec une loi de comportement 'utilisateur', on n'a détecté un écrasement mémoire dans le routine
   lcidbg.F90 appelé par nmcomp.F90.
   
   
   Solution :
   ----------
   Véronique a proposé un correctif que je rentre en l'état.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    aucune
DEJA RESTITUE DANS : 13.1.17
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24978 DU 29/03/2016
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    [FORMATION] Plantage LIRE_MAILLAGE
FONCTIONNALITE
   Problème :
   ----------
   Lorsqu'on fait LIRE_MAILLAGE au format Aster et que le fichier est de type MED, on plante salement dans lrmast, l342.
   
   
   Solution :
   ----------
   En Fortran, il n'est pas possible de faire un diagnostic sur le type d'un fichier. Tout ce qu'on pourra faire, c'est constater qu'à
   la lecture du fichier, rien de ce qui avait été attendu n'a été trouvé.
   
   Je propose donc de remplacer l'assert par un message d'erreur qui dirait :
   """
   Problème à la relecture du fichier de maillage. 
   
   Conseil : Il se peut que votre fichier de maillage ne soit pas au format Aster.
    Vérifiez que le format de relecture est bien cohérent avec le format du fichier.
   """
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    unitaire
DEJA RESTITUE DANS : 13.1.17
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24642 DU 18/12/2015
AUTEUR : BERRO Hassan
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.1.1,  sdld103c, sdls114a, sdlv122a, sdlv124a et sdnl301a s'arrêtent en debug_jeveux
FONCTIONNALITE
   Problème
   ========
   
   Erreur JEVEUX lors du lancement des cas-tests suivants en mode debugjeveux
   - sdld103c
   - sdls114a
   - sdlv122a
   - sdlv124a
   - sdnl301a
   
   L'erreur rencontrée en de type : objet inexistant /xxxx________LG.0004/
   
   Analyse et correction
   =====================
   
   L'objet non trouvé est en effet volontairement supprimé afin d'optimiser la taille mémoire du concept table produit par l'opérateur
   POST_GENE_PHYS.
   
   LG.0004 est un objet d'entiers (pseudo-logicals) correspondant normalement au paramètre (colonne) no. 4 de la table et de longeur =
   au nombre de lignes de la table. Cet objet permet d'identifier si le dit-paramètre est bien stocké pour la ligne respective.
   
   Comme la table PGP dispose d'une structure fixe, le choix a été fait de mutualiser un nombre de ces objet LG.**** pour les
   paramètres stockés sur toutes les lignes de la table, par ex. le NUME_ORDRE, ou le NOM_CMP d'un champ.
   
   L'erreur a été tracée dans la routine pgpcrt qui crée la structure de la table et mutualise ces objets. Le problème est dans un
   appel à jeveuo en mode lecture de l'objet descripteur des paramètres de la table produite TBLP - suivi par des instructions d'écriture.
   
   En mode debugjeveux ces dernières instructions ne sont pas validées et la structure de la table n'est plus gérée correctement.
   
   La correction consiste simplement à passer d'un jeveuo - 'L' en jeveuo 'E' dans pgpcrt.
   
   Au passage, je corrige un bug similaire (lecture jeveux) dans la routine pgpcal.
   
   * PAS DE RISQUE DE RESULTATS FAUX *
   * A REPORTER IMPERATIVEMENT EN V12 *
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdld103c, sdls114a, sdlv122a, sdlv124a et sdnl301a
DEJA RESTITUE DANS : 13.1.17
NB_JOURS_TRAV  : 0.75

--------------------------------------------------------------------------------
RESTITUTION FICHE 23802 DU 12/05/2015
AUTEUR : CUVILLIEZ Sam
TYPE anomalie concernant Code_Aster (VERSION 12.1)
TITRE
    RUPT : CALC_G / OPTION 'CALC_K_G' plante en lissage LAGRANGE_REGU sur maillage quadratique
FONCTIONNALITE
   Problème
   ########
   
   Un calcul de G sur fissure maillée avec maillage quadratique a mis en évidence que le lissage de type LAGRANGE_REGU est buggé.
   
   Pour rappel, dans l'opérateur CALC_G, LISSAGE_THETA / LISSAGE_G = 'LAGRANGE_REGU' est version régularisée du lissage de LAGRANGE,
   consistant à prendre systématiquement des fonctions de forme linéaires et à étendre le support de chaque champ thêta sur 4 mailles
   consécutives (contre 2 avec LAGRANGE).
   
   Une analyse de J.P. puis de T.dS a montré que certains cas étaient mal prévus dans le programmation, ce qui conduit à des écrasements.
   
   Solution
   ########
   
   On résorbe cette fonctionnalité, car on dispose d'une solution de contournement : lissage LAGRANGE-LAGRANGE + utilisation de
   NB_POINT_FOND (équi-répartition de N points le long du fond de fissure, et utilisation de fonctions de forme linéaires quelques soit
   le degré d'interpolation).
   
   Impact dans src
   ###############
   
   Pas d'impact dans validation
   
   --- 
   
   Suppression de toutes les lignes relatives à LAGRANGE_REGU dans les fichiers suivants :
   
   (M) bibfor/algorith/cakg3d.F90
   (M) bibfor/algorith/cgveli.F90
   (M) bibfor/algorith/gcour3.F90
   (M) bibfor/algorith/gveri3.F90
   (M) bibfor/calculel/mecagl.F90
   (M) bibfor/elements/gcour2.F90
   (M) bibfor/elements/gimpgs.F90
   (M) bibfor/elements/gksimp.F90
   (M) bibfor/include/asterfort/cakg3d.h
   (M) bibfor/include/asterfort/gcour2.h
   (M) bibfor/include/asterfort/gcour3.h
   (M) bibfor/include/asterfort/mecagl.h
   (M) bibfor/op/op0100.F90
   (M) bibpyt/Messages/rupture1.py
   
   --- 
   
   Suppression des routines suivantes, qui n'ont plus lieu d'être appelées par cakg3d (CALC_K_G en 3D local) et mecagl (CALC_G en 3D local)
   
   (R) bibfor/elements/gkmet4.F90
   (R) bibfor/elements/gmatr4.F90
   (R) bibfor/elements/gmeth4.F90
   (R) bibfor/include/asterfort/gkmet4.h
   (R) bibfor/include/asterfort/gmatr4.h
   (R) bibfor/include/asterfort/gmeth4.h
   
   -> résolution du système linéaire [A]{G(s)}={b} pour CALC_G
   -> résolution des systèmes linéaires [A]{G(s)}={b} et [A]{K_i(s)}={b_i} pour CALC_K_G
   
   --- 
   
   Les tests pour lesquels la suppression des appels à CALC_G et TEST_TABLE concernés suffit sont :
   
   (M) astest/forma07a.comm
   (M) astest/hplv103a.comm
   (M) astest/sslv110a.comm
   (M) astest/sslv134a.comm
   (M) astest/sslv134j.comm
   (M) astest/sslv322b.comm
   (M) astest/sslv322c.comm
   (M) astest/ssnv185c.comm
   (M) astest/ssnv185n.comm
   
   --- 
   
   Pour le test suivant, les seules grandeurs testées sont issues de CALC_G + LAGRANGE_REGU. 
   
   (M) astest/sslv134l.comm
   
   -> SSLV134 - Fissure circulaire en milieu infini. La modélisation L est 3D + X-FEM avec fond de fissure fermé. Il y a 32 points en
   fond de fissure trouvés par DEFI_FISS_XFEM. Je remplace par un lissage LAGRANGE-LAGRANGE avec NB_POINT_FOND=16.
   
   Validation
   ##########
   
   Tous les tests CALC_G de src et validation
   
   Impact doc
   ##########
   
   U2.05.01 : Notice d'utilisation des opérateurs de mécanique de la rupture pour l'approche classique...
   
   U4.82.03 : Opérateur CALC_G
   U4.82.30 : Opérateur POST_K_TRANS
   U4.82.31 : Commande POST_GP 
   
   R7.02.01 : Taux de restitution de l'énergie en thermo-élasticité linéaire 
   
   V3.04.110 : SSLV110 - Fissure elliptique dans un milieu infini
   V3.04.322 : SSLV322 - Fissure longitudinale semi-elliptique débouchant en peau interne d'un tube sous pression
   V3.04.134 : SSLV134 - Fissure circulaire en milieu infini
   V6.04.185 : SSNV185 - Fissure débouchante dans une plaque 3D de largeur finie avec X-FEM
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 12.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 13.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U2.05.01,U4.82.03,U4.82.30,U4.82.31,R7.02.01,V3.04.110,V3.04.322,V3.04.134,V6.04.185
VALIDATION
    tests CALC_G
DEJA RESTITUE DANS : 13.1.17
NB_JOURS_TRAV  : 2.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24694 DU 11/01/2016
AUTEUR : PERONY Raphael
TYPE evolution concernant Code_Aster (VERSION 12.4)
TITRE
    cas test aster pour outil-métier AC-Seisme
FONCTIONNALITE
   -----------------
   OBJET
   -----------------
   Dans le cadre du projet R&D Assemblage Combustible, un nouvel outil métier "AC-seisme" a été crée (issue24693) pour le calcul
   dynamique d'une rangée d'assemblages combustibles.
   L'objet de cette fiche est d'introduire, en parallèle, un cas test Aster, miroir des fichiers de commandes utilisés dans l'outil métier.
   Cela permettra de détecter les évolutions à apporter aux sources (commandes Aster) de l'outil métier en fonction des évolutions du
   code_aster.
   
   Accessibilité : RESTREINT (CONFIDENTIEL)
   
   -------------------------
   PRESENTATION DU CAS TEST
   -------------------------
   Le cas test ssnl106a réalise le calcul dynamique d'une rangée hétérogène de 3 assemblages combustibles sous la sollicitation d'un
   accélérogramme.
   Un TEST_TABLE teste la valeur maximale de l'effort de choc pour chacune des interfaces cloison/assemblage ou assemblage/assemblage.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V5.02.106
VALIDATION
    sdnl106a
DEJA RESTITUE DANS : 13.1.13
NB_JOURS_TRAV  : 2.0

