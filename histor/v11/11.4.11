================================================================
Version 11.4.11 (révision bb9fa9e6f0b3) du 2013-12-09 17:20 +0100
================================================================

--------------------------------------------------------------------------------
--- AUTEUR boiteau      BOITEAU Olivier     

--------------------------------------------------------------------------------
RESTITUTION FICHE 021334 DU 2013-08-29 14:51:35
TYPE anomalie concernant Code_Aster (VERSION 12.4)
TITRE
    Résolution linéaire fausse en parallèle avec MUMPS en OOC
FONCTIONNALITE
   PROBLEME
   =========
      Un calcul STAT_NON_LINE plante ou ne converge pas lorsqu'on utilise
      le solveur linéaire MUMPS avec le point de fonctionnement suivant:
        OUT-OF-CORE + parallélisme MPI + renuméroteur=METIS/SCOTCH.
      
      En IN-CORE ou en séquentiel ou avec un autre renuméroteur, ce calcul
      fonctionne.
   
   FONCTIONNALITE IMPACTEE
   =======================
       Potentiellement tous calculs avec MUMPS 4.10.0 + METIS/SCOTCH +
       OOC + parallélisme.
   
   ANALYSE
   =======
      Ce calcul révèle un bug grave de MUMPS. Dans cette configuration il
      fournit un résultat faux. Et comme par défaut, en non linéaire,on ne
      vérifie pas la qualité de ses résultats (pour gagner du temps et
      comme il y'a le garde-fou Newton), le calcul STAT_NON_LINE plante
      plus loin lors de l'intégration d'une loi de comportement.
   
      On a isolé et soumis le pb à l'équipe MUMPS début septembre. Ils 
      l'ont reproduit chez eux et ils ont pris la mesure de sa gravité
      (résultats faux ds une configuration assez courante).
      On a eu une réunion dédiée le lundi 28 octobre au matin.
   
      Ils ont circonscrit le pb et ils ont proposé il y'a quelques jours
      une correction. Elle fonctionne chez eux, sur la matrice envoyée et
      aussi, chez nous, sur l'étude complète sur Aster4.
   
   SOURCES MODIFIEES (pour Jean-Pierre)
   =================
     Une correction simple est: dans MUMPS_4.10.0, il faut
     rajouter la ligne ci-dessous au fichier src/mumps_io_basic.c
   
     ((mumps_files+type)->mumps_io_current_file=
                mumps_io_pfile_pointer_array+file_number_arg;
   
     Cette ligne est à insérer entre les lignes 106 et 107 du fichier,
     ce qui donne:
   
     AVANT:
     106  /* Do change the current file */
     107  ((mumps_files+type)->mumps_io_current_file_number)
          =file_number_arg;
   
   
     APRES:
     106  /* Do change the current file */
     107  ((mumps_files+type)->mumps_io_current_file)=
          mumps_io_pfile_pointer_array+file_number_arg;
     108  ((mumps_files+type)->mumps_io_current_file_number)=
          file_number_arg;
   
   DETAILS
   ========
      * Cette correction ne corrige pas la MUMPS v5.0beta que je teste en
        ce moment. Mais elle produit un planton propre (erreur mumps -90)
        au lieu d'un résultat faux.
   
      * La validation de ce patch sur ce cas-test a mis en exergue un bug
        réçent ds Aster // (interface de fonctions MPI). Mathieu vient de
        la corriger.
        Cela s'appelle de la validation croisée Aster/MUMPS !
   
   NON-REGRESSION
   ==============
      sur ASTER4
      * 365 tests utilisant MUMPS en seq + qqes tests perfs en //.
   
   VALIDATION
   ==========
      * L'étude qui a suscité cette fiche, sur Aster4 en 12.0.17 
        fe690154.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    informatique,non-régression
NB_JOURS_TRAV  : 7.0
--------------------------------------------------------------------------------


