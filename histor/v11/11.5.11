==================================================================
Version 11.5.11 (révision a28247b9b9b1) du 2014-06-06 07:52 +0200
==================================================================

--------------------------------------------------------------------------------
--- AUTEUR delmas       DELMAS Josselin     

--------------------------------------------------------------------------------
RESTITUTION FICHE 022036 DU 2014-01-30 09:55:03
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : Phimeca
TITRE
    MN18 - TOUT='OUI' dangereux avec POST_ELEM/INTEGRALE
FONCTIONNALITE
   Probléme :
   ========
   Dans le calcul joint, on peut voir qu'il est dangereux d'utiliser POST_ELEM/INTEGRALE avec
   TOUT='OUI'.
   La documentation n'est pas claire au sujet de cette option : on parle seulement d'un
   calcul qui se fait alors sur "toute la structure".
   
   Si le modèle a été affecté aussi avec TOUT='OUI', POST_ELEM/INTEGRALE va calculer une
   intégrale sur un domaine constitué d'entités de dimension géométrique différente. Par
   exemple dans l'exemple joint, la modélisation D_PLAN a été affectée aux quadrangles de la
   demi-couronne et aux éléments de bord de ce maillage (SEG2). On calcule donc une intégrale
   totale qui est la somme d'une intégrale curviligne et surfacique.
   
   Ce qui est dangereux c'est que POST_ELEM calcule aussi des moyennes qui sont définies
   comme "l'intégrale divisée par le volume". Or dans le cas où le domaine d'intégration
   n'est pas homogène en dimension, ce volume n'a aucun sens !
   
   Il faut corriger le calcul. Plusieurs solutions à discuter (pas forcément exclusives) :
   - Interdiction de TOUT='OUI' (pas forcément suffisant)
   - Vérification de la présence de mailles de dimension différente dans le domaine d'intégration
   - Ajout d'un mot-clé TYPE_MAILLE obligatoire comme DEFI_GROUP
   
   Réalisation :
   ===========
   On rajoute un mot clé OBLIGATOIRE 'TYPE_MAILLE' dans POST_ELEM/INTEGRALE.
   TYPE_MAILLE peut être égale a 1D, 2D ou 3D.
   TYPE_MAILLE agit comme un filtre, excluant les mailles qui ne sont pas de la bonne dimension.
   
   On assure ainsi l’homogénéité des mailles fournies a POST_ELEM (en dimension).
   
   On rajoute en plus 2 messages  (prepost2_7 et prepost2_8).
   Un message averti l'utilisateur si le filtre élimine des mailles du groupe (prepost2_7 de
   type <A>), on indique juste le nombre de mailles éliminées.
   
   Un deuxième message informe l'utilisateur si après passage du filtre le groupe de mailles
   est vide (prepost2_8 de type <F>), et l'invite a vérifier ce qu'il a renseigné dans
   TYPE_MAILLE.
   
   On modifie les cas-tests en conséquence pour faire apparaître le mot clé .
   (les 3 cas: 1D ,2D ,3D sont traités)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u4.81.22
VALIDATION
    passage des tests incriminxc3xa9s
DEJA RESTITUE DANS : 11.5.10, 12.1.21
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR flejou       FLEJOU Jean Luc     

--------------------------------------------------------------------------------
RESTITUTION FICHE 022428 DU 2014-04-14 13:18:44
TYPE anomalie concernant Code_Aster (VERSION 12.1)
TITRE
    En version 12.1.15, revision ca8a0a48, le cas-test ssnl133a est NOOK sur Aster5.
FONCTIONNALITE
   Ce test est le déversement d'une poutre en L. Il est très sensible numériquement. Il
   utilise le pilotage.
   
   On extrait les Forces et Déplacements en fonction du paramètre de pilotage. Les
   TEST_FONCTION sont fait pour les valeurs de PARA=[1.0, 1.2, 1.4, 1.6, 2.0, 3.0, 4.0, 5.0]
   qui ont très peut de chance d'être dans les PARA calculés par le pilotage. Il existe donc
   une interpolation linéaire lors des TEST_FONCTION.
   
   Écart relatif entre toutes les machines : TOLE les plus faibles
   # PARA   DX                     DZ                     TOLEMAx    TOLEMAz
   [ 1.0 ,  1.9557493476051e-01 ,  8.1849504447187e-01 ,  1.0e-06 ,  1.0e-06],
   [ 1.2 ,  2.2002897190547e+01 ,  4.5085481299695e+01 ,  1.6e-05 ,  1.2e-05],
   [ 1.4 ,  5.1370576657920e+01 ,  5.9127404636640e+01 ,  3.7e-06 ,  5.3e-06],
   [ 1.6 ,  7.2308205991507e+01 ,  6.1009065262385e+01 ,  5.9e-06 ,  9.7e-06],
   [ 2.0 ,  9.9971410898183e+01 ,  5.5985793815570e+01 ,  2.6e-06 ,  7.8e-06],
   [ 3.0 ,  1.3415732610325e+02 ,  3.9186782348842e+01 ,  1.0e-06 ,  7.0e-06],
   [ 4.0 ,  1.5070726838590e+02 ,  2.7660150436455e+01 ,  1.0e-06 ,  6.5e-06],
   [ 5.0 ,  1.6092764983263e+02 ,  2.0199366888417e+01 ,  1.0e-06 ,  6.2e-06],
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V6.02.133
VALIDATION
    passage cas test
DEJA RESTITUE DANS : 11.5.10, 12.1.19
NB_JOURS_TRAV  : 0.5
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR desoza       DE SOZA Thomas      

--------------------------------------------------------------------------------
RESTITUTION FICHE 022286 DU 2014-03-14 11:44:29
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : DeltaCad
TITRE
    En version 12.1.11, revision 684e7a1c, le cas-test ssnl502h s'arrete par manque de convergence sur clap0f0q.
FONCTIONNALITE
   Contexte
   --------
    - Suite à la restitution de la fiche 18908 "Supprimer GROT_GDEP pour DKT ou mieux 
   la valider", le cas-test ssnl502h s'arrête pour non-convergence.
   
   Analyse
   -------
   a) Lors de la restitution des travaux effectués dans la fiche 18908, les cas-tests 
   ssnl502 (e,f,g,h) et ssnv138(f,g,h,i) ont été restitués mais pas les sources te0031 
   et gener_medkt2.cata. Les sources non-restituées concernent la modélisation DKT et 
   non la modélisation DKTG utilisée dans le cas-test ssnl502h.
   
   b) ssnl502h (DKTG) : l'absence de convergence ne relève vraisemblablement pas d'une 
   variable non initialisée mais de l'instabilité numérique consécutive à l'instabilité 
   mécanique étudiée. En augmentant la découpe du pas de temps dans DEFI_LIST_INST 
   (SUBD=10 au lieu de SUBD=4), le calcul converge avec des NOOK. Les écarts sont très 
   faibles et concernent les valeurs de NON_REGRESSION. On les met à jour.
   
   c) La prise en compte des sources entraîne des NOOK pour le cas-test 
   ssnl502e(DKT).les écarts sont très faibles et concernent les valeurs de NON_
   REGRESSION. On les met à jour.
   
   Validation
   ----------
   On vérifie les modifications sur aster4, clap0f0q et sur aster5.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssnv138*,ssnl502*
DEJA RESTITUE DANS : 11.5.10, 12.1.21
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR courtois     COURTOIS Mathieu    

--------------------------------------------------------------------------------
RESTITUTION FICHE 022690 DU 2014-06-02 06:47:35
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 11.5.9, revision 7aa7fc75, le cas-test zzzz141a s'arrete en erreur fatale sur Aster4.
FONCTIONNALITE
   Le report de issue22649 n'avait pas été fait en v11.
   Le test zzzz141a fonctionne correctement.
   
   Dans ce_ihm_parameters.py et salomeVisu.py, on récupérait mal le chemin vers les 
   "templates" (il manquait l'attribut `__file__`).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz141a
DEJA RESTITUE DANS : 12.1.22
NB_JOURS_TRAV  : 0.2
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR cuvillie     CUVILLIEZ Sam       

--------------------------------------------------------------------------------
RESTITUTION FICHE 022585 DU 2014-05-18 19:27:09
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En NEW11.5.8, revision 38388c41, le cas-test sslp319b est NOOK sur Aster5.
FONCTIONNALITE
   Problème :
   ========
   En NEW11.5.8, révision 38388c41, le cas-test sslp319b est NOOK sur Aster5.
   
   Correction :
   ==========
   On fait le report de issue22417 qui n'avait pas été fait.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    rien de particulier
DEJA RESTITUE DANS : 12.1.21
NB_JOURS_TRAV  : 0.1
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR delmas       DELMAS Josselin     

--------------------------------------------------------------------------------
RESTITUTION FICHE 022586 DU 2014-05-18 19:29:51
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En NEW11.5.8, révision 38388c41, le cas-test ssnl119a est NOOK sur Aster5
FONCTIONNALITE
   Problème :
   ========
   En NEW11.5.8, révision 38388c41, le cas-test ssnl119a est NOOK sur Aster5.
   
   Correction :
   ==========
   Ce test utilise la loi LABORD_1D qui est hors du périmètre AQ en v11 et a été supprimée
   depuis.
   On se contente d'augmenter le TOLE_MACHINE du TEST_RESU posant problème en le passant de
   13% à 16%.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage du test incriminé
NB_JOURS_TRAV  : 0.5
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR bereux       BEREUX Natacha      

--------------------------------------------------------------------------------
RESTITUTION FICHE 022581 DU 2014-05-17 07:47:11
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En NEW11.5.8, revision 38388c41, le cas-test gcpc002b est NOOK sur Aster5.
FONCTIONNALITE
   Problèmes:
   =========
   A) En NEW11.5.8,  le cas-test gcpc002b est NOOK sur Aster5.
   
   B) En 12.1.19 le cas-test gcpc002b est NOOK sur Aster5.
   
   Les NOOK sont les suivants:
   "
    ---- RESULTAT         NUME_ORDRE       NOM_CHAM         NOM_CMP          NOEUD          
    MAILLE           POINT
         RESU             1                SIGM_ELNO        SIXZ             NO15854        
    MA4204           0
         REFERENCE        LEGENDE          VALE_REFE                VALE_CALC               
   ERREUR           TOLE            
   NOOK  NON_REGRESSION   XXXX             -0.68973250258877        -0.68723546225895       
   0.362%           0.2000%         
    OK   SOURCE_EXTERNE   XXXX             -0.71500000000000        -0.68723546225895       
   3.88%            4.0%          "
   
   Analyse:
   =======
   A) On met à jour la valeur de non-régression en prenant la valeur calculée en 11.5.8 sur
   aster 5
   
   B) En 12.1.20, le test gcgp002b est OK
    
   Entre temps (issue22642) les valeurs de non-régression du test ont été mises à jour (en
   prenant la valeur calculée sur aster5)
   
    ---- RESULTAT         NUME_ORDRE       NOM_CHAM         NOM_CMP          NOEUD          
    MAILLE           POINT
         RESU             1                SIGM_ELNO        SIXZ             NO15854        
    MA4204           0
         REFERENCE        LEGENDE          VALE_REFE          VALE_CALC          ERREUR     
        TOLE            
    OK   NON_REGRESSION   XXXX             -0.695911377875    -0.695911377875    
   2.646688E-11%   0.0001%         
    OK   SOURCE_EXTERNE   XXXX             -0.715             -0.695911377875   
   2.66973736011%   4.0%            
   
   Correction: 
   ==========
   A) gcpc002.comm mis à jour en v11
   B) aucune en v12
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    gcpc002b
NB_JOURS_TRAV  : 0.1
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR pellet       PELLET Jacques      

--------------------------------------------------------------------------------
RESTITUTION FICHE 022582 DU 2014-05-17 07:49:41
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En NEW11.5.8, révision 38388c41, le cas-test sdls106c s'arrete en erreur sur Aster5.
FONCTIONNALITE
   Problème :
   ----------
   En NEW11.5.8, révision 38388c41, le cas-test sdls106c est cassé sur Aster5.
   
   La remontée d'erreur est la suivante :
   
     La dimension de l'espace réduit est : 0
     Elle est inférieure au nombre de modes, on la prend égale à : 32
      
      !-------------------------------------------------------!
      ! <F> <DVP_2>                                           !
      !                                                       !
      ! Erreur numérique (floating point exception).          !
      !                                                       !
      !                                                       !
      ! Cette erreur est fatale. Le code s'arrête.            !
      ! Il y a probablement une erreur dans la programmation. !
      ! Veuillez contacter votre assistance technique.        !
      !-------------------------------------------------------!
   
   Analyse :
   ---------
   Le plantage a lieu dans vp2ini.F90 quand on calcule 1/omeshi car
   omeshi=0.
   
   Correction:
   -----------
   Sur les conseils d'O. Boiteau, je me contente de contourner le problème
   en modifiant le fichier de commande SOLVEUR/NPREC : 9 -> 8.
   J'émets une fiche d'anomalie (issue22700) pour une correction du problème,
   c'est-à-dire, a minima, d'émettre un message d'erreur propre pour conseiller
   de changer de solveur ou de modifier NPREC.
   
   Validation :
   ------------
   Le test est alors OK sur aster5 en version 11 (debug et nodebug).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage du test incriminé
NB_JOURS_TRAV  : 0.5
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR courtois     COURTOIS Mathieu    

--------------------------------------------------------------------------------
RESTITUTION FICHE 022703 DU 2014-06-03 12:17:56
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Stanley : Pb lorsque champ constant
FONCTIONNALITE
   Problème
   --------
   
   Lorsqu'on trace un champ constant, il y a un problème avec la scalar bar
   dans Paravis car min=max.
   
   Correction
   ----------
   
   En cas d'égalité entre le min et le max, on augmente le max de 1 %.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    stanley/paravis
DEJA RESTITUE DANS : 12.1.22
NB_JOURS_TRAV  : 0.2
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR delmas       DELMAS Josselin     

--------------------------------------------------------------------------------
RESTITUTION FICHE 022711 DU 2014-06-05 08:33:12
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 11.5.10, revision 386de8d0, le cas-test sdls106e est NOOK sur clap0f0q.
FONCTIONNALITE
   Problème :
   ========
   En version 11.5.10, revision 386de8d0, le cas-test sdls106e est NOOK sur clap0f0q.
   
   Correction :
   ==========
   Ce test est instable comme l'atteste la fiche issue22378. Sur 10 runs, seuls 8 sont OK.
   
   Un travail a été fait pour stabliser ce test mais seulement en version default. En v11, on
   se contente d'augmenter un peu le PRECISION en passant de 4e-3 à 4.5e-3.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage du test incriminé
NB_JOURS_TRAV  : 0.5
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR lefebvre     LEFEBVRE Jean-Pierre

--------------------------------------------------------------------------------
RESTITUTION FICHE 022713 DU 2014-06-05 09:08:29
TYPE express concernant Code_Aster (VERSION 11.4)
TITRE
    En version 11.5.10 (rxc3xa9vision 386de8d0) sur aster5,  le test perf004e s'arrxc3xaate par manque de mxc3xa9moire
FONCTIONNALITE
   Le test perf004a utilise MUMPS pour le pré-condionnement de GCPC, en le lançant avec 2.628 Mo une alarme est 
   émise et sur aster5 ce test de performance s'arrête en erreur car le gestionnaire de travaux Slurm 
   interrompt l'exécution (Exceeded job memory limit). 
   On modifie le paramètre de lancement en affectant 3000 Mo de mémoire, le test s'exécute alors normalement en 
   version testing et stable-updates.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test perf004e
DEJA RESTITUE DANS : 12.1.22
NB_JOURS_TRAV  : 0.2
--------------------------------------------------------------------------------


