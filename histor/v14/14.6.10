==================================================================
Version 14.6.10 (révision 036c455e0e8b) du 2020-12-04 10:23 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29092 GEOFFROY Dominique       L'opérande AFFE_VARC dans AFFE_MATERIAU ne semble pas prendre en compte GROUP_MA
 30310 ABBAS Mickael            Dépassement de tableau dans les lois intégrées par Runge-Kutta
 30370 ALVES-FERNANDES Vinicius [FAUX] Hujeux calcule mal le module de cisaillement en ELAS_ORTH
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29092 DU 02/09/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 14.4)
- TITRE : L'opérande AFFE_VARC dans AFFE_MATERIAU ne semble pas prendre en compte GROUP_MA
- FONCTIONNALITE :
  | Problème :
  | ========
  | Un utilisateur est bloqué dans son étude car les éléments D_PLAN_DIL ne savent pas calculer l'option CHAR_MECA_TEMP_R.
  | 
  | Correction :
  | ==========
  | 
  | On ajoute D_PLAN_DIL aux listes des éléments qui ne calculent pas :
  | - CHAR_MECA_TEMP_R
  | - CHAR_MECA_SECH_R
  | - CHAR_MECA_HYDR_R

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : passage de l'étude fournie
- DEJA RESTITUE DANS : 15.2.20


================================================================================
                RESTITUTION FICHE 30310 DU 05/10/2020
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Dépassement de tableau dans les lois intégrées par Runge-Kutta
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Lors du développement de REGU_VISC (issue29969), on a révélé un bug dans l'intégration explicite des lois de comportement par 
  | Runge-Kutta. Certains tests (ssnp02e pour NORTON ou les tests VENDOCHAB par exemple) terminent en "Bus Error" sur Eole et  
  | uniquement en non-debug.
  | 
  | Correction
  | -----------
  | 
  | 
  | Le problème vient de la variable toutms dans gerpas. Cette variable ne sert qu'aux comportements cristallins.
  | Problème: elle est dimensionnée (tableau automatique) avec des entiers définis par ailleurs, en particulier nbphas qui vaut ... 
  | zéro quand ce n'est pas un comportement cristallin. Ce qui donne un vecteur toutms de longueur nulle. Peu propice à un 
  | comportement raisonnable !
  | Ca passe plus depuis qu'on a changé (un peu) le dimensionnement en entrée de tous les comportements.
  | En effet, par précaution, le grand Eric L. a pris l’habitude de donner les dimensions explicites des tableaux, en particulier 
  | de la matrice tangente. Ce qui provoque des erreurs d’alignement avec les variables mal dimensionnées comme toutms dans gerpas.
  | 
  | Cette fiche est là pour reporter la correction en V14, car ce bug existe depuis toujours et on a juste eu du bol que le défaut 
  | d’alignement ne soit pas apparu avant.
  | 
  | On modifie lcmate (définition des caractéristiques matériaux) en mettant nbcomm(1) = 1. C'est cette vraible qui sert à donner 
  | nbphas
  | 
  | La correction en V15 est directement faite dans un commit de la fiche 29969. Elle consiste à initialiser nbphas à 1

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien


================================================================================
                RESTITUTION FICHE 30370 DU 21/10/2020
================================================================================
- AUTEUR : ALVES-FERNANDES Vinicius
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : [FAUX] Hujeux calcule mal le module de cisaillement en ELAS_ORTH
- FONCTIONNALITE :
  | Problème :
  | -----------
  | La matrice de Hooke dans Hujeux pour un matériau orthotrope n'est pas correcte. Il manque un facteur 2 dans les termes associées aux cisaillement.
  | 
  | Correction :
  | ------------
  | On rajoute le facteur 2 dans les routines de Hujeux où la matrice de Hooke est construite (9 au total).
  | 
  | Cas-test :
  | ----------
  | On rajoute le cas-test ssnv210, pour lequel on compare les solutions obtenues entre un matériau isotrope 
  | et orthotrope avec les mêmes caractéristiques élastiques, en D_PLAN (a) et 3D (b).
  | 
  | Impact documentaire :
  | ----------------------
  | v6.04.210 : Essai de cisaillement drainé avec le modèle de Hujeux
  | r7.01.23 : Loi de comportement cyclique Hujeux pour les sols (on rajoute la référence au cas-test ssnv210)
  | 
  | Risque de résultats faux :
  | ---------------------------
  | Depuis l'introduction des matériaux orthotropes avec Hujeux (ELAS_ORTH) en v9.2.22, les termes en cisaillement du tenseur de Hooke ne sont pas calculés correctement. Cela conduit à des résultats faux pour des chemins de chargement avec une composante en cisaillement (cas d'un chargement sismique par exemple).

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 9.2.22
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 9.2.22
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v6.04.210, r7.01.23
- VALIDATION : cas-test
- DEJA RESTITUE DANS : 15.2.21
- NB_JOURS_TRAV  : 2.0

