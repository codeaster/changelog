==================================================================
Version 14.2.18 (révision 1b988be1f19a) du 2018-11-23 09:30 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 27388 BÉREUX Natacha          [D206] Performance des conditions aux limites
 28021 TARDIEU Nicolas          PQUASI.18 - Intégrer la fonctionnalité fieldsplit de PETSc
 28214 SELLENET Nicolas         fdlv101a et calc_modes
 28195 MATHIEU Tanguy           [RUPT] POST_RCCM enlever le mot clé TYPE_RESU_MECA par défaut dans U4.83.11
 28115 COURTOIS Mathieu         Adaptation as_run au gestionnaire de batch PBS
 26118 KHAM Marc                CALC_ESSAI_GEOMECA: choix de la remontée des paramètres
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 27388 DU 06/02/2018
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : evolution concernant code_aster (VERSION 14.2)
- TITRE : [D206] Performance des conditions aux limites
- FONCTIONNALITE :
  | Problème
  | ========
  | 1) Contexte :
  | ~~~~~~~~~~~~~
  | Dans aster, on impose les relations linéaires entre degrés de liberté par l'intermédiaire de multiplicateurs de Lagrange. Ces
  | multiplicateurs de Lagrange conduisent à un système linéaire de type "point-selle". Un système linéaire de type point selle pose 
  | deux difficutés: 
  | - impossibilité  d'utiliser des solveurs linéaires performants (GC + préconditionneur multi-grille par exemple) 
  | - calcul de valeurs propres plus difficile.
  | D'où l'existence dans code_aster d'une fonctionnalité dédiée à l'élimination des multiplicateurs de Lagrange ELIM_LAGR. Cette
  | fonctionnalité  permet de projeter le problème sur un espace réduit : le
  | noyau de la matrice des contraintes. L'étape principale de cette procédure est le calcul d'une base du noyau de cette matrice.
  | Le calcul de la base est effectué par une factorisation QR. L'implémentation est séquentielle.
  | La fonctionnalité ELIM_LAGR manque de robustesse (elle échoue sur un calcul de modes sur un alternateur) et de performance. 
  | 
  | 2) LP 2018 :
  | ~~~~~~~~~~~~
  | L'objectif du LP 2018 est de permettre l'élimination des multiplicateurs de Lagrange sur des modèles vibratoires industriels afin
  | d'améliorer la précision du calcul des modes. 
  | 
  | 3) Travail effectué : 
  | ~~~~~~~~~~~~~~~~~~~~~
  | (a) ajout d'une bibliothèque externe (SuperLU) 
  |     compilée et installée comme un paquet externe optionnel de PETSc. 
  | (b) réécriture intégrale du calcul de la base 
  |     Ce calcul repose désormais sur une factorisation LU réalisé par SuperLU (au lieu d'une factorisation QR native de code_aster)
  | 
  | 4) Remarques :
  | ~~~~~~~~~~~~~~
  | (a) On n'utilise pas l'interface PETSc/SuperLU
  | 
  | (b) Dans code_aster, les matrices assemblées sont des structures de données qui comportent des références au maillage. Par exemple,
  | le PRNO effectue une correspondance entre des noeuds et des degrés de liberté. Lorsqu'on projette le problème sur une espace
  | vectoriel réduit (noyau), cette correspondance n'a plus de sens. On ne peut donc pas reconstruire une MATR_ASSE complète après
  | élimination des multiplicateurs de Lagrange. 
  | 
  | 5) Post RTA 
  | ~~~~~~~~~~~
  | - Je ne supprime pas la commande ELIM_LAGR. 
  | En effet, on ne peut pas utiliser ELIM_LAGR ='OUI' dans le mot-clé solveur de  CALC_MODES. Si on veut calculer les modes à partir
  | d'un modèle avec des multiplicateurs de Lagrange éliminés, il faut enchaîner les commandes suivantes :
  | K2 = ELIM_LAGR( MATR_RIGI = K);
  | M2 = ELIM_LAGR( MATR_RIGI = K, MATR_ASSE = M );
  | MODES = CALC_MODES( MATR_RIGI=K2, MATR_MASS=M2, ....)
  | - La commande ELIM_LAGR ne retourne pas de MATR_ASSE_GENE. 
  | En effet, si on appelle CALC_MODES avec des matrices de type MATR_ASSE_GENE, on obtient en retour un MODE_GENE et pas un MODE_MECA.
  | De plus le calcul ne s'effectue pas jusqu'au bout car on aurait besoin de connaître un modèle généralisé. 
  | - La commande ELIM_LAGR retourne une matr_asse_elim_r. C'est une sd à part qui n'est acceptée qu'en entrée de CALC_MODES. 
  | - Il faudra sans doute quand même un jour supprimer la commande ELIM_LAGR pour que l'élimination des Lagranges soit faite de façon
  | transparente à l'intérieur de CALC_MODES. J'émets une fiche à cette intention issue28239. 
  | - Je ne restitue pas pour l'instant le cas de validation. J'émets une fiche pour le faire plus tard issue28240.
  | - Le calcul fonctionne en parallèle : j'ajoute deux modélisations au cas test zzzz352 (c et d). Ces modélisations sont identiques à
  | zzzz352a et c mais s'exécutent sur deux procs. 
  | - je supprime les ddls fictifs (dans la routine apbloc).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.55.03, R3.03.05, V1.01.352, u4.50.01, v1.01.380
- VALIDATION : zzzz352a zzzz352b
- NB_JOURS_TRAV  : 60.0


================================================================================
                RESTITUTION FICHE 28021 DU 11/09/2018
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : PQUASI.18 - Intégrer la fonctionnalité fieldsplit de PETSc
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | On ajoute la valeur "FIELDSPLIT" au mot-clé solveur. Ce dernier permet de définir très facilement un préconditionneur par blocs. 
  | La syntaxe est la suivante sur un exemple de THHM:
  | PARTITION_CMP=(3,1,2)
  | NOM_CMP=('DX','DY','DZ','TEMP','PRE1','PRE2')
  | OPTION_PETSC='mes options'
  | 
  | Le mot-clé partition permet d'indiquer que les 3 premières composantes 'DX','DY','DZ' vont être considérées comme un 1er bloc,
  | 'TEMP' comme un deuxième bloc puis  'PRE1','PRE2' comme un 3ème bloc. Les options qui définissent le préconditionneur sont données
  | dans un chaine de caractères sur une ligne. Si on rencontre un bloc de type déplacement (composantes 'DX','DY','DZ'), on définit les
  | modes de corps rigides qui sont utiles pour la convergence des méthodes multigrilles.
  | On peut définir autant de blocs que nécessaire.
  | 
  | Correction/Développement
  | ------------------------
  | Un nouveau module est créé, aster_fieldsplit_module.F90 qui comporte toutes les fonctionnalités.
  | On a allongé la taille maximale de la chaine de caractères dans getvtx à 2550 pour pouvoir définir le préconditionneur.
  | 
  | Validation
  | ----------
  | J'ajoute un nouveau cas-test petsc04 qui comporte 4 modélisations :
  | - petsc04a, problème de Stokes en séquentiel
  | - petsc04b, problème de Stokes en parallèle
  | - petsc04c, problème THM en séquentiel
  | - petsc04d, problème THM en séquentiel
  | 
  | Les résultats sont validés par une résolution avec MUMPS (référence 'AUTRE_ASTER').

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.50.01,V1.04.119
- VALIDATION : Nouveau test
- NB_JOURS_TRAV  : 20.0


================================================================================
                RESTITUTION FICHE 28214 DU 08/11/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : fdlv101a et calc_modes
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Dans le test fdlv101a, après correction d'un petit bug dans calc_modes_multi_bandes.py, on plante dans un assert False.
  | 
  | En fait, cet assert est dû au fait qu'un mot-clé est manquant.
  | 
  | Je crois en regardant qu'il y a une mauvaise évaluation d'une condition de bloc qui a pour conséquence la non-apparition d'un
  | mot-clé (NIVEAU_PARALLELISME du mot-clé facteur CALC_FREQ).
  | 
  | 
  | Solution :
  | ----------
  | Le problème vient de la condition du bloc qui est mal évaluée. La condition est :
  | condition= """length('FREQ') > 2"""
  | 
  | Mais en python length n'existe pas.
  | 
  | Si on remplace length par len, la condition est bien évaluée et le mot-clé apparaît.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : fdlv101a
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 28195 DU 05/11/2018
================================================================================
- AUTEUR : MATHIEU Tanguy
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : [RUPT] POST_RCCM enlever le mot clé TYPE_RESU_MECA par défaut dans U4.83.11
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | La documentation U4.83.11 indique que le mot clé TYPE_RESU_MECA est obligatoire avec la valeur EVOLUTION par défaut.
  | En réalité, le catalogue de la commande montre que le mot clé TYPE_RESU_MECA est bien obligatoire, mais sans valeur par défaut.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Modification de la documentation U4.83.11.
  | Suppression de la valeur par défaut pour TYPE_RESU_MECA.
  | L'option TYPE_RESU_MECA = B3200 étant la plus utilisée et l'option TYPE_RESU_MECA = EVOLUTION étant amenée à disparaitre, l'ordre
  | des paragraphes de la documentation a été modifié en cohérence.
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Sans objet

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.83.11
- VALIDATION : worklow de relecture de l'appli doc
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 28115 DU 05/10/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant astk/asterstudy (VERSION 2018)
- TITRE : Adaptation as_run au gestionnaire de batch PBS
- FONCTIONNALITE :
  | Modification des paramètres de soumission des jobs :
  | 
  | - On utilise le "walltime" pour LSF ('-W' au lieu de '-c') et PBS ('-walltime' au lieu de '-cput') comme on le fait pour Slurm.
  | 
  | - Définition du nombre de noeuds, nombre de processus MPI pour PBS : '-cpus' n'existe plus apparemment. On calcule le nombre
  | processus par noeud pour pouvoir utiliser "-nodes=nbnodes:ppn=cpu_per_node".

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : aucune
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 26118 DU 23/02/2017
================================================================================
- AUTEUR : KHAM Marc
- TYPE : evolution concernant code_aster (VERSION 14.1)
- TITRE : CALC_ESSAI_GEOMECA: choix de la remontée des paramètres
- FONCTIONNALITE :
  | Objet:
  | ------
  | Dans le cadre du projet Risque Sismique, TEGG fait remonter des améliorations de la macro CALC_ESSAI_GEOMECA (voir fiche chapeau 26119)
  | 
  | Dans cette fiche, on souhaite donner la possibilité de faire remonter (à minima sous la forme de
  | tableaux) les paramètres de calcul disponibles (variables internes etc.). On étudiera la possibilité de donner à l'utilisateur le
  | choix des couples Y=f(X) à tracer au format xmgrace.
  | 
  | Actions propres à cette fiche:
  | ------------------------------
  | * Remontée des options de couleur, marqueur et style de courbes, dans les mots-clés suivants :
  |   - COULEUR_NIV1/2
  |   - MARQUEUR_NIV1/2
  |   - STYLE_NIV1/2
  | * Remontée d'une courbe quelconque de niveau 2 dans:
  |   - GRAPHIQUE = ('ABSC-ORDO')  ex. ABSC=INST   et ORDO=V23
  |   - NOM_CMP   = ('V23')
  | 
  | Cas-test
  | ---------
  | comp012E
  | 
  | Docs:
  | -----
  | - V6.07.112 :  comp012
  | - U4.90.21  : calc_essai_geomeca

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : v6.07.112, u4.90.21
- VALIDATION : cas-test
- NB_JOURS_TRAV  : 3.0

