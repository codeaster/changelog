==================================================================
Version 14.2.18xx (révision 9cb332d6b144) du 2018-11-26 13:29 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 28141 COURTOIS Mathieu         Commande COPIER
 28158 SELLENET Nicolas         Erreur DVP_1
 28187 SELLENET Nicolas         Lagrange et asterhpc
 28190 SELLENET Nicolas         Plantage dans getvc8
 28181 SELLENET Nicolas         DEFI_MATERIAU cas des listes de valeurs réelles pour un paramètre
 28193 SELLENET Nicolas         Plantage mumps03a
 28198 SELLENET Nicolas         Alléger MaterialBehaviour.h
 28192 COURTOIS Mathieu         Exception dans asterxx
 28216 SELLENET Nicolas         Problème POURSUITE
 28135 LEFEBVRE Jean-Pierre     INCLUDE dans asterxx
 28204 SELLENET Nicolas         Unsupported value: MODE_FLAMB
 28208 SELLENET Nicolas         NotImplementedError: Unsupported keyword: Cele_c
 28210 SELLENET Nicolas         NotImplementedError: Type of field XXXX
 28215 SELLENET Nicolas         Erreur Noyau.N_Exception.AsException: type inconnu
 28237 SELLENET Nicolas         Erreur "KeyError: XXX"
 28013 SELLENET Nicolas         Message d'erreur "* takes exactly ** arguments (*** given)"
 28276 COURTOIS Mathieu         asterxx - merge invalide
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 28141 DU 15/10/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Commande COPIER
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Tests qui échouent dans COPIER.
  | 
  | 
  | Correction
  | ----------
  | 
  | On corrige les derniers tests qui échouent à cause de `Boost.Python.ArgumentError`.
  | => Il faut que l'export Boost Python reflète l'héritage C++.
  | `PCFieldOnMesh` hérite de `GenericDataField`, il faut le dire dans 'PCFieldOnMeshInterface.cxx'.
  | 
  | Autres tests : passage de liste de fonctions pour MFront/Umat.
  | Dans VectorUtilities, il faut ajouter BaseFunction pour que la conversion list<->std::vector soit automatique.
  | 
  | Correction des macros de 'Contrib.diffusion_H2'.
  | 
  | 
  | COPIER(cabl_precont) :
  | - On ajoute de méthodes d'accès au `PrestressingCableDefinition` : `getModel`, `getMaterialOnMesh` et `getElementaryCharacteristics`.
  | 
  | COPIER(evol*) :
  | - Il faut affecter le maillage (via le modèle) du résultat original. Même problème que dans issue28094 sur l'unicité du modèle dans
  | le ResultsContainer.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : 36 tests
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 28158 DU 18/10/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Erreur DVP_1
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Beaucoup de tests plantent avec l'erreur DVP_1 :
  | fdnv100a hplv101a mfron01a mfron01d mfron01g mfron01h mfron01j mfron02g mfron02i mfron03b mfron03c mfron03d mfron03e mfron03f
  | mfron03g mfron03h mfron03i mfron03j mfron04a mfron04b mfron04c mfron04d mfron05b mfron05c mfron05d mumps03a sdll126e sdls08a
  | sdlv124a sdlv124b sdlv132c sdlv132d sdnv143a sdnv143b sdnv143c sdnv143d shll100a shll100b shlv301a sslp100c ssnl112a ssnl117b
  | ssnv187e ssnv187f ssnv187g ssnv187h ssnv187i ssnv187j ssnv187k ssnv187l ssnv227b ssnv227c zzzz382a zzzz387b
  | 
  | 
  | Solution :
  | ----------
  | Il y a plusieurs problèmes que je corrige dans cette fiche.
  | 
  | Il manquait le cas HARM_GENE dans gcucon.F90. On le rajoute et l'erreur disparaît de certains tests.
  | 
  | De nombreux tests plantaient dans matcod.F90.
  | 
  | Ce plantage était dû au fait que le nommage du vecteur jeveux devant contenir les paramètres matériaux (notamment dans le cas de
  | MFront) n'était pas correct. Il s'appelait :
  | "000000010000002.LISV_R8         "
  | alors qu'il devrait s'appeler :
  | "00000001.0000002.LISV_R8        "
  | 
  | Il manquait un point. En corrigeant ce problème, on répare 31 tests.
  | 
  | Certains tests plantaient à cause d'un mauvais merge de la routine ischar_iden.F90. Je modifie la routine et cela répare 6 tests.
  | 
  | Les tests mfron03h, i et j plantaient encore car ils utilisaient toujours l'ancienne manière pour lire dans une liste python (avec
  | le underscore). Je corrige.
  | 
  | Le test ssnl112a plante car le nettoyage du matériau codé dans le nouveau MECA_STATIQUE était incomplet. Il faut donc ajouter les
  | objets manquants pour qu'ils soient détruits à l'issu du calcul et qu'ainsi le test se termine correctement.
  | 
  | J'ajoute aussi macro_rota_globale qui faisait planter ssnl117b.
  | 
  | Le test shll100b plante car un type de résultat présent dans le capy n'était pas prévu dans la retranscription de la commande. En
  | ajoutant ce type, le test devient ok.
  | 
  | Il y avait aussi un bug dans recu_fonction.py. Dans create_result, on cherchait le type du mot-clé RESU_GENE mais il manquait
  | ".getType()". Forcément, on ne trouvait pas le type mais l'objet. En corrigeant ce bug, on corrige shll100a.
  | 
  | En corrigeant ces éléments, on corrige 47 tests (sur 54).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : liste fiche
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 28187 DU 30/10/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : evolution concernant asterxx (VERSION 11.8)
- TITRE : Lagrange et asterhpc
- FONCTIONNALITE :
  | Dans le cadre de la parallélisation distribuée, la nouvelle architecture HPC de code_aster utilise un maillage prédécoupé. Les
  | différents processeurs peuvent facilement communiquer des données nodales aux niveau de raccords préétablis dans le maillage
  | prédécoupé. Cependant le découpage et les raccords ne tiennent pas compte des conditions aux limites dualisées faisant intervenir
  | ultérieurement des noeuds n’appartenant pas aux raccords.
  | 
  | La stratégie adoptée est d'ajouter les raccords "à la volée" au moment de la création des conditions aux limites dualisées.
  | L'instance chargement est alors de type ParallelMechanicalLoad, une classe dérivée du type GenericMechanicalLoad.
  | 
  | Afin d'exploiter au maximum l’architecture Fortran de Code_Aster, la construction du ParallelMechanicalLoad s'appuie sur des objets
  | partiels classiques créés temporairement pour l'occasion :
  | 
  |   --------------------------
  |  | parallel mechanical load |
  |   --------------------------
  |            |
  |            |   --------------    ---------------    --------------    ---------------
  |            `->| partial load |->| partial model |->| partial mesh |->| parallel mesh |
  |                --------------    ---------------    --------------    ---------------
  |                     |                      |
  |                     `-> AFFE_CHAR_MECA     |    ----------------
  |                                            `- >| parallel model |
  |                                                 ----------------
  | 
  | Ces objets partiels sont les mêmes sur tous les processeurs et ne contiennent que les noeuds impliqués dans les conditions dualisées.
  | 
  | Cette approche se limite les mots clés DDL_IMPO et LIAISON_DDL et à l'utilisation des DDL de déplacement. L'objectif de ce travail
  | est d'automatiser et de généraliser la création du ParallelMechanicalLoad.
  | 
  | Travail effectué :
  | ==================
  | 
  | 1/ Le ParallelMechanicalLoad est produit automatiquement par la commande AFFE_CHAR_MECA.
  | 
  | La commande AFFE_CHAR_MECA produit un GenericMechanicalLoad si le modèle est séquentiel ou si tous les chargements sont de type
  | Neumann, et elle produit un ParallelMechanicalLoad si le modèle est parallèle et que tous les chargements sont de type Dirichlet. Il
  | n'est pas possible de mélanger des chargements de Neumann et de Dirichlet dans un même AFFE_CHAR_MECA en parallèle, sinon erreur
  | utilisateur. Les étapes dans le cas parallèle et Dirichlet sont :
  | 
  | * création du "partial mesh" par extraction via "parallel mesh" des mailles support des groupes de noeuds impliqués dans le chargement,
  | * création du "partial model" s'appuyant sur le "partial mesh" avec une modélisation bidon,
  | * transfert du '.PRNM' du "parallel model" vers le "partial model"
  | * création du "partial load" via un AFFE_CHAR_MECA sur le "partial model"
  | * construction du ParallelMechanicalLoad
  | 
  | Une fois le ParallelMechanicalLoad produit, les instances partielles et les SD associées ne sont plus utiles et sont détruites à la
  | sortie de l'opérateur AFFE_CHAR_MECA.
  | 
  | 2/ La généralisation à tous les DDL est assurée par l'étape de transfert du '.PRNM' qui assure la cohérence des DDL présents.
  | 
  | Pour cela on transfert (via une commande binder python) dans le c++, le physicalNodesComponentDescriptor du ligrel du modele
  | parallèle sur le physicalNodesComponentDescriptor du ligrel du modèle partiel, ce qui est équivalent à transférer le '.PRNM'
  | 
  | Un test avec application sur des DDL 'PRES' et les tests sur LIAISON_SOLIDE (avec ou sans rotations) permettent de valider l'approche.
  | 
  | 3/ La généralisation aux différents chargements de Dirichlet est effectué pour les mots clé où il est possible de définir les
  | chargements sur des groupes de noeuds.
  | 
  | Après quelques adaptations dans le ParallelFiniteElementDescriptor, les chargements suivants fonctionnent et sont testés par
  | comparaison aux cas séquentiels :
  | 
  | LIAISON_DDL, DDL_IMPO, LIAISON_OBLIQUE, LIAISON_UNIF, LIAISON_SOLIDE, DDL_POUTRE, LIAISON_GROUP et LIAISON_RBE3
  | 
  | Pour les autres chargements de Dirichlet, une erreur utilisateur est émise.
  | 
  | On teste également le fonctionnement d'un mélange Neumann / Diriclet en parallèle pour les mots clé suivants :
  | 
  | FORCE_NODALE, PESANTEUR, ROTATION, VECT_ASSE, EVOL_CHAR
  | 
  | 4/ création des mailles support
  | 
  | Dans le partial mesh, les POI1 sur les noeuds sont remplacé par les mailles supports des noeuds. La stratégie pour communiquer les
  | mailles est la même que celle utilisé pour communiquer les noeuds (avec un offset). On applique, en fonction de la dimension du
  | maillage, la modélisation "D_PLAN" ou "3D" au lieu des "DIS_T"
  | 
  | La validation se fait via DDL_POUTRE qui à besoin de la connectivité pour calculer son orientation et sur LIAISON_SOLIDE qui à
  | besoin de récupérer la longueur de mailles minimale.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests AFFE_CHAR_MECA parallèles
- NB_JOURS_TRAV  : 33.0


================================================================================
                RESTITUTION FICHE 28190 DU 31/10/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Plantage dans getvc8
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Les tests ahlv100b, ahlv100d, ahlv100e, sdld101a, sdld21c, sdld27e, sdld313d, sdls08a, shll100c et zzzz208a plante dans la fonction
  | python getvc8. Il faut corriger.
  | 
  | 
  | Solution :
  | ----------
  | Dans getvc8, le cas où on donne les complexes sous forme 'RI' ou 'MP' n'est pas prévue. Il faut donc modifier un peu modifier la
  | fonction python pour que cela fonctionne.
  | 
  | J'en profite aussi pour modifier IMPR_GENE qui faisait un ULOPEN d'une unité logique sans jamais la refermer. Cela sait imposé de
  | devoir faire un DEFI_FICHIER 'LIBERER' dans le fichier de commande pour pouvoir manipuler le fichier produit par IMPR_GENE. En
  | libérant l'UL correctement, on n'a plus besoin de faire DEFI_FICHIER. Je le supprime donc de sdls08a.
  | 
  | En faisant cela, on répare 8 tests.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : liste fiche
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28181 DU 26/10/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 14.3)
- TITRE : DEFI_MATERIAU cas des listes de valeurs réelles pour un paramètre
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Le cas des listes de valeurs réelles derrière un paramètre n'est pas prévu pour la commande DEFI_MATERIAU. Ce qui fait planter le
  | test ssns112c.
  | 
  | 
  | Solution :
  | ----------
  | En fait dans le cas des listes de réels, on stocke le paramètre comme une fonction en y adjoignant autant de tableaux LISV_R8 que de
  | paramètres.
  | 
  | Il faut donc modifier la classe MaterialBehaviour en conséquence.
  | 
  | En faisant cela et en supprimer l'usage d'INCLUDE dans le test ssns112c, il devient ok.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssns112c
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28193 DU 02/11/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Plantage mumps03a
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Le test mumps03a plante dans un assert de la routine ascopr.
  | 
  | 
  | Solution :
  | ----------
  | Le problème vient du fait que les matr_elem dans asterxx sont uniques. Il n'en existe pas différents types.
  | 
  | Or contrairement à ce que laisser penser la doc, il en existe différents types dans aster legacy : matr_elem_depl_r,
  | matr_elem_depl_c, etc.
  | 
  | Il faut donc modifier l'objet ElementaryMatrix pour le faire devenir template.
  | 
  | En faisant cela, le test mumps03a va plus loin mais plante dans CALC_CHAR_CINE. Le problème de cette commande, c'est que le type
  | retourné n'est pas correct. Il faut donc modifier le type de retour pour que dans tous les cas, la commande retourne un cham_no.
  | 
  | En corrigeant cela, on plante ensuite avec le message suivant :
  | """
  | Solveur MUMPS :
  |  Limite atteinte : le solveur MUMPS est utilisé par plus de 5 matrices simultanément.
  | """
  | 
  | Ici le problème vient du fait que le ménage des instances MUMPS n'est pas correctement fait. Il faut donc modifier cet élément.
  | 
  | En corrigeant ce dernier élément, le test devient ok.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : mumps03a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28198 DU 06/11/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : evolution concernant asterxx (VERSION 11.8)
- TITRE : Alléger MaterialBehaviour.h
- FONCTIONNALITE :
  | Demande :
  | ---------
  | Le fichier MaterialBehaviour.h est très compliqué à compiler car il est très long et contient de nombreux templates.
  | 
  | 
  | Solution :
  | ----------
  | En réalité, on n'a pas besoin de tous les matériaux présents dans ce fichier car pour la plupart d'entre eux, j'ai créé un classe
  | générique.
  | 
  | On a besoin de classes particulières que dans 2 cas :
  | - Si on a une conversion implicite de mot-clé ; exemple : RUPT_FRAG, mot-clé CINEMATIQUE qui peut valoir "UNILATER", "GLIS_1D", ou
  | "GLIS_2D". Dans ce cas, ce n'est pas la valeur du mot-clé qui est recopié dans les objets jeveux mais un double valant 0.0, 1.0 ou
  | 2.0. C'est une glute de rcstoc.F90 qu'il faut prévoir au cas par cas.
  | - Si on a des fonctions de traction dans ce cas, il faut construire un objet .&&RDEP qui servira à évaluer la fonction.
  | 
  | Si on ne garde que ces classes particulières, on n'en conserve que 9 :
  | - BetonDoubleDpMaterialBehaviour
  | - BetonRagMaterialBehaviour
  | - DisEcroTracMaterialBehaviour
  | - ElasMetaMaterialBehaviour
  | - ElasMetaFoMaterialBehaviour
  | - MetaTractionMaterialBehaviour
  | - RuptFragMaterialBehaviour
  | - RuptFragFoMaterialBehaviour
  | - TractionMaterialBehaviour

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests asterxx
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28192 DU 31/10/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Exception dans asterxx
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Ce développement a pour objectif de faire converger la gestion des erreurs en fortran, celle des exceptions en C++ et en Python.
  | 
  | 
  | Développement
  | -------------
  | 
  | Les exceptions Python sont créées dans `LibAster.cxx`: `AsterError` est l'exception de base qui hérite de `Exception`, puis les
  | exceptions spécialisées.
  | On a limité les exceptions à 5 types d'erreur :
  | 
  | . . AsterError
  | . .  |
  | . .  +-- ConvergenceError
  | . .  |. . . Raised in case of convergence problem.
  | . .  |
  | . .  +-- IntegrationError
  | . .  |. . . Raised during local integration (of the behavior for example).
  | . .  |
  | . .  +-- SolverError
  | . .  |. . . Raised during solving phases.
  | . .  |
  | . .  +-- ContactError
  | . .  |. . . Raised during contact algorithms.
  | . .  |
  | . .  +-- TimeLimitError
  | . . . . . . Raised when the time limit is reached.
  | 
  | 
  | L'attribut `id_message` permet de savoir quel message a été émis précisément en cas de besoin.
  | 
  | Correspondance avec les anciennes exceptions :
  | 
  | aster.error = AsterError                            # 21
  | aster.NonConvergenceError = ConvergenceError        # 22
  | aster.EchecComportementError = IntegrationError     # 23
  | aster.BandeFrequenceVideError = SolverError         # 24
  | aster.MatriceSinguliereError = SolverError          # 25
  | aster.TraitementContactError = ContactError         # 26
  | aster.MatriceContactSinguliereError = SolverError   # 27
  | aster.ArretCPUError = TimeLimitError                # 28
  | aster.PilotageError = ConvergenceError              # 29
  | aster.BoucleGeometrieError = ContactError           # 30
  | aster.BoucleFrottementError = ContactError          # 31
  | aster.BoucleContactError = ContactError             # 32
  | aster.EventError = ConvergenceError                 # 33
  | aster.ActionError = ConvergenceError                # 34
  | aster.ResolutionError = SolverError                 # 35
  | 
  | On n'utilise plus les numéros directement dans la programmation Fortran mais les "defines" de `asterf.h`:
  | 
  | #define ASTER_ERROR 1
  | #define CONVERGENCE_ERROR 2
  | #define INTEGRATION_ERROR 3
  | #define SOLVER_ERROR 4
  | #define CONTACT_ERROR 5
  | #define TIMELIMIT_ERROR 6
  | 
  | Exemple :
  | call utmess('Z', 'MECANONLINE9_12', num_except=SOLVER_ERROR)
  | 
  | 
  | Fonctionnement
  | --------------
  | 
  | En C++, les exceptions sont définies par template de "ErrorCpp< id >" (les identifiants sont dans `asterf.h`/`astercxx.h`).
  | 
  | L'appel en fortran se fait toujours via 'call utmess' avec 'num_except=...'.
  | La fonction 'uexcp' lance l'exception C++ correspondant à 'num_except':
  | 
  | SOLVER_ERROR => ErrorCpp< SOLVER_ERROR >
  | 
  | On traduit les exceptions C++ en exception Python : "ErrorCpp< id >" traduit en "ErrorPy[id]".
  | 
  | 
  | Remarque
  | --------
  | 
  | En C++11, les spécifications d'exception sont dépréciées (sauf 'noexcept').
  | 
  | -    bool update() throw( std::runtime_error ) {
  | +    bool update() {
  | 
  | Dans l'objet 'TimeStepManager', on renomme 'ConvergenceError' en 'EventError'.
  | 
  | 
  | En passant
  | ----------
  | 
  | On démarre une nouvelle interface Python/C++/Fortran pour remplacer à terme `aster_module.c` et `aster_core_module.c`.
  | 
  | DEFI_MATER_GC : refactoring de l'affectation des paramètres matériau par défaut, ne fonctionnait toujours pas avec les mots-clés non
  | définis.
  | 
  | RAFF_XFEM : Macro qui ne retournait pas l'objet produit.
  | 
  | 
  | Bilan
  | -----
  | 
  | Ceci corrige 29 tests sur 42.
  | 
  | Restent :
  | 
  | erreu01a. .  <F>_ABNORMAL_ABORT. . . . pb rccoma/rcvala
  | erreu03a. .  <F>_ABNORMAL_ABORT. . . . pb rccoma/rcvala
  | erreu05a. .  <S>_ERROR. . . . . . . . .validation sous try
  | erreu09a. .  <S>_ERROR. . . . . . . . .pb passage arguments de l'exception
  | erreu11a. .  NOOK_TEST_RESU. . . . . . assert dans ischar_iden
  | forma43a. .  <S>_ERROR. . . . . . . . .validation sous try
  | sdll137b. .  <S>_ERROR. . . . . . . . .CALC_ESSAI
  | sdll137c. .  <S>_ERROR. . . . . . . . .CALC_ESSAI
  | shll102a. .  <F>_ERROR. . . . . . . . .objet '.NUME.NUEQ' inexistant
  | ssnp125a. .  <S>_ERROR. . . . . . . . .validation sous try
  | ssnp125b. .  <S>_ERROR. . . . . . . . .validation sous try
  | zzzz162a. .  <S>_ERROR. . . . . . . . .LIRE_RESU/EVOL_VARC not yet implemented
  | zzzz208b. .  <S>_ERROR. . . . . . . . .AttributeError: 'GeneralizedModel' object has no attribute 'LIST_SOUS_STRUCT'
  | ------------ --------------------- ---------- ---------- 
  | . 42 tests. .  13 errors

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : asterxx + 42 tests listés dans la fiche
- NB_JOURS_TRAV  : 10.0


================================================================================
                RESTITUTION FICHE 28216 DU 08/11/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Problème POURSUITE
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Je suis en train de corriger fdlv112d et en faisant des poursuites, j'ai un phénomène étrange qui se produit. Lorsque la base est
  | écrite, j'ai ce message qui s'écrit :
  | """
  | champmat                 <class 'code_aster.Objects.materialonmesh_ext.MaterialOnMesh'>
  | rigigen                  <class 'code_aster.Objects.generalizedassemblymatrix_ext.GeneralizedAssemblyMatrixDouble'>
  | rigi_ele                 <class 'code_aster.Objects.elementarymatrix_ext.ElementaryMatrixDisplacementDouble'>
  | nume_ddl                 <class 'libaster.DOFNumbering'>
  | ...
  | """
  | 
  | Mais à relecture, cette fois-ci, j'ai :
  | """
  | champmat                 <class 'code_aster.Objects.generalizedassemblymatrix_ext.GeneralizedAssemblyMatrixDouble'>
  | rigigen                  <class 'code_aster.Objects.elementarymatrix_ext.ElementaryMatrixDisplacementDouble'>
  | rigi_ele                 <class 'libaster.DOFNumbering'>
  | nume_ddl                 <class 'code_aster.Objects.elementarycharacteristics_ext.ElementaryCharacteristics'>
  | ...
  | """
  | 
  | A priori, il y a un décalage entre ce qui est écrit et ce qui est relu. champmat devient un GeneralizedAssemblyMatrixDouble à la
  | place de rigigen qui lui devient un ElementaryMatrixDisplacementDouble.
  | 
  | 
  | Solution :
  | ----------
  | Dans le serializer, il y avait un bug dans le cas où une erreur de "depickling" est survenue.
  | 
  | En fait, on a 2 listes :
  | - Une avec les noms des DataStructure.
  | - Une autre avec les instances de ces DataStructure.
  | 
  | Quand il y a une erreur, on ne peut pas créer l'instance mais on garde néanmoins le nom. Donc cela créé un décalage, le nom est
  | gardé mais on passe à l'instance suivante. Ainsi, on associe un nom avec une instance qui n'a rien à voir.
  | 
  | En corrigeant ce problème, la POURSUITE de fdlv112d fonctionne correctement.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : fdlv112d
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 28135 DU 12/10/2018
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : anomalie concernant asterxx (VERSION 14.3)
- TITRE : INCLUDE dans asterxx
- FONCTIONNALITE :
  | Problème
  | --------
  | Remplacer la commande INCLUDE qui ne sera pas reconduite dans asterxx. Aujourd'hui environ 165 fichiers de commandes utilisent cette
  | fonctionnalité.
  | 
  | Correction
  | ---------- 
  | Plusieurs possibilités sont utilisées pour substituer cette commandes dans les tests :
  |    1- effectuer un "stage" intermédiaire en ajoutant dans le ou les  fichier(s) include actuel(s) les commandes POURSUITE et FIN et
  | modifier le fichier .export associé pour utiliser plusieurs fichiers de commandes s'enchaînant.
  |    2- remplacer le fichier include par une fonction python acceptant quelques arguments, effectuant l'import des commandes aster
  | nécessaires et renvoyant une ou plusieurs valeurs, on effectue alors un import de fichier python dans le fichier de commandes actuel.
  |    3- faire un import du fichier d'include si ce dernier se limite à des instructions en python. 
  | 
  | Le traitement des cas des includes (INCLUDE(DONNEE= ...)) sera effectué en créant un nouveau dépôt regroupant les fichiers associés
  | aux tests, qui sera installé dans le répertoire share/aster en interne (INTRANET).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests src
- NB_JOURS_TRAV  : 9.0


================================================================================
                RESTITUTION FICHE 28204 DU 07/11/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Unsupported value: MODE_FLAMB
- FONCTIONNALITE :
  | Problème :
  | ----------
  | 31 tests plantent avec le message d'erreur suivant :
  | Unsupported value: MODE_FLAMB.
  | 
  | 
  | Solution :
  | ----------
  | Cette exception est levée par la méthode create_result de la macro MODE_ITER_SIMULT.
  | 
  | Je suppose qu'à l'époque la sd_resultat de type mode_flamb devait ne pas exister.
  | 
  | On peut supprimer cette exception.
  | 
  | Mais sur le test ssns102b, ce n'est pas suffisant pour qu'il termine ok. Il faut aussi ajouter des méthodes get et set pour la
  | matrice de rigidité sur l'objet BucklingModeContainer.
  | 
  | Cela répare 27 tests.
  | 
  | Avec ce correctif, le test ssll404a plante toujours à cause d'un type non conforme renvoyé par la macro MODE_ITER_INV. Il y avait un
  | test mal fait qui avait pour conséquence de renvoyer un mode_meca à la place d'un mode_flamb. En corrigeant cela, le test devient ok.
  | 
  | Au total 30 tests sont réparés.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : liste fiche
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28208 DU 07/11/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : NotImplementedError: Unsupported keyword: Cele_c
- FONCTIONNALITE :
  | Problème :
  | ----------
  | 12 tests plantent avec le message suivant :
  | NotImplementedError: Unsupported keyword: Cele_c
  | 
  | 
  | Solution :
  | ----------
  | Cela est dû au fait que CELE_C est un mot-clé complexe. Il est donné dans ces tests sous forme 'RI'. Ce cas de figure n'était pas
  | prévu dans defi_materiau.py.
  | 
  | En modifiant defi_materiau, l'erreur disparaît mais les tests ne vont toujours pas au bout.
  | 
  | On plante ensuite car il manque des méthodes d'ajout de la matrice de rigidité dans la sd mode_meca_c.
  | 
  | Il y a aussi un autre problème, le type retourné par dyna_vibra ne correspond pas au catalogue. Ainsi la macro ne réalisait pas le
  | bon calcul.
  | 
  | En corrigeant l'ensemble de ces problèmes, les 12 tests deviennent ok :
  | ahlv100a ahlv100f ahlv100g ahlv100h ahlv100i ahlv100j ahlv100k ahlv100m ahlv100q ahlv100r ahlv100s ahlv100t

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : liste fiche
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28210 DU 07/11/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : NotImplementedError: Type of field XXXX
- FONCTIONNALITE :
  | Problème :
  | ----------
  | 17 tests plantent avec l'erreur :
  | NotImplementedError: Type of field XXXX
  | 
  | Voici la liste :
  | ssll11g ssls113a ssls122b ssls135a ssnp158a ttll100a ttnp200a zzzz126a zzzz130a zzzz215a zzzz227a zzzz249a zzzz323a zzzz323c
  | zzzz323d zzzz323e zzzz331a
  | 
  | 
  | Solution :
  | ----------
  | Il y a plusieurs problèmes.
  | 
  | Le premier concerne crea_champ et la méthode create_result. Dans cette méthode, il y avait un test trop restrictif qui datait de
  | l'époque où toutes les sd n'existaient pas. En supprimant ce test, on répare de nombreux tests.
  | 
  | Le deuxième problème concerne lire_champ. Ici, de la même manière, il y avait le même type de test restrictif. On le supprime.
  | 
  | Après avoir corrigé ces problèmes, il reste 2 tests cassés.
  | 
  | Le premier est dû au fait que la méthode create_result de crea_champ ne savait pas traiter le cas où on extrait un champ à partir
  | d'un char_meca. En traitant ce cas, on répare ssls122b.
  | 
  | Et le dernier est dû à un platage dans getvc8. Mon correctif de la semaine dernière n'était pas correct, il ne prenait pas en compte
  | le cas où l'utilisateur donne un tuple de valeurs complexes. Je modifie donc getvc8 en conséquence pour que ce cas fonctionne.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : liste fiche
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 28215 DU 08/11/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Erreur Noyau.N_Exception.AsException: type inconnu
- FONCTIONNALITE :
  | Problème :
  | ----------
  | 17 tests plantent avec le message d'erreur suivant :
  | Noyau.N_Exception.AsException: type inconnu: <code_aster.Objects.generalizedassemblymatrix_ext.GeneralizedAssemblyMatrixDouble
  | object at 0x7f11ed3fd5d0> <class 'code_aster.Objects.generalizedassemblymatrix_ext.GeneralizedAssemblyMatrixDouble'>
  | 
  | Voici la liste :
  | fdlv101a fdlv102a fdlv112b fdlv112d fdlv112e fdlv112k forma11c sdll132a sdll132b sdll132c sdll132d sdls106h sdls300a sdlv133a
  | sdnx100a sdnx100b sdnx100f 
  | 
  | 
  | Solution :
  | ----------
  | Pour faire fonctionner un maximum de ces cas tests, je supprime INCLUDE dans fdlv112d, sdnx100a, sdnx100b et sdnx100f.
  | 
  | J'ajoute aussi un pointeur vers MechanicalModeContainer dans GeneralizedAssemblyMatrix pour dyna_iss_vari.
  | 
  | Dans la commande lire_impe_miss, j'ajoute un appel à GeneralizedAssemblyMatrix.setGeneralizedDOFNumbering dans post_exec.
  | 
  | Dans la commande proj_matr_base, j'ajoute un appel à GeneralizedAssemblyMatrix.setModalBasis dans post_exec.
  | 
  | Modification de dyna_iss_vari qui ne renvoyait (pas de return) rien alors que la capy prévoyait un tran_gene. Je supprime aussi
  | l'appel get_concept (qui n'existe plus) en utilisant le MechanicalModeContainer ajouté dans GeneralizedAssemblyMatrix.
  | 
  | J'ajoute aussi __getstate__ et __setstate__ à GeneralizedAssemblyMatrixComplex pour que la poursuite se passe bien avec ces objets.
  | 
  | Suppression du message d'erreur de ResultsContainer.getModel qui provoquait un plantage dans la POURSUITE.
  | 
  | Je supprime le UL.EtatInit qui est encore présent dans miss_calcul.py et je le remplace par UL.Etat(ulaster, etat="F").
  | 
  | Je modifie le type retourné par dyna_vibra car le cas harm_gene n'était pas prévu.
  | 
  | Je rajoute l'attribut cata_sdj qui manquait à DynamicMacroElement et à HarmoGeneralizedResultsContainer.
  | 
  | J'ajoute les fonctions EXTR_MATR_GENE à GeneralizedAssemblyMatrixDouble et GeneralizedAssemblyMatrixComplex.
  | 
  | J'ajoute EXTR_VECT_GENE_C, RECU_VECT_GENE_C aux classes GeneralizedAssemblyVectorComplex et GeneralizedAssemblyVectorDouble.
  | 
  | En faisant tout cela, on répare 10 tests.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : liste fiche
- NB_JOURS_TRAV  : 1.5


================================================================================
                RESTITUTION FICHE 28237 DU 16/11/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Erreur "KeyError: XXX"
- FONCTIONNALITE :
  | Problème :
  | ----------
  | De tests plantent avec l'erreur "KeyError: XXX".
  | 
  | 
  | Solution :
  | ----------
  | Il subsiste encore des usages imprudents des dictionnaires dans les macros.
  | 
  | Pour corriger ces problèmes, on remplace l'usage de l'opérateur [] par un get sur le dictionnaire.
  | 
  | En faisant cela, on répare 23 tests.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : liste fiche
- NB_JOURS_TRAV  : 0.01


================================================================================
                RESTITUTION FICHE 28013 DU 06/09/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Message d'erreur "* takes exactly ** arguments (*** given)"
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Les tests sdnd123a, sdld102e, sdll149c, sdls106e, sdls106f, sdlv132b et zzzz317f plantent avec le message d'erreur suivant : "*
  | takes exactly ** arguments (*** given)"
  | 
  | Les commandes qui posent problèmes sont les suivantes : 
  | CALC_STABILITE
  | CREA_ELEM_SSD
  | IMPR_ACCE_SEISME
  | 
  | 
  | Solution :
  | ----------
  | On remplace les arguments par des get sur **args dans CALC_STABILITE, CREA_ELEM_SSD et IMPR_ACCE_SEISME et l'erreur disparaît.
  | 
  | On répare ainsi 4 tests.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : liste fiche
- NB_JOURS_TRAV  : 0.01


================================================================================
                RESTITUTION FICHE 28276 DU 26/11/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant asterxx (VERSION 14.3)
- TITRE : asterxx - merge invalide
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Erreur lors de la fusion avec la branche 'default', le module elim_lagr_comp_module a été supprimé mais est encore utilisé dans
  | matasspetsc.F90.
  | 
  | 
  | Correction
  | ----------
  | 
  | On supprime les appels qui restent.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : build & tests
- NB_JOURS_TRAV  : 0.5

