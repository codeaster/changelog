==================================================================
Version 14.2.13 (révision d0b0869989b6) du 2018-10-12 14:04 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 28089 COURTOIS Mathieu         Noms des paramètres des formules différents en Python et Fortran
 28099 KHAM Marc                coquille: write trainant dans la loi de rankine
 27876 MATHIEU Tanguy           [RUPT] [Chantier CALC_G] – DEFI_FOND_FISS : modification de la définition ...
 27962 YU Ting                  D124.18 - Introduction d'un test de convergence pour la méthode LAC
 27943 LEFEBVRE Jean-Pierre     Mot-clef SOUS_TITRE incorrect dans IMPR_RESU
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 28089 DU 28/09/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Noms des paramètres des formules différents en Python et Fortran
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les noms des paramètres des formules sont stockés en Python (.nompar) et en fortran (.NOVA).
  | Dans le .NOVA, c'est un K8. Dans le Python, on prend les noms tels quels.
  | 
  | Si on évalue une formule avec des noms longs, ça fonctionne en Python, ça planterait en Fortran.
  | Si on limite les noms à 8 caractères dans '.nompar', on casse une douzaine de tests dans la liste du submit.
  | 
  | 
  | Évolution
  | ---------
  | 
  | On augmente la taille des chaines de caractères dans l'objet Jeveux '.NOVA' à 24 caractères.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : d4.02.02
- VALIDATION : submit
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28099 DU 01/10/2018
================================================================================
- AUTEUR : KHAM Marc
- TYPE : anomalie concernant code_aster (VERSION 14.5)
- TITRE : coquille: write trainant dans la loi de rankine
- FONCTIONNALITE :
  | une coquille est restée dans la loi de Rankine:
  | 
  | un write(6,*) est resté non commenté
  | 
  | ce qui fait que des lignes blanches sont affichées.
  | 
  | report en stable
  | sans impact doc. sans ajout test

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas-test
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 27876 DU 10/07/2018
================================================================================
- AUTEUR : MATHIEU Tanguy
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [RUPT] [Chantier CALC_G] – DEFI_FOND_FISS : modification de la définition du front de fissure
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Dans le cadre du chantier de refonte de l’opérateur CALG_G, le contenu et l’utilisation de l’opérateur DEFI_FOND_FISS sont aussi
  | repensés (la structure de données sd_fond_fiss étant une entrée de CALC_G).
  |  
  | L’objet de cette fiche est de modifier la manière de définir le front de fissure.
  |     En 2D : la définition du fond de fissure par le mot-clé NŒUD doit être interdite. Seule la définition par un groupe de nœuds
  | doit être autorisée.
  |     En 3D : la définition du front de fissure par les mot-clé NŒUD, GROUP_NO et MAILLE doit être interdite. Seule la définition par
  | un groupe de mailles doit être autorisée. 
  | Une erreur sera générée dans le fortran (et non dans le catalogue) en cas d’utilisation en 3D du mot clé GROUP_NO. La mise en place
  | de cette erreur fera l’objet la fiche issue28061.
  | 
  | Correction/Développement
  | ------------------------
  | Modification du catalogue de commande defi_fond_fiss.py conformément aux deux paragraphes ci-dessus.
  | Modification des fichiers de commande des cas test utilisant DEFI_FOND_FISS en cohérence.
  | Modification de la documentation U4.82.01 en cohérence.
  | Les maillages n'ont pas été modifiés, quand cela était nécessaire on a créé les groupes souhaités avec DEFI_GROUP.
  | 
  | Résultat faux
  | -------------
  | Sans objet

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.82.01
- VALIDATION : passage des tests mentionnés


================================================================================
                RESTITUTION FICHE 27962 DU 22/08/2018
================================================================================
- AUTEUR : YU Ting
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : D124.18 - Introduction d'un test de convergence pour la méthode LAC
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Introduction des tests de convergence pour la méthode LAC en 2D et 3D
  | 
  | 
  | Développement
  | -------------
  | 
  | On introduit 13 nouvelles modélisations aux cas tests pour couvrir la méthode LAC, dont 4 modélisations sont dans ssnp150 pour 2D
  | avec des maillages de 6 finesses :
  | - modélisation c test TRIA3
  | - modélisation d test TRIA6
  | - modélisation e test QUAD4
  | - modélisation f test QUAD8
  | 
  | dont 9 modélisations sont dans ssnv219 pour 3D avec des maillages de 4 ou 5 finesses :
  | - modélisation e test TETRA4
  | - modélisation f test TETRA10
  | - modélisation g test HEXA8
  | - modélisation h test HEXA20 
  | - modélisation i test HEXA27
  | - modélisation j test PYRA5
  | - modélisation k test PYRA13 
  | - modélisation l test PENTA6
  | - modélisation m test PENTA15 
  | 
  | Leurs résultats sont comparés avec les valeurs théoriques en termes de taux de convergence pour le déplacement et la pression. Mais
  | on observe souvent des sur-convergences qui viennent du raffinement de maillage pour la géométrie courbée de contact. Donc
  | validations plutôt par des valeurs de non régression.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V6.03.150 V6.04.219
- VALIDATION : tests analytique / de non régression
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 27943 DU 03/08/2018
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : anomalie concernant Documentation (VERSION 11.*)
- TITRE : Mot-clef SOUS_TITRE incorrect dans IMPR_RESU
- FONCTIONNALITE :
  | On corrige les documents U4.91.01 et U4.03.01 suite à la modification du type associé au mot clé TITRE ou Sous_TITRE qui passe de
  | liste de chaîne de caractères à chaîne de caractères. 
  | Il faudra corriger au fil de l'eau les documents U dont seul le paragraphe syntaxe contient encore le type l_Kn associé au mot clé
  | TITRE ou SOUS_TITRE

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 0.1

