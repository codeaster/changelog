==================================================================
Version 14.5.12 (révision 9b26e4a5a467) du 2020-04-07 12:20 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29737 ABBAS Mickael            Ecrasement mémoire probable en XFEM+THM
 29733 GEOFFROY Dominique       Bug COMB_SISM_MODAL multi-appui appui décorrelé avec excitation multidirectio...
 29446 GEOFFROY Dominique       En version 14.4.15 (14.5) le cas-test perf013g est en NO_RESU_FILE sur GAIA (...
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29737 DU 02/04/2020
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Ecrasement mémoire probable en XFEM+THM
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les routines en-dessous du te0588 ne sont pas prudentes: elles écrivent parfois dans les contraintes+ et les variables internes+
  | alors que l'option ne le permet pas (en prédiction par exemple)
  | Risque d'écrasement (aléatoire).
  | 
  | 
  | Correction
  | ----------
  | 
  | On protège ces écritures en testant l'option avant.
  | Comme pour les joint hydros (JHMS), on a le droit qu'aux comportements élastiques.
  | Pour la vérif dans les catalogues des lois de comportement on change l'attribut du catalogue de TYPMOD2='THM' à TYPMOD2='XFEM_HM',
  | Il a fallu modifier Cond_Calcul pour les options CHAR_MECA_FLUX_*, CHAR_MECA8TEMP_R et FLHN_ELGA

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- DEJA RESTITUE DANS : 15.1.14
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29733 DU 01/04/2020
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 13.5)
- TITRE : Bug COMB_SISM_MODAL multi-appui appui décorrelé avec excitation multidirectionnelle
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | L’opérateur COMB_SISM_MODAL en MULTI_APPUI=’DECORRELE’, avec des excitations multi-axiales (plusieurs AXE ou TRI_SPEC=’OUI’), avec le mot-clé 
  | GROUP_APPUI précisé pour définir les groupes d’appuis décorrélés donne l’erreur suivante : 
  |    !------------------------------------------------------------------------------!
  |    ! <EXCEPTION> <SEISME_29>                                                      !
  |    !                                                                              !
  |    !   La définition du groupe d'appuis n'est pas correcte dans le cas décorrélé: !
  |    !   au moins une excitation appartient à plusieurs groupes d'appuis.           !
  |    !   Les groupes d'appuis doivent être disjoints.                               !
  |    !------------------------------------------------------------------------------!
  | 
  | Analyse :
  | =======
  | 
  | Il a été établi que ce message pouvait être envoyé non seulement si le nœud support avait été
  | défini lors d'une occurrence précédente de GROUP_APPUI mais aussi dans la même occurrence si l'appui a déjà été concerné par une
  | autre direction. Cela concerne le multi-appui décorrélé. Cela expliquerait alors que ça passe en monoaxial et pas en triaxial.
  | 
  | Correction :
  | ==========
  | 
  | Dans la routine ascalc, on change la condition menant à l'émission du message SEISME_29 : le message n'est pas émis s'il s'agit de la 
  | même occurrence avec une direction différente.
  | 
  | Validation :
  | 
  | On ajoute à la modélisation A deux appels à COMB_SISM_MODAL (par duplication du dernier COMB_SISM_MODAL du test).
  | Ces deux nouveaux appels valideront les combinaisons :
  | - MULTI_APPUI='CORRELE'   + TRI_SPEC (même reférence ANALYTIQUE que le calcul MONO_APPUI)(déjà fonctionnel mais non testé)
  | - MULTI_APPUI='DECORRELE' + TRI_SPEC (reférence NON_REGRESSION) (cas qui ne fonctionnait pas avant correction)
  | 
  | 
  | Doc : 
  | V2.05.002 : mise à jour de la modélisation A de sdlx02, on précisant bien que pour le cas décorrélé, on ne se trouve pas dans un réel exemple d'utilisation
  | avec la même excitation aux deux appuis.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V2.05.002
- VALIDATION : sdlx02a


================================================================================
                RESTITUTION FICHE 29446 DU 06/01/2020
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 14.5)
- TITRE : En version 14.4.15 (14.5) le cas-test perf013g est en NO_RESU_FILE sur GAIA (mpi)
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | En version 14.4.15 (14.5) le cas-test perf013g est en NO_RESU_FILE sur GAIA (mpi)
  | 
  | 
  | Correction :
  | ==========
  | 
  | Problème rencontré en v15.
  | On reporte issue29297 en v14.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas-test OK

