==================================================================
Version 14.1.19 (révision 3d798433f4b1) du 2018-06-07 20:15 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 26753 DU 24/07/2017
AUTEUR : DEGEILH Robin
TYPE evolution concernant code_aster (VERSION 14.1)
TITRE
    [RUPT] Modification maillages des tests perfe02a et ssnv108a issus du plugin CT
FONCTIONNALITE
   Problème:
   =========
   Suite à la fiche issue26697 sur des erreurs dans les tests unitaires du plugin CT (suite à évolution des mailleurs), il faut changer
   les maillages des tests code_aster perfe02a et ssnv108a.
   
   Solution:
   =========
   J'ai re-généré les maillages des cas tests aster ssnv108a et perfe02a en utilisant les tests du même nom dans smeca (avec la version
   V8_INTEGR du 24/05/2018, donc a priori basé sur Salome 8.5 RC2). Les maillages sont visuellement très proches des maillages
   originaux. J'ai mis à jour les valeurs des tests qui étaient en NOOK (écart max de 1.3%).
   
   Impact doc:
   =========
   Mise à jour des valeurs de non-régression dans les doc des cas tests
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V6.04.108, V1.02.012
VALIDATION
    ssnv108a, perfe02a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27370 DU 01/02/2018
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant code_aster (VERSION 14.2)
TITRE
    Erreur de compilation avec Intel2018
FONCTIONNALITE
   Problème :
   ========
   Les routines lub_module.F90, yacs_module.F90 et yacsnl_module.F90 ne compilent pas avec la version 2018 du compilateur Intel ifort :
   l'affectation de la valeur du pointeur dans la déclaration est refusée. 
   Correction :
   ==========
   On sépare en deux instructions la déclaration et l'affectation du pointeur.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    compilation des routines lub_module.F90, yacs_module.F90 et yacsnl_module.F90
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27492 DU 22/03/2018
AUTEUR : ABBAS Mickael
TYPE anomalie concernant code_aster (VERSION 11.8)
TITRE
    FAUX - Blindage LIAISON_UNIL et CONTACT discret contre les matrices non-symétriques
FONCTIONNALITE
   Problème
   --------
   
   Avec la mise en place ALGO_CONT='PENALISATION' pour LIAISON_UNIL, on souhaite interdire l’utilisation de matrices non-symétriques
   avec l’algorithme des contraintes actives, qu’il soit utilisé en formulation DISCRETE ou LIAISON_UNIL.
   
   
   Correction
   ----------
   
   En V14, on passe les messages en erreur fatale et on change les conseils:
   1/ Pour LIAISON_UNILATER: des algorithmes de type PENALISATION 
   2/ Pour le contact: la formulation CONTINUE ou la pénalisation.
   
   En V13, Pour LIAISON_UNILATER, on invite l'utilisateur à se mettre dans une version plus récente.
   
   Résultat faux
   -------------
   
   L'utilisation conjointe de LIAISON_UNILATER et de matrices non-symétriques peut conduire à des résultats faux.
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 13.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 14.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    src
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27632 DU 26/04/2018
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant code_aster (VERSION 14.4)
TMA : Necs
TITRE
    Erreur <JEVEUX_26> dans CREA_RESU
FONCTIONNALITE
   Problème :
   ========
   Lors de l'appel à CREA_RESU dans une boucle python for, on obtient un message d'erreur du type
   
      !-------------------------------------------------------------!
      ! <EXCEPTION> <JEVEUX_26>                                     !
      !                                                             !
      ! Objet inexistant dans les bases ouvertes : .MODELE    .LGRF !
      ! l'objet n'a pas été créé ou il a été détruit                !
      ! Ce message est un message d'erreur développeur.             !
      ! Contactez le support technique.                             !
      !-------------------------------------------------------------!
   
   Analyse :
   =======
   Le problème survient car CREA_RESU, en mode "reuse" et quand un pas de temps a déjà été stocké, fait quelques vérifications
   complémentaires qui ne sont pas blindées. Il faut protéger l'appel à dismoi dans lrcomm en évitant de lui passer un nom de modèle
   vide (c'est le cas dans crtype si nb_modele > 1) Dans ce cas il faut émettre un message d'erreur en suggérant d'ajouter le nom du
   modèle dans CREA_RESU.
   
   Correction effectuée :
   ====================
   On ajoute une vérification que le nom du modèle n'est pas vide avant l'appel à dismoi. S'il est vide on émet un message d'erreur
   pour prévenir que le modèle est absent et qu'il faut renseigner le mot-clé MODELE. 
   
   Validation : bonne émission du message sur le test fourni.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test fourni

--------------------------------------------------------------------------------
RESTITUTION FICHE 27513 DU 29/03/2018
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant code_aster (VERSION 14.1)
TMA : Necs
TITRE
    Erreur de programmation dans PROJ_CHAMP
FONCTIONNALITE
   Problème:
   ========
   Lorsqu'on utilise PROJ_CHAMP en version unstable sur aster5 pour post-traiter une 
   étude Vercors, on obtient le message d'erreur suivant :
   
   Description de l'erreur :
      !-------------------------------------------------------!
      ! <EXCEPTION> <DVP_1>                                   !
      !                                                       !
      ! Erreur de programmation.                              !
      !                                                       !
      ! Condition non respectée:                              !
      !     .false.                                           !
      ! Fichier pjefca.F90, ligne 163                         !
      !                                                       !
      !                                                       !
      !                                                       !
      ! Il y a probablement une erreur dans la programmation. !
      ! Veuillez contacter votre assistance technique.        !
      !-------------------------------------------------------!
   
   Analyse :
   =======
   
   Dans le cadre de issue27226, j'ai modifié le traitement des mots-clés TOUT_2, GROUP_MA_2 et MAILLE_2 de VIS_A_VIS. Précédemment en
   construisait directement une liste de nœuds à partir de ces données (plus celles issus de GROUP_NO_2 et NOEUD_2). Maintenant, on
   récupère d'abord les mailles afin de supprimer les mailles qui ne sont pas de la même dimension topologique que la maille de la plus
   grande dimension topologique. Pour trouver cette dimension, j'utilise la routine pjefca comme cela est fait dans les cas TOUT_1,
   GROUP_MA_1 ...
   
   Or dans le cas présent, le maillage sur lequel on souhaite projeter contient seulement des éléments POI1 et la routine pjefca ne
   prend pas en compte la dimension topologique 0, d'où le ASSERT.
   
   Le problème se corrige facilement en intégrant le cas de la dimension 0 au cas de la dimension 1.
   
   Point sur lequel faire attention :
   --------------------------------
   Si un utilisateur cherche à projeter un champ sur un groupe de maille ne contenant que des POI1 (avant ou après correction)(mot-clé
   GROUP_MA_1), il va tomber sur le même ASSERT que celui du présent problème. Avec la modification que je propose pour corriger ce
   problème, le calcul va aller un peu plus loin et l'utilisateur obtiendra le message suivant au lieu du ASSERT :
   
   
      !--------------------------------------!
      ! <EXCEPTION> <CALCULEL4_57>           !
      !                                      !
      !  Il n'y a pas de mailles a projeter. !
      !--------------------------------------!
   
   Ce message ne me semble pas très clair. Pour clarifier les choses je propose le message suivant :
   
      !------------------------------------------------------------------------------!
      ! <EXCEPTION> <CALCULEL4_57>                                                   !
      !                                                                              !
      !  Aucune des mailles du maillage 1 fournies ne permet d'effectuer la          !
      !  projection souhaitée.                                                       !
      !                                                                              !
      !  Conseil :                                                                   !
      !     Vérifiez que les mailles fournies ne sont pas toutes ponctuelles (POI1). !
      !------------------------------------------------------------------------------!
   
   Travail effectué :
   ================
   
   1- Modification de pjefca pour que les mailles POI1 soient considérées de dimension topologique 1D. (ce qui règle le problème de
   cette fiche)
   2- Modification du message CALCULEL4_57 pour qu'il soit plus clair pour l'utilisateur comme proposé ci-dessus.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test perso

--------------------------------------------------------------------------------
RESTITUTION FICHE 27666 DU 14/05/2018
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE evolution concernant code_aster (VERSION 14.2)
TITRE
    Alarme contact3_97
FONCTIONNALITE
   Objet de la fiche : 
   ===================
   clarté de <A> CONTACT3_97
   
   Résolution : 
   ===========
   
   Améliorer PENE_MAXI dans la documentation U4.44.11.
   
   D'autre part, on propose de supprimer purement et simplement le message d'alarme contact3_97:
   - il n'apporte pas une information pertinente à l'utilisateur ;
   - l'utilisateur ne peut rien faire pour l'enlever ;
   - le mettre en <I> engendrerait encore beaucoup de texte dans le fichier de message.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : U4.44.11
VALIDATION
    sources
NB_JOURS_TRAV  : 1.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27544 DU 04/04/2018
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant code_aster (VERSION 11.8)
TMA : Necs
TITRE
    Affichages intempestifs dans MED
FONCTIONNALITE
   Problème :
   ========
   la fiche issue26834 réactive certains messages intempestifs de MED à l'ouverture d'un fichier.
     IMPR_RESU(FORMAT='MED',
               RESU=_F(RESULTAT=RESUNL,
                       INFO_MAILLAGE='NON',
                       IMPR_NOM_VARI='OUI',),
               UNITE=80,
               INFO=1,
               PROC0='OUI',)
   
   /home/I27518/TRAV2018/Y2/V8_PRE_default_codeaster_prerequisites_calibre_9_mpi/tools/src/Medfichier-331-hdf51814/src/ci/MEDfileVersionOpen.c
   [71] : Erreur à l'ouverture du fichier 
   /home/I27518/TRAV2018/Y2/V8_PRE_default_codeaster_prerequisites_calibre_9_mpi/tools/src/Medfichier-331-hdf51814/src/ci/MEDfileVersionOpen.c
   [71] : fort.80
   
   Analyse :
   =======
   Le message med est écrit car on tente d'ouvrir un fichier med en mode lecture alors que le fichier MED n'a pas été créé. Cependant,
   ce qui est embêtant est que les fichiers fort.80, ou par exemple fort.97, si on choisit l'unité 97, existent déjà sans avoir été
   créés par MED.
   
   La routine as_mfiope, renvoie un code retour négatif en plus du message. Mais dès que l'appel est fait en mode création (3) (dans
   irmhdf), le fichier fort.80 devient un fichier med et le problème disparaît. 
   
   J'ai tenté d'utiliser la routine mfiexi, qui teste l'existence du fichier, mais elle teste justement seulement l'existence du
   fichier, elle ne regarde pas si c'est une fichier med. L'autre seule routine de MEDFile*** que l'on peut appeler sans avoir ouvert
   le fichier est justement MEDFileCompatibility, mais elle ressort avec un code retour négatif et émet elle aussi un message.
   
   Il faut donc voir pourquoi le fichier est déjà existant. Cela est du à l'appel à ulopen dans op0039. Cet utilitaire ouvre le fichier
   tout en gérant la table des unités logiques. Avec l'aide de Jean-Pierre, nous avons constaté que dans DEFI_FICHIER, ce n'est pas
   ulopen mais uldefi qui est utilisé pour les fichiers binaires. Cette routine, contrairement à ulopen, ne crée pas le fichier,
   cependant elle n'est pas tout à fait équivalente car elle ne permet pas de modifier les informations sur le fichier MED (F_MED), ce
   qui pose problème si on souhaite écrire des résultats MED dans des fichiers différents.
   
   Correction :
   ==========
   On duplique ulopen en ulaffe en supprimant le open (et les close). J'appelle ulaffe dans
   op0039 dans le cas med. Les tests utilisant IMPR_RESU fonctionnent en v13 et v14.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tests utilisant impr_resu

--------------------------------------------------------------------------------
RESTITUTION FICHE 27644 DU 30/04/2018
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant code_aster (VERSION 14.1)
TITRE
    En version 14.1.14, augmentation des temps CPU de MODI_MODELE_XFEM
FONCTIONNALITE
   Problème
   --------
   
   En version 14.1.14, on observe une augmentation des temps CPU de MODI_MODELE_XFEM
   suite à [#27342] allocate some large arrays on the heap in te0514
   
   158.2% sslv315h   MODI_MODELE_XFEM_26     0.91     2.35  te0074 fiche
   ...
   106.8% zzzz358a   MODI_MODELE_XFEM_17     0.88     1.82
   
   Analyse
   -------
   
   Dans issue27342, on a remplacé des tableaux automatiques de très grandes tailles par des allocations
   Fortran dans le TE principal de MODI_MODELE_XFEM.
   Cela a eu logiquement pour effet de dégrader les performances de MODI_MODELE_XFEM. On ne reviendra pas sur cette modification
   nécessaire compte-tenu du danger des tableaux automatiques dans ce cas de figure (plusieurs Mo).
   
   Sur la plupart des cas-tests, ce changement est sans conséquence car l'appel à MODI_MODELE_XFEM est négligeable devant celui de la
   résolution (quelques secondes).
   
   Toutefois, les cas-tests effectuant de la propagation de fissure (PROPA_FISS) appellent régulièrement MODI_MODELE_XFEM sur des
   modèles de taille croissante. Cela conduit donc à augmenter beaucoup plus  nettement le temps de ceux-ci. En particulier, le temps
   alloué dans le fichier .export pour quelques cas-tests est très nettement inférieur au temps effectif sur Aster5.
   
   Correction
   ----------
   
   On remplace les allocations dynamiques faites avec AS_ALLOCATE par l'instruction native "allocate". Cela permet de regagner
   légèrement en performance mais reste toutefois beaucoup plus lent que les tableaux automatiques. Exemple sur sslv320c avec Calibre 9 :
   - temps avant issue27342 : 25s
   - temps après issue27342 : 70s
   - temps après remplacement des AS_ALLOCATE : 60s
   
   On corrige les cas-tests dont le temps effectif n'est plus en accord avec le fichier export (tout en restant inférieur à 300s).
   
   sslv320a :
   - time_limit 60
   - temps Aster5 : 95s
   ==> on passe à time_limit 150
   
   sslv320b :
   - time_limit 45
   - temps Aster5 : 125s
   ==> on passe à time_limit 150
   
   sslv320c :
   - time_limit 70
   - temps Aster5 : 100s
   ==> on passe à time_limit 150
   
   sslv320c :
   - time_limit 60
   - temps Aster5 : 75s
   ==> on passe à time_limit 150
   
   zzzz255a :
   - time_limit 100
   - temps Aster5 : 80s
   ==> on passe à time_limit 150
   
   zzzz255b :
   - time_limit 100
   - temps Aster5 : 110s
   ==> on passe à time_limit 150
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tests XFEM
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25081 DU 12/04/2016
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant code_aster (VERSION 11.8)
TITRE
    Problème de compilation avec gcc et metis5
FONCTIONNALITE
   Il y a un problème de compilation avec gcc avec metis5. Ce problème ne se produit que dans code_aster. C'est peut-être dû à un
   conflit avec nos includes ou ceux de Python.
   Un *hotfix* (révision 9b82612d53f8 du dépôt metis), permet de contourner le problème, le dépôt metis n'évoluant pas, il n'est pas
   gênant de disposer de notre propre correction.
   Fiche mise sans suite
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26953 DU 27/09/2017
AUTEUR : TRAN Van-xuan
TYPE anomalie concernant code_aster (VERSION 13.4)
TITRE
    Erreur de construction dans POST_T_Q
FONCTIONNALITE
   Problème
   ========
   
   '''
   La macro POST_T_Q plante lors de mon étude, avec le message suivant :
   
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         ! Erreur dans la macro POST_T_Q                                                    !
         ! Traceback (most recent call last):                                               !
         !    File "/home/E46273/salome_meca/V2017/tools/Code_aster_stable-v134_smeca/lib/  !
         ! aster/Build/B_MACRO_ETAPE.py", line 115, in _Build                               !
         !     ier = apply(self.definition.proc, (self,), d)                                !
         !    File "/home/E46273/salome_meca/V2017/tools/Code_aster_stable-v134_smeca/lib/  !
         ! aster/Noyau/N_OPS.py", line 35, in __call__                                      !
         !     return func(*args, **kwargs)                                                 !
         !    File "/home/E46273/salome_meca/V2017/tools/Code_aster_stable-v134_smeca/lib/  !
         ! aster/Contrib/post_t_q_ops.py", line 2432, in post_t_q_ops                       !
         !     self, pgl, ds, Ss, di, Si, Sh, Sv, Sq, INFO, FISSURE, syme_char, abscs,      !
         ! ndim)                                                                            !
         !    File "/home/E46273/salome_meca/V2017/tools/Code_aster_stable-v134_smeca/lib/  !
         ! aster/Contrib/post_t_q_ops.py", line 1468, in get_saut                           !
         !     sauttt [1]= NP.array ([[(Splq[0][j])/Sig_Y for j in range (len (Spls[1]))]]) !
         !  IndexError: index 12 is out of bounds for axis 0 with size 12                   !
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         ! Erreurs dans la construction de la macro POST_T_Q !
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   '''
   
   Correction
   ----------
   
   La commande POST_T_Q est hors du périmètre qualifié de code_aster (son appel se fait manuellement depuis le module Contrib).
   
   Le bug signalé ici ne sera donc pas corrigé tant que cette commande n'est pas officiellement intégrée dans le code, avec un
   responsable identifié.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

