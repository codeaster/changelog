==================================================================
Version 14.6.11 (révision e019c77d878c) du 2020-12-11 12:17 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 30436 GENIAUT Samuel           [RUPT] Utilisation de post_k1_k2_k3 sur un bi-matériau
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 30436 DU 17/11/2020
================================================================================
- AUTEUR : GENIAUT Samuel
- TYPE : anomalie concernant code_aster (VERSION 14.6)
- TITRE : [RUPT] Utilisation de post_k1_k2_k3 sur un bi-matériau
- FONCTIONNALITE :
  | Problème :
  | ---------
  | 
  | Dans l'étude en PJ, on cherche à calculer les facteurs d'intensité de contraintes via l'opérateur POST_K1_K2_K3, sur une fissure débouchant sur une interface bi-matériau. 
  | Malgré les restrictions du front à une partie "mono-matériau", le calcul s’arrête dans POST_K1_K2_K3 par une erreur développeur dvp_1.
  | 
  | 
  | Analyse :
  | ---------
  | 
  | Dans cette configuration, le front de fissure à traiter possède un nœud situé sur une interface bi-matériau.
  | La récupération automatique du matériau via la sd_resultat dans POST_K1_K2_K3 ne fonctionne pas.
  | En effet, celle-ci est basée sur la recherche du matériau sur un voisinage des mailles du front, et dans ce cas,
  | on est arrêté par ASSERT( vk8_mater(icmp) .eq. vcesv(iad) )dans la routine postkutil.F90.
  | Pour traiter ces configurations particulières, on peut "écraser" le matériau de la sd_resultat en renseignant
  | le mot-clé MATER de POST_K1_K2_K3. Toutefois, même si MATER est renseigné, la routine postkutil est appelée à l'identique
  | et on s’arrête au même endroit. Ce problème est dû à la manière dont est programmée la macro post_k1_k2_k3_ops.py.
  | On appelle systématique postkutil.F90 qui renvoie la modélisation et le matériau. Puis ensuite, on traite le cas où le
  | mot-clé MATER est renseigné. Il faudrait en réalité que dans le cas où MATER est renseigné,
  | on ne fasse pas la recherche du matériau via postkutil. On pourrait ne pas appeler postkutil lorsque MATER est renseigné
  | mais postkutil est indispensable pour récupérer la modélisation. Une solution est alors de modifier le fonctionnement de postkutil.
  | 
  | 
  | Correction :
  | ----------
  | 
  | On remplace le ASSERT( vk8_mater(icmp) .eq. vcesv(iad) ) dans la routine postkutil.F90 par un message d'erreur : 
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <EXCEPTION> <RUPTURE1_13>                                                                      ║
  |  ║                                                                                                ║
  |  ║ La recherche du matériau associé au front de fissure a échouée. Cela arrive notamment quand    ║
  |  ║ des noeuds du front du fissure sont situés à la frontière entre plusieurs matériaux.           ║
  |  ║                                                                                                ║
  |  ║ Solution : Renseigner le mot-clé MATER de POST_K1_K2_K3 avec le matériau souhaité.             ║
  |  ║                                                                                                ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | On modifie ensuite post_k1_k2_k3_ops.py et postkutil.F90 de manière à ce que lorsque MATER est renseigné,
  | postkutil ne cherche pas à trouver le matériau et ne renvoie que la modélisation.
  | Pour cela, on modifie également l'interface C permettant d'appeler postkutil.F90 depuis python.
  | Rq : il a été nécessaire de supprimer le dossier build de src pour que cette modification soit prise en compte!!
  | 
  | Validation :
  | ----------
  | 
  | Sur le test fourni, on a vérifié la bonne émission du message d'erreur.
  | On a ensuite ajouté le mot-clé MATER et constaté que le calcul se terminait alors sans erreur.
  | On a également lancé tous les tests de src utilisant POST_K1_K2_K3.
  | 
  | 
  | Impact doc : 
  | ----------
  | Aucun

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test fourni
- DEJA RESTITUE DANS : 15.2.22

