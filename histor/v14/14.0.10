==================================================================
Version 14.0.10 (révision 7964c9a398bd) du 2017-09-28 14:06 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 26920 DU 20/09/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    [perform] POST_ELEM/Weibull
FONCTIONNALITE
   Lorsqu'on omet de définir les paramètres du comportement WEIBULL dans la commande DEFI_MATERIAU, on s'arrête brutalement dans la
   commande POST_ELEM / WEIBULL avec un message du type :
      ! <F> <JEVEUX_26>                                            !
      !                                                            !
      ! Objet inexistant dans les bases ouvertes : .MATERIAU.NOMRC !
   
   En fait la routine peweib appelée par la commande POST_ELEM ne testait pas le paramètre en retour de la routine chmrck : dans le cas
   où on ne trouve pas le comportement WEIBULL elle renvoie une valeur nulle qui n'était pas testée. On s'arrêtait brutalement dans
   rccome car le nom du matériau était vide.
   On ajoute un message d'erreur fatale si la valeur renvoyée est nulle :
      !---------------------------------------------------------------------------!
      ! <F> <UTILITAI6_59>                                                        !
      !                                                                           !
      !  La définition des paramètres du comportement WEIBULL n'a pas été trouvée !
      !  dans le champ de matériau CM                                             !
      !                                                                           !
      !                                                                           !
      ! Cette erreur est fatale. Le code s'arrête.                                !
      !---------------------------------------------------------------------------!
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssna108a modifié
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26909 DU 18/09/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version  14.0.7-c51704c56772, le test zzzz337a échoue sur AthosDev_mpi
FONCTIONNALITE
   Problème :
   ==========
   En version MPI, le test zzzz337a échoue dans la commande CALC_MODE. 
   
   Analyse :
   =========
   Le solveur MUMPS n'est pas utilisable dans le calcul du concept mode22, on s'arrête avec l'erreur suivante : 
   
      !-------------------------------------------------------------------------------------------------------!
      ! <F> <ALGELINE5_72>                                                                                    !
      !                                                                                                       !
      !     Par précaution, cette fonctionnalité est pour l'instant interdite avec le solveur linéaire MUMPS. !
      !                                                                                                       !
      !     Conseil :                                                                                         !
      !     Changer de solveur linéaire : sous le mot-clé facteur SOLVEUR,                                    !
      !     poser METHODE='MULT_FRONT'.                                                                       !
      !                                                                                                       !
      !                                                                                                       !
      ! Cette erreur est fatale. Le code s'arrête.                                                            !
      !-------------------------------------------------------------------------------------------------------!
   
   Correction :
   ============
   On utilise MULT_FRONT comme solveur, mais comme le test utilise 4 cpus, il est nécessaire d'augmenter le nombre de sous-bandes
   fréquentielles.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz337a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26908 DU 18/09/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version  14.0.7-c51704c56772, le test sdlx201b échoue sur AthosDev_mpi
FONCTIONNALITE
   Problème :
   ==========
   Le test sdlx201b échoue en erreur de syntaxe.
   
   Analyse :
   =========
   Le mot clé SOLVEUR figure en double dans certaines commandes. 
   
   Correction :
   ============
   On supprime le mot-clé figurant en doublon dans différentes commandes et on corrige un appel CALC_MODES qui ne fonctionne pas avec
   MUMPS.
   Une valeur de non régression est ajustée : 
   
   
   @@ -570,7 +567,7 @@
   TEST_RESU(RESU=(_F(RESULTAT=MOM6,
                       NUME_ORDRE=1,
                       NOM_CHAM='DEPL',
                       TYPE_TEST='SOMM_ABS',
   -                   VALE_CALC=3.92704133585,),
   +                   VALE_CALC=3.92699275088,),
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdlx201b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26913 DU 19/09/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE evolution concernant Code_Aster (VERSION 14.4)
TITRE
    A101.18 - Règle supplémentaire dans IMPR_RESU et DEFI_MATERIAU
FONCTIONNALITE
   Problème :
   ==========
   Les commandes IMPR_RESU et DEFI_MATERIAU peuvent être valides sans que l'on ait renseigné un seul mot clé.
   Rien n'oblige dans le catalogue d'éviter de produire une commande vide.
   
   Correction :
   ============ 
   L'organisation du catalogue de la commande DEFI_MATERIAU permet d'ajouter une règle sur la présence de la plupart des mots clés
   facteurs, hormis ceux concernant la THM situés dans des blocs, mais dans ce cas il existe le mot clé simple COMP_THM dont on peut
   tester la présence.
   Concernant IMPR_RESU, le mot clé facteur RESU est toujours situé dans un bloc, il n'est donc pas possible d'ajouter une règle sans
   revoir complètement l'organisation des blocs. On ajoute tout de même une règle pour le mot clé CONCEPT qui ne peut-être utilisé que
   pour les format MED et RESULTAT.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    liste submit
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26839 DU 29/08/2017
AUTEUR : MICHEL-PONNELLE Sylvie
TYPE evolution concernant Code_Aster (VERSION 14.1)
TMA : Necs
TITRE
    option SIEF_ELGA pour les éléments d'interface
FONCTIONNALITE
   Besoin :
   pouvoir initialiser un champ de contrainte avec la commande CREA_CHAM dans un modèle 
   contenant des éléments d'interface
   
   Travail effectué :
   ================
   
   On ajoute la ligne :
   
        CondCalcul('-', ((AT.PHENO,'ME'),(AT.INTERFACE,'OUI'),)),
   
   dans le catalogue d'option SIEF_ELGA.
   
   Validation : On modifie le test ssns110a en créant un champ SIEF_ELGA à l'issue du 
   calcul intial.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssns110a

--------------------------------------------------------------------------------
RESTITUTION FICHE 25576 DU 13/09/2016
AUTEUR : FLEJOU Jean Luc
TYPE evolution concernant Code_Aster (VERSION 13.4)
TMA : Necs
TITRE
    A203.xx - EPSI_ELGA pour POU_D_TGM
FONCTIONNALITE
   Travail effectué :
   ==================
   Ajout des options EPSI_ELGA, EPME_ELGA et EPSP_ELGA aux éléments MECA_POU_D_TGM, ainsi que les options **_ELNO correspondantes. On
   suit les recommandations données :
   - option EPSI_ELGA dans te0537
   - ménage dans te0531 
   
   Validation :
   ============
   1- On complète zzzz336c comme cela a été fait dans zzzz336b pour les POU_D_EM -> calcul de EPSI_ELGA, EPME_ELGA et EPSP_ELGA. Les
   calculs sont linéaires avec chargement thermique, calculs mono-matériaux et bi-matériaux. Cela valide EMPE_ELGA et EPSI_ELGA en
   linéaire + thermique + bi-matériaux. On appelle également les options ELNO sans les tester car elles passent par un te générique. On
   remarque au passage que l'option EPSP_ELGA n'est pas correctement validée pour les POU_D_EM.
   
   2- Dans ssnl119a (calcul non-linéaire tri-matériaux), on compare la composante V1 (EPSPEQ) des variables internes de la loi
   VMIS_ISOT_TRAC à la composante EPXX de EPSP_ELGA pour un sous-point ayant plastifié. Cela valide correctement EPSP_ELGA pour les
   POU_D_EM.
   3- On duplique ssnl119a en POU_D_TGM, on fait la même comparaison entre VARI_ELGA et EPSP_ELGA pour la validation de EPSP_ELGA. On
   utilise ensuite la relation def_tot = def_plas + contraintes/young. Cela permet de valider EPSI_ELGA en non-linéaire.
   
   
   Impact doc :
   ============
   v1.01.336 : ajout des nouveaux tests à la modélisation C
   v6.02.119 : ajout des  nouveaux tests à la modélisation A + ajout de la nouvelle modélisation.
   Compléter les tableaux de [U2.01.05] Contraintes, efforts, forces et déformations
   
   EPSP_(ELGA, ELN0, NOEU) ne sont pas dans [U2.01.05]
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : v1.01.336, v6.02.119, U2.01.05
VALIDATION
    zzzz336c, ssnl119a et b

--------------------------------------------------------------------------------
RESTITUTION FICHE 26681 DU 07/07/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    DEFI_LIST_INST - Couverture des schémas d'adaptation automatique du pas de temps
FONCTIONNALITE
   Objet de la fiche :
   ===================
   Couvrir la commande DEFI_LIST_INST / DELTA_GRANDEUR + ITER_NEWTON.
   
   Travail effectué :
   ================
   
   Ajout des modélisations f et g à ssnp140.
   F : Méthode ITER_NEWTON : il faut décliner le test ssnp140e en une nouvelle modélisation ssnp140f. Dans le .comm au niveau de
   defi_list_inst DEFLIST1, il faut choisir :
                                          MODE_CALCUL_TPLUS='ITER_NEWTON',
                                          NB_ITER_NEWTON_REF=2,
   
   G : Méthode DELTA_GRANDEUR : il faut décliner le test ssnp140e en une nouvelle modélisation ssnp140g. Dans le .comm au niveau de
   defi_list_inst DEFLIST1, il faut choisir :
                                          MODE_CALCUL_TPLUS='DELTA_GRANDEUR',
                                          NOM_CHAM='DEPL',NOM_CMP='DY',VALE_REF=0.005E0,
   
   Impact doc : 
   V6.03.140 : ajout des deux nouvelles modélisations.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : v6.03.140
VALIDATION
    ssnp140f et g

--------------------------------------------------------------------------------
RESTITUTION FICHE 26896 DU 14/09/2017
AUTEUR : COURTOIS Mathieu
TYPE evolution concernant Code_Aster (VERSION 14.1)
TITRE
    A-t-on besoin d'un MUMPS très précis avec DYNA_LINE_TRAN ?
FONCTIONNALITE
   Problème
   --------
   
   L'appel à l'option POSTTRAITEMENTS='AUTO' est très coûteux car il impose de nombreuses descente-remontées
   lors du raffinement itératif.
   
   
   Correction
   ----------
   
   DYNA_VIBRA en linéaire transitoire sur base physique (ex. DYNA_LINE_TRAN) incorpore un processus itératif qui améliore la solution.
   Si on passe POSTTRAITEMENTS='SANS' par défaut dans DYNA_VIBRA pour ce type de calcul, ça n'améliore pas les choses.
   
   Si on traite DYNA_LINE_TRAN comme les opérateurs non-linéaires (c'est à dire qui ont un processus itératif englobant et donc ne
   justifient pas de passer du temps sur les post-traitements), cela revient à mettre RESI_RELA=-1 et les cas-tests en CPU_LIMIT
   reviennent dans les clous.
   
   Exemple : sdll102a en CPU_LIMIT à plus de 45 secondes revient à 15 secondes.
   
   12 tests actuellement en erreur sur Calibre9 sont corrigés.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test sd*
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26968 DU 02/10/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.9-47dc2776d1b5, le cas test sdls108a échoue sur AthosDev_Valid et Eole_Valid
FONCTIONNALITE
   Suite au changement de solveur par défaut et à la modification de CALC_MODES, le test de validation sdls108a s'arrête en erreur :  
    !-------------------------------------------------------------------------------------------------------!
      ! <F> <ALGELINE5_72>                                                                                    !
      !                                                                                                       !
      !     Par prÃ©caution, cette fonctionnalitÃ© est pour l'instant interdite avec le solveur linÃ©aire MUMPS. !
      !                                                                                                       !
      !     Conseil :                                                                                         !
      !     Changer de solveur linÃ©aire : sous le mot-clÃ© facteur SOLVEUR,                                    !
      !     poser METHODE='MULT_FRONT'.                                                                       !
      !                                                                                                       !
      !                                                                                                       !
      ! Cette erreur est fatale. Le code s'arrÃªte.                                                            !
      !-------------------------------------------------------------------------------------------------------!
   
   On impose la valeur SOLVEUR=_F(METHODE='MULT_FRONT') pour faire passer le test de validation.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdls108a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26830 DU 23/08/2017
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.3 - 79a25600c448 le cas wtnv135g échoue sur clap0f0q
FONCTIONNALITE
   Problème
   ========
   
   En version 14.0.3 - 79a25600c448 le cas wtnv135g échoue sur clap0f0q
   
   
   Solution
   ========
   
   En version 14.0.8, le test n'est plus cassé.
   C'est probablement réparé par une des fiches restituées la semaine dernière
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    wtnv125g
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26829 DU 23/08/2017
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.3 - 79a25600c448 les cas tests wtnv135e et wtnv135f sont nook sur clap0f0q
FONCTIONNALITE
   Problème
   ========
   
   En version 14.0.3 - 79a25600c448 les cas tests wtnv135e et wtnv135f sont NOOK sur clap0f0q.
   
   
   Solution
   ========
   
   En version 14.0.8, les tests ne sont plus cassés
   C'est probablement réparé par une des fiches restituées la semaine dernière
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    les tests
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26538 DU 02/06/2017
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    FAUX en v12 : suite issue 25573
FONCTIONNALITE
   Problème
   ========
   
   La fiche issue25573 n'a pas pu être reportée en V12, car elle casse erreu02a.
   
   Solution
   ========
   
   Impact doc uniquement FSQ 12.8 car l'impact FSQ 13.4 est traité dans issue25573.
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 12.0.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26737 DU 20/07/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Documentation (VERSION 10.*)
TMA : Necs
TITRE
    La doc V2.03.115 (sdls115a) n'est pas à jour
FONCTIONNALITE
   Objet de la fiche :
   ===================
   
   Dans la doc V2.03.115, il manque plusieurs résultats testés.
   
   Travail effectué :
   ================
   
   Réfection des figures pour ajout repère et autres groupes de nœuds.
   Mise en adéquation des conditions limites avec les fichiers de commande.
   Ajout des TEST_RESU manquant et mise en forme.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : V2.03.115
VALIDATION
    doc + sans source

--------------------------------------------------------------------------------
RESTITUTION FICHE 26447 DU 10/05/2017
AUTEUR : TAMPANGO Yannick
TYPE evolution concernant Documentation (VERSION 11.4)
TMA : Necs
TITRE
    Documentation du cas test sdll152a : V2.02.152
FONCTIONNALITE
   Contexte :
   ==========
   Le cas-test sdll152a de non-régression a été ajouté par issue25032 afin de vérifier le calcul des critères d'instabilité de Connors.
   C'est un test d'interaction fluide structure qui  modélise un tube sous écoulement transverse avec un profil de
   vitesse parabolique pour le fluide.
   
   # Simple test of an FSI stability calculation with transverse flow on 
   # a clamped-clamped steel tube (beam model).
   #
   #. . . . . . . . . . . . . . . . . . ^ y-axis
   #. . . . . . . . . . . . . . . . . . |
   #. . . . . . . . . . . . . . . . . . --> x-axis. . . (x) z axis
   #. //|. . . .  L, D. . . . . |//
   #. //|=======================|//
   #. //|. . .  ^ ^ ^ ^ ^. . .  |//
   #. . . . . ^ | | | | | ^. . . . . . 
   #. . . . ^ | | | | | | | ^. 
   #. . . ^ | | | | | | | | | ^ 
   #. . . | | | | | | | | | | |. 
   #
   #. . . . . Fluid velocity (parabolic function)
   
   La doc du cas test n'avait pas été réalisée dans la fiche originale issue25032. L'objectif de la fiche actuelle est de créer la
   documentation.
   
   Travail effectué :
   ================
   
   Création du document v2.02.152. Modélisation A, B, C, D et E.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : V2.02.152
VALIDATION
    doc

--------------------------------------------------------------------------------
RESTITUTION FICHE 24633 DU 16/12/2015
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant Documentation (VERSION 10.*)
TMA : Necs
TITRE
    Doc V2.04.123 : SDLV123
FONCTIONNALITE
   Problème
   ========
   
   '''
   La doc V2.04.123 n'est pas cohérente pour la modélisation A avec le test :
   
   - le maillage indiqué est un quart de cercle sur la doc alors que sdlv123a.mmed est un carré
   - dans les chargements, il s'agit d'un rectangle
   - dans la solution de référence il s'agir d'un disque
   
   Par contre l'image du maillage de la modélisation b est la bonne.
   '''
   
   Correction
   ==========
   
   On reprend entièrement la documentation.
   
   1.3- Chargements :
   --------------
   a- Le quart du domaine total de 200m sur 200m n'est pas un rectangle mais un carré de 100m par 100m => on corrige 
   b- Création d'une nouvelle figure pour les chargements, carrée avec la localisation des différents groupes impliquées dans les
   chargements et conditions aux limites.
   c- ajout des conditions aux limites et chargements non spécifiés dans la doc mais présents dans le test
   d- suppression de la figure EDF qui n'a rien à faire là selon moi
   
   2- Solution de référence :
   ------------------------
    pas de modifications
   
   3- Modélisation A :
   -----------------
   3.1 : Remplacement de la figure représentant le maillage
   3.2 : Modifications du nombres de nœuds et de mailles. Correction du tableau donnant les couronnes, les valeurs ne correspondant pas
   au fichier de commande.
   3.3 : 
   a-Suppression du paragraphe sur le calcul de la valeur de référence. En effet les valeurs de référence présentes dans le fichier de
   commande sont données en 2.2
   b- ajout d'un tableau pour les résultats testés
   
   4- Modélisation B :
   -----------------
   3.2 : Correction du tableau donnant les couronnes, les valeurs ne correspondant pas au fichier de commande.
   3.3 : ajout d'un tableau pour les résultats testés
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : v2.04.123
VALIDATION
    sans objet

