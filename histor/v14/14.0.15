==================================================================
Version 14.0.15 (révision 78a36928fb9a) du 2017-11-10 07:32 +0100
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 27095 DU 30/10/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    Augmentation Temps CPU : CALC_FONC_INTERP
FONCTIONNALITE
   Problème
   --------
   
   Dans le test sdlx301a, l'appel à CALC_FONC_INTERP dure 1.78 s contre 0.08 s en 14.0.11.
   
   
   Correction
   ----------
   
   Lors de la réalisation de issue27022, on a initialisé le context d'évaluation des FORMULE avec toutes les fonctions du module 'math'.
   À chaque évaluation, on appelle la fonction 'initial_context()' pour disposer de ces fonctions.
   
   Dorénavant, on construit ce contexte uniquement lors de la création de la formule.
   
   On retrouve le temps d'exécution précédent.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdlx301a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27109 DU 02/11/2017
AUTEUR : PLESSIS Sarah
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.12, couverture des routines rcma02 et rcmo02
FONCTIONNALITE
   suite à la révision  	[#27018] POST_RCCM : evolutions 2017, les routines suivantes ne sont pas couvertes
   rcma02
   rcmo02
   Apparemment elles ne sont plus appelées.
   
   ==> on les supprime.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27070 DU 23/10/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.12, les test ssnl133c et zzzz159f sont NOOK sur Eole, Calibre9 et Clap0f0q
FONCTIONNALITE
   Problème :
   =========
   Depuis le basculement par défaut au solveur 'MUMPS' les tests ssnl133c et zzzz159f sont NOOK sur Eole, Calibre9 et clap0f0q. 
   
   Solution :
   ========
   On insère l'appel à MULT_FRONT en modifiant explicitement le fichier de commandes ssnl133c.comm et le fichier zzzzz159f.3. Les
   valeurs de référence sont rétablies à leurs valeurs avant la révision 0182e232d0d7
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssnl133c et zzzz159f
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26432 DU 05/05/2017
AUTEUR : COURTOIS Mathieu
TYPE evolution concernant Code_Aster (VERSION 12.4)
TMA : Necs
TITRE
    A203.xx - Type de SD pour SPEC_OSCI
FONCTIONNALITE
   Objectif
   --------
   
   La SD résultat de CALC_FONCTION(SPEC_OSCI) est de type nappe car un SRO peut être calculé pour plusieurs amortissement.
   
   Dans le cas où le calcul est fait pour un seul amortissement, manipuler une fonction au lieu d'une nappe serait plus souple
   d'utilisation.
   
   
   Réalisation
   -----------
   
   On ajoute le mot-clé TYPE_RESU qui peut prendre les valeurs 'NAPPE' ou 'FONCTION', NAPPE étant la valeur par défaut. 
   
   On modifie la fonction `sdprod` de CALC_FONCTION pour que dans le cas SPEC_OSCI, si TYPE_RESU='FONCTION' et qu'il n'y a qu'un
   amortissement dans AMOR_REDUIT, on crée une `fonction_sdaster`. Dans les autres cas, on crée une `nappe_sdaster`.
   
   
   Validation
   ----------
   
   Dans zzzz100d, on duplique un appel à CALC_FONCTION/SPEC_OSCI avec un seul AMOR_REDUIT. On crée une table à partir de la fonction
   créée (ce qui ne fonctionne pas avec une nappe). On vérifie avec TEST_TABLE que l'on a bien la même valeur.
   
   On passe la liste des tests utilisant CALC_FONCTION/SPEC_OSCI.
   
   
   Impact doc
   ==========
   
   U4.32.04 : On précise la sd créée en fonction de TYPE_RESU et du nombre d'AMOR_REDUIT.
   
   Pas de modif de la doc de zzzz100 : doc succincte.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.32.04
VALIDATION
    zzzz100d

--------------------------------------------------------------------------------
RESTITUTION FICHE 26709 DU 17/07/2017
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.1 le cas test mfron05b est NO_RESU_FILE sur clap0f0q
FONCTIONNALITE
   Problème
   ========
   
   En version 14.0.1 le cas test mfront05b est NO_RESU_FILE sur clap0f0q  
   
      !-------------------------------------------------------!
      ! <F> <DVP_2>                                           !
      !                                                       !
      ! Erreur numérique (floating point exception).          !
      !                                                       !
      !                                                       !
      ! Cette erreur est fatale. Le code s'arrête.            !
      ! Il y a probablement une erreur dans la programmation. !
      ! Veuillez contacter votre assistance technique.        !
      !-------------------------------------------------------!
   
   The first bad revision is:
   changeset:   6993:f5a8698f04c4
   user:        Mickael Abbas <mickael.abbas@edf.fr>
   date:        Mon Jul 10 17:48:56 2017 +0200
   summary:     [#23755] Final access to external state variables in MFront and testcase
   
   Solution
   ========
   
   Le problème vient d'une variable non-initialisée dans verift appelée par mfront_varc.
   Cette routine récupère les données correspondant à la variable de commande de la température et calcule la déformation thermique.
   Au début de la routine, on demande s'il y a des variables de commande de température à l'instant '+'. Si ce n'est pas le cas, il est
   inutile de récupérer la température à l'instant "-" et "TREF3. Quand il n'y a pas de température (et donc n'existe pas à l'instant
   "+"), la température "-" n'est PAS initialisée. Or dans mfront_varc, on fait delta_temp = temp_plus - temp_moins => avec temp_moins
   non initialisée => FPE
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    mfron05b
NB_JOURS_TRAV  : 1.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25432 DU 19/07/2016
AUTEUR : DROUET Guillaume
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    D124.17 - Méthode LAC pour PYRA/PENTA
FONCTIONNALITE
   Problématique:
   ==============
   
   Il faut permettre le contact LAC pour les PYRAMIDES et les PENTAEDRES:
   - Découpe
   +
   - Éléments finis 
   
   Il n'y aucun développement à faire du côté éléments finis. En effet, ces éléments 3D génèrent des éléments de type quadrangle ou
   triangle sur la zone de contact qui est traitée par la méthode LAC. Les seuls développements à réaliser sont donc au niveau de la
   découpe. On doit adapter l'opérateur CREA_MAILLAGE/DECOUPE_LAC pour pouvoir traiter les PYRA 5 13 et les PENTA 6 15 18.
   
   Développements réalisés:
   ========================
   
   Il faut trouver une découpe non invasive pour chaque élément 3D selon le type de l'élément 2D qui "touche" la zone de contact. On a 
   choisi d'utiliser des découpes "non conformes" dans le sens où l'on ne découpe pas un élément avec le même type d'élément. Les 
   découpes permettant de disposer d'un DDL interne sur les patchs de contact LAC sont les suivantes:
   
   PYRA5 contact sur QUAD4 => 2D: 4 TRIA3 3D: 4 TETRA4
   PYRA5 contact sur TRIA3 => 2D: 3 TRIA3 3D: 1 PYRA5 et 3 TETRA4
   
   PYRA13 contact sur QUAD8 => 2D: 4 TRIA6 3D: 4 TETRA10
   PYRA13 contact sur TRIA6 => 2D: 3 TRIA6 3D: 1 PYRA13 et 3 TETRA10
   
   PENTA6 contact sur TRIA3 => 2D: 3 TRIA3 3D: 3 PYRA5 et 1 TETRA4
   PENTA6 contact sur QUAD4 => 2D: 4 TRIA3 3D: 2 PYRA5 et 2 TETRA4
   PENTA15 contact sur TRIA6 => 2D: 3 TRIA6 3D: 3 PYRA13 et 1 TETRA10
   PENTA15 contact sur QUAD8 => 2D: 4 TRIA6 3D: 2 PYRA13 et 2 TETRA10
   
   Il n'existe apparemment pas de découpe non invasive pour le PENTA 18 contact sur TRIA6, on ne traite donc pas du tout cet élément
   dans cette
   fiche.
   
   Tous les impacts sont donc localisés dans les routines gérant les découpes. 
   
   Vérification:
   =============
   
   Étant donné que toutes les combinaisons d'intersection QUAD X / TRIA X sont déjà testées, on rajoute uniquement deux test dans 1
   modélisation (Z) au cas test ssnp170 (patch test de taylor) pour vérifier la découpe.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.23.02, V6.03.170
VALIDATION
    ssnp170 , validation graphique et submit
NB_JOURS_TRAV  : 7.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26138 DU 01/03/2017
AUTEUR : BÉREUX Natacha
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    A301.xx - Montée de version PETSc 3.x
FONCTIONNALITE
   Montée de version PETSc : 3.7.3 -> 3.8.1
   ========================================
   La version officielle de  PETSc est la version 3.8.1 (depuis le 04/11/2017)
   Cette version inclut des évolutions de l'interface fortran de PETSc (avec de meilleures vérifications des interfaces dans les appels
   de fonctions).
   
   Évolutions de code_aster nécessaires:
   =====================================
   - Chaque routine faisant appel à des structures de données PETSc doit comporter les lignes suivantes: 
      #include "asterf_petsc.h" 
      use aster_petsc_module 
   - quelques évolutions d'API (MatGetSubmatrix - > MatCreateSubMatrix)
   - quelques évolutions des objets Fortran : PETSC_NULL_OBJECT  disparaît, remplacé par des objets spécifiques (PETSC_NULL_KSP ...),
   
   Évolutions des prérequis : 
   =========================
   Hypre : on passe à la version 2.2
   
   Compatibilité avec les versions précédentes :
   =============================================
   code_aster continue à fonctionner avec la version 3.7.3 de PETSc.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tests petsc
NB_JOURS_TRAV  : 5.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26882 DU 11/09/2017
AUTEUR : ZENTNER Irmela
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    En version 14.0.6-b6592df2c497, les tests sdls128a et sdls128b sont CPU_LIMIT sur AthosDeb_Valid et Eole_Valid
FONCTIONNALITE
   Problème:
   ---------------
   
   -Temps CPU fortement augmenté de sdls128a et sdls128b
   - sdls128a NOOK: exemple: ('NOOK', 'XXXX', '0.0360586', '0.03605865', '1.386632E-04%', '0.0001%')
   
   Remarque: la fiche issue26741 ne concerne pas le test sdls128a mais uniquement la nouvelle 
   modélisation b.
   
   Analyse:
   -----------------
   Le problème du temps CPU fortement augmenté ressemble à un problème déjà traité par Georges (fiche 
   26368)en début d'année:
   
   " Le temps CPU du cas test sdls128a ( test de validation ) a fortement augmenté:
   - 2045s ( version 13.3.13) contre 715s (version 13.3.0). 
   L'augmentation a lieu dans la commande DEFI_SOL_EQUI ( 712s -> 2043s)."
   
   Selon le cas de chargement, il a été proposé de distinguer les valeurs par défaut de 
   TOUT_CHAM : 'OUI' pour onde plane mais 'NON' pour mono-appui.
   
   On constate que dans le cas test sdls128a, TOUT_CHAMP n'est pas renseigné, et la valeur 'OUI' est 
   pris par défaut ce qui ne devrait pas être le cas (TOUT_CHAMP = "NON" par défaut pour mono-appui);
   
   
   Il y a des erreur dans les valeurs de référence de sdls128b. Ces valeurs sont corrigées.
   
   Solution :
   --------------------------
   Je rajoute TOUT_CHAMP = 'NON' aux cas test. En parallèle, il faudrait analyser l'origine du problème.
   Il est possible que les options par défaut ont été changées, par exemple dans le cadre des 
   développements des 3 composantes dans DEFI_SOL_EQUI, fiche 26128.
   
   sdls128a NOOK: je rajoute des décimales.
   sdls128b: corrections des valeurs NOOK
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdls128a, sdls128b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26926 DU 21/09/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    D124.17 restitution d'un cas test de réservoir traité avec LAC
FONCTIONNALITE
   On propose un nouveau cas-test dans la base de validation :
   
   tag : benchmark 
   clef Doc : V6.04.249
   Titre : 
   Étude sismique en mode push-over d'un réservoir d'éffluents  primaires avec prise en compte du contact par la méthode Local Average
   Contact.
   
   Contexte :
   Dans le cadre des études sismiques sur le parc nucléaire concernant les réservoirs de Traitement d'Effluents Primaires (TEP),  on
   met en place une modélisation qui traite le contact surfacique entre la base du réservoir et le béton avec la méthode Local Average
   Contact.
   
   La structure étudiée est de grande taille à faible épaisseurs (coque) avec des ancrages localisés, du contact surfacique
   (béton-couronne) et la prise en compte de la  plasticité de l'ensemble du réservoir. Ce calcul révèle plusieurs difficultés liés à
   la résolution numérique.
   
   Validation :
   ============
   On ne dispose pas pour le moment d'un test de source_externe. On compare le résultat de  deux calculs aster sur la base de méthode
   LAC : calcul 3D et calcul 3D_SI. Référence de résultat AUTRE_ASTER. 
   
   Cette validation sera améliorée par la suite dans la fiche chapeau Issue26925.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : V6.04.249
VALIDATION
    ssnv249
NB_JOURS_TRAV  : 3.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25683 DU 19/10/2016
AUTEUR : FILIOT Astrid
TYPE anomalie concernant Documentation (VERSION 13.*)
TITRE
    Elasticité anisotrope : qui est qui
FONCTIONNALITE
   Voici quelques éclaircissements sur la mise en œuvre de l'anisotropie : 
   
   1. La doc d'AFFE_CARA_ELEM indique clairement que le mot-clé MASSIF permet de définir les repères locaux des éléments 
   avec la convention suivante : repère local (x,y,z)=(L,T,N).
   Comme le suggère Nicolas, on pourrait faire référence dans ce paragraphe aux conventions qui sont mises en regard pour 
   définir les matériaux dont les propriétés sont spécifiées dans le repère local des éléments.
   Par exemple, on pourrait évoquer les matériaux transverses isotropes ELAS_ISTR, pour lequel l'axe "privilégié" est porté 
   par N d'après la convention adoptée par aster (voir [R4.01.02]).
   
   **Petit exemple d'application : imaginons une géométrie cylindrique constituée d’un matériau isotrope transverse, dont 
   l'axe privilégié est parallèle à l'axe du cylindre. Le repère global est (X,Y,Z).
   Si le cylindre a été défini avec un axe selon Z, alors pas besoin de toucher aux repères locaux, on a bien Z // N.
   Si le cylindre a été défini avec un axe selon Y, alors il faut faire une rotation des repères locaux pour amener N // à 
   Y (se fait dans ce cas avec "AFFE_CARA_ELEM(MASSIF=_F(ANGLE_REP = (0.,0.,-90.),),)" ). Cela produit bien la 
   transformation : (X,Y,Z) -> (x,y,z)=(L,T,N)=(X,-Z,Y), et l’axe privilégié du matériau est le long de l’axe du cylindre 
   comme on le souhaite.**
   
   --> Donc pas d’anomalie ici. On rajoute un complément « pédagogique » dans la doc [U4.42.01] Opérateur AFFE_CARA_ELEM / 
   13 Mot clé MASSIF pour que le lecteur ait bien en tête que ce sont les conventions de définition des matériaux dont les 
   propriétés sont spécifiées dans le repère local des éléments qui dictent la rotation des repères locaux qu’on va 
   effectuer.
   
   2. La doc est cohérente avec la mise en donnée réelle d’aster. Certes, ce n’est pas nécessairement intuitif d’avoir opté 
   pour N comme axe privilégié d’un matériau transverse isotrope, car de mon expérience on l'appelle généralement l’axe « 
   longitudinal ». Mais avec l’exemple donné ci-dessus, on peut imaginer que cette mise en donnée a été choisie pour être 
   cohérente avec une géométrie cylindrique généralement définie avec un axe de révolution selon Z.
   
   --> Pas d’anomalie non plus. On ajoute une ligne dans les docs [U4.43.01] Opérateur DEFI_MATERIAU / 3.5 Mot clé 
   ELAS_ISTR, et [R4.01.02] ELASTICITE ANISOTROPE / 3.2.2 Isotropie transverse, pour préciser que la mise en donnée aster 
   n’est pas forcément cohérente avec une convention usuelle qui est d’appeler l’axe privilégié des matériaux transverses 
   isotropes l’axe « longitudinal », de sorte que l’utilisateur ne suspecte pas d’erreurs dans la doc.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : U4.42.01,U4.43.01,R4.01.02
VALIDATION
    EDA
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 22665 DU 26/05/2014
AUTEUR : BÉREUX Natacha
TYPE evolution concernant Code_Aster (VERSION )
TITRE
    D128.17 - MODE_VIBR
FONCTIONNALITE
   Doublon avec issue22900
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 21252 DU 05/08/2013
AUTEUR : SELLENET Nicolas
TYPE evolution concernant Documentation (VERSION )
TITRE
    [U4.01.07] Nouveautés et modifications de la version 11
FONCTIONNALITE
   Cf. issue23587.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 23889 DU 03/06/2015
AUTEUR : SELLENET Nicolas
TYPE evolution concernant Documentation (VERSION )
TITRE
    Nouveautés et modifications de la version 12
FONCTIONNALITE
   Cf. issue23587.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 23587 DU 09/03/2015
AUTEUR : DE SOZA Thomas
TYPE evolution concernant Documentation (VERSION )
TITRE
    C208.xx - Nouveautés de code_aster (V13)
FONCTIONNALITE
   Pour permettre d'identifier rapidement les nouveautés des versions de code_aster, des documents sont produits pour identifier
   celles-ci rapidement.
   
   Cette fiche se proposait de le faire pour les versions 11 et 12. Compte-tenu de la fin de la période de maintenance de ces versions,
   on ne traite pas ce besoin.
   
   En revanche, les nouveautés de la version 13 mise en production en juin 2017 ont été compilées dans le document [U4.01.09] dans le
   cadre du contrat de gestion technique.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : U4.01.09
VALIDATION
    sans objet
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26780 DU 03/08/2017
AUTEUR : DE SOZA Thomas
TYPE aide utilisation concernant Code_Aster (VERSION 14.4)
TITRE
    débuggage impossible avec une loi MFront
FONCTIONNALITE
   Contexte
   ========
   
   '''
   Nous travaillons à la réalisation de gros calculs 3D très gourmands car beaucoup de temps est passé dans
   l'intégration du comportement. En effet nous nous intéressons à des comportements de plasticité cristalline en grande déformation
   (Simo-Miehe). De plus nous faisons le calcul sur des maillages 3D volumineux (environ 100000 mailles tetra) issus de données
   expérimentales.
   Pour ces raisons, on souhaite faire le calcul avec la loi MFront 'FiniteStrainSingleCrystal', qui présente l'avantage d'être bien
   codée en implicite, avec la matrice tangente.
   On a un peu simplifié la loi MFRONT pour l'optimiser au maximum, en retirant l'écrouissage cinématique que l'on n'utilise pas. 
   
   Que ce soit avec la loi optimisée ou issue du dépôt TFEL, on obtient au bout d'un certain temps un plantage dans la résolution de la
   LdC:
   
      !--------------------------------------------------------------!
      ! <EXCEPTION> <DVP_2>                                          !
      !                                                              !
      ! Erreur numérique (floating point exception).                 !
      !                                                              !
      ! --------------------------------------------                 !
      ! Contexte du message :                                        !
      !    Option         : FULL_MECA                                !
      !    Type d'élément : MEDPTR3                                  !
      !    Maillage       : MAIL                                     !
      !    Maille         : M77851                                   !
      !    Type de maille : TRIA3                                    !
      !    Cette maille appartient aux groupes de mailles suivants : !
      !       TOUT                                                   !
      !    Position du centre de gravité de la maille :              !
      !       x=11.232837 y=14.250350 z=0.000000                     !
      !                                                              !
      !                                                              !
      !                                                              !
      ! Il y a probablement une erreur dans la programmation.        !
      ! Veuillez contacter votre assistance technique.               !
      !--------------------------------------------------------------!
   
   Mais avant ce plantage, plusieurs pas de temps sont calculés et le comportement de la loi semble normal.
   Le problème étant assez fortement non linéaire (transition élastique-plastique "violente"), on utilise MUMPS comme solveur ou METIS,
   mais cela ne change rien.
   En exemple, on a reproduit le plantage sur un maillage 2D simple (D_PLAN) mais on a aussi le problème en 3D.
   
   Comme on a un "floating point exception" on imagine bien qu'il doit y avoir une division par zéro quelque part. Ce n'est pas évident
   à localiser lorsqu'on regarde les équations de la loi.
   
   On a essayé de rajouter des print dans la loi via des commandes 'cout' mais cela rend le .mess totalement illisible et gigantesque.
   
   Dans les fiches précédentes sur des problèmes similaires, (issue21461, issue22872, issue23229), il est mentionné une fonction
   intéressante de STAT_NON_LINE que l'on arrive pas à reproduire. En cas d'échec de la loi de comportement, une mise en donnée pour
   SIMU_POINT_MAT est faite pour reproduire le plantage sur un calcul sur point matériel. On récupère les variables internes,
   contraintes et déformations de la maille sur laquelle se produit le plantage ce qui permet de lancer un calcul se prêtant mieux au
   débuggage de la loi. 
   
   Comment obtenir donc la mise en donnée SIMU_POINT_MAT dans notre cas? Ne l'a-t-on pas car on utilise une loi MFRONT?
   '''
   
   Résumé des analyses
   ===================
   
   Simulation point matériel en cas d'erreur
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   La fonctionnalité de code_aster qui produit une mise en données SIMU_POINT_MAT en cas d'erreur d'intégration ne peut se déclencher
   que si la loi de comportement échoue "proprement", c'est à dire en renvoyant un code retour
   ==> ce n'est pas le cas ici car on tombe dans une FPE.
   
   Une fonctionnalité similaire existe dans MFront et est activable en compilant la loi avec l'option suivante :
   '--@AsterGenerateMTestFileOnFailure=true'
   ==> pour les mêmes raisons que dans code_aster, cela n'est malheureusement pas utile ici.
   
   Localisation des erreurs dans MFront
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   Deux solutions sont conseillées en cas d'erreur dans MFront :
   
   1. Lancer le calcul Aster en mode "debug" et activer la remontée d'erreur (ERREUR=_F(ERREUR_F='ABORT') dans DEBUT)
   ==> si le plantage se produit dans une expression mathématique, on peut parfois remonter à la ligne fautive dans la loi de
   comportement MFront (par exemple une exponentielle ou une puissance).
   
   2. Compiler la loi de comportement MFront avec des informations de debug qui permettront d'accéder aux valeurs de
   contraintes/déformations/résidu local ('--debug' dans la ligne de compilation)
   ==> on accède ainsi à un affichage (abondant) qui permet d'observer d'éventuelles divergences du résidu ou des contraintes et donc
   comprendre ce qui pose problème.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    calcul joint
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 27096 DU 30/10/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    Augmentation Temps CPU : CREA_CHAMP
FONCTIONNALITE
   Problème
   --------
   
   En version 14.0.13, le temps CPU des tests suivants a augmenté:
   284.1% wtnv141b   CREA_CHAMP_47          3.71    14.25 
   275.5% wtnv141a   CREA_CHAMP_38          1.92     7.21
   160.3% ssnp136a   CREA_CHAMP_29          0.58     1.51
   162.1% ssnp136a   CREA_CHAMP_34          1.03     2.70
   127.5% ssnp104b   CREA_CHAMP_28          0.80     1.82
   
   
   Correction
   ----------
   
   Identique au problème relevé dans issue27095.
   
   wtnv141b
    * CREA_CHAMP passe de 7.93 à 1.82 s
   wtnv141a
    * CREA_CHAMP passe de 4.01 à 0.88 s
   ssnp136a
    * CREA_CHAMP passe de 0.73 à 0.28 s
    * CREA_CHAMP passe de 1.35 à 0.49 s
   ssnp104b
    * CREA_CHAMP passe de 0.94 à 0.39 s
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    les 4 tests incriminés
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27097 DU 30/10/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Augmentation temps CPU : DYNA_VIBRA
FONCTIONNALITE
   Problème
   --------
   
   En version 14.0.13, le temps CPU des tests suivant a augmenté:
   225.4% sdll144a   DYNA_VIBRA_25         11.12    36.18
   226.4% sdll144a   DYNA_VIBRA_26         11.32    36.95 
   217.5% sdll144b   DYNA_VIBRA_25         11.58    36.7
   214.8% sdll144b   DYNA_VIBRA_26         11.66    36.70
   
   Correction
   ----------
   
   Identique au problème relevé dans issue27095.
   
   sdll144a
    * DYNA_VIBRA passe de 20.67 à 4.62 s
    * DYNA_VIBRA passe de 21.14 à 4.67 s
   sdll144b
    * DYNA_VIBRA passe de 20.26 à 4.71 s
    * DYNA_VIBRA passe de 21.27 à 4.76 s
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdll144a, sdll144b
NB_JOURS_TRAV  : 0.5

