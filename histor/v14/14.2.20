==================================================================
Version 14.2.20 (révision 23beb6dd740f) du 2018-12-05 20:56 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 28164 SELLENET Nicolas         Mauvaise impression des penta18 dans impr_resu
 28107 BÉREUX Natacha          EXCEPTION lors d'un MODI_REPERE
 25804 BÉREUX Natacha          B105.16 : en version 13.2.15, les tests ssnl130b et hpla310a s'arretent pour ...
 28260 LEFEBVRE Jean-Pierre     Report correctif ischar_iden
 28205 GRANET Sylvie            bug dans ccfnrn.F90
 28291 TARDIEU Nicolas          tests petsc04a/b/c/d sous scibian sur station
 28277 COURTOIS Mathieu         La fiche issue27388 a cassé AsterStudy
 28259 COURTOIS Mathieu         MACR_SPECTRE - limitation mot-clé multiple RESU pour le cas 'DEPL'
 27688 COURTOIS Mathieu         Ajout d'une MACRO pour l'identification des combinaisons dimensionnantes dans...
 28218 COURTOIS Mathieu         Différence de convergence lié à DEFI_FONCTION ?
 27679 LEFEBVRE Jean-Pierre     Base non recopiee
 28002 SELLENET Nicolas         Fuite mémoire dans dll_mfront.c
 27176 BÉREUX Natacha          En version 14.0.16, couverture et PETSC
 27132 BÉREUX Natacha          Vérification des interfaces des routines PETSc
 26271 FLEJOU Jean Luc          ORIENTATION/AFFE_CARA_ELEM : impact inattendu sur les résultats
 26892 FLEJOU Jean Luc          [bb0110] MACR_CARA_POUTRE computes wrong JX
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 28164 DU 19/10/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Mauvaise impression des penta18 dans impr_resu
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Au format MED, les PENTA18 sont imprimés comme des PENTA15.
  | 
  | 
  | Solution :
  | ----------
  | Maintenant MED sait gérer les PENTA18, on peut donc lever cette restriction.
  | 
  | J'ajoute un test unitaire qui imprime un champ de déplacement au format MED et qui vérifie qu'après relecture, les valeurs relues
  | sont correctes.
  | 
  | J'ajoute le test zzzz114a pour valider.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.01.114
- VALIDATION : test fiche
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28107 DU 03/10/2018
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : EXCEPTION lors d'un MODI_REPERE
- FONCTIONNALITE :
  | Problème :
  | =========
  | Une erreur fatale est déclenchée dans MODI_REPERE avec un concept RESULTAT réentrant. La documentation indique que 
  | cela est fortement déconseillé, mais ne dit pas que c'est impossible. Je me retrouve avec l'erreur suivante :
  | 
  |    !---------------------------------------------------------------------------------------------------------------!
  |    ! <EXCEPTION> <MODELISA3_15>                                                                                    !
  |    !                                                                                                               !
  |    ! MODI_REPERE / RESULTAT / concept réentrant                                                                    !
  |    !     Le mot clé REPERE doit prendre une des valeurs "COQUE_INTR_UTIL" ou "COQUE_UTIL_INTR" ou "COQUE_UTIL_CYL" !
  |    !---------------------------------------------------------------------------------------------------------------!
  | 
  | Analyse: 
  | ========
  | Dans MODI_REPERE, le concept résultat peut (au sens où ce n'est pas interdit par le catalogue) être réentrant.  
  | Cette usage est réservé aux changements de repère de type "COQUE_INTR_UTIL" ou "COQUE_UTIL_INTR" ou "COQUE_UTIL_CYL". 
  | Dans tous les autres cas, on interdit d'avoir le même concept résultat en entrée et en sortie. 
  | Le message est donc justifié mais pas clair => je le modifie. Je modifie aussi la doc u4.74.01.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : u4.74.01
- VALIDATION : sans
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 25804 DU 25/11/2016
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : anomalie concernant code_aster (VERSION 13.4)
- TITRE : B105.16 : en version 13.2.15, les tests ssnl130b et hpla310a s'arretent pour cause de matrice singuliere dans STAT_NON_LINE sur calibre9 et athosdev mpi avec np=3
- FONCTIONNALITE :
  | Problème
  | ========
  | Les cas-tests hpla310a et ssnl130b s'arrêtent avec le message d'erreur FACTOR_11 (matrice singulière lors de la factorisation avec
  | MUMPS) lorsqu'ils sont lancés en parallèle avec 3 processeurs 
  | 
  | Analyse
  | =======
  | Les deux cas-tests ont des problèmes différents:
  | 1) hpla310a n'est pas fait pour fonctionner en parallèle. Avec 3 procs, on est arrêté par:
  | - un message demandant de préciser 
  | DISTRIBUTION=_F(METHODE='CENTRALISE') dans AFFE_MODELE 
  | - puis par le message d'erreur CONTACT3_44 
  | "La normale que vous avez choisie NORMALE='ESCL'/'MAIT_ESCL' n'est pas prévue pour des calculs parallèles. Il faut utiliser l'option
  | par défaut NORMALE='MAIT' ou changer de surface maître/esclave. "
  | En suivant les deux conseils ( préciser DISTRIBUTION=_F(METHODE='CENTRALISE') et choisir NORMALE='MAIT' dans DEFI_CONTACT ) le cas
  | test fonctionne jusqu'au bout avec la version mpi sur 3 processeurs et les résultats sont OK
  | 
  | 2) ssnl130b s'arrête bien avec FACTOR_11 sur 3 processeurs. La doc du cas-test dit qu'il est normal que le conditionnement soit très
  | mauvais (raideur d'un tapis de ressorts pouvant s'annuler) et il est connu que dans ce cas MUMPS a un comportement différent en
  | séquentiel et en parallèle. NPREC=9 permet au calcul d'aller jusqu'au bout
  | 
  | Solution 
  | =========
  | 1) Je ne fais rien 
  | 2) Je rajoute NPREC=9 dans SOLVEUR

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : hpla310a ssnl130b
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28260 DU 23/11/2018
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Report correctif ischar_iden
- FONCTIONNALITE :
  | Problème :
  | --------
  | Dans la version asterHPC il est parfois nécessaire de disposer du nom du chargement dans la fonction ischar_iden. Cette correction
  | est à reporter dans la version legacy. 
  | 
  | Correction :
  | ----------
  | Pour assurer la cohérence avec la version asterHPC, on ajoute l'argument facultatif load_name dans la fonction fortran ischar_iden.
  | On modifie les routines appelant cette fonction avec l'argument supplémentaire (vebume.F90 et ischar.F9O).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : liste submit
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28205 DU 07/11/2018
================================================================================
- AUTEUR : GRANET Sylvie
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : bug dans ccfnrn.F90
- FONCTIONNALITE :
  | Nicolas s'est aperçu que l'initialisation du temps était incomplète dans ccfnrn.F90.
  | ! Initialisation
  |         partps(1) = time
  |         partps(1) = time    
  |         partps(1) = 0.D0
  | 
  | Alors qu'il faudrait
  |         partps(1) = time
  |         partps(2) = time    
  |         partps(3) = 0.D0
  | 
  | Cela peut avoir un impact potentiel sur des calculs temporaires utilisant des réactions nodales (problème non rencontré pour
  | l'instant). En THM seul partps(1) est visiblement utilisé donc a priori pas d'impact de ce coté là. Cela pourrait avoir un impact
  | pour dyna_harmo
  | On corrige.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Passage des cas tests
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 28291 DU 28/11/2018
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : tests petsc04a/b/c/d sous scibian sur station
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Sous scibian (version openmpi) les tests petsc04a/b/c/d sortent en erreur. Cette fiche est en doublon avec issue28305.
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Avec la fonctionnalité fieldsplit, on veut pouvoir définir dynamiquement le solveur, ce que l'on fait à l'aide d'une chaîne de
  | caractères. *Mais* on ne veut pas utiliser le fichier .petscrc. J'ai mis en place un mécanisme mais qui est déficient comme le
  | montre l'erreur sur Scibian.
  | En fait, il manque un appel à la routine PetscLogDefaultBegin() comme signalé dans le message d'erreur.
  | Cette correction est déjà faite dans asterxx.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Passage tests
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28277 DU 26/11/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : La fiche issue27388 a cassé AsterStudy
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le catalogue de la version de développement 14.2.18 ne peut pas être importé dans AsterStudy.
  | 
  | 
  | Correction
  | ----------
  | 
  | La déclaration de la nouvelle sd `matr_asse_elim_r` n'a pas été faite dans 'DataStructure.py'.
  | 
  | On ajoute un cas-test vocab01b pour vérifier que l'on peut importer le catalogue tel que c'est fait dans AsterStudy (c'est à dire en
  | utilisant les objets de 'code_aster.Cata.Language' au lieu de 'bibpyt/Noyau...').

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : vocab01b
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28259 DU 22/11/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 13.6)
- TITRE : MACR_SPECTRE - limitation mot-clé multiple RESU pour le cas 'DEPL'
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | MACR_SPECTRE avec un champ de type 'DEPL' interdit de fournir plus de 3 résultats.
  | 
  | 
  | Analyse
  | -------
  | 
  | (analyse détaillée dans la fiche)
  | 
  | La limite à la prise en compte de 3 calculs vient de pratiques habituelles.
  | Rien n'empêche de prendre en compte plus de calculs transitoires.
  | 
  | 
  | Correction
  | ----------
  | 
  | On supprime la limite à 3 occurrences de résultat y compris quand on donnes des champs de déplacements et on le précise dans la doc
  | de MACR_SPECTRE.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.32.11
- VALIDATION : sdnv112a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 27688 DU 24/05/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Ajout d'une MACRO pour l'identification des combinaisons dimensionnantes dans le calcul du ferraillage
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Développer dans le cadre du projet "Outil de calcul avancé en génie civil" une MACRO 
  | permettant :
  | 
  | - d'obtenir un champ de résultats contenant les résultats obtenus avec différentes 
  | combinaisons de chargement définies par l'utilisateur (en lien avec les normes EC2). 
  | Dans le but d'être en capacité de visualiser les résultats pour toutes les 
  | combinaisons. 
  | - d'obtenir les cartes de ferraillage, c'est-à-dire, de distinguer et de stocker 
  | élément par élément la combinaison dimensionnante en terme de ferraillage et 
  | l'évaluation du ferraillage associé. Résultats que l'on souhaite ensuite être en 
  | mesure de visualiser. Pour ce faire, la macro utilise la commande CALC_FERRAILLAGE 
  | (fonctionnelle pour les éléments de structures voiles et planchers uniquement) pour 
  | chaque combinaison. A l'appel de la macro, les combinaisons sont nommées et classées 
  | par catégories (ELS, ELU), selon les normes de l'EC2. 
  | 
  | 
  | Développement
  | -------------
  | 
  | Une nouvelle macro-commande est produite : COMBINAISON_FERRAILLAGE.
  | 
  | Au passage, CALC_FERRAILLAGE est étendu aux concepts mult_elas.
  | 
  | 
  | Validation
  | ----------
  | 
  | - nouvelle modélisation sdls126e
  | - nouveaux cas-tests : sdls145a et sdls146a

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.81.44, v3.03.126, v3.03.145, v3.03.146
- VALIDATION : sdls126e, sdls145a, sdls146a
- NB_JOURS_TRAV  : 20.0


================================================================================
                RESTITUTION FICHE 28218 DU 09/11/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : Différence de convergence lié à DEFI_FONCTION ?
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | On fait un calcul thermique non linéaire. On définit les paramètres matériaux RHO_CP et LAMBDA.
  | 
  | On utilise un prolongement constant à gauche.
  | Dans le 1er cas, la courbe RHO_CP est définie en 0, 20, 50, ..., 300.
  | Dans le second cas, la courbe RHO_CP est définie en 20, 50, ..., 300.
  | 
  | Le second calcul ne converge pas au pas de temps numéro 676.
  | 
  | 
  | Analyse
  | -------
  | 
  | En thermique non linéaire (mot-clé THER_NL de DEFI_MATERIAU), on peut définir RHO_CP ou BETA.
  | Si BETA (enthalpie) est fourni, on l'utilise directement, sinon on le calcule en intégrant RHO_CP (cf. r5.02.02). Le prolongement de
  | BETA est obligatoirement linéaire (cf. u4.43.01).
  | 
  | Si on compare les fonctions BETA calculées par DEFI_MATERIAU, on a dans le 1er cas:
  | 
  |       1 -  0.00000D+00  2.00000D+01  5.00000D+01  1.00000D+02  1.50000D+02
  |       6 -  2.00000D+02  2.50000D+02  3.00000D+02  3.50000D+02  0.00000D+00
  |      11 -  7.20600D-02  1.82265D-01  3.73390D-01  5.73490D-01  7.80190D-01
  |      16 -  9.90940D-01  1.20497D+00  1.42104D+00
  | 
  | et dans le second :
  | 
  |       1 -  2.00000D+01  5.00000D+01  1.00000D+02  1.50000D+02  2.00000D+02
  |       6 -  2.50000D+02  3.00000D+02  3.50000D+02  0.00000D+00  1.10205D-01
  |      11 -  3.01330D-01  5.01430D-01  7.08130D-01  9.18880D-01  1.13291D+00
  |      16 -  1.34898D+00
  | 
  | Les deux fonctions sont bien identiques à une constante près... là où elles sont définies.
  | 
  | Or à partir de l'instant de calcul numéro 666, l'enthalpie est évaluée en dessous de 20°C.
  | Donc à partir de cet instant, on a des valeurs différentes.
  | 
  | 
  | Correction
  | ----------
  | 
  | Quand on intègre l'enthalpie BETA à partir de RHO_CP, on y ajoute une constante de sorte que l'enthalpie soit toujours positive (on
  | fait un prolongement linéaire et on calcule cette constante pour que l'enthalpie soit nulle à T=-273.15°C).
  | Cette constante est sans impact sur les résultats car elle disparaît lors de la résolution (cf. r5.02.02).
  | 
  | On modifie une valeur de non régression qui varie de 1.2e-4% sur ort001a (toutes les valeurs testées en non régression sont
  | identiques sur 12 chiffres significatifs entre Aster5 et Calibre9).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.43.01
- VALIDATION : étude jointe
- NB_JOURS_TRAV  : 2.5


================================================================================
                RESTITUTION FICHE 27679 DU 22/05/2018
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : anomalie concernant code_aster (VERSION 14.4)
- TITRE : Base non recopiee
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | A la suite d'un long calcul MPI, l'exécution s'arrête par manque de temps cpu et ne recopie pas la base globale que l'on avait
  | demander de compresser.  
  | 
  | 
  | analyse et conseils
  | -------------------
  | La gestion des fichiers est à la main du gestionnaire de travaux qui détruit systématiquement le répertoire de travail, il n'y a
  | donc pas moyen de récupérer les fichiers si la recopie par le script échoue. L'opération de compression de la base peut réclamer
  | beaucoup de temps, dans la mesure du possible il est préférable d'effectuer cette opération lorsqu'on a déjà récupéré la base. La
  | compression n'apportera un gain de taille important que si on a détruit beaucoup de concepts stockés sur la base Globale au cours de
  | l'exécution.
  | Il est possible de conserver un peu de temps cpu pour les opérations effectuées après la commande FIN en utilisant le mot clé
  | RESERVE_CPU dans DEBUT ou POURSUITE. L'estimation de la durée de l'opération par l'utilisateur n'est pas facile et dépend du type de
  | calcul (et des échecs précédents).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 0.05


================================================================================
                RESTITUTION FICHE 28002 DU 04/09/2018
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 14.5)
- TITRE : Fuite mémoire dans dll_mfront.c
- FONCTIONNALITE :
  | Problème
  | --------
  | En faisant passer valgrind sur ssnv205b, on observe une fuite mémoire dans la routine ddl_mfront.c faisant l'interface entre mfront
  | et le fortran.
  | 
  | 
  | Analyse :
  | ---------
  | Étonnamment, je n'ai plus aucun message de valgrind concernant dll_mfront.c.
  | 
  | Je classe la fiche sans suite.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv205b avec valgrind
- NB_JOURS_TRAV  : 0.02


================================================================================
                RESTITUTION FICHE 27176 DU 29/11/2017
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : En version 14.0.16, couverture et PETSC
- FONCTIONNALITE :
  | Problème : 
  | ==========
  | En version 14.0.16 mpi coverage, les tests 
  | petsc01j
  | zzzz380a
  | zzzz352b
  | zzzz352a
  | échouent avec une erreur PETSc "Segmentation Violation, probably memory access out of range". 
  | 
  | Analyse :
  | ========
  | Tous ces tests concernent les Lagrange. Avec la nouvelle implémentation des Lagrange, ils passent sans problème (y compris en
  | version mpi, coverage)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : petsc01j, zzzz352b, zzzz352a
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 27132 DU 09/11/2017
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : anomalie concernant code_aster (VERSION 14.1)
- TITRE : Vérification des interfaces des routines PETSc
- FONCTIONNALITE :
  | Problème
  | ========
  | Lorsqu'on compile des routines aster contenant des appels à des routines PETSc, on est averti par le compilateur que l'interface des
  | ces routines est implicite.
  | Pourtant, cette interface devrait être explicite.
  | 
  | En effet, on procède dans aster comme préconisé dans http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Sys/UsingFortran.html:
  | 
  | To use PETSc with Fortran you must use both PETSc include files and modules. 
  | At the beginning of every function and module definition you need something like
  | 
  | #include "petsc/finclude/petscmat.h"
  | use petscmat
  | 
  | Les interfaces Fortran des routines de la librairie PETSc sont normalement définies dans les modules fournis par la librairie 
  | => dans l'exemple précédent, c'est petscmat.mod qui doit contenir l'interface.
  | 
  | On constate que les modules (.mod) sont bien présents dans la librairie PETSc compilée, ils sont bien trouvés par le compilateur,
  | mais ils ne semblent pas contenir l'interface.
  | 
  | Le problème n'est pas nouveau. Il était présent depuis au moins la version 3.7.3
  | 
  | Analyse 
  | =======
  | Les interfaces des routines Fortran dans la librairie PETSc sont générées automatiquement (package Sowing/bfort-tool)
  | sauf pour certaines fonctions (fonctions avec des arguments de type chaine de caractères, pointeur de fonction, argument pouvant
  | prendre la valeur NULL, arguments polymorphiques) 
  | Ce sont ces interfaces qui manquent dans la librairie PETSc (et dont l'absence est détectée par gfortran -Wimplicit-interface)
  | 
  | Solution 
  | ========
  | Je vais soumettre les interfaces manquantes à PETSc, puis on effectuera une montée de version PETSc qui corrigera ce problème
  | d'interfaces absentes : j'émets une fiche d'évolution dans ce but issue28301
  | Rien à faire dans aster.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 26271 DU 15/03/2017
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : ORIENTATION/AFFE_CARA_ELEM : impact inattendu sur les résultats
- FONCTIONNALITE :
  | Message original :
  | ------------------
  | Dans l'analyse modale avec un modèle filaire de tuyauterie (modélisation POU_D_T/POU_C_T), les fréquences propres obtenues exhibent
  | une influence non-négligeable et inattendue du mot clé ORIENTATION (sous AFFE_CARA_ELEM), sachant qu'a priori le changement des
  | repères locaux n'est pas censé modifier les résultats pour les comportements isotropes.
  | 
  | 
  | Analyses et tests complémentaires : (Cf tableau)
  | -----------------------------------
  | Dans le maillage, il y a 3 coudes en POU_C_T, dont un qui n'est pas plan. Pour ce coude, une partie est modélisée en POU_D_T puis
  | est prolongée par 1 seul élément POU_C_T, on a donc un modèle mixte entre les POU_C_T et POU_D_T dans un des coudes. Les tests
  | réalisés :
  | Avec la version V12, à partir des données initiales fournies :
  | * Sans_orie_V12, Avec_Orie_V12 : correspond à l'étude fournie avec et sans orientation. L'écart le plus important est de 0.353% sur
  | le 9ème mode ce qui correspond à un écart de 0.08Hz entre 22.850Hz et 22.769Hz. 
  | Avec la version V13 :
  | * Sans_orie_V13, Avec_Orie_V13 : correspond à la même étude en V13, en remplaçant les éléments de POU_C_T par des éléments POU_D_T
  | (plusieurs mailles dans le coude). Il n'y a plus d'écart entre les solutions.
  | 
  | Remarques :
  | -----------
  | * C'est une des raisons du remplacement des POU_C_T par des POU_D_T. Bien que l'écart soit faible dans ce cas-ci, il pouvait être
  | plus important dans d'autres études et fonction de la modélisation des coudes.
  | * Dans le cas test un seul élément de type POU_C_T est utilisé pour modéliser un coude à 90°. Comme pour toute modélisation par
  | éléments finis, une analyse des résultats par rapport à la discrétisation en espace doit être réalisée.
  | * Lors du passage de la modélisation de POU_C_T vers POU_D_T, tous les cas tests de validations et de références sont passés sans
  | aucune modification des valeurs de références. Parmi ces cas tests, il y a des lignes de tuyauteries, mais aussi des cas tests avec
  | des références analytiques obtenues avec une théorie de poutre prenant en compte la courbure.
  | 
  | 
  | Tableau de résultats sur les 12 premier modes :
  | -----------------------------------------------
  | numéro    Sans_orie_V12    Avec_Orie_V12     Sans_orie_V13    Avec_Orie_V13
  |     1       5.79820E+00      5.79800E+00       5.83264E+00      5.83264E+00
  |     2       8.69050E+00      8.70396E+00       8.80472E+00      8.80472E+00
  |     3       9.61671E+00      9.62479E+00       9.75441E+00      9.75441E+00
  |     4       1.31983E+01      1.32018E+01       1.33032E+01      1.33032E+01
  |     5       1.40979E+01      1.41150E+01       1.42324E+01      1.42324E+01
  |     6       1.75472E+01      1.76104E+01       1.76826E+01      1.76826E+01
  |     7       2.00331E+01      2.00400E+01       2.01177E+01      2.01177E+01
  |     8       2.15846E+01      2.15553E+01       2.18600E+01      2.18600E+01
  |     9       2.27694E+01      2.28501E+01       2.30391E+01      2.30391E+01
  |    10       2.84667E+01      2.84789E+01       2.90475E+01      2.90475E+01
  |    11       3.05609E+01      3.05452E+01       3.06474E+01      3.06474E+01
  |    12       3.50485E+01      3.50961E+01       3.51779E+01      3.51779E+01

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : passage de l'étude
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 26892 DU 13/09/2017
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : [bb0110] MACR_CARA_POUTRE computes wrong JX
- FONCTIONNALITE :
  | Message initial :
  | =================
  | I recently had to deal with Beam elements. I made some tests, but it seems that the result of MACR_CARA_POUTRE is wrong with respect
  | to JX. The value computed is arround 1.291E4. Expected is a value of around 217.0E6 (from standard tables), which means a factor of
  | around 200 too less.
  | 
  | Analyses : à partir de "POUTRE_ArbitraryCrossSection_ToBitbucket.zip"
  | ==========
  | On compare un résultat MACR_CARA_POUTRE de 1.291E4 a une valeur de 217.0E6 (from standard tables)
  | On compare donc 1.291E4 et 217.0E6 (attention aux puissances !!), le coefficient c'est 16808 et pas 200.
  | 
  | Lorsque l'on fait une analyse de sensibilité sur JX en fonction du nombre d'élément triangle dans le maillage:
  |         JX            TRIA3
  | mesh1   3.47735E-02     448    <== maillage initial de la fiche !!! On ne retrouve pas 1.291E4 ???
  | mesh2   2.15793E-01    1802    <== maillage un peu plus fin
  | mesh3   2.16334E-01   12276    <== maillage encore un peu plus fin
  | 
  | Unités du maillage en [m]
  | 
  | En prenant, la formule de la documentation de JX pour une section creuse (AFFE_CARA_ELEM, page 23) qui est une approximation, on
  | trouve Jx=0.2088m²
  | Cette formule est issue de "Formulas for Stress and Strain" 5ème edition, Roark, Young (§9:Torsion, table 20, page 293).
  | 
  | On compare donc JX=217.0E6[unité inconnue] (from standard tables, which one and which units), avec
  | JX(formule)=0.2088m²
  | JX(mesh2)  =0.2158m²
  | JX(mesh3)  =0.2163m²
  | 
  | 
  | Conclusion :
  | ============
  | Pas de soucis, MACR_CARA_POUTRE fonctionne très bien.
  | Faire une analyse de sensibilité au maillage me semble toujours une bonne idée !!

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.42.02
- VALIDATION : vérification source
- NB_JOURS_TRAV  : 2.0

