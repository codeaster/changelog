==================================================================
Version 14.3.11 (révision a03cb7e06368) du 2019-05-06 08:42 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 28751 PIGNET Nicolas           Problème DECOUPE_LAC + ORIE_PEAU
 28535 PIGNET Nicolas           Interdire modi_maillage si DECOUPE_LAC a été utilisé.
 28753 PIGNET Nicolas           DEFI_CONTACT avec info = 2
 28758 GUILLOUX Adrien          Introduction d'un amortissement dans la loi de FLAMBAGE
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 28751 DU 09/04/2019
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : Problème DECOUPE_LAC + ORIE_PEAU
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Il y a un problème avec decoupe_lac et orie_peau. Il faut que ces commandes soient bien ordonnées sinon on finit dans un assert dans
  | stat_non_line (orie_peau puis decoupe_lac et pas l'inverse).
  | 
  | La documentation ne mentionne pas ce problème.
  | 
  | Il faudrait peut être blindé modi_maillage pour qu'il ne puisse pas faire de orie_peau s'il trouve des données ".PATCH"
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Si on détecte un ".PATCH" dans le maillage lors de MODI_MAILLAGE, on sort en erreur fatale avec le message suivant
  | 
  | "Vous essayer de faire un MODI_MAILLAGE après avoir fait CREA_MAILLAGE/DECOUPE_LAC sur ce même maillage. C'est interdit.
  | 
  | Conseil: Effectuez toutes les opérations MODI_MAILLAGE avant d'effectuer CREA_MAILLAGE/DECOUPE_LAC sur votre maillage."
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Oui. Le problème pouvait se produire après un modi_maillage sur un decoupe_lac car on modifiait la structure .PATCH que l'on utilise
  | ensuite dans STAT_NON_LINE sans que cela soit forcément détecté et donc conduire à des résultats potentiellement faux
  | 
  | Fiche en doublon avec 28535

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 13.2.10
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 13.2.10
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.23.02
- VALIDATION : perso
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 28535 DU 15/02/2019
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : Interdire modi_maillage si DECOUPE_LAC a été utilisé.
- FONCTIONNALITE :
  | Fiche en doublon avec 28751
  | 
  | Problème/Objectif
  | -----------------
  | 
  | Si l'on utilise une opération de MODI_MAILLAGE après découpe LAC, on a de très grandes chances de casser la structure de données
  | .PATCH lorsque l'on réalise des opérations sur les mailles découpées. On propose d'avertir l'utilisateur et de lui suggérer
  | d'utiliser MODI_MAILLAGE avant DECOUPE_LAC si possible
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | On corrige en même temps que 28751
  | 
  | Si on détecte un ".PATCH" dans le maillage lors de MODI_MAILLAGE, on sort en erreur fatale avec le message suivant
  | 
  | "Vous essayer de faire un MODI_MAILLAGE après avoir fait CREA_MAILLAGE/DECOUPE_LAC sur ce même maillage. C'est interdit.
  | 
  | Conseil: Effectuez toutes les opérations MODI_MAILLAGE avant d'effectuer CREA_MAILLAGE/DECOUPE_LAC sur votre maillage."
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Non.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : perso
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 28753 DU 09/04/2019
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : DEFI_CONTACT avec info = 2
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | INFO=2 ne marche pas dans defi_contact avec la méthode LAC. On finit dans un assert.
  | 
  | Je propose que l'on ne s'arrête plus en assert avec INFO=2 et LAC. On continue normalement.
  | 
  | On pourra rajouter plus d'informations avec INFO = 2 si besoin est plus tard.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | On compète surfcp pour la méthode LAC et on enlève l'assert
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Non

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnp170a
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 28758 DU 10/04/2019
================================================================================
- AUTEUR : GUILLOUX Adrien
- TYPE : evolution concernant code_aster (VERSION 14.2)
- TITRE : Introduction d'un amortissement dans la loi de FLAMBAGE
- FONCTIONNALITE :
  | Deux mots-clés facultatifs sont introduits dans COMPORTEMENT de DYNA_VIBRA :
  | 
  | - AMOR_FL pendant le flambage,
  | 
  | - AMOR_POST_FL qui est une liste d'amortissement par enfoncement.
  | 
  |  
  | 
  | S'ils ne sont pas renseignés, les valeurs d'amortissement pendant et après le flambage sont celles de la phase élastique indiquées
  | par AMOR_NOR
  | 
  |  
  | 
  | Validation :
  | 
  | Ajout d'une modélisation D au cas test sdnd105 (Cas test de non régression)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.53.03 U4.53.21
- VALIDATION : sdnd105
- NB_JOURS_TRAV  : 4.0

