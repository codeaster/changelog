==================================================================
Version 14.4.10 (révision 712ae5840d3b) du 2019-11-08 10:33 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 27208 DROUET Guillaume         Normale='ESCL'/'MAIT_ESCL' en calcul parallèle
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 27208 DU 06/12/2017
================================================================================
- AUTEUR : DROUET Guillaume
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Normale='ESCL'/'MAIT_ESCL' en calcul parallèle
- FONCTIONNALITE :
  | Problème:
  | =========
  | 
  | Suite à une analyse sur la variabilité des résultats 1 proc Vs 3 procs sur ssnv104b Issue25948, il apparaît que le choix de normale
  | esclave est problématique en calcul parallèle.
  | 
  | Analyse:
  | ========
  | 
  | Après une seconde analyse, le problème est bien présent pour la méthode discrète dans le cas NORMAL = ESCLAVE ou MAITRE_ESCLAVE mais
  | quelque soit le mot clé APPARIEMENT choisi.
  | 
  | Le problème vient la non-réinitialisation du vecteur sdappa(1:19)//'.TGNO' à chaque appariement. Cet oubli ne pose pas de problème
  | en séquentiel. En effet les données sont écrasées proprement. Cependant, en mpi le ALL_REDUCE+SUM par sdmpic provoque un cumul du
  | vecteur lors des appariements successifs.
  | 
  | 
  | Solution:
  | =========
  | 
  | Pour corriger le problème on réinitialise les vecteurs sdappa(1:19)//'.TGNO' sdappa(1:19)//'.TGEL' à chaque re-calul des normales.
  | 
  | 
  | Résultats:
  | ==========
  | 
  | Il y des résultats faux en version MPI sur 2 proc et plus lors de l'utilisation de NORMALE = ESCLAVE ou MAITRE_ESCLAVE pour les
  | méthodes discrètes. Le sous-cas APPARIEMENT= NODALE est blindé depuis Issue25948.

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 13.1.22
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 13.1.22
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : submit + ssnv104b sur 3 proc
- DEJA RESTITUE DANS : 15.0.14
- NB_JOURS_TRAV  : 1.5

