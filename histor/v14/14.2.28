==================================================================
Version 14.2.28 (révision d752ecaf4c46) du 2019-02-14 07:51 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 27298 TAMPANGO Yannick         Couverture de mdtrib
 28176 TAMPANGO Yannick         Erreur possible avec DYNA_LINE/GENE et vitesse initiale
 28475 LEFEBVRE Jean-Pierre     En version 14.2.25-70421d6daf0d, les tests crack01a, forma07b, sslv110f, sslv...
 28467 COURTOIS Mathieu         CALC_MISS - Erreur code retour incorrect
 26861 BOITEAU Olivier          CALC_MODES + PROCHE/SEPARE/AJUSTE: pb cv ou résultats faux avec MUMPS
 28479 TARDIEU Nicolas          Bug dans FIELDSPLIT
 28471 ALVES-FERNANDES Vinicius Message d'erreur de CALC_ESSAI_GEOMECA
 28449 FLEJOU Jean Luc          SPMX_ELGA : une petite trace
 28494 FLEJOU Jean Luc          Impression au format MED
 28458 GEOFFROY Dominique       En 14.2.25-03746caea57d, le test ssns106a est NOOK sur clap0f0q
 28498 DROUET Guillaume         Message indigne dans DECOUPE_LAC
 28503 ABBAS Mickael            Calcul LAC qui plante depuis maj
 28457 BOITEAU Olivier          En 14.2.25-03746caea57d, les tests miss10a, miss10b et miss10c sont NO_TEST_R...
 28469 ALVES-FERNANDES Vinicius Restitution cas test de colonne de sol 1D sdls128 avec la loi d'Iwan
 28072 KUDAWOO Ayaovi-Dzifa     En version 14.2.10-1f27ce5c5d4e, le cas tes ssnv249b est NOOK sur Aster5_vali...
 28480 BOITEAU Olivier          En version 14.2.26, augmentation des temps CPU
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 27298 DU 15/01/2018
================================================================================
- AUTEUR : TAMPANGO Yannick
- TYPE : anomalie concernant code_aster (VERSION 14.1)
- TITRE : Couverture de mdtrib
- FONCTIONNALITE :
  | Problème
  | ========
  | 
  | Couverture de mdtrib : routine qui effectue un tri -méthode de Bulle- de modes selon la souplesse décroissante avant d'imprimer
  | quelques informations à l'utilisateur.
  | 
  | mdtrib est appelé dans DYNA_VIBRA uniquement : 
  | - dans un calcul transitoire sur base généralisée
  | - en présence de chocs
  | - si l'option VERI_CHOC est activée
  | - si INFO=2
  | 
  | 
  | Correction
  | ==========
  | 
  | On met INFO=2 dans un appel à DYNA_VIBRA avec VERI_CHOC dans le cas-test sdnl111b.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Cas test
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28176 DU 24/10/2018
================================================================================
- AUTEUR : TAMPANGO Yannick
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : Erreur possible avec DYNA_LINE/GENE et vitesse initiale
- FONCTIONNALITE :
  | Problème :
  | ==========
  | 
  | Utilisation de vitesse initiale (et par extension déplacement initial) dans DYNA_VIBRA et DYNA_LINE. Lorsqu'on travaille sur base
  | généralisée, on remarque que la vitesse initiale du concept résultat après restitution dans la base physique n'est pas la même que
  | celle donnée en entrée de DYNA_VIBRA ou DYNA_LINE.
  | 
  | On illustre le soucis à partir du cas test sdnd103a (masse-ressort) modifié. 
  | La vitesse initiale est V0 = 3.49065850398866E-03
  | 
  | Suivant la méthode de calcul, on obtient les résultats suivants pour la vitesse à l'instant 0 :
  | 1- Calcul sur base physique (DYNA_LINE/PHYS)                          3.49065850398866E-03
  | 2- Calcul sur base modale (DYNA_LINE/GENE)                            3.49065850398866E-08
  | 3- Calcul sur base modale (DYNA_VIBRA/GENE )                          3.49065850398866E-03
  | 4- Calcul sur base modale normalisée par MASS_GENE (DYNA_VIBRA/GENE ) 7.75701889775258E-06
  | 5- Calcul sur base modale normalisée par RIGI_GENE (DYNA_VIBRA/GENE ) 3.49065850398866E-08
  | 
  | Analyse :
  | ============
  | 
  | Après analyse, on remarque que le problème vient de la projection de la vitesse initiale sur la base avant l'appel à DYNA_VIBRA. En
  | effet, lorsqu'on qu'on fait la projection de la vitesse initiale sur la base généralisée, on oublie de renseigner le mot clé
  | facultatif TYPE_VECT qui a pour objectif de préciser le type du vecteur projeté, à savoir ici "VITE". La projection effectuée par
  | l'opérateur dépend du type de vecteur à projeter (voir doc PROJ_BASE). Par défaut ce mot clé prend la valeur "FORC".
  | DYNA_LINE embarque la projection de la vitesse initiale sur base modale, mais avec la même erreur que dans le cas test (sans
  | préciser le TYPE_VECT).
  | 
  | Correction :
  | ============
  | Correction du cas test sdnd103a en précisant le TYPE_VECT dans PROJ_BASE
  | 
  | Correction de DYNA_LINE en précisant le TYPE_VECT dans la fonction qui effectue la projection de l'état initial.

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 13.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 14.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Cas test
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28475 DU 01/02/2019
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : En version 14.2.25-70421d6daf0d, les tests crack01a, forma07b, sslv110f, sslv319b,sslv319d	, sslv134h,,sslv154b, sslv155c, sslv322b, sslv322c et zzzz339b échouent sur clap0f0q
- FONCTIONNALITE :
  | Problème :
  | ----------
  | 
  | Avec la version 14.2.25-70421d6daf0d plusieurs tests de calcul de G échouent sur clap0f0q. Le problème ne se produit qu'avec la
  | version optimisée. 
  | 
  | Le passage des tests avec valgrind lève une erreur dans la routine te0048 :
  | 
  | ==1004== Conditional jump or move depends on uninitialised value(s)
  | ==1004==    at 0x97786B2: te0048_ (te0048.F90:512)
  | ==1004==    by 0x886F782: te0000_ (te0000.F90:762)
  | ==1004==    by 0x885B4EA: calcul_ (calcul.F90:387)
  | ==1004==    by 0x8DF7A4D: cakg3d_ (cakg3d.F90:450)
  | ==1004==    by 0x9310B41: op0100_ (op0100.F90:369)
  | ==1004==    by 0x9736304: ex0000_ (ex0000.F90:428)
  | ==1004==    by 0x9736866: execop_ (execop.F90:66)
  | ==1004==    by 0x9736AE3: expass_ (expass.F90:41)
  | ==1004==    by 0x80ED813: aster_oper (aster_module.c:1682)
  | ==1004==    by 0x42075AC: PyCFunction_Call (methodobject.c:81)
  | ==1004==    by 0x426FD70: PyEval_EvalFrameEx (ceval.c:4035)
  | ==1004==    by 0x426FCC4: PyEval_EvalFrameEx (ceval.c:4121)
  | ==1004==  Uninitialised value was created by a stack allocation
  | ==1004==    at 0x9775BA8: te0048_ (te0048.F90:19)
  | 
  | Un "goto 999" noyé dans les 500 lignes de code, saute l'étape d'affectation des variables coeff et coeff3, ce qui produit une erreur
  | dans l'évaluation de sqrt sur certaines plates-formes.  
  | 
  | Correction
  | ----------
  | 
  | On affecte les deux variables à 0.d0 et on supprime le goto 999. 
  | 
  | Résultat potentiellement faux suivant les valeurs de coeff et coeff3.

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 13.1.4
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 13.1.4
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : crack01a, forma07b, sslv110f, sslv319b,sslv319d	, sslv134h,,sslv154b, sslv155c, sslv322b, sslv322c et zzzz339b
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28467 DU 31/01/2019
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 13.4)
- TITRE : CALC_MISS - Erreur code retour incorrect
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Quand on n'utilise pas tous les matériaux définis dans DEFI_SOL_MISS, il peut y avoir des trous dans la numérotation qui pose
  | problème ensuite.
  | 
  | 
  | Correction
  | ----------
  | 
  | Les matériaux sont affectés par le numéro d'occurrence de leur définition.
  | Si les derniers ne sont pas affectés, ça ne pose pas de problème.
  | En revanche, s'il y a des matériaux "intermédiaires" non utilisés, il y a des erreurs par la suite.
  | 
  | On ajoute donc un test final : si des occurrences des matériaux ne sont pas utilisées, et que ce ne sont pas les dernières, on émet
  | un message d'erreur.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u7.02.34
- VALIDATION : tests miss
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 26861 DU 05/09/2017
================================================================================
- AUTEUR : BOITEAU Olivier
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : CALC_MODES + PROCHE/SEPARE/AJUSTE: pb cv ou résultats faux avec MUMPS
- FONCTIONNALITE :
  | PROBLEME
  | ========
  |   Lors du passage du solveur linéaire MUMPS en solveur par défaut (à la place de MULT_FRONT,
  |   en sept. 2017, v14.1), on s'est aperçut que la combinaison
  |                               CALC_MODES + SEPARE/AJUSTE/PROCHE + MUMPS
  |   pouvait conduire à des résultats faux ou incomplets.
  |   Elle a donc été interdite (message UTMESS_F ALGELINE5_72 + mise à jour des documentations).
  |   Les cas-tests ayant mis en exergue le problème (quelque soit la plate-forme) sont:
  | sdld02e  
  | sdll135a
  | sdll135c
  | sdll135e
  | ssll404a
  | sdll09a 
  | sdll135d
  | sdls503a
  | sdlx01a
  | 
  | ANALYSE
  | =======
  |    1/ Une partie du problème vient d'un bug dans la manipulation du déterminant transmis par MUMPS. Il 
  | peut se produire un dépassement de capacité dans le calcul
  |                 mantis = mantis/ (10**ie) (ligne 166 de bibfor/algeline/mtdete.F90).
  | Le problème (mis en exergue par sdld02e) se corrige avec la changement:
  |                mantis = mantis/ (10.d0**ie).
  |   
  |    Vérification: sdld02e sur C9 et Aster5.
  | 
  |   2/ Tous les autres problèmes semblent provenir du fait que les systèmes linéaires résolus
  |   dans le cas de cette méthode modale par itération inverse sont singuliers. C'est voulu par la
  |   méthode (cf. 4.3.1 R5.01.01). Le pb est que, lorsque le pb est trop singulier (critère paramétrable
  |   par SOLVEUR/NPREC) MUMPS ne résout alors que la partie régulière du système et fausse donc le résultat.
  |   Alors que les deux autres solveurs directs, MULT_FRONT et LDLT, ne se posent pas de pb et continuent
  |   la résolution jusqu'à une éventuelle division par zéro.
  | 
  |   Normalement, dans les autres options de calcul modal (les plus usitées: BANDE, PLUS_PETITE...), on
  |   évite ce biais en décalant d'un certain pourcentage le shift menant à ces matrices quasi-singulières
  |   (paramètres PREC/NMAX_ITER_SHIFT..). Cela permet des systèmes linéaires numériquement plus propres
  |   et met donc sur un pied d'égalité (à peu près) tous les solveurs directs concernant leur sensibilité
  |   à la détection de singularité.
  | 
  |   Ici, avec les options PROCHE/SEPARE/AJUSTE, ce n'est pas fait. Ou du moins pas fait de manière 
  | homogène! Pour gagner du temps (sinon la méthode serait trop coûteuse) ou du fait de l'historique de 
  | développement (ou pas copier/coller):
  |    - certaines parties de l'algo appliquent le principe (vertueux) de décalage de shift,
  |    - d'autres ne gèrent rien,
  |    - d'autres décalent forfaitairement de 1% ! Et a priori, sans borne d'arrêt !
  |    - d'autres décale... le résultat de 5%.
  | 
  |   Bref, en prenant en compte tous ces éléments et sans rien révolutionner, juste en demander 
  | ponctuellement à MUMPS d'ignorer son traitement des singularités (NPREC=99) et ensuite en reprenant la 
  | valeur réellement paramétrée par l'utilisateur, on arrive à s'en sortir.
  | 
  |   3/Cela avait oublié dans la fiche mais le pb peut aussi se poser si on active le mot-
  | clé 'AMELIORATION'='OUI' des autres options de calcul modal ('BANDE'...). Car cela switch, en post-
  | traitement, vers l'option 'PROCHE'.
  | Cf. cas-tests sdls106h
  |               sdls504a
  |               ssll103a
  |               zzzz337a
  | 
  | 4/ Restitution (None ou complète ou partielle) à discuter en EDA.
  | 
  | VALIDATION
  | ===========
  | Sur C9 et Aster5 avec MUMPS à la place de MULT_FRONT
  | sdld02e  
  | sdll135a
  | sdll135c
  | sdll135e
  | ssll404a
  | sdll09a 
  | sdll135d
  | sdls503a
  | sdlx01a
  | sdls106h
  | sdls504a
  | ssll103a
  | zzzz337a
  | 
  | NON-REGRESSION
  | ==============
  |   Les 560 cas-tests modaux sur C9 avec MUMPS solveur linéaire par défaut.
  | 
  | RESTITUTION PROPOSEE
  | =====================
  | M astest/sdld02e.comm  ----------------> pour vérifier résolution pb 1
  | M astest/sdls106h.comm ----------------> pour vérifier résolution pb 3
  | M astest/ssll404a.comm ----------------> pour vérifier résolution pb 2: option PROCHE/SEPARE/AJUSTE
  | M bibfor/algeline/mtdete.F90 ----------> pour résoudre pb 1
  | M bibfor/op/op0044.F90 ----------------> pour pallier aux pb 2/3
  | M bibpyt/Messages/algeline5.py --------> pour enlever le message d'arrêt qui verrouillait l'usage.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.52.02,R6.02.03
- VALIDATION : informatique, non-régression
- NB_JOURS_TRAV  : 5.0


================================================================================
                RESTITUTION FICHE 28479 DU 04/02/2019
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Bug dans FIELDSPLIT
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Pour certaines tailles de maillages, le test petsc04c plante dans PETSc, qui se plaint que la taille d'un certain vecteur n'est pas
  | cohérente.
  | 
  | Correction/Développement
  | ------------------------
  | Dans la routine setNearNullSpace du module aster_fieldsplit_module.F90, on supposait injustement que le vecteur global d'inconnues
  | avait une taille de blocs de 3 (en gros, que le nombre total d'inconnues est divisible par 3). Ce n'est bien sûr pas le cas. On
  | règle le problème en fixant la taille de blocs à 1.
  | 
  | 
  | Résultat faux
  | -------------
  | Sans objet

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Passage tests
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28471 DU 01/02/2019
================================================================================
- AUTEUR : ALVES-FERNANDES Vinicius
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : Message d'erreur de CALC_ESSAI_GEOMECA
- FONCTIONNALITE :
  | Anomalie et solution :
  | --------------------------
  | Le message d'erreur de CALC_ESSAI_GEOMECA relatif au signe de la contrainte effective moyenne fait allusion à la convention standard
  | de code_aster (traction positive). Or, les modifications apportées à la macro-commande des issue26111 à issue26119 changent la
  | convention utilisé à celle de la mécanique des sols (compression positive).
  | 
  | On modifie le message d'erreur pour être conforme à cette nouvelle convention.
  | 
  | Fichier impactés :
  | --------------------
  | bibpyt/Messages/compor2.py
  | 
  | Impact documentaire :
  | ---------------------
  | Aucun, car la doc u4.90.21 est à jour dans le serveur.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : .
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 28449 DU 24/01/2019
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : SPMX_ELGA : une petite trace
- FONCTIONNALITE :
  | Dans NOM_CHAM_INTO d['AUTRES'] il y a "SPMX_ELGA"
  | 
  | Cette option n'existe plus depuis 11.0.22 (fiche 16205)
  | ==> suppression du catalogue de NOM_CHAM_INTO

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : aucune
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 28494 DU 07/02/2019
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Impression au format MED
- FONCTIONNALITE :
  | Lors de l'étude d'une plaque dans le plan XY (tous les Z sont à 0)
  | - maillage
  | - export au format MED ==> maillage 2D 
  | - modèle avec des DKT ==> pas de problème pour code_aster, c'est normal.
  | - résultats au format MED ==> paquet de nouilles dans le fichier MED (le dump est vraiment bizarre) et quand salome peut ouvrir le
  | fichier, c'est du n'importe quoi. Cela doit venir des sous-points qui ne peuvent exister qu'en 3D alors que les coordonnées sont en 2D.
  | 
  | Dans code_aster :
  | -----------------
  | La détection de la dimension au moment de la sauvegarde au format MED ne devrait pas se faire sur la dimension du maillage mais sur
  | le modèle (D_PLAN, AXIS, ... ) ou sur les résultats.
  | 
  | Dans irmail, il y a
  |     call dismoi('DIM_GEOM_B', noma, 'MAILLAGE', repi=ndim)
  | Puis les impressions suivant les différents formats, avec ndim en entrée
  | 
  | Remplacement par 
  |     call dismoi('DIM_GEOM', nomo, 'MODELE', repi=repi)
  | avec repi      =   1  : 1D                      ndim=1
  | !              =   2  : 2D                      ndim=2
  | !              =   3  : 3D                      ndim=3
  | !              = 120  : 1D+2D     MELANGE       ndim=2
  | !              = 103  : 1D+3D     MELANGE       ndim=3
  | !              =  23  : 2D+3D     MELANGE       ndim=3
  | !              = 123  : 1D+2D+3D  MELANGE       ndim=3

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u7.05.21
- VALIDATION : passage cas tests
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28458 DU 28/01/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : En 14.2.25-03746caea57d, le test ssns106a est NOOK sur clap0f0q
- FONCTIONNALITE :
  | Analyse
  | =======
  | Les NOOK du cas test ssns106a semblent liés à une propagation des 
  | erreurs de précisions.
  | Les valeurs testées correspondent au ratio de pente GAMMA_F (par rapport 
  | à la pente élastique) pour des sections peu résistantes (GAMMA_F= 
  | 0,00125146425926961 ).
  | 
  | On a vérifié des valeurs intermédiaires de calculs avant l'obtention de 
  | cette valeur:
  | 
  |                           Aster5          Clap0f0q       Erreur
  | EFM                   3221651,652        3221651,748     2,98023E-08
  | M1                    8263,147742        8263,147683        7,07885E-09
  | M2                    8263,13931        8263,139252        7,07209E-09
  | dkappa                  2,09E-06        2,09E-06        3,54336E-08
  | pend=(M1-M2)/dkappa     4,03E+03        4,03E+03        6,59321E-06
  | GAMMA_F=pend/EFM        1,25E-03        1,25E-03        6,62366E-06
  | 
  | Solution
  | ===========
  | On propose de tester ces valeurs de non-regression (proches de 1e-3) 
  | avec un CRITERE=ABSOLU pour assurer le passage des tests sur toutes les 
  | plateformes.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Màj cas-test
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28498 DU 08/02/2019
================================================================================
- AUTEUR : DROUET Guillaume
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Message indigne dans DECOUPE_LAC
- FONCTIONNALITE :
  | Problème :
  | ==========
  | 
  | La routine cnmpmc émet un message d'erreur complètement inapproprié:
  | "le mot-clé MAILLAGE est obligatoire avec le mot-clé QUAD_LINE."
  | 
  | Strictement AUCUN rapport avec la choucroute.
  | 
  | Mais le développeur s'est bien aperçu qu'il faisait nawak puisqu'on a ce magnifique commentaire: 
  | "!à changer avec un message approprié"
  | 
  | (ne pas faire attention à l'orthographe)
  | 
  | Je ne sais pas quel message il faut émettre en fait
  | 
  | Analyse :
  | =========
  | 
  | C'est un message d'erreur inutile qui aurait dû être remplacé par un ASSERT lors de l’intégration de la méthode LAC.
  | 
  | Correction :
  | ============
  | 
  | On supprime l'appel inutile à 'ALGELINE2_93', et on met un assert.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : submit
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28503 DU 11/02/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : Calcul LAC qui plante depuis maj
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | La simulation 2D AXI du dudgeonnage ne fonctionne plus depuis le commit 41870f9ecfcc.
  | 
  | Je mets en PJ une portion de simulation qui reproduit rapidement le bug.
  | 
  | 
  | Correction
  | ----------
  | 
  | La modification réalisée dans mmmbca_lac ne semble pas correcte.
  | On ne comprend pas bien pourquoi mais le patch permettant de revenir à la situation stable
  | 
  | On patche donc

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28457 DU 28/01/2019
================================================================================
- AUTEUR : BOITEAU Olivier
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : En 14.2.25-03746caea57d, les tests miss10a, miss10b et miss10c sont NO_TEST_RESU sur Aster5_Valid et Eole_Valid
- FONCTIONNALITE :
  | PROBLEME
  | ========
  |    Fichier validation/miss10a.comm avec des TEST_RESU incomplets.
  | 
  | SOLUTION
  | ========
  | Le fichier de commande miss10a.comm a été enrichi des bon TEST_RESU (avec 
  | VALE_REFE/REFERENCE/PRECISION) La PRECISION a été fixée à 0.1%. En pratique, avec miss10c 
  | (miss10a sur 24 threads) on est a, au pire 1.d-8% sur Aster5 et à 3.d-8% sur EOLE.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : non-régression
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 28469 DU 31/01/2019
================================================================================
- AUTEUR : ALVES-FERNANDES Vinicius
- TYPE : evolution concernant code_aster (VERSION 14.3)
- TITRE : Restitution cas test de colonne de sol 1D sdls128 avec la loi d'Iwan
- FONCTIONNALITE :
  | Evolution :
  | ------------
  | On propose de restituer une nouvelle modélisation au cas-test sdls128 de la base de validation de code_aster. Cette modélisation met
  | en pratique l'usage du modèle d'Iwan dans une étude d'effet de site 1D. Les résultats de cette modélisation sont comparés à la
  | modélisation standard avec DEFI_SOL_EQUI (sdls128a). On rajoute à la modélisation A les tests de non régression afin d'obtenir les
  | valeurs à comparer pour la modélisation proposée.
  | 
  | Impact sources :
  | ----------------
  | A validation/astest/sdls128d.comm
  | A validation/astest/sdls128d.export
  | A validation/astest/sdls128d.12
  | M validation/astest/sdls128a.comm
  | 
  | Impact documentaire :
  | ----------------------
  | V2.03.128 : ajout de la modélisation D

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : V2.03.128
- VALIDATION : cas-test
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 28072 DU 24/09/2018
================================================================================
- AUTEUR : KUDAWOO Ayaovi-Dzifa
- TYPE : anomalie concernant code_aster (VERSION 14.1)
- TITRE : En version 14.2.10-1f27ce5c5d4e, le cas tes ssnv249b est NOOK sur Aster5_valid et Eole_Valid
- FONCTIONNALITE :
  | Cette fiche est à raccorder au chantier issue27609: traitement automatique du coefficient de pénalisation de frottement. On
  | observait un écart de de 371%. La valeur de référence AUTRE_ASTER de ce test n'est pas très claire pour moi. 
  | 
  | L'écart observé s'explique par l'influence du coefficient de pénalisation en frottement (via GLIS_MAXI) sur le glissement donc "DX".
  | 
  | Un autre constat est que ce test utilise DECOUPE_LAC + ALGO_CONT different de LAC.
  | 
  | Correction proposée:
  | ===================
  | 
  | - Proposer une valeur de référence AUTRE_ASTER cohérente pour tester l'automatisation du coefficient de pénalisation en frottement:
  | On fait deux calculs STAT_NON_LINE avec deux algos differents. Le premier avec DEFI_CONTACT/ALGO_CONT=STD qui sert de référence et
  | le deuxième DEFI_CONTACT/ALGO_CONT=PENALISATION avec GLIS_MAXI=0.1.
  | 
  | - La valeur de référence initiale sera inévitablement modifiée.
  | 
  | - Pour de raisons d'efficacité, le premier calcul STAT_NON_LINE s'arrete après deux pas de temps tandis que le deuxième calcul va
  | jusqu'à 16. On compare les résultats à l'instant 2. 
  | 
  | - Avec la nouvelle valeur de référence, pour valider la sensibilité de l'algorithme de pénalisation automatique de frottement, on
  | devrait avoir le constat suivant: plus en resserrant GLIS_MAXI/ALGO_FROT=PENALISATION plus on converge vers la solution
  | ALGO_FROT=STANDARD "-4.0938398172E-06".C'est bien le cas. 
  | 
  | - Pour expliquer l'écart avec la référence initiale. La valeur de référence initiale a été choisie avec une valeur de COEF_PENA_FROT
  | qui n'était pas suffisament blindée. Une étude de sensibilité sur le coefficient de pénalisation aurait pu déceler ce constat (A
  | l'époque seule une valeur magique du COEF_PENA_FROT fonctionnait d'où le chantier robustesse en frottement qui a été mené).
  | L'algorithme d'automatisation du frottement d'une part et les chantiers de robustesse de frottement de l'autre montrent que
  | désormais on peut converger avec les réglages par défaut avec la méthode standard en frottement sur le cas-test de réservoir et que
  | l'automatisation de la pénalisation donne les mêmes ordres de grandeurs que la méthode de référence de frottement (STANDARD). 
  | 
  | 
  | Résultats obtenus: (Tests sur Eole)
  | ==================================
  | 
  | 1. écart initial:
  | [2] 
  | [2] --------------------------------------------------------------------------------
  | [2]  ---- RESULTAT         NUME_ORDRE       NOM_CHAM         NOM_CMP          GROUP_NO
  | [2]       MECA             16               DEPL             DX               ANC_N
  | [2]       REFERENCE        LEGENDE          VALE_REFE            VALE_CALC            ERREUR           TOLE            
  | [2] NOOK  NON_REGRESSION   XXXX             4.16212E-07          -1.13062661797E-06   371.646809311%   0.0001%         
  | [2] NOOK  AUTRE_ASTER      XXXX             4.16212E-07          -1.13062661797E-06   371.646809311%   10.0%  
  | 
  | 
  | 2. DEFI_CONTACT/ALGO_CONT=STD VS le calcul initial (AVEC DECOUPE_LAC): Nouvelle valeur de référence AUTRE_ASTER pour valider la
  | pénalisation automatique du frottement: sensibilité par rapport au GLIS_MAXI. 
  | 
  | --------------------------------------------------------------------------------
  |  ---- RESULTAT         NUME_ORDRE       NOM_CHAM         NOM_CMP          GROUP_NO
  |       MECA             17               DEPL             DX               ANC_N
  |       REFERENCE        LEGENDE          VALE_REFE           VALE_CALC           ERREUR           TOLE            
  | NOOK  NON_REGRESSION   XXXX             4.16212E-07         -4.0938398172E-06   1083.59485483%   0.0001%         
  | NOOK  AUTRE_ASTER      XXXX             4.16212E-07         -4.0938398172E-06   1083.59485483%   10.0%           
  | 
  | 
  | 3. DEFI_CONTACT/ALGO_CONT=STD VS le calcul initial (GLIS_MAXI=0.1)
  | 
  | [0] NOOK  AUTRE_ASTER      XXXX             4.16212E-07         1.84585655579E-07   55.6510490859%   10.0%     
  | 
  | 
  | 4. DEFI_CONTACT/ALGO_CONT=STD VS le calcul initial (GLIS_MAXI=0.01)
  | 
  | [8]  ---- RESULTAT         NUME_ORDRE       NOM_CHAM         NOM_CMP          GROUP_NO
  | [8]       MECA             17               DEPL             DX               ANC_N
  | [8]       REFERENCE        LEGENDE          VALE_REFE            VALE_CALC            ERREUR           TOLE            
  | [8] NOOK  NON_REGRESSION   XXXX             4.16212E-07          -2.48823109216E-07   159.782781183%   0.0001%         
  | [8] NOOK  AUTRE_ASTER      XXXX             4.16212E-07          -2.48823109216E-07   159.782781183%   10.0%           
  | 
  | 5. Valeur par défaut pour GLIS_MAXI (Avec DECOUPE_LAC)
  | 
  | [2] 
  | [2] --------------------------------------------------------------------------------
  | [2]  ---- RESULTAT         NUME_ORDRE       NOM_CHAM         NOM_CMP          GROUP_NO
  | [2]       MECA             16               DEPL             DX               ANC_N
  | [2]       REFERENCE        LEGENDE          VALE_REFE            VALE_CALC            ERREUR           TOLE            
  | [2] NOOK  NON_REGRESSION   XXXX             4.16212E-07          -1.13062661797E-06   371.646809311%   0.0001%         
  | [2] NOOK  AUTRE_ASTER      XXXX             4.16212E-07          -1.13062661797E-06   371.646809311%   10.0%    
  | 
  | 6.   Valeur par défaut pour GLIS_MAXI (Sans DECOUPE_LAC)
  | [0] NOOK  NON_REGRESSION   XXXX             4.16212E-07          -1.09465692258E-06   363.004652097%   0.0001%   
  | 
  | 
  | Restitution:
  | ============
  | 
  | - Cas test ssnv249b 
  | - Doc [V6.04.249] chez GD

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv249b
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 28480 DU 04/02/2019
================================================================================
- AUTEUR : BOITEAU Olivier
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : En version 14.2.26, augmentation des temps CPU
- FONCTIONNALITE :
  | PROBLEME
  | ========
  |    Augmentation des temps CPU des cas-tests sdlv133a et sdnx100h.
  | 
  | ANALYSE
  | =======
  |   Il me semble qu'il n'y a en fait pas de problème. Cette augmentation provient du fait 
  |   que ces cas-tests ont été modifiés afin de vérifier la bon fonctionnement de MISS3D en
  |   parallèle OpenMP.
  |   Donc le sdlv133a est passé de 1 à 4 threads OpenMP et l'autre, de 1 à 2.
  | 
  |   Ainsi les opérateurs utilisant directement ou indirectement (MUMPS) des BLAS vont plus
  |   vite mais du coup... accusent un surcoût en temps CPU. Comme les pbs sont de petites
  |   tailles ici le surcoût CPU (jusqu'à 200%) est très largement supérieur au gain en temps
  |   elapsed effectif.
  |   Ce ne sont d'ailleurs pas des opérateurs utilisant MISS3D: CALC_MODES, MODE_STATIQUE...
  | 
  |  ==> En bref, il n'y a pas de pb. Il faut juste changer les valeurs de référence ou alors
  |   ... comparer les temps elasped (peut-être plus tributaires des contingences matérielles:
  |    lenteurs réseaux ou disque...).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : non-régression, performance
- NB_JOURS_TRAV  : 0.3

