==================================================================
Version 14.5.17 (révision f2c6bb87371d) du 2020-06-09 15:57 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29891 YU Ting                  MACR_LIGN_COUP, utilisation d'un repère local à la ligne de coupe
 29881 YU Ting                  Plusieurs relations linéaires différentes dans LIAISON_RBE3
 29926 COURTOIS Mathieu         Report incomplet de #27242
 28683 ABBAS Mickael            anomalie liste instant négative avec dyna_non_line
 28985 ABBAS Mickael            Incohérence prise en compte thermique selon ETAT_INIT
 28912 ABBAS Mickael            [FAUX] CALC_ERC_DYN est faux s'il y a plus d'une composante observée
 29833 ABBAS Mickael            Erreur IMPR_RESU d'un champ META_ELNO
 29949 YU Ting                  OBSERVATION de DYNA_LINE_TRAN donne INST uniquement à t0
 28955 MATHIEU Tanguy           [RUPT] Il y a des incohérences dans la sortie de POST_RCCM avec l'option TYPE...
 29913 PIGNET Nicolas           Contact analysis with shells with a Vertical line of contact
 29037 FLEJOU Jean Luc          Impression des des coordonnées des noeuds d'un élément TUYAU après un POST_CHAMP
 29004 AUDEBERT Sylvie          [Sismique spectrale] Erreur rencontrée en mode multi-appui décorrelé
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29891 DU 19/05/2020
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 14.4)
- TITRE : MACR_LIGN_COUP, utilisation d'un repère local à la ligne de coupe
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Lors de l'utilisation MACR_LIGN_COUPE en définissant le vecteur normal du repère local avec REPERE=LOCAL et VECT_Y (en 3D),
  | on a aperçu que les tables de sorties sont identiques, pour 2 choix de VECT_Y orthogonaux !
  | 
  | 
  | Analyse
  | ------------------------
  | 
  | MACR_LIGN_COUPE est une macro commande qui fait :
  | (1) créer un nouveau maillage (seulement la ligne de coupe)
  | (2) projet le résultat ou champ sur ce nouveau maillage
  | (3) si on ne veut pas utiliser le repère globle, 
  |     on oriente le résultat avec MODI_REPERE (avec ANGL_NAUT = (alpha,beta,gamma) )
  | (4) on obtient les valeurs dans ce résultat avec POST_RELEVE_T
  | 
  | Le problème vient de la détection de dimension dans MODI_REPERE à l'étape (3).
  | Pour un cas en 3D, le nouveau maillage (la ligne ) est toujours détecté en 2D.
  | Donc seulement la 1ère valeur (alpha) dans ANGL_NAUT est prise en compte pour orienter le résultat.
  | 
  | En vérifiant, une alarme ALGORITH12_43 est prévu pour avertir ce cas (2D détectée mais avec 3 valeurs dans ANGL_NAUT).
  | Mais elle est déactivée dans macro commande comme toutes les autres alarmes.
  | 
  | 
  | Correction
  | ------------------------
  | 
  | Dans MODI_REPERE, la dimension est détectée à partir du maillage.
  | J'ai ajouté une vérification après la détection :
  |     - si type_cham = VECT_3D ou TENS_3D, mais ndim = 2, on émet une alarme :
  | 
  |     44 : _("""
  |  La modélisation détectée à partir du maillage est de dimension 2 (2D).
  |  Mais le mot-clé TYPE_CHAM %(k1)s indique la dimension 3 (3D).
  |  La dimension 3 (3D) est retenue.
  | """),
  | 
  | Et je force ndim = 3.
  | 
  | 
  | Validation
  | -------------
  | 
  | Dans sslv07a, j'ai ajouté une comparaison des valeurs entre MACR_LIGN_COUPE et [MODI_REPERE + POST_RELEVE_T] pour un repère local. 
  | 
  | 
  | Résultat faux
  | -------------------
  | 
  | Résultat faux lors de l'utilisation MODI_REPERE sur un maillage 1/2D en donnant ANGL_NAUT = (alpha,beta,gamma),
  | seulement alpha est pris en compte (cas rare, car une alarme est remise). 
  | Résultat faux lors de l'utilisation MACR_LIGN_COUPE sur un maillage 3D avec un repère local.  
  | 
  | Au moins depuis v13

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 13.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 13.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sslv07a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29881 DU 14/05/2020
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 13.6)
- TITRE : Plusieurs relations linéaires différentes dans LIAISON_RBE3
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Lors de l'utilisation du mot-clé : LIAISON_RBE3 de l'opérateur AFFE_CHAR_MECA, un comportement anormal est identifié.
  | 
  | Lors de la définition des DDL des noeuds esclaves impliqués dans la relation linéaire (DDL_ESCL) seule la première relation semble
  | considérée (cf. PJ: Hoist.zip). Les relations données pour les noeuds après le premier sont sans effet.
  | 
  | De plus, dans la documentation u4.44.01.pdf, il est précisée que la liste doit avoir une longueur égale aux nombre de nœuds
  | esclave(ce qui ne semble obligatoire en pratique).
  | 
  | 
  | Correction
  | ------------------------
  | 
  | Effectivement, dans la doc, il est précisée que la liste doit avoir une longueur égale aux nombre de nœuds esclave.
  | Mais en pratique, il y a 2 cas :
  | - longueur égale aux nombre de nœuds esclave
  | - sinon, détecter si la longueur = 1, et on l'applique pour tous les noeuds 
  | - else => faux
  | 
  | Modif de la doc u4.44.01
  | 
  | MAIS dans carbe3.F90, il y a aussi une erreur dans la boucle de lecture DDL_ELSC.
  | Quand longueur égale aux nombre de nœuds esclave, on prend toujours le 1er élément au lieu du i-ème. Je l'ai corrigé.
  | 
  | Résultat faux
  | -------------
  | 
  | Résultat faux lors de l'utilisation LIAISON_RBE3 dans AFFE_CHAR_MECA avec plusieurs DDL_ELSC différents. 
  | (seulement le 1er est pris en compte, et il est appliqué pour tous les DDL_MAIT)
  | 
  | Depuis toujours (au moins v13)

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 13.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 13.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.44.01
- VALIDATION : test perso
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29926 DU 29/05/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Report incomplet de #27242
- FONCTIONNALITE :
  | Un commit de la fiche issue27242 a supprimé des fichiers en double dans astest.
  | 
  | En v14, il reste :
  |    C2013:     8 Duplicated data files
  | 
  | Suppression des doublons et correction des export.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : aslint


================================================================================
                RESTITUTION FICHE 28683 DU 27/03/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : anomalie liste instant négative avec dyna_non_line
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Un post-traitement par RECU_FONCTION échoue à trouver 0. dans la liste d'un résultat
  | 
  | 
  | Développement
  | -------------
  | 
  | Il y a plusieurs problèmes:
  | 
  | 1/ DEFI_LIST_REEL ne produit pas zéro, mais 1.5959455978986625E-016  à cause d'erreurs d'arrondis successifs (on le voit d'ailleurs
  | dans les instants calculés de DYNA_NON_LINE). En effet, on part de -0.2 et on additionne 0.001. Du point de vue de l'arithmétique
  | flottante, on n'a strictement AUCUNE chance de trouver 0.d0 !
  | 
  | 
  | 2/ en post-traitement dans RECU_FONCTION, on utilise une autre liste qui, elle commence à 0.d0 (qui est un vrai zéro !) et non la
  | valeur à E-16, on ne trouve donc pas la valeur. Il faut donc que dans RECU_FONCTION, on soit en ABSOLU et non en RELATIF pour cette
  | valeur dans la liste. Si on met ABSOLU avec 1E-8, tout va bien !
  | Mais c'est problématique parce qu'on n'a pas forcément "envie" d'être en ABSOLU sur toutes les valeurs ! Il faudrait améliorer ça.
  | J'émets une évolution pour qu'on y réfléchisse.
  | 
  | 
  | On corrige donc uniquement le point 1: lors de la création de la liste, si la valeur est inférieure à r8prem, alors on met 0.d0. Ce
  | qui corrige le problème en post-traitement (on a un vrai zéro)
  | En solution de secours, on peut utiliser le truc avec ABSOLU dans RECU_FONCTION

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : l'étude
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28985 DU 16/07/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 13.6)
- TITRE : Incohérence prise en compte thermique selon ETAT_INIT
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le comportement de STAT_NON_LINE en thermo-mécanique est différent seon que l'on ai un calcul avec etat initial ou sans état initial
  | 
  | Dans le cas avec état initial, le calcul est incrémental et on tient compte uniquement de l'incrément de température ans bien savoir
  | de quel état incrément on parle. 
  | Dans le cas sans état initial, le calcul utilise un incrément de température par rapport au VALE_REF donné dans AFFE_MATERIAU/AFFE_VARC
  | 
  | 
  | Correction
  | -----------
  | 
  | 
  | C'est un problème difficile et lié au chantier COMP_ELAS/COMp_INCR
  | 
  | En effet, en mode "COMP_ELAS", nous ne sommes PAS en incrémental et donc, contrairement à ce que pense (légitimement !) les
  | utilisateurs, STAT_NON_LINE n'est pas toujours incrémental, ça dépend !
  | 
  | 
  | C'est un problème très ancien. Avant, dans STAT_NON_lINE, on avait deux mots-clefs pour le comportement: COMP_INCR et COMP_ELAS.
  | Nous avons modifié ça au profit d'un seul mot-clef COMPORTEMENT et on décide "en souterrain" de la nécessité d'être incrémental ou
  | pas. Pour ça, on utilise la présence du mot-clef ETAT_INIT. Il est très clair que cette solution ergonomique pour l'utilisateur est
  | aussi génératrice d'erreurs difficiles à comprendre comme celle évoquée par Emmanuel. Malheureusement, le chantier est lourd parce
  | qu'il faut reprendre tous les cas où les lois sont déclarées "COMP_ELAS". Fondamentalement , il n'y a que l'élasticité qui pose
  | souci, ça pourrait être considéré comme "pas trop dur" en pratique. Malheureusement, il y a des effets de bord liés à la mécanique de
  | la rupture et des problèmes qu'on l'on a dans ce cas pour la prise en compte des états initiaux. Ce problème est en passe d'être
  | bien traité dans le nouveau CALC_G par exemple.
  | 
  | 
  | 
  | En attendant, je propose d'être plus précis dans la doc et d'alarmer l'utilisateur quand il est en COMP_ELAS et qu'il y a du AFFE_VARC.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.51.03
- VALIDATION : src
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 28912 DU 11/06/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [FAUX] CALC_ERC_DYN est faux s'il y a plus d'une composante observée
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | La commande CALC_ERC_DYN ne peux que donner des résultats faux s'il y a plus d'une composante observée
  | 
  | En effet, l'accès au PRNO dans le calcul de la matrice G (routine materc) est incorrect
  | 
  | La correction est assez complexe, car la suite de l'opérateur reprend cet objet.
  | 
  | 
  | Correction
  | ----------
  | 
  | Au vu de l'usage actuel (nul) et des travaux de la thèse de Zouhair, je propose d'interdire l'usage de cette commande avec autre
  | chose que des SEG2, ce qui limite par ricochet les risques de résultats faux.
  | Il n'y a qu'un test de cette commande
  | 
  | RESU_FAUX
  | 
  | La commande donne potentiellement (sûrement) des résultats faux dès que le maillage support n'est pas constitué de SEG2

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 14.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 15.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdld07a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29833 DU 29/04/2020
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Erreur IMPR_RESU d'un champ META_ELNO
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Il y a un bug lorsqu'on imprime via IMPR_RESU un champ META_ELNO provenant d'un CREA_RESU
  | En effet, la carte COMPORTMETA étant absente (créée normalement lors d'un CALC_META), l'impression du nom des phases n'est pas
  | possible et on tombe sur un erreur (champ inexistant)
  | Contournement: mettre IMPR_NOM_VARI = 'NON' dans IMPR_RESU
  | 
  | Correction
  | ----------
  | 
  | Ne pas tenter d'afficher le nom des variables de métallurgie lorsque la carte n'existe pas => émission d'une alarme
  | (désactivable donc par IMPR_NOM_vARI)
  | 
  | A faire en 14 aussi

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : hsnv123a modifié
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29949 DU 05/06/2020
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : OBSERVATION de DYNA_LINE_TRAN donne INST uniquement à t0
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | la valeur de la colonne INST de la table issue d'OBSERVATION dans DYNA_LINE_TRAN est toujours à t0
  | 
  | 
  | Correction
  | ------------------------
  | 
  | Une erreur dans dlnewi, j'avais mis 't0' (instant initial) dans la table au lieu de 'temps'( instant actuel). C'est corrigé.
  | 
  | 
  | Résultat faux
  | ---------------------------
  | colonne d'instant dans la table OBSERVATION, depuis 14.3.13 en utilisant OBSERVATION dans DYNA_VIBRA (type de calcul DYNA_LINE_TRAN)
  | pour SCHEMA = 'NEWMARK' / 'WILSON'

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 14.3.13
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 14.3.13
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test perso
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28955 DU 28/06/2019
================================================================================
- AUTEUR : MATHIEU Tanguy
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : [RUPT] Il y a des incohérences dans la sortie de POST_RCCM avec l'option TYPE_RESU='VALE_MAX'
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Lors du calcul du facteur d'usage d'une étude, un utilisateur remarque des incohérences entre les sorties 'DETAILS' et 'VALE_MAX' de POST_RCCM.
  | En effet, il s'attend à avoir la combinaison d'instants de chargement donnant le Salt maximum et les données associées (Sp, Ke, Nadm et dommage) en ligne 3 et 4. Or, ce n'est pas le cas.
  | Il indique qu'il semble que les valeurs SP, KE, SALT, NADM et DOMMAGE soient prises comme les max des valeurs calculées pour le segments mais sans lien entre les autres.
  | De plus, le choix des valeurs TABL_RESU_1/2 et INST_SALT_1/2 semble assez aléatoire.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Le format VALE_MAX est décrit dans le § 2.2.2.6 de la notice d'utilisation U2.09.03.
  | 
  | Il est indiqué qu'il affiche les valeurs maximales de SN, SN*, SP, KE, SALT, NADM et DOMMAGE calculé par l'opérateur. Toutefois, rien n'impose que ces valeurs soient prises aux mêmes instants, ni pour la même combinaison de transitoire. 
  | Le comportement observé n'est donc pas une anomalie au sens propre. Toutefois, la pertinence de ce type d'affichage est à questionner. La fiche issue29951 a été ouverte en ce sens.
  | La documentation U4.83.11 sera mise a jour pour y faire aussi apparaitre la différence entre les TYPE_RESU = 'VALE_MAX' et 'DETAILS' afin de la rendre plus visible.
  | 
  | De plus, l'étude de cette fiche a montré qu'il y avait effectivement une erreur dans le salt maximal affiché par cette option et dans les grandeurs associées ( TABL_RESU_1/2 et INST_SALT1/2) que l'on corrige.
  | 
  | 
  | Pour vérifier cette correction, on ajoute un test dans rccm01b sur la valeur de Salt issue de POST_RCCM, TYPE_RESU='VALE_MAX'.
  | 
  | 
  | Résultat faux
  | -------------
  | Le Salt affiché par POST_RCCM pour un calcul en EVOLUTION avec TYPE_RESU='VALE_MAX' est faux. Il semble erroné depuis a minima la version 11.

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 11.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 11.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.83.11, V1.01.107
- VALIDATION : rccm01c
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 29913 DU 26/05/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Contact analysis with shells with a Vertical line of contact
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | This is an issue that we have known for quite a bit but did not have the chance to report it. I have finally constructed a very
  | simple case that demonstrates the problem (all files in vertical_contact.zip). In our case we use shell elements for buildings and
  | we have contacts between the shells that may be horizontal (for example contact between wall and slab) or vertical (between wall and
  | wall). For the latter case, and essentially when the line of contact is along the global Z axis, there is always an error in
  | Code_Aster even before starting any analysis. The error is:
  | 
  |    ! L'algorithme de Newton a échoué lors de la projection du point de coordonnées !
  |    !   (500.000000,0.000000,250.000000)                                            !
  |    ! sur la maille M40.                                                            !
  |    ! Erreur de définition de la maille ou projection difficile. 
  | The interesting thing is that if we rotate the geometry of the model so that there are no vertical contact lines this error does not
  | appear anymore. It is really a pity because the contact and continue formulation is really powerful in our opinion and we have
  | always obtained great results. This example I have sent is very simple that serves well its purpose, but I have also another with a
  | few walls and slabs that combines horizontal and vertical contact lines that can serve as validation in the future if there is any
  | attempt to fix this bug. What I had observed is that rotating by 90 degrees a model (making the vertical contact lines horizontal
  | results in a correct solution for a given nonlinear static analysis. Interestingly rotating the model by 45 degrees I had the
  | correct solution but the solver needed more iterations. This information is given in case it can be useful for the developers.
  | 
  | 
  | Correction
  | ------------------------
  | 
  | Il y a deux problèmes:
  | 1- la projection ne marche pas car on suppose que l'élément est dans le plan xOy (projection sur SEG2) hors ce n'est pas le cas ici.
  | Il suffit de calculer correctement les coordonnées pour réparer la projection
  | 2- les contributions de contact sont nulles car hors plan xOy donc il y a interpénétration (comme si pas de contact). Le problème
  | est que dans les te de la méthode CONTINUE, une maille SEG2 est supposée dans le plan xOy (z=0 et DZ=0) hors ce n'est pas le cas
  | ici. Ça demande de revoir complètement les te et les catalogues de la méthode CONTINUE dans ce cas.
  | 
  | Solution adoptée:
  | On corrige la projection car elle peut servir ailleurs et on émet une erreur fatale si la maille SEG2 (dans la configuration
  | déformée) est hors plan xOy pour la méthode CONTINUE. Comme ça, les tests ssnv129d continue de fonctionner (DKT dans le plan xOy).
  | 
  | Rmq: Il faut voir si on veut traiter ce cas dans le futur (voir chantier contact)
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Oui, si on utilise la méthode CONTINUE avec des DKT hors du plan xOy et que la projection n'échoue pas.

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 11.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 11.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv129d
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29037 DU 16/08/2019
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : Impression des des coordonnées des noeuds d'un élément TUYAU après un POST_CHAMP
- FONCTIONNALITE :
  | Le problème est reproduit sur un cas issu des tests de code_aster.
  | Le soucis vient de la commande CREA_TABLE qui est appliquée à un champ issu de POST_CHAMP, lorsque le champ est aux sous-points. Les
  | éléments à sous-points : poutres multi-fibres, plaques multi-couches, tuyaux
  | 
  | extr_fib = POST_CHAMP(RESULTAT=RESUTY, INST=inst_fin,
  |     EXTR_TUYAU=_F(NOM_CHAM='SIEF_ELGA', NUME_COUCHE=3, NIVE_COUCHE='SUP', ANGLE=0.0,),
  | )
  | On demande l'extraction d'un champ. Les coordonnées du lieu d'extraction ne sont pas enregistrées, ni mémorisées. On a donc une
  | maille support (ici un tuyau) avec un champ ELGA. 
  | Dans le champ, on a les composantes par point de gauss, la maille ET l'information que le champ contient UN sous-point (initialisé à
  | la création du CHAM_ELEM). On perd donc l'information concernant les sous-points.
  | 
  | Quand on fait :
  | T_hau=CREA_TABLE( RESU=_F(RESULTAT=extr_fib, NOM_CHAM='SIEF_ELGA',), )
  | Code_aster calcule les coordonnées de chaque points de gauss avec l'option COOR_ELGA sur des éléments à sous-points. Comme il y a un
  | seul sous-point dans le champ (opération fait par POST_CHAMP), code_aster va chercher les coordonnées du sous-point n°1. On ne
  | calcule donc pas les bonnes coordonnées.
  | 
  | ==> Ce problème existe pour tous les éléments à sous-points. Pour le corriger, il faudrait donc modifier les catalogues et certaines
  | options de tous les éléments à sous-points, documentation et cas tests.
  | 
  | Solution provisoire (avant stabilisation) : A reporter en V14
  | -------------------------------------------
  | - détection dans CREA_TABLE que le champ à imprimer dans la table n'a pas le même nombre de sous-point que l'élément support
  | - impression d'un message d'alarme
  | 
  | 
  | Solution définitive (après stabilisation) :
  | -------------------------------------------
  | ==> Cf fiche issue29943

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : passage cas test
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 29004 DU 25/07/2019
================================================================================
- AUTEUR : AUDEBERT Sylvie
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : [Sismique spectrale] Erreur rencontrée en mode multi-appui décorrelé
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Nous utilisons l'opérateur COMB_SISM_MODAL pour effectuer l'analyse spectrale en multi-appui, avec la définition suivante des
  | groupes d'appuis corrélés.
  | 
  | SPEC = COMB_SISM_MODAL(.........
  | 
  |                           GROUP_APPUI=(_F(GROUP_NO =('ENCTR','GUIDE1','GUIDE10','GUIDE8',),),
  |                                        _F(GROUP_NO =('GUIDE6','SUPP1','SUPP3','GUIDE7',),),
  |                                        _F(GROUP_NO =('SUPP2',),),
  |                                        _F(GROUP_NO =('GUIDE2','GUIDE4',),),
  |                                        _F(GROUP_NO =('ENCPI', 'GUIDE3'),),
  |                                        _F(GROUP_NO =('GUIDE5', 'GUIDE9',),),
  |                                        ),
  |                          ..........)
  | 
  | Bien que les noeuds dans les groupes sont uniques, le code s'arrête en exception à l’initialisation de l'opérateur COMB_SISM_MODAL
  | avec le message suivant:
  | 
  | 
  |    !------------------------------------------------------------------------------!
  |    ! <F> <SEISME_29>                                                              !
  |    !                                                                              !
  |    !   La définition du groupe d'appuis n'est pas correcte dans le cas décorrélé: !
  |    !   au moins une excitation appartient à plusieurs groupes d'appuis.           !
  |    !   Les groupes d'appuis doivent être disjoints.                               !
  |    !                                                                              !
  |    !                                                                              !
  |    ! Cette erreur est fatale. Le code s'arrête.                                   !
  |    !------------------------------------------------------------------------------!
  |    
  | Le maillage a été vérifié, il s'agit d'une erreur dans le fortran.
  | 
  | 
  | Correction :
  | ==========
  | 
  | La vérification des groupes d'appuis par rapport aux excitations peut aboutir à une erreur fatale quand les occurrences d'EXCIT ne sont pas
  | bien ordonnées. Cependant faire cette mise en ordre est lourde pour l'utilisateur, de plus dans certains cas cela ne suffit pas pour
  | éviter l'erreur fatale :
  | 
  | le seul cas incontournable se produit si on a deux directions sur un appui (par exemple X et Z) et deux
  | directions différentes sur un autre (ex : X et Z). Dans ce cas le calcul plantera. 
  | 
  | On propose donc une modification des sources pour ne pas avoir à ce soucier de l'ordre des occurrences et pour ne pas avoir de cas 
  | impossible à traiter. Les vérifications des groupes d'appuis seront désormais faites en traitant les directions de manière disjointe.
  | 
  | Validation :
  | ==========
  | 
  | On met en place un test de NON_REGRESSION pour valider le développement  à partir de sdlx02a.
  | On reprend le cas décorrélé et l'on remplace les TRI_SPEC par des un groupe de 3 AXEs dans chaque direction.  
  | On donne des occurrences de manière désordonnée (ex : X de l'appui 1, puis Y de l'appui 2, puis Y de l'appui 1, ...) 
  | de manière à faire planter le code sur la même erreur que l'étude fourni.
  | En lançant le calcul avec la correction, on n'a plus d'erreur et les résultats sont les mêmes.
  | J'ai testé 2 cas pour être sur que le problème ne se corrige pas à cause des données :
  |       
  |  Cas 1 (celui du test)     Cas 2
  | 
  | X N1 = SPEC1               X N1 = SPEC1
  | Y N1 = SPEC1               Y N1 = SPEC1
  | Z N1 = SPEC2               Z N1 = SPEC1
  | X N2 = SPEC1               X N2 = SPEC2
  | Y N2 = SPEC1               Y N2 = SPEC2
  | Z N2 = SPEC2               Z N2 = SPEC2
  | 
  | 
  | On restitue avec le cas 1 dans sdlx02a uniquement.
  | 
  | Impact doc :
  | ----------
  | 
  | V2.05.002 : ajout du nouveau calcul
  | 
  | 
  | Charge : 2.5 jours + 0.5 non comptabilisé dans issue29833

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V2.05.002
- VALIDATION : sdlx02a

