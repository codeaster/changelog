========================================================================
Version 9.3.18 du : 24/10/2008
========================================================================


-----------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR courtois     COURTOIS Mathieu       DATE 21/10/2008 - 18:13:15

--------------------------------------------------------------------------------
RESTITUTION FICHE 012472 DU 2008-09-11 15:58:46
TYPE evolution concernant Code_Aster (VERSION )
TMA : DeltaCad
TITRE
   Table_container avec variables d'accxc3xa8s
FONCTIONNALITE
   Les "table_container" sont de vraies "table_sdaster" qui contiennent 3 (et uniquement 3)
   colonnes permettant de stocker des r�f�rences � d'autres structures de donn�es Aster.
   Jusqu'ici elles ne sont produites que par la commande CALCUL.
   Afin de les utiliser � d'autres fins, on souhaite leur ajouter d'autres colonnes
   quelconques pour avoir d'autres variables d'acc�s aux objets stock�s. Par exemple : une
   base modale rep�r�e avec un pas de temps/num�ro d'ordre + ...
   
   Cela revient � autoriser plus de 3 colonnes tout en v�rifiant que les 3 colonnes de base
   sont toujours pr�sentes (sd_veri).
   
   On valide le d�veloppement dans CALCUL qui ajoute 2 colonnes dans la table_container
   produite : on stocke syst�matiquement le pas de temps INST et le num�ro d'ordre NUME_ORDRE.
   
   CALCUL devient (facultativement) r�entrant puisque l'on peut maintenant diff�rencier les
   diff�rents pas de temps calcul�s.
   
   
   Syntaxes
   --------
   a) T1 = CALCUL ( ... ) <-- cr�ation d'une table container T1
   
   b) T1 = CALCUL ( reuse=T1, TABLE=T1, ...) <-- enrichissement de la 
                                                 table T1
   
   c) T2 = CALCUL ( TABLE=T1, ...) <-- cr�ation de T2 � partir de T1
                                       (T1 reste inchang�e)
   
   
   D�veloppement
   =============
   
   1) catapy: calcul.capy
   ----------------------
   - Ajout du mot-cle TABLE
   TABLE =SIMP(statut='f',typ=table_container)
   - rendre reentrant un concept issu de CALCUL:
   CALCUL=OPER(nom="CALCUL",op=26,sd_prod=table_container,reentrant='f',
   
   2) bibfor
   ---------
   a) op0026.f:
   - on stocke INST et NUME_ORDRE dans la table
   - Lecture du mot-cl� TABLE :
   Si TABLE est pr�sent: Avant de stocker, on verifie la presence du champ au  numero d'ordre
   specifie. S'il est present, on emet une alarme et on ecrase la ligne de la table contenant
   le champ et le numero d'ordre en question.
   
   b) detrsd.f
   modification de l'ASSERT de la ligne 241 car le nombre de parametres est superieur a 3.
   
   3) bibpyt
   ---------
   - sd_table_container.py
     on verifie le nom des 5 parametres
     on verifie que toutes les colonnes sont remplies.
   - calculel6.py
     L'alarme CALCULEL6_2 est emise pour signaler a l'utilisateur qu'une
     ligne de la table est ecrasee par une autre. Ce cas se produit
     lorsque l'on cherche a stocker un objet d�j� present � l'instant 
     demande.
   
   Validation
   ==========
   a) cas-test pynl01a :
   - ajout d'une commande CALCUL pour illustrer l'enrichissement d'une
   table container, et validation du champ de contraintes stocke dans la table avec celui
   obtenu par STAT_NON_LINE.
   
   b) liste restreinte
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
   pynl01a
--------------------------------------------------------------------------------



-----------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR torkhani     TORKHANI Mohamed       DATE 23/10/2008 - 15:07:23

--------------------------------------------------------------------------------
RESTITUTION FICHE 012024 DU 2008-04-22 14:57:59
TYPE evolution concernant Code_Aster (VERSION )
TITRE
   caractxc3xa9ristiques non symxc3xa9triques pour xc3xa9lxc3xa9ments discrets
FONCTIONNALITE
   Contexte
   ------------
   Pour prendre en compte les coefficients dynamiques des paliers et des joints dans le calcul des rotors, 
   il est INDISPENSAVBLE de pouvoir utiliser des matrices non symetriques (Cadre Projet ODYMAT - Liv 111, 
   portage de Cadyro dans Code_Aster)
   
   Objectif
   ----------
   - Pouvoir affecter aux elements discrets des caracteristiques non symetriques (stockage plein avec les 
   operateurs K_T_L, A_T_L, K_TR_N, A_TR_N, M_TR_N),
   - Creer de nouveaux operateurs K_T_L_NS, A_T_L_NS, K_TR_N_NS, A_TR_N_NS, M_TR_N_NS pour permettre  de 
   prendre en compte des coefficients non symetriques (ex: Kxy different de Kyx).
   
   Operateur impacte
   -------------------------
   AFFE_CARA_ELEM
   1)- Operation transparente pour l'utilisateur d'un element discret classique (ie. stockage symetrique).
   2)- Changement de syntaxe pour l'utilisateur qui veut affecter des caracteristiques non symtriques  (ie. 
   stockage sous forme de matrice pleine) : il doit renseigenr l'option SYME = 'NON' sous  AFFE_CARA_ELEM 
   (option SYME = 'OUI' par defaut).
   
   Perimetre
   -------------
   Differentier le traitement pour les elements symetriques (stockage de matrice symetrique ou de matrice 
   pleine) en ce qui concerne le calcul modal par MODE_ITER_SIMULT/MODE_ITER_INV.
   
   Details
   ---------
    * On permet le stockage non-symetrique des matrices de rigidite, masse et amortissement pour les elements 
   discrets. Desormais le perimetre des elements discrets 
      comprend, pour un calcul modal de type MODE_ITER_SIMULT :
      - matrice M reelle symetrique ou non (options MASS_MECA et MASS_MECA_DIAG),
      - matrice K reelle symetrique ou non (option RIGI_MECA),
      - matrices C reelle symetrique ou non (option AMOR_MECA).
   
   * Afin de renseigner une matrice pleine non-symetrique pour un element discret, il suffit : 
      - de faire appel aux elements discrets classique avec la nomenclature habituelle MECA_DIS_T(ou TR)_N(ou 
   L),
      - d'activer, sous AFFE_CARA_ELEM, DISCRET et pour chaque element, l'option SYME = 'NON',
      - d'utiliser les options nouvelles K_T_L_NS, A_T_L_NS, K_TR_N_NS, A_TR_N_NS, M_TR_N_NS au  lieu des 
   options classiques K_T_L, A_T_L, K_TR_N, A_TR_N, M_TR_N,
      - de rensigner les matrices pleines (colonne par colonne).
   
   * En plus:
     - Pour l'option REPERE = 'LOCAL', permettre les changements de repere (local vers global et global vers 
   local pour les matrices pleines non-symetriques)
     - Pour POST_ELEM, permettre le calcul correct des energies cinetique ECIN_ELEM_DEPL et de deformation 
   EPOT_ELEM_DEPL des elements discrets non-symetriques.
     - Pour POST_ELEM, permettre le calcul correct des caracteristiques de type MASS_INER (Masse, CDG, 
   inerties).
     - Le chargement de type pesanteur CHAR_MECA_PESA_R est correctement pris en consideration.
     - Les options suivantes sont aussi permises avec ces elements (contrainte SIEG_ELGA_DEPL, effort    
   generalise EFGE_ELNO_DEPL, gyroscopie MECA_GYRO).
     
   !!! Cette restitution a ete facilitee par l'effort de Olivier Boiteau qui a etendu le perimetre des solveurs 
   QZ et SORENSEN aux matrices aux pbs generalises et aux quadratiques avec matrices K, M et C reelles (K peut 
   etre complexe) symetriques ou non.
   Ce chantier permet donc de tester les nouvelles "modelisations" d'elements discrets avec les modes QZ et 
   Sorensen (approches 'Complexe', 'Reel' et 'Imag') et de "valider" l'extension du perimetre de ces methodes 
   aux matrices non-symetriques !!!
   
   Mise en garde
   -------------------
   L'utilisation des elements discrets non-symetriques est validee seulement pour un calcul modal de type 
   MODE_ITER_SIMULT/MODE_ITER_INV. Cette fiche ne garantit pas l'utilisation de ces elements pour d'autres 
   types de calcul lineaire ou non-lineaire (MECA_STATIQUE, DYNA_LINE_TRAN, DYNA_LINE_HARM, DYNA_TRAN_MODAL, 
   DYNA_NON_LINE, etc.)
   
   Validation
   -------------
      - non regression sur les cas-tests deja restitues utilisant des elements discrets classiques.
      - SDLL123c: introduction d'un cas-test avec K et M non-symetiques en generalise et quadratique. 
   Ds ce cas-test, les solutions donnes par QZ, SORENSEN (3 approches) et PYTHON/LAPACK sont comparees. 
   Meme instrumentation que SDLL123b pour jouer sur le defaut de symetrie et sur la technique de reduction 
   lineaire pour passer d'Aster a Python.
   
   Sources modifiees
   -------------------------
   - gener_me2dt0/1.cata, gener_med2tr0/1.cata, gener_medit0/1.cata, gener_medtr0/1.cata
   - amor_meca.cata, mass_meca.cata, mass_meca_diag.cata, mass_iner.cata, meca_gyro.cata, rigi_meca.cata
   - char_meca_pesa_r.cata, ecin_elem_depl.cata, epot_elem_depl.cata, efge_elno_depl.cata, sief_elga_depl.cata
   - affe_cara_elem.capy
   - aceadi.f, affdis.f, afdi2d.f, afdi3d.f, meamme.f, meamgy.f, memame.f, merime.f, mecara.f, me2mme.f, 
   pemain.f, ptenci.f, ptenpo.f, te0009.f te0041.f-te0045.f
   
   Ajout de routines fortran
   --------------------------------
   - mapvec.f : extension de mavec.f pour passage de matrice pleine a matrice colonne vecteur
   - vecmap.f : extension de vecma.f pour passage de matrice colonne vecteur a matrice pleine
   - lctr2m.f : transposee de matrice de dimension donne
   - lcso2m.f : somme de matrices de dimension donnee
   - lcdi2m.f : difference de matrices de dimension donnee
   - lcps2m.f : produit d?un scalaire par une matrice de dimension donnee
   - uplstr.f : routine permettant de stocker sous forme pleine une matrice triangulaire superieure symetrique 
                (equivalent de upletr.f pour les matrices triangulaires superieure anti-symetrique)
   - ut2agl.f : passage en 2D d'une matrice triangulaire anti-symetrique du repere global au repere local
   - ut2alg.f : passage en 2D d'une matrice triangulaire anti-symetrique du repere local au repere global
   - ut2pgl.f : passage en 2D d'une matrice pleine non-symetrique du repere global au repere local
   - ut2plg.f : passage en 2D d'une matrice pleine non-symetrique du repere local au repere global
   - utpagl.f : passage en 3D d'une matrice triangulaire anti-symetrique du repere global au repere local
   - utppgl.f : passage en 3D d'une matrice pleine non-symetrique du repere global au repere local
   - utpplg.f : passage en 3D d'une matrice pleine non-symetrique du repere local au repere global
   
   Impact Doc
   ----------
   - Doc U4.42.01-I AFFE_CARA_ELEM
   - Doc V du cas-test SDLL123
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
   SDLL123 + liste restreinte
NB_JOURS_TRAV  : 10.0
--------------------------------------------------------------------------------



========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

 CASTEST AJOUT sdll123c                     torkhani M.TORKHANI         644    644      0
 CASTEST MODIF pynl01a                       rezette C.REZETTE          412     93      1
 CASTEST MODIF ssns106f                     torkhani M.TORKHANI         736      2      1
CATALOGU MODIF compelem/grandeur_simple__   torkhani M.TORKHANI         913     66      1
CATALOGU MODIF options/amor_meca            torkhani M.TORKHANI          34      4      2
CATALOGU MODIF options/char_meca_pesa_r     torkhani M.TORKHANI          53      3      2
CATALOGU MODIF options/ecin_elem_depl       torkhani M.TORKHANI          63      7      1
CATALOGU MODIF options/efge_elno_depl       torkhani M.TORKHANI          97      7      1
CATALOGU MODIF options/epot_elem_depl       torkhani M.TORKHANI          69      7      1
CATALOGU MODIF options/mass_iner            torkhani M.TORKHANI          41      3      1
CATALOGU MODIF options/mass_meca_diag       torkhani M.TORKHANI          49      4      2
CATALOGU MODIF options/mass_meca            torkhani M.TORKHANI          43      3      1
CATALOGU MODIF options/meca_gyro            torkhani M.TORKHANI          45      7      1
CATALOGU MODIF options/rigi_meca            torkhani M.TORKHANI          49      3      1
CATALOGU MODIF options/sief_elga_depl       torkhani M.TORKHANI          93      7      1
CATALOGU MODIF typelem/gener_me2dt0         torkhani M.TORKHANI         177     22     10
CATALOGU MODIF typelem/gener_me2dt1         torkhani M.TORKHANI         201     28     10
CATALOGU MODIF typelem/gener_me2tr0         torkhani M.TORKHANI         183     24      9
CATALOGU MODIF typelem/gener_me2tr1         torkhani M.TORKHANI         216     40     10
CATALOGU MODIF typelem/gener_medit0         torkhani M.TORKHANI         195     25      9
CATALOGU MODIF typelem/gener_medit1         torkhani M.TORKHANI         233     40      9
CATALOGU MODIF typelem/gener_medtr0         torkhani M.TORKHANI         222     41      9
CATALOGU MODIF typelem/gener_medtr1         torkhani M.TORKHANI         324     93      8
CATALOPY MODIF commande/affe_cara_elem      torkhani M.TORKHANI         712    149      3
CATALOPY MODIF commande/calcul               rezette C.REZETTE           56      3      2
 FORTRAN AJOUT algeline/uplstr              torkhani M.TORKHANI          57     57      0
 FORTRAN AJOUT utilifor/lcdi2m              torkhani M.TORKHANI          36     36      0
 FORTRAN AJOUT utilifor/lcps2m              torkhani M.TORKHANI          36     36      0
 FORTRAN AJOUT utilifor/lcso2m              torkhani M.TORKHANI          36     36      0
 FORTRAN AJOUT utilifor/lctr2m              torkhani M.TORKHANI          36     36      0
 FORTRAN AJOUT utilifor/ut2agl              torkhani M.TORKHANI          83     83      0
 FORTRAN AJOUT utilifor/ut2alg              torkhani M.TORKHANI          83     83      0
 FORTRAN AJOUT utilifor/ut2pgl              torkhani M.TORKHANI          66     66      0
 FORTRAN AJOUT utilifor/ut2plg              torkhani M.TORKHANI          66     66      0
 FORTRAN AJOUT utilifor/utpagl              torkhani M.TORKHANI          91     91      0
 FORTRAN AJOUT utilifor/utppgl              torkhani M.TORKHANI          66     66      0
 FORTRAN AJOUT utilifor/utpplg              torkhani M.TORKHANI          66     66      0
 FORTRAN AJOUT utilitai/mapvec              torkhani M.TORKHANI          39     39      0
 FORTRAN AJOUT utilitai/vecmap              torkhani M.TORKHANI          39     39      0
 FORTRAN MODIF algeline/vpmain              torkhani M.TORKHANI         129      8      9
 FORTRAN MODIF algorith/crvrc1              torkhani M.TORKHANI         120      6      6
 FORTRAN MODIF algorith/crvrc2              torkhani M.TORKHANI         113      2      2
 FORTRAN MODIF algorith/meacmv              torkhani M.TORKHANI         467      2      2
 FORTRAN MODIF algorith/mecgme              torkhani M.TORKHANI         310      2      2
 FORTRAN MODIF algorith/memsth              torkhani M.TORKHANI          91      2      2
 FORTRAN MODIF algorith/mergth              torkhani M.TORKHANI         162      2      2
 FORTRAN MODIF algorith/merim2              torkhani M.TORKHANI         445      2      2
 FORTRAN MODIF algorith/merimp              torkhani M.TORKHANI         318      2      2
 FORTRAN MODIF algorith/merxth              torkhani M.TORKHANI         173      2      2
 FORTRAN MODIF algorith/nmdepr              torkhani M.TORKHANI         254      2      2
 FORTRAN MODIF algorith/nmrefe              torkhani M.TORKHANI         211      2      2
 FORTRAN MODIF algorith/nmvcpr              torkhani M.TORKHANI         222      2      2
 FORTRAN MODIF algorith/nmvgme              torkhani M.TORKHANI         225      2      2
 FORTRAN MODIF algorith/nsldc               torkhani M.TORKHANI         282      2      2
 FORTRAN MODIF algorith/ntoptc              torkhani M.TORKHANI         142      2      2
 FORTRAN MODIF algorith/op0046              torkhani M.TORKHANI         262      2      2
 FORTRAN MODIF algorith/pascou              torkhani M.TORKHANI         197      2      2
 FORTRAN MODIF algorith/phi2el              torkhani M.TORKHANI         137      2      2
 FORTRAN MODIF algorith/vecgme              torkhani M.TORKHANI         253      2      2
 FORTRAN MODIF algorith/vechde              torkhani M.TORKHANI         547      2      2
 FORTRAN MODIF algorith/vechmc              torkhani M.TORKHANI         188      2      2
 FORTRAN MODIF algorith/vechme              torkhani M.TORKHANI         458      7      3
 FORTRAN MODIF algorith/vechnl              torkhani M.TORKHANI         433      2      2
 FORTRAN MODIF algorith/vechth              torkhani M.TORKHANI         706      2      2
 FORTRAN MODIF algorith/vecsme              torkhani M.TORKHANI         159      2      2
 FORTRAN MODIF algorith/vectfl              torkhani M.TORKHANI         125      2      2
 FORTRAN MODIF algorith/vectme              torkhani M.TORKHANI         149      2      2
 FORTRAN MODIF algorith/vecyme              torkhani M.TORKHANI         160      2      2
 FORTRAN MODIF algorith/vefnme              torkhani M.TORKHANI         288      2      2
 FORTRAN MODIF algorith/vefpme              torkhani M.TORKHANI         197      2      2
 FORTRAN MODIF algorith/vemsme              torkhani M.TORKHANI         319      2      2
 FORTRAN MODIF algorith/verstp              torkhani M.TORKHANI         195      2      2
 FORTRAN MODIF algorith/vetnth              torkhani M.TORKHANI         379      2      2
 FORTRAN MODIF calculel/me2mme              torkhani M.TORKHANI         606      8      3
 FORTRAN MODIF calculel/meamgy              torkhani M.TORKHANI         103      7      3
 FORTRAN MODIF calculel/meamme              torkhani M.TORKHANI         267     19     11
 FORTRAN MODIF calculel/meca01              torkhani M.TORKHANI         718      2      2
 FORTRAN MODIF calculel/mecalc              torkhani M.TORKHANI         570      5      2
 FORTRAN MODIF calculel/mecalm              torkhani M.TORKHANI        2478      2      2
 FORTRAN MODIF calculel/mecara              torkhani M.TORKHANI          93      7      3
 FORTRAN MODIF calculel/meimme              torkhani M.TORKHANI         117      2      2
 FORTRAN MODIF calculel/memam2              torkhani M.TORKHANI         140      2      2
 FORTRAN MODIF calculel/memame              torkhani M.TORKHANI         210      7      3
 FORTRAN MODIF calculel/memath              torkhani M.TORKHANI         111      2      2
 FORTRAN MODIF calculel/meonme              torkhani M.TORKHANI         119      2      2
 FORTRAN MODIF calculel/merifs              torkhani M.TORKHANI         142      2      2
 FORTRAN MODIF calculel/merige              torkhani M.TORKHANI          97      2      2
 FORTRAN MODIF calculel/merime              torkhani M.TORKHANI         194      7      3
 FORTRAN MODIF calculel/merit1              torkhani M.TORKHANI         140      2      2
 FORTRAN MODIF calculel/merit2              torkhani M.TORKHANI         154      2      2
 FORTRAN MODIF calculel/merit3              torkhani M.TORKHANI         159      2      2
 FORTRAN MODIF calculel/mertth              torkhani M.TORKHANI         179      2      2
 FORTRAN MODIF calculel/metnth              torkhani M.TORKHANI         162      2      2
 FORTRAN MODIF calculel/op0026               rezette C.REZETTE          396    151     34
 FORTRAN MODIF calculel/op0038              torkhani M.TORKHANI         160      2      2
 FORTRAN MODIF calculel/op0175              torkhani M.TORKHANI         559      2      2
 FORTRAN MODIF calculel/vetrth              torkhani M.TORKHANI         171      2      2
 FORTRAN MODIF elements/ptenci              torkhani M.TORKHANI         270      8      4
 FORTRAN MODIF elements/ptenpo              torkhani M.TORKHANI         199     18      1
 FORTRAN MODIF elements/te0009              torkhani M.TORKHANI          96     16      3
 FORTRAN MODIF elements/te0041              torkhani M.TORKHANI         543    207     22
 FORTRAN MODIF elements/te0042              torkhani M.TORKHANI         286     96      5
 FORTRAN MODIF elements/te0043              torkhani M.TORKHANI         317    133      3
 FORTRAN MODIF elements/te0044              torkhani M.TORKHANI         331    123      3
 FORTRAN MODIF elements/te0045              torkhani M.TORKHANI         502    275     61
 FORTRAN MODIF modelisa/aceadi              torkhani M.TORKHANI         394    120     14
 FORTRAN MODIF modelisa/acearm              torkhani M.TORKHANI         246     48      7
 FORTRAN MODIF modelisa/acearp              torkhani M.TORKHANI         500     58     14
 FORTRAN MODIF modelisa/afdi2d              torkhani M.TORKHANI         319     88     12
 FORTRAN MODIF modelisa/afdi3d              torkhani M.TORKHANI         348     88     12
 FORTRAN MODIF modelisa/affdis              torkhani M.TORKHANI          62      9      6
 FORTRAN MODIF utilitai/detrsd               rezette C.REZETTE          592      2      2
 FORTRAN MODIF utilitai/pecage              torkhani M.TORKHANI         203      2      2
 FORTRAN MODIF utilitai/pecapo              torkhani M.TORKHANI         391      2      2
 FORTRAN MODIF utilitai/peecin              torkhani M.TORKHANI         368      2      2
 FORTRAN MODIF utilitai/peepot              torkhani M.TORKHANI         320      2      2
 FORTRAN MODIF utilitai/peingl              torkhani M.TORKHANI         733      2      2
 FORTRAN MODIF utilitai/pemain              torkhani M.TORKHANI         223      9      5
 FORTRAN MODIF utilitai/peritr              torkhani M.TORKHANI         402      2      2
 FORTRAN MODIF utilitai/peweib              torkhani M.TORKHANI         437      2      2
  PYTHON MODIF Messages/table0               rezette C.REZETTE           88      8      1
  PYTHON MODIF SD/sd_cara_elem              torkhani M.TORKHANI          52      5      1
  PYTHON MODIF SD/sd_table_container         rezette C.REZETTE           81      2      2


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :   15        1444      1444             +1444
 MODIF :  108       30380      2334     462     +1872
 SUPPR :    0           0                 0        +0
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :  123       31824      3778     462     +3316 
