==================================================================
Version 15.3.12 (révision d7a485c3199d) du 2021-03-16 09:35 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 28735 BETTONTE Francesco       [MAC3] Mise à jour des maillages et suppression des assemblages non utilisés
 30781 PIGNET Nicolas           Ajouter le support des groupe_ma au ConnectionMesh
 30784 PIGNET Nicolas           Etendre TEST_RESU au hpc
 30766 ABBAS Mickael            [NGC] Problème de perf de CREA_MAILLAGE/COQUE_SOLIDE
 30764 TARDIEU Nicolas          Mieux tester le code retour des appels MPI
 30743 COURTOIS Mathieu         En version 15.3.8 - Des cas-tests de vérification mpi xxParallelMechanicalLoa...
 30787 COURTOIS Mathieu         Détection automatique de la plate-forme
 30773 COURTOIS Mathieu         Simplifier l'interface C++/Fortran pour les arguments de type bool
 30783 BETTONTE Francesco       [prima] Ecriture d'un maillage avec MEDCoupling dans code_aster se passe mal
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 28735 DU 04/04/2019
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : evolution concernant code_aster (VERSION 14.3)
- TITRE : [MAC3] Mise à jour des maillages et suppression des assemblages non utilisés
- FONCTIONNALITE :
  | Problème
  | --------
  | Suite à la validation des paramètres MAC3 les maillages des cas test nécessitent d’être mis à jour, avec évolution des valeurs de non régression pour les tests.
  | Les conceptions nommés AF3G et AFA ne sont plus utilisées et ne sont donc pas utilisées : à supprimer.
  | 
  | 
  | Correction
  | ----------
  | 
  | * Mise à jour des maillages et correction des valeurs de non-régression.
  | * Suppression des conceptions AFA et AF3G

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 30781 DU 17/03/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Ajouter le support des groupe_ma au ConnectionMesh
- FONCTIONNALITE :
  | Problème
  | -----------------
  | Avant de pourvoir étendre l'utilisation des group_ma dans affe_char_meca, il faut déjà le faire pour le ConnectionMesh
  | 
  | 
  | Développement
  | ------------------------
  | 
  | On a complètement réécrit la création de l'objet ConnectionMesh.
  | A partir de group_ma et group_no, on vient récupérer l'ensemble des noeuds de ces groupes puis on récupère l'ensemble des mailles portées par ces noeuds. Le
  | nouveau maillage sera composé uniquement de ces mailles (il en général plus petit que le ParallelMesh en entré).
  | 
  | Ensuite, il faut faire les communications nécessaires afin que tout les cpu créent ce maillage (identique entre chaque cpu). Pour cela, il faut communiquer les
  | coordonnés et numérotation des noeuds, la connectivité des cellules et le contenu des groupes. C'est le cpu propriétaire d'une maille ou d'un noued qui doit
  | l'envoyer et les autres les recevoir.
  | 
  | J'en profite pour rajouter les méthodes suivantes dans l'API sur le ConnectionMesh
  | - getGroupsOfCells
  | - getGroupsOfNodes
  | - hasGroupOfCells
  | - hasGroupOfNodes
  | - getCells
  | - getParallelMesh
  | - getGlobalNumbering
  | 
  | J'ai corrigé un bug à la création du .MAEX du ParallelMesh. Il n'était pas créé s'il n'y avait pas de joint
  | 
  | Je rajoute aussi une méthode printMedFile sur le maillage pour facilement l'exporter (Mesh, ParallelMesh, ConnectionMesh)
  | 
  | Je vérifie avec 4 nouveaux tests élémentaires (mesh003* - V1.01.141). Pour chaque test, je crée 15 connectionMesh différents pour tester toutes les combinaisons
  | de groupes et d'éléments et sur différent nombre de cpu
  | 
  | Les performances semblent bonnes même sur un gros maillage (je rajouterai plus tard un test de performance pour ça)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.01.141
- VALIDATION : mesh003a
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 30784 DU 18/03/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Etendre TEST_RESU au hpc
- FONCTIONNALITE :
  | Objectif
  | -----------------
  | 
  | TEST_RESU n'est pas compatible avec le hpc car un test peut être sur un proc (car le groupe est dessus) mais pas sur un autre (car il n'a pas le groupe). Le
  | test est alors en erreur ou nook
  | 
  | pour l'instant, on met des hasGroup("toto", local=True) afin de faire le test. Ce n'est pas pratique quand on veut ajouter des tests en hpc
  | 
  | Je propose de remédier à cela en faisant les comm nécessaires.
  | 
  | J'ai fait un truc qui marche pour les RESU et CHAM_NO. Je vais blinder les autres cas pour être tranquille. On les ajoutera au fur et à mesure des besoins.
  | 
  | 
  | Développement
  | ------------------------
  | J'ai fait le développement dans TEST_RESU pour les mot-clés: CHAM_NO, CHAM_ELEM et RESU. J'interdis dans le cas d'une CARTE et de GENE.
  | 
  | Le dev consiste essentiellement à rajouter des comms pour envoyer à tout le monde le résultat. Je n'ai pas ajouté la possibilité d'utiliser TYPE_RESU car ça
  | complique pas mal car il faut savoir qui est le proprio des valeurs.
  | 
  | Je n'autorise que GROUP_MA et GROUP_NO (NOEUD et MAILLE sont bien évidement interdit en HPC)
  | 
  | Je teste cela dans zzzz502e (où je peux enfin enlever mes hasGroup(**, local=True))
  | 
  | Maintenant, on a la même syntaxe en std et hpc, ce qui va faciliter la création de nouveaux tests.
  | 
  | Il faudra rajouter le reste le jour où on en aura besoin

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz502e
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30766 DU 10/03/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [NGC] Problème de perf de CREA_MAILLAGE/COQUE_SOLIDE
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | L’opération CREA_MAILLAGE/COQUE_SOLIDE est très sous-performante.
  | Pour 26000 éléments après une heure de calcul, on n'a toujours pas fini.
  | 
  | 
  | Correction
  | ----------
  | 
  | Il y a deux phases dans cette opération:
  | - passage de HEXA8 en HEXA9 par le module crea_maillage_module
  | - orientation des faces supérieures et inférieures par le module SolidShell_Mesh_module
  | 
  | Après un essai rapide, il s'agit d'un problème lors de la deuxième phase, la première ne prenant que 1.15 s.
  | 
  | Dans la routine oriesb9 de SolidShell_Mesh_module, il y a une double boucle sur le maillage très sous-performante et donc qui 
  | dépend du nombre de mailles du maillage.
  | 
  | On corrige en utilisant la connectivité inverse pour préparer les informations et éviter la double-boucle.
  | 
  | Après correction, la commande ne prend plus que 1.5s
  | 
  | 
  | Vérifié sur l'ensemble des tests utilisant CREA_MAILLAGE/COQUE_SOLIDE

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : les tests utilisant COQUE_SOLIDE
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30764 DU 09/03/2021
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Mieux tester le code retour des appels MPI
- FONCTIONNALITE :
  | Objectif
  | ---------
  | 
  | Actuellement on ne teste que MPI_SUCCESS comme valeur de code-retour des appels MPI. Ce n'est pas suffisant pour analyser les 
  | problèmes sur de gros cas.
  | 
  | 
  | Développement
  | -------------
  | 
  | Je remplace la macro AS_ASSERT par une nouvelle macro AS_MPICHECK :
  | 
  | /* AS_MPICHECK checks the return code of an MPI function */
  | #define AS_MPICHECK(code) int ret = (code); \
  |                     if ( (ret) == MPI_ERR_COMM ) { \
  |             DEBUG_LOC; DBGV("Invalid communicator: %s", #code); \
  |             INTERRUPT(17); } \
  |                     else if ( (ret) == MPI_ERR_TYPE ) { \
  |             DEBUG_LOC; DBGV("Invalid datatype argument: %s", #code); \
  |             INTERRUPT(17); } \
  |                     else if ( (ret) == MPI_ERR_COUNT ) { \
  |             DEBUG_LOC; DBGV("Invalid count argument: %s", #code); \
  |             INTERRUPT(17); } \
  |                     else if ( (ret) == MPI_ERR_TAG ) { \
  |             DEBUG_LOC; DBGV("Invalid tag argument: %s", #code); \
  |             INTERRUPT(17); } \
  |                     else if ( (ret) == MPI_ERR_RANK ) { \
  |             DEBUG_LOC; DBGV("Invalid source or destination rank: %s", #code); \
  |             INTERRUPT(17); } \
  |                     else if ( (ret) != MPI_SUCCESS ) { \
  |             DEBUG_LOC; DBGV("Unknown error: %s", #code); \
  |             INTERRUPT(17); }

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests perso
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30743 DU 01/03/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 15.3.8 - Des cas-tests de vérification mpi xxParallelMechanicalLoad sont en ABNORMAL_ABORT
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.3.8 - Les cas-tests de vérification mpi xxParallelMechanicalLoad* sont en ABNORMAL_ABORT
  | 
  | Erreur émise par AFFE_CHAR_MECA :
  | 
  | [1]   File "/projets/simumeca/install/unstable/mpi/lib/aster/code_aster/Commands/affe_char_meca.py", line 112, in create_result
  | [1]     raise TypeError("Not allowed to mix up Dirichlet and Neumann loadings in the same parallel AFFE_CHAR_MECA")
  | 
  | 
  | Correction
  | ----------
  | 
  | La modification faite dans le catalogue de AFFE_CHAR_MECA pour assurer la compatibilité ascendante avec l'ancienne syntaxe LIAISON="ENCASTRE" fait que DDL_IMPO
  | vaut [] (liste vide) s'il n'était pas présent.
  | 
  | La vérification faite ensuite pour s'assurer qu'on ne mélange pas conditions de Neumann et de Dirichlet croit, à tort, qu'il y a des conditions de Dirichlet.
  | On corrige la manière dont est faite cette vérification.
  | Et on évite d'ajouter DDL_IMPO=[] s'il est absent.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelMechanicalLoad001h
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30787 DU 18/03/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Détection automatique de la plate-forme
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | On peut maintenant taper `./waf configure`, la plate-forme est automatiquement détectée pour choisir la configuration dans `env.d/`.
  | 
  | Si on tourne dans un conteneur sur base Scibian9, on va choisir `env.d/scibian9_std.sh` (car on regarde dans `/etc/issue`) donc avec les prérequis cherchés dans
  | `$HOME/dev/codeaster-prerequisites` ce qui n'est pas ce qu'on veut.
  | 
  | 
  | Développement
  | -------------
  | 
  | On cherchait parmi les configurations connues (eole, gaia, cronos, scibian9, calibre9), puis s'il existe un fichier `*_std.sh` dans /opt/public (répertoire
  | attendu dans le conteneur).
  | 
  | On inverse cette recherche en commençant par `/opt/public`.
  | Ainsi, s'il existe un fichier `/opt/public/xxxx_std.sh`, on considère que la plate-forme est xxxx, même si `/etc/issue` indique "Scibian 9". On prend donc le
  | fichier d'environnement `/opt/public/scibian9_std.sh` et non pas celui de `env.d/`.
  | 
  | De même si on lance `./waf_mpi`, on cherche `/opt/public/*_mpi.sh`

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : waf configure install
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30773 DU 14/03/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Simplifier l'interface C++/Fortran pour les arguments de type bool
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Quand on veut appeler une fonction Fortran ayant des 'aster_logical' en arguments, on fait un wrapper qui prend des entiers à la place.
  | On aimerait interfacer directement la fonction utilisant les booléens.
  | 
  | 
  | Développement
  | -------------
  | 
  | Les 'aster_logical' sont de longueur 1 (défini par logical(kind=1)).
  | Les bool C++ sont en général aussi de longueur 1, même si ce n'est apparemment pas dans la norme.
  | Dans 'waf configure', on ajoute donc la vérification qu'ils sont tous les deux de longueur 1. Sinon l'enrobage des vecteurs Jeveux de booléens ne peut pas
  | fonctionner comme ça (wkvectc/jeveuoc).
  | 
  | Le passage des arguments est toujours par référence en Fortran. Pour un entier, on passe donc en C/C++ par un ASTERINTEGER*.
  | 
  | Pour utiliser un type C de longueur 1, il faut faire un cast en (char*).
  | Pour la lisibilité, on définit, en C, ASTERLOGICAL qui vaut simplement `char`.
  | 
  | On écrit en C++ :
  | 
  | CALLO_NMDOCC( modelName, materialFieldName, (ASTERLOGICAL *)&_initialState,
  |  . . . . . . .(ASTERLOGICAL *)&_implex, mapComporName, (ASTERLOGICAL *)&_verbosity );
  | 
  | Ici, le dernier argument est "optional" dans nmdocc fortran, ça ne pose pas de problème.
  | 
  | En revanche, ça provoque un segfault si on appelle nmdocc sans passer l'argument optionnel depuis nmdorc.
  | C'est normal, model est déclaré 24 dans op0070 et fixé à 19 dans nmdocc.
  | Comme dans les routines appelées sous nmdocc, on suppose que model, chmate sont de taille connue, on tronque les variables dans nmdorc.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz258a
- NB_JOURS_TRAV  : 1.5


================================================================================
                RESTITUTION FICHE 30783 DU 17/03/2021
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : aide utilisation concernant code_aster (VERSION 15.4)
- TITRE : [prima] Ecriture d'un maillage avec MEDCoupling dans code_aster se passe mal
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En unstable_mpi lors de l'écriture sur disque d'un maillage MED l'API medcoupling remonte le plantage suivant :
  | 
  | ===================
  | Extrait de l'output
  | ===================
  | 0] /home/A21173/aster_prerequisites/build_v15_mpi_2016/prerequisites/src/Medfichier-410-hdf51103-py365/src/hdfi/_MEDfileCreate.c [173] : Erreur à la création du
  | fichier 
  | [0] /home/A21173/aster_prerequisites/build_v15_mpi_2016/prerequisites/src/Medfichier-410-hdf51103-py365/src/hdfi/_MEDfileCreate.c [173] :
  | /home/F82953/aster/medcoupling/debug_d_plan_grma_tri_quad.med
  | [0] /home/A21173/aster_prerequisites/build_v15_mpi_2016/prerequisites/src/Medfichier-410-hdf51103-py365/src/ci/MEDfileVersionOpen.c [108] : Erreur à la création
  | du fichier 
  | [0] /home/A21173/aster_prerequisites/build_v15_mpi_2016/prerequisites/src/Medfichier-410-hdf51103-py365/src/ci/MEDfileVersionOpen.c [108] :
  | /home/F82953/aster/medcoupling/debug_d_plan_grma_tri_quad.med
  | [0] HDF5-DIAG: Error detected in HDF5 (1.10.3) thread 0:
  | [0]   #000: /home/A21173/aster_prerequisites/build_v15_mpi_2016/prerequisites/src/Hdf5-1103-cm3121/src/H5F.c line 664 in H5Fclose(): not a file ID
  | [0]     major: File accessibilty
  | [0]     minor: Inappropriate type
  | 
  | 
  | Analyse
  | -------
  | Le chemin spécifié pour le fichier est local et n'existe pas sur le serveur.
  | 
  | Solutions
  | ---------
  | * écrire le med dans le dossier d’exécution et le récupérer via astk/asterstudy  par fichier nommé
  | * écrire le med dans REPE_OUT et récupérer tout le dossier
  | * écrire le med dans un chemin existant sur le serveur

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test joint
- NB_JOURS_TRAV  : 0.5

