==================================================================
Version 15.1.17 (révision 878fa09e16cf) du 2020-04-29 10:52 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29804 COURTOIS Mathieu         Erreurs aslint passées à l'as
 29805 COURTOIS Mathieu         Mauvais type pour NOM_OBJET
 29790 PIGNET Nicolas           En version 15.1.15  révision 83bef037fa1e, les cas-tests (mpi)  de vérificati...
 29751 FLEJOU Jean Luc          Forces pour les vagues : SF1D1D('ACCE_X','ACCE_Y','ACCE_Z')
 29252 BETTONTE Francesco       Depuis la version 15.0.11, les cas-tests validation mac3c08a mac3c08b mac3c08...
 29721 BETTONTE Francesco       En version 15.1.12 les cas-tests sdll130b et ssll119b sont en NOOK_TEST_RESU ...
 29746 ABBAS Mickael            En version 15.1.13 le cas test ssls119b est en ERROR sur  Calibre9
 29530 YU Ting                  Tests wtnp128a/b/c/d en version NEW15 sous RHEL7
 29699 YU Ting                  Variables non initialisées dans le contact
 29771 DROUET Guillaume         DEFI_CONTACT : 2 valeurs de COEF_FROT simultanément ?
 29812 ABBAS Mickael            Reliquats suppression SHB
 29795 COURTOIS Mathieu         [bb0140] Introduction de l'orthotropie en thermique non linéaire
 29663 YU Ting                  EXTRAPOLE dans le calcul CONTACT LAC
 29669 DROUET Guillaume         En version 14.5.7 le cas-test ssnp165c est en FPE sur clap0f0q
 29797 HAMON François           Mfront Stable/Unstable
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29804 DU 23/04/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Erreurs aslint passées à l'as
- FONCTIONNALITE :
  | Problème
  | --------
  |
  | submit détecte mal la révision de référence dans la branche default.
  | Donc certaines vérifications n'ont pas été faites et forcément des trucs sales sont rentrés.
  |
  |
  | Correction
  | ----------
  |
  | On corrige 2 tableaux mal dimensionnés, incohérents avec l'interface correspondante,
  | 18 variables non utilisées, des lignes trop longues.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE :
- VALIDATION : submit
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29805 DU 23/04/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Mauvais type pour NOM_OBJET
- FONCTIONNALITE :
  | Problème
  | --------
  |
  | Dans issue27107, NOM_OBJET, qui a remplacé NOM_SST, est resté en K8 dans la table_container
  | produite par CALC_CORR_SSD.
  | Il doit être en K16 dans une table_container.
  |
  |
  | Correction
  | ----------
  |
  | On modifie le type en K16.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE :
- VALIDATION : sdls106e
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29790 DU 20/04/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : En version 15.1.15  révision 83bef037fa1e, les cas-tests (mpi)  de vérification fdlv112i , petsc02a , petsc02c et wtnv136c sont en ABNORMAL_ABORT sur eole, gaia, Calibre9 et Scibian9
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  |
  | En version 15.1.15  révision 83bef037fa1e, les cas-tests (mpi)  de vérification fdlv112i , petsc02a , petsc02c et wtnv136c sont en
  | ABNORMAL_ABORT sur eole, gaia, Calibre9 et Scibian9. Je joins en pièce jointe un *.tar.gz des outputs et erreur (eole).
  |
  |
  |
  | Tous ces cas-test indiquent la même erreur :
  |
  |     !--------------------------------------------------------!
  |     ! <F> <JEVEUX1_67>                                       !
  |     !                                                        !
  |     ! La valeur 0 affectée à l'attribut LONMAX est invalide. !
  |     ! Ce message est un message d'erreur développeur.        !
  |     ! Contactez le support technique.                        !
  |     !                                                        !
  |     !                                                        !
  |     ! Cette erreur est fatale. Le code s'arrête.             !
  |     !--------------------------------------------------------!
  |
  | Les opérateurs au cours desquels cette erreur intervient sont :
  |
  | fdlv112i : ASSE_MATRICE
  | petsc02a : MECA_STATIQUE
  | petsc02c : MECA_STATIQUE
  | wtnv136c : STAT_NON_LINE
  |
  |
  | Correction/Développement
  | ------------------------
  |
  | C'est des situations un peu particulières où un proc n'a pas de ddl à assembler (ex Lagrange que sur le proc0) d'où l'erreur.
  |
  | Il suffit de sortir de la routine sans rien allouer s'il n'y pas de ddl à assembler

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE :
- VALIDATION : fdlv112i, petsc02a, petsc02c, wtnv136c
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29751 DU 06/04/2020
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Forces pour les vagues : SF1D1D('ACCE_X','ACCE_Y','ACCE_Z')
- FONCTIONNALITE :
  | Ajout des accélérations solides ACCE_* dans les paramètres de la FORCE
  | ======================================================================
  | Fiche chapeau : issue28225
  |
  | La force peut dépendre des paramètres suivant
  |   'INST' (temps)
  |   'X','Y','Z' (position noeud)
  |   'DX','DY','DZ' (vecteur directeur poutre)
  |   'VITE_X','VITE_Y','VITE_Z' (vitesse structure)
  |   'ACCE_X’,’ACCE_Y’,’ACCE_Z’ (accélération structure) <== nouveauté
  |
  |
  | * Mise à disposition dans les "te" qui calculent SF1D1D (force suiveuse sur les éléments linéique) du champ d'accélération.
  | * Les éléments concernés : CABLE, POU_D_E, POU_D_T, BARRE
  | * Utilisation possible avec une fonction python comme dans les cas tests : sdnl141a, sdnl142a, sdnl143a
  | * Nom des composantes : 'ACCE_X','ACCE_Y','ACCE_Z'
  |
  | Impacts & vérifications  :
  | ==========================
  | Utilisateur   : Aucun, si ce n'est qu'il peut ajouter ACCE_[X|Y|Z] dans les paramètres de la fonction.
  | Documentation : Préciser dans U4.44.01, que la force peut dépendre des paramètres listés ci-dessus.
  | Vérification  : comparaison d'un IMPR_RESU(ACCE) à la fin du calcul avec une impression au cours du calcul des ACCE passées à la
  | fonction python dans les cas tests sdnl141a, sdnl142a, sdnl143a.
  |
  | Remarques :
  | -----------
  | Nettoyage les "TE" des chargements répartis.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : u4.44.01
- VALIDATION : passage cas test avec FORCE_POUTRE
- NB_JOURS_TRAV  : 9.0


================================================================================
                RESTITUTION FICHE 29252 DU 28/10/2019
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : Depuis la version 15.0.11, les cas-tests validation mac3c08a mac3c08b mac3c08c mac3c08d mac3c08e mac3c08f  sont en NOOK_TEST_RESU sous GAIA
- FONCTIONNALITE :
  | Problème
  | --------
  |
  | * Les cas tests mac3c08* sont en NOOK sur Gaia depuis la révision: 889d530e0117
  | * Les cas tests mac3c08* sont en NOOK sur Eole depuis la révision: 24c8ef971db3
  |
  | Aucun changement des cas tests entre ces deux versions.
  |
  | * POST_MAC3COEUR réalise d'une part une extraction de données au format tableau et d'autre part le calcul d'une série d'indicateurs
  | sur le calcul réalisé qui font l'objet de tests de non - régression. Ces derniers n'ont pas vocation à être calculés à la précision
  | machine près.
  |
  | Corrections
  | -----------
  |
  | * Introduction d'un arrondi sur les indicateurs calculés par POST_MAC3COEUR.
  | * Mise à jours des valeurs de référence
  | * Passage en ASBOLU des tests de valeurs proches de 0.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE :
- VALIDATION : cas tests
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 29721 DU 30/03/2020
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 15.1.12 les cas-tests sdll130b et ssll119b sont en NOOK_TEST_RESU sur Scibian9 et clap0f0q
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | En version 15.1.12 les cas-tests sdll130b et ssll119b sont en NOOK_TEST_RESU sur Scibian9 et clap0f0q
  |
  | La variable 'rigige' dans le te0517 n'est pas initialisée.
  |
  | Correction/Développement
  | ------------------------
  |
  | Initialisation de la variable 'rigige' dans le te0517.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE :
- VALIDATION : cas tests
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 29746 DU 06/04/2020
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 15.1.13 le cas test ssls119b est en ERROR sur  Calibre9
- FONCTIONNALITE :
  | Problème
  | --------
  |
  | En version 15.1.13 le cas test ssls119b est en ERROR sur  Calibre9
  |
  |    !-------------------------------------------------------------!
  |    ! <F> <FACTOR_57>                                             !
  |    !                                                             !
  |    ! Solveur MUMPS :                                             !
  |    !   La solution du système linéaire est trop imprécise :      !
  |    !   Erreur calculée   : 1.1285e-06                            !
  |    !   Erreur acceptable : 1e-06   (RESI_RELA)                   !
  |    !                                                             !
  |    ! Conseils :                                                  !
  |    !   On peut augmenter la valeur du mot clé SOLVEUR/RESI_RELA. !
  |    !                                                             !
  |    !                                                             !
  |    ! Cette erreur est fatale. Le code s'arrête.                  !
  |    !-------------------------------------------------------------!
  |
  | Correction
  | ----------
  |
  | Le problème est "un peu" difficile. Il est fait pour vérifier le verrouillage en cisaillement, en plus, COEF_RIGI_DRZ est un peu
  | "tangent"
  | Ob met RESI_RELA à 1.E-5 avec un commentaire dans le fichier comm.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V3.03.119
- VALIDATION : ssls119b
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29530 DU 31/01/2020
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : Tests wtnp128a/b/c/d en version NEW15 sous RHEL7
- FONCTIONNALITE :
  | Problème
  | -----------------
  |
  | Lors du portage de la version de développement 15 sur le cluster RHEL d'EdF R&D Chine, les tests wtnp128a/b/c/d sortent en erreur
  | avec le message suivant :
  |
  |  Instant de calcul:  1.000000000000e+01 - Niveau de découpe: 1
  | --------------------------------------------------------------------------------------
  | |     NEWTON     |     RESIDU     |     RESIDU     |    PILOTAGE    |     OPTION     |
  | |    ITERATION   |     RELATIF    |     ABSOLU     |  COEFFICIENT   |   ASSEMBLAGE   |
  | |                | RESI_GLOB_RELA | RESI_GLOB_MAXI |      ETA       |                |
  | --------------------------------------------------------------------------------------
  |
  |    !----------------------------------------------------------------!
  |    ! <A> <ALGELINE7_20>                                             !
  |    !                                                                !
  |    !  On ne peut pas remplir la composante DX du noeud numéro N1    !
  |    !  pour le champ "&&VEFPME.0000001"                              !
  |    !                                                                !
  |    !                                                                !
  |    ! Ceci est une alarme. Si vous ne comprenez pas le sens de cette !
  |    ! alarme, vous pouvez obtenir des résultats inattendus !         !
  |    !----------------------------------------------------------------!
  |
  |
  |    !--------------------------------------------------------------!
  |    ! <E> <MODELISA9_77>                                           !
  |    !                                                              !
  |    !  manque le paramètre  ???^B????                               !
  |    !                                                              !
  |    !  pour la maille  M4165                                       !
  |    !                                                              !
  |    ! --------------------------------------------                 !
  |    ! Contexte du message :                                        !
  |    !    Option         : RIGI_MECA_TANG                           !
  |    !    Type d'élément : HM_J_DPQ8S                               !
  |    !    Maillage       : MA                                       !
  |    !    Maille         : M4165                                    !
  |    !    Type de maille : QUAD8                                    !
  |    !    Cette maille appartient aux groupes de mailles suivants : !
  |    !       G_2D_36 I3 FISSURE                                     !
  |    !    Position du centre de gravité de la maille :              !
  |    !       x=0.000000 y=-0.500000 z=0.000000                      !
  |    !                                                              !
  |    !                                                              !
  |    ! Cette erreur sera suivie d'une erreur fatale.                !
  |    !--------------------------------------------------------------!
  |
  |
  | Correction
  | ------------------------
  |
  | L'erreur vient de ncra (un vecteur de dimension 2) appelé par rcvalb :
  |
  | call rcvalb(fami, kpg, spt, poum, j_mater,&
  |                     ' ', 'THM_RUPT', 0, ' ', [0.d0],&
  |                     2, ncra(1), para(1), icodre, 1)
  |     dimension du  __|    |__
  |    vecteur à lire            vecteur à lire
  |
  | Par défaut, si on donne un composant, rcvalb lira le vecteur à partir de ce composant avec le dimension donné.
  | Mais cette fois, il n'arrive pas à trouver le 2ème composant correctement. Cela conduit à l'erreur en dessus.
  |
  | Les 4 cas tests marchent sur Mu, en corrigeant :
  | - la dimension de ncra : character(len=8) à character(len=16)
  | - de ncra(1) à ncra dans l'appel de rcvalb

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE :
- VALIDATION : wtnp128abcd
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29699 DU 23/03/2020
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Variables non initialisées dans le contact
- FONCTIONNALITE :
  | Problème
  | -----------------
  |
  | Nicolas a aperçu une valeur, ds_contact%loop(i_loop)%vale_calc, non initialisées, ni affectée dans zzzz231a.
  | Cela conduit un FPE dans asterxx.
  |
  |
  | Analyse
  | ------------------------
  |
  | En analysant, RESU3=STAT_NON_LINE planté dans zzzz231a est un calcul de contact avec RESOLUTION = 'NON' (dans DEFI_CONTACT).
  | Dans ce cas là, on force REAC_GEOM = "SANS", "ALGO_RESO_GEOM" = "POINT_FIXE".
  |
  | Normalement dans cfmmcv, on obtient le nombre des changements de status pour ds_print (ds_contact%loop(i_loop)%vale_calc),
  | qui vient de l'affection dans mmbclc/mmstat/mmmbca.
  |
  | Cependant, lors de RESOLUTION = 'NON' (donc l_all_verif=true), on set directement contact convergé et on ne calcule pas.
  | Donc pas d'affection de cette valeur, et elle n'est jamais initialisée non plus.
  |
  | Dans ce cas là, cette valeur affichée à 0 n'a pas de bon sens.
  |
  |
  | Correction
  | -------------
  |
  | J'initialise les valeurs à 0.
  |
  | Je supprime cette colonne dans la table de convergence si RESOLUTION = 'NON' pour toutes les zones dans InitTableCvg.
  | Donc je détecte aussi dans cfmmcv RESOLUTION si l'on doit obtenir la valeur ds_contact%loop(i_loop)%vale_calc ou pas.
  |
  | lorsque RESOLUTION = 'NON' n'est pas pour toutes les zones, la colonne existe.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE :
- VALIDATION : zzz231a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29771 DU 14/04/2020
================================================================================
- AUTEUR : DROUET Guillaume
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : DEFI_CONTACT : 2 valeurs de COEF_FROT simultanément ?
- FONCTIONNALITE :
  | Problème:
  | =========
  |
  | Dans ma mise en données de simulation de résilience, j'observe dans le .mess 2 valeurs de COEF_FROT :
  | * 210000., qui est la valeur que je souhaite imposer
  | * 100. qui est la valeur de ce paramètre par défaut dans le catalogue de la commande DEFI_CONTACT
  |
  | Analyse:
  | ========
  |
  | C'est un problème du catalogue qui est BEAUCOUP trop compliqué comme on l'a déjà vu avec les copains.
  |
  | ALGO_FROT       =SIMP(statut='f',typ='TXM',defaut="STANDARD", into=("STANDARD","PENALISATION"),),
  | b_frot_std      =BLOC(condition = """equal_to("ALGO_FROT", 'STANDARD') """,
  |                         COEF_FROT  =SIMP(statut='f',typ='R',defaut=100.E+0), ),
  |
  | b_contstd_frotstd=BLOC(condition = """(equal_to("ALGO_CONT", 'STANDARD') and equal_to("ALGO_FROT",'STANDARD' ) )""",
  |                                              COEF_FROT  =SIMP(statut='f',typ='R',defaut=100.E+0),),
  |
  | Comme on le voit, COEF_FROT est simultanément dans deux blocs (!), le superviseur est très gentil !
  |
  | Il prend pas la valeur par défaut, mais bien 2.10000E5, dans la routine cazocc qui ne le lit qu'une fois. Il n'y a pas de résultats
  | faux.
  |
  | Correction:
  | ===========
  |
  | On corrige le catalogue en supprimant le doublon pour COEF_FROT étant donné qu'il est bien défini pour toutes les combinaisons
  | ALGO_FROT ALGO_CONT dans des sous-bloc.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE :
- VALIDATION : submit + test perso
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29812 DU 24/04/2020
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Reliquats suppression SHB
- FONCTIONNALITE :
  | Problème
  | --------
  |
  | Les type_elem SH6 et S15 dans catalo/cataelem/Commons/mesh_types n'ont pas été supprimés lors de la suppression de l'élément SHB
  |
  | Il reste également les schémas d'intégration qui y sont liés ainsi qu'à d'autres endroits du code (projection par exemple)
  |
  |
  | Correction
  | ----------
  |
  | On fait le ménage

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE :
- VALIDATION : src
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29795 DU 21/04/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [bb0140] Introduction de l'orthotropie en thermique non linéaire
- FONCTIONNALITE :
  | Objectif
  | --------
  |
  | Prise en compte des matériaux orthotropes en thermique non linéaire.
  |
  |
  | Développement
  | -------------
  |
  | - Modification de DEFI_MATERIAU pour ajouter le mot-clé facteur THER_NL_ORTH, idem THER_ORTH mais prenant des fonctions.
  | - Modification de THER_NON_LINE pour rendre facultatif, et non caché, le mot clé CARA_ELEM.
  |
  | L’ensemble de l’algorithmie de THER_NON_LINE est modifiée pour passer le cara_elem puis le PCAMASS aux termes élémentaires.
  | Dans ceux-ci, la gestion de l’orthotropie est largement copiée sur celle de la thermique linéaire.
  | A noter que ntfcma.F90, routine permettant la récupération des fonctions matériau pour la thermique non linéaire est
  | assez impactée.
  |
  | Pour chaque dimension - 2D et 3D - quatre termes élémentaires sont modifiés (char_ther_evolni, mtan_rigi_mass,
  | resi_rigi_mass, flux_elga). Au passage, j’en profite pour blinder par message d’erreur le calcul de SOUR_ELGA et
  | ETHE_ELEM en orthotropie : cela plantait déjà pour l’orthotropie linéaire sans être blindé.
  |
  |
  | Validation
  | ----------
  |
  | L’ensemble des 11 tests de thermique orthotrope ont été tournés avec succès en les passant en non linéaire
  | (avec DEFI_CONSTANTE pour les lois matériaux).
  | Inversement, un large ensemble de tests de thermique non linéaire ont été exécutés en les passant en orthotropie
  | "simulée" avec des fonctions identiques pour LAMBDA_L / LAMBDA_T / LAMBDA_N.
  |
  | Enfin, concernant une simulation non linéaire avec une réelle orthotropie, on restitue deux modélisations dans
  | un nouveau cas-test ttnl304 (semblable à ttnl303 en définissant des propriétés orthotropes) dont les valeurs testées
  | sont uniquement de non régression.
  |
  |
  | Documentation
  | -------------
  |
  | u4.43.01 DEFI_MATERIAU
  | u4.54.02 THER_NON_LINE => la partie “theorique” du comportement est dans cette doc
  | v4.22.304 - TTNL304 - Mur infini soumis à un saut de température avec des propriétés orthotropes

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.43.01, u4.54.02, v4.22.304
- VALIDATION : ttnl304a, ttnl304b
- NB_JOURS_TRAV  : 10.0


================================================================================
                RESTITUTION FICHE 29663 DU 05/03/2020
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : EXTRAPOLE dans le calcul CONTACT LAC
- FONCTIONNALITE :
  | Problème
  | -----------------
  |
  | Dans issue29645, un calcul de CONTACT LAC avec la prédiction par EXTRAPOLE est planté toujours avec les résidus :
  | RESI_GLOB_MAXI = 0 et RESI_GLOB_RELA = -1
  |
  |
  | Analyse
  | ------------------------
  |
  | La prédiction par EXTRAPOLE n'est pas un choix optimal pour des calculs de contact.
  | Car à la phase de prédiction, lors du calcul du seconde membre :
  | - elle ne prend pas en compte des termes de contact.
  | - elle prend en compte seulement les conditions Dirichlet par dualisation (AFFE_CHAR_MECA).
  |
  | Donc les résidus sont souvent mal calculés à la 1ère itération, et on trouvera RESI_GLOB_MAXI = 0 ou/et RESI_GLOB_RELA = -1.
  | Normalement Newton rattrapera plus tard grâce à l'actualisation de contact à la fin de chaque itération avec le déplacement de
  | prédiction.
  |
  |
  | Dans le calcul d'issue29645, le problème vient du contact rasant (jeu presque nul, pression non nulle mais faible).
  | Le chargement du 1er pas de temps est trop faible. Donc l'actualisation de contact à la fin de la 1ère itération n'a fait rien.
  | Donc les itérations suivantes répètent comme la 1ère itération.
  |
  | En augmentant le chargement du 1er pas de temps, le plantage est disparu.
  |
  |
  | Résultat
  | -------------
  |
  | L'impact doc u4.51.03 STAT_NON_LINE:
  | Préciser que la prédiction par EXTRAPOLE n'est pas optimale pour les calculs de contact.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : u4.51.03
- VALIDATION : test perso
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 29669 DU 09/03/2020
================================================================================
- AUTEUR : DROUET Guillaume
- TYPE : anomalie concernant code_aster (VERSION 14.5)
- TITRE : En version 14.5.7 le cas-test ssnp165c est en FPE sur clap0f0q
- FONCTIONNALITE :
  | Le test ssnp165c est ok sur clap0f0q en en version 15.1.14 et en version 14.5.12.
  |
  | On classe sans suite

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE :
- VALIDATION : sans suite
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 29797 DU 21/04/2020
================================================================================
- AUTEUR : HAMON François
- TYPE : anomalie concernant code_aster (VERSION 14.4)
- TITRE : Mfront Stable/Unstable
- FONCTIONNALITE :
  | Problème
  | --------
  | Erreur floating point lors d'une utilisation d'une loi Mfront en version Untable fonctionnant en Stable.
  |
  |
  | Solution
  | -------
  | Une protection a été mise sur les variables d'état les initialisant à NAN. Ainsi, en Unstable la valeur de la température est NAN
  | alors qu'en Stable elle était de 2.61855e-322 .
  |
  | Pour les lois natives en fortran, une protection a été insérée avec "if (isnan(T)) then" pour traiter le cas de T= NAN. Pour le cas
  | de Mfront, la protection suivante est utilisée : "if (T!=T)"  pour le cas T=NAN.
  |
  | Bonne journée

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE :
- VALIDATION : Non regression
- NB_JOURS_TRAV  : 0.5
