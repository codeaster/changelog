==================================================================
Version 15.1.14xx (révision 5ae7eb504a61) du 2020-04-10 12:29 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29712 COURTOIS Mathieu         op0039 : traitement de PROC0
 29730 PIGNET Nicolas           Reprogrammer la création de .NOEX dans le ParallelMesh
 28244 PIGNET Nicolas           verima et asterhpc
 28366 PIGNET Nicolas           problème de DEFI_GROUP en parallèle
 27991 PIGNET Nicolas           DEFI_GROUP ne fonctionne pas avec un ParallelMesh
 28283 PIGNET Nicolas           Bug dans IMPR_RESU en calcul parallèle
 28368 PIGNET Nicolas           xxParallelMesh002a : corruption de la mémoire 'matrixFactorization'
 29709 COURTOIS Mathieu         test001a, test001d, test001h en segfault avec la version MPI
 25308 PIGNET Nicolas           Problème avec le Model
 25848 PIGNET Nicolas           Modification de la sd interf_dyna_clas
 26617 PIGNET Nicolas           Héritage objets Parallel
 27229 PIGNET Nicolas           A203.xx - Problème matériau codé
 28689 PIGNET Nicolas           Commande de découpage de maillage
 28805 PIGNET Nicolas           limitations à cause de la détection GROUP_MA / NO dans sous-maillages
 28929 PIGNET Nicolas           Intégrer le partitionneur de maillage dans asterxx
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29712 DU 27/03/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : op0039 : traitement de PROC0
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Dans IMPR_RESU, on peut n'imprimer que sur le proc0 (c'est le défaut).
  | 
  | Si on utilise un maillage parallèle, il faut imprimer sur tous les procs.
  | Dans op0039, on a désactivé le test sur PROC0 et on imprime donc systématiquement sur tous les processeurs
  | quelque que soit le type de maillage.
  | 
  | 
  | Correction
  | ----------
  | 
  | On ajoute un test en début d'op.
  | On récupère le maillage sur la première occurrence de RESU avec le mot-clé MAILLAGE ou 
  | [RESULTAT ou CHAM_GD] + dismoi.
  | Si dimoi(PARALLEL_MESH)==NON et PROC0=OUI et RANK!=0, on quitte l'op sans rien faire avec une information :
  | 
  | [1,1]<stdout>:On ne fait rien sur le processeur #1 (car PROC0='OUI').
  | 
  | 
  | Vérifier dans mumps01b en ajoutant IMPR_RESU : l'info est affichée, pas de fort.80 créé.
  | 
  | Au passage : Il y aussi PROC0 dans FIN, je pense que l'info PARALLEL_MESH doit être globale et connue dès
  | le lancement (en fonction des arguments, type de maillage fichier ou répertoire par exemple).
  | Pour le moment, si PROC0, les autres ne font rien et n'écrivent pas non plus "ARRET NORMAL".
  | Je déplace de deux lignes ce message pour que le statut soit correct sur ces procs.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : mumps01b
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29730 DU 31/03/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant asterxx (VERSION 11.8)
- TITRE : Reprogrammer la création de .NOEX dans le ParallelMesh
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | L'objet .NOEX permet de savoir si les noeuds d'un sous-domaine sont éventuellement partagés avec un autre sous-domaine.
  | 
  | Il est actuellement reconstruit de manière addoc en relisant les groupes de noeuds EXT_* présents dans chaque sous-domaine.
  | 
  | Ces groupes sont créés par le partionneur séquentiel comme des groupes de noeuds supplémentaires (qui n'existent pas dans le
  | maillage complet). Le nouveau partionneur parallèle ne crée pas ces objets.
  | 
  | Les groupes de noeuds EXT_* peuvent être récréés à partir des joints entre les sous-domaines de manière très simple.
  | 
  | Je propose de créer maintenant l'objet .NOEX du ParallelMesh à partir des joints et plus des groupes EXT_* afin de pouvoir
  | d'utiliser la même routine pour le partionneur séquentiel et parallèle. 
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Je crée maintenant l'objet .NOEX du ParallelMesh à partir des joints et plus des groupes EXT_* afin de pouvoir
  | d'utiliser la même routine pour le partionneur séquentiel et parallèle.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelMesh002a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28244 DU 20/11/2018
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : verima et asterhpc
- FONCTIONNALITE :
  | Problème
  | -----------------
  | J'ai codé un truc bizarre dans verima aux alentours de la ligne 53. Il faut corriger en faisant un broadcast sur tous les procs.
  | 
  | 
  | Correction
  | ------------------------
  | Il faut s'assurer dans verima que pour les ParallelMesh, les GROUP_NO et GROUP_MA soient dans le maillage global et pas forcément local.
  | 
  | J'ai créé deux utilitaires existGrpNo et existGrpMa pour tester l’exigence d'un groupe dans le maillage local et global. Il devrait
  | pouvoir servir partout où on fait des tests sur l'existence de groupes. Ceci de fait sans communication supplémentaire car on
  | s'appuie sur les objets .PAR_GRPNOE et .PAR_GRPMAI.
  | 
  | J'ai crée deux autres utilitaires cleanListOfGrpNo et cleanListOfGrpMa qui permettent à partir d'une liste de groupes de ne
  | conserver que ceux qui sont dans le maillage local (et eventuellement s'arrêter s'ils ne sont pas dans le maillage global).
  | 
  | J'en profite pour permette d'allouer et recopier un objet jeveux à partir d'un std::vector ou std::set.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelMechanicalLoad001a
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 28366 DU 20/12/2018
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : problème de DEFI_GROUP en parallèle
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Afin de définir un groupe des noeuds à partir d'un groupe des mailles, DEFI_GROUP ne marche que en séquentiel. Voici l'exemple en
  | parallèle qui est en bloqué.
  | 
  | 
  | Correction
  | ------------------------
  | 
  | J'ai fait les développements nécessaires pour que DEFI_GROUP marche avec un ParallelMesh. La principale difficulté est qu'il faut
  | bien identifier les groupes à créer (uniquement ceux contenus dans le maillage local) et faire les vérifications sinon
  | 
  | Il faut bien penser à mettre aussi à jour la liste des groupes globaux et pas seulement locaux. 
  | 
  | J'ai testé la plupart des cas possibles dans xxParallelTHM001c

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelTHM001c
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 27991 DU 30/08/2018
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant asterxx (VERSION 11.8)
- TITRE : DEFI_GROUP ne fonctionne pas avec un ParallelMesh
- FONCTIONNALITE :
  | Objectifs
  | -----------------
  | 
  | Si on veut utiliser la commande DEFI_GROUP sur un ParallelMesh, on plante sur le typage de la SD produite avec le message :
  | 
  | Traceback (most recent call last):
  |   File "xxParallelMesh001a.py", line 17, in <module>
  |     pMesh2=DEFI_GROUP(reuse =pMesh2,MAILLAGE=pMesh2,CREA_GROUP_NO=_F(TOUT_GROUP_MA='OUI'))
  |   File "/home/B07947/dev/codeaster-a12da/install/lib/aster/code_aster/Commands/ExecuteCommand.py", line 142, in run
  |     raise exc.original(exc.msg)
  | TypeError: Cannot type result of the command DEFI_GROUP
  | Exception raised: NameError("global name 'maillage_p_sdaster' is not defined",))
  | 
  | Je joins la modif dans le cas test xxParallelMesh001a.py.
  | 
  | 
  | Développement
  | ------------------------
  | 
  | 
  | Les dévellopements ont été faits dans issue28366.
  | 
  | Je me contente de rajouter le test proposé

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelMesh001a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28283 DU 27/11/2018
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Bug dans IMPR_RESU en calcul parallèle
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Lors d'IMPR_RESU pour un GROUP_MA dans un calcul en parallèle avec AsterXX, si le processus ne trouve pas de mailles de ce groupe
  | dans son propre sous-maillage, il reste être bloqué sans rien faire.
  | 
  | Voici l'exemple en pièce jointe :
  | - maillage découpé en 4, et GROUP_MA='Haut' existe dans les sous-maillage 0 et 2, pas 1 ni 3
  | 
  | Donc si on lance le calcul, les processus 1 et 3 sont bloqués à l'étape d'IMPR_RESU pour GROUP_MA='Haut', et le calcul n'arrête pas
  | jusqu'à la fin du temps donné.
  | 
  | Développement
  | ------------------------
  | Le test fourni marche grâce à des développements qui ont lieux dans aster_legacy.
  | 
  | Je rajoute dans xxParallelTHM001c, l'impression des fichiers avec des groupes qui n'existent pas dans tout les maillages pour
  | s'assurer que ça marche bien

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelTHM001c
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28368 DU 20/12/2018
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : xxParallelMesh002a : corruption de la mémoire 'matrixFactorization'
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Lors des derniers commits pour issue28231, le test xxParallelMesh002a a été partiellement désactivé.
  | 
  | En effet, si on laisse 'monSolver.matrixFactorization( matrAsse )', le test échoue "après" FIN sans doute à cause d'une corruption
  | de la mémoire.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | Je confirme que si on ne supprime pas la matrice dans le destructeur de AssemblyMatrix, le test marche (comme ceux de issue29631)
  | 
  | 
  | Le problème semble venir de FIN() car si je l’enlève du test et que je laisse la destruction de la matrice mumps dans le destructeur
  | de AssemblyMatrix. Tout ce passe bien.
  | 
  | En fait le problème semble venir d'un problème entre le garbage collector du python et l'opérateur FIN()
  | 
  | Car FIN() détruit les objets sur la base globale qui peuvent être utilisés par le destructeur C++ quand le garbage collector python
  | veut détruire ses instance.
  | 
  | La segfault ici vient que l'on veut détruire la matrice MUMPS (appel garbage_collector) alors que FIN() semble l'avoir déjà
  | détruite. D'où le segfault. Pour détruire, la matrice MUMPS on se base sur l’existence de l'objet .REFA. Or pour le proc 0, il a été
  | détruit avant de passer dans le destructeur C++ et donc on essaye pas de supprimer la matrice MUMPS, par contre pour le cpu il
  | existe encore mais plus la matrice
  | 
  | Ce conflit entre le garbage collector et FIN() ne pose pas de problème en général car soit c'est des objets purement C++ et donc
  | FIN() ne les connaît (et n'essaye pas de les détruire) donc pas de problème dans le garbage collector, soit c'est des copies
  | d'objets jeveux dans le C++ et alors pas de problème non plus car FIN() les détruits ou le garbage collector appelle le destructeur
  | de ces objets par jedetr, et donc un objet détruit par jeveux n'est pas détruit deux fois.
  | 
  | Mais que ce passe t-il pour les allocations hors jeveux et hors c++ (petsc, mumps, miss), c'est un peu au bonheur la chance de qui
  | les détruit en premier.
  | 
  | Pour réparer ici, il faudrait être capable de savoir si la matrice mumps a été détruit par quelqu'un d'autre avant de faire DETR_MAT.
  | 
  | Ceci peut engendrer des fuites mémoires. Ici c'est MUMPS mais je pense que Pestc doit être pareil.
  | 
  | 
  | Pour corriger (comme propose Mathieu) : faire la destruction uniquement si jeveux est ON.
  |  ~AssemblyMatrixClass() {
  | #ifdef _DEBUG_CXX
  |         std::cout << "DEBUG: AssemblyMatrixClass.destr: " << this->getName() << std::endl;
  | #endif
  |         if ( _description->exists() && ( _solverName == "MUMPS" || _solverName == "PETSC" ) &&
  |              get_sh_jeveux_status() == 1 ) {
  |             CALLO_DELETE_MATRIX( getName(), _solverName );
  |         }
  |     };

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelMesh002a
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 29709 DU 27/03/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : test001a, test001d, test001h en segfault avec la version MPI
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les tests test001a et test001h (qui sont ok avec un build séquentiel) échouent en segfault lors
  | de l'appel à `model.setSplittingMethod()`.
  | 
  | 
  | Correction
  | ----------
  | 
  | Dans l'objet Model, en MPI, il y a un attribut supplémentaire : _partialMesh
  | 
  | Celui est bien initialisé par le constructeur qui prend en entrée un 'PartialMesh'.
  | Si on passe un 'Mesh', l'attribut n'est pas initialisé.
  | Ensuite dans 'setSplittingMethod', `_partialMesh_>exists()` conduit logiquement à un segfault.
  | 
  | Dans le constructeur utilisant un 'Mesh', on initialise '_partialMesh' à nullptr.
  | Il suffit ensuite de le vérifier dans 'setSplittingMethod'.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test001a, test001h
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 25308 DU 14/06/2016
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : Problème avec le Model
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | NT a repéré une différence dans le sd produites d'une part par Aster et d'autre part par AsterXX.
  | 
  | Je joins les fichiers permettant de reproduire le problème.
  | 
  | Cette fiche est pour moi.
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Les deux sd produites sont maintenant identiques. 
  | 
  | Sans suite
  | 
  | Sans sources

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test joint
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 25848 DU 12/12/2016
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant asterxx (VERSION 11.4)
- TITRE : Modification de la sd interf_dyna_clas
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Il faudrait ajouter l'appel à DEFI_INTERF_DYNA à asterxx et ajouter aussi un test qui permette de tester cet appel.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | L'objet C++ a déjà été crée  et les tests appelants DEFI_INTERF_DYNA marcheny tous.
  | 
  | On classe sans suite.
  | 
  | Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : fdlv104a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 26617 DU 20/06/2017
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant asterxx (VERSION 11.4)
- TITRE : Héritage objets Parallel
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Il faut revoir la façon dont est pris en compte l'héritage entre les objet Parallel et séquentiel (ex : Mesh et ParallelMesh).
  | 
  | Je pense qu'idéalement, il faudrait que MeshInstance et ParallelMeshInstance hérite d'un objet commun BaseMeshInstance qui devrait
  | être virtuel pur.
  | 
  | Ensuite pour exposer cet objet en boost python, il existe des techniques (boost::noncopyable). Il va falloir creuser un peu mais là,
  | je n'ai pas le temps.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | L'objet MeshClass et ParallelMeshClass héritent d'un même objet BaseMeshClass.
  | 
  | Ce développement a été commité dans aa089c929ede.
  | 
  | Je classe sans suite.
  | 
  | Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelMesh001a
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 27229 DU 14/12/2017
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant asterxx (VERSION 11.8)
- TITRE : A203.xx - Problème matériau codé
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Dans RCMFMC, on créé la carte des matériaux codés. Le problème, c'est qu'on ajoute des objets au matériau en entrée.
  | 
  | Dans le cadre d'aster legacy, ce n'était pas un problème car cet enrichissement était caché par le fait que ces objets étaient sur
  | la volatile et donc supprimés à l'issue du calcul.
  | 
  | Avec asterxx, on peut faire des commandes éclatés un peu dans tous les sens sans passer par la destruction des objets sur la
  | volatile donc on se retrouve avec des objets ajoutés dans le matériau. C'est vraiment sale.
  | 
  | Il faudrait changer cela proprement dans aster legacy donc ne pas créer d'objets sur une sd en entrée (qu'il n'est pas légitime
  | d'enrichir).
  | 
  | Correction/Développement
  | ------------------------
  | Ce problème a été résolu par issue29549
  | 
  | Petit commentaire sur ce qui a été fait
  | 
  | le matériau codé est maintenant créé sur une structure de données temporaire (volatile) et plus sur le cham_mater qui peut être
  | spécifiée par le développeur. (pour l'instant "&&MATECO").
  | 
  | Les deux objets .MATE_CODE et .CODI sont maintenant créés sur le matériau codé indépendant du reste et rend plus propre
  | l'utilisation de ces structures temporaires.
  | 
  | J'ai fait les modifs nécessaires dans le c++.
  | 
  | Ce sera ajouter dans aster_legacy à la fusion
  | 
  | 
  | Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : astest
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28689 DU 29/03/2019
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant asterxx (VERSION 11.8)
- TITRE : Commande de découpage de maillage
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Dans le but d'intégrer le découpeur de maillage dans aster et asterstudy, je propose l'ajout d'une nouvelle commande (DECOUPE_MAILLAGE).
  | 
  | Pour cela, il faudrait ajouter le découpeur de maillage en dépendance d'aster. Donc ajouter MEDLoader et compagnie en dépendance
  | d'aster.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Fiche en doublon avec issue29528
  | 
  | Je classe sans suite
  | 
  | Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28805 DU 02/05/2019
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : limitations à cause de la détection GROUP_MA / NO dans sous-maillages
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | L'utilisation des opérateurs est actuellement limitée dans des calculs en parallèle sur Asterxx à cause de la mauvaise détection
  | GROUP_MA / NO au niveau des processeurs : Quand des groupes n'existent que dans certains sous-maillages repartis, des processeurs ne
  | les trouvent pas dans leurs propres sous-maillages et ils s'arrêtent. Par exemple, DEFI_GROUP, option OBSERVATION dans STAT_NON_LINE
  | / DYNA_VIBRA (besoin dans le projet Risques Sismiques), etc.
  | 
  | Une proposition possible : une détection pour GROUP_MA / NO au niveau global (dans tous les opérateurs) avant l'exécution de calcul.
  | 
  | Correction/Développement
  | ------------------------
  | Le problème est plus fondamentale que juste détecter la présence ou non d'un groupe. Il faut aussi savoir comment la programmation
  | est faite derrière pour continuer même s'il n'y a pas de groupes locaux.
  | 
  | Dans issue28244, j'ai modifié verima pour qu'il ne garde que les groupes locaux et s’arrête si un groupe global n'existe pas.
  | Malheureusement, ça ne suffit pas toujours.
  | 
  | J'ai corrigé DEFI_GROUP dans issue28366. Pour les autres c'est du cas par cas.
  | 
  | Il faudra créer une fiche et la résoudre chaque fois qu'il y aura un bug.
  | 
  | Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelTHM001c
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28929 DU 14/06/2019
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant asterxx (VERSION 11.8)
- TITRE : Intégrer le partitionneur de maillage dans asterxx
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Le partitionneur de maillage pour asterxx est présent dans le dépôt : https://git.forge.pleiade.edf.fr/git/salome-
  | prototypes.decoupeuraster.git.
  | Il est indispensable pour réaliser un calcul distribué avec asterxx. L'intégrer dans les sources du code ou dans 
  | Salome_Meca est nécessaire.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Fiche en doublon avec issue29528
  | 
  | Je classe sans suite
  | 
  | Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.2

