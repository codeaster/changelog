==================================================================
Version 15.1.21 (révision d3886423698b) du 2020-05-18 14:54 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29879 FLEJOU Jean Luc          SDNL141 : Poutre immergée oscillante soumise à un champ de vague
 29886 BETTONTE Francesco       Catalogue CALC_PRESSION
 28893 BETTONTE Francesco       En version 14.3.13, couverture de *_MAC3COEUR
 29235 BÉREUX Natacha           Erreur développeur LDLT_INC + MATR_DISTRIBUEE
 29580 DEGEILH Robin            [RUPT] post-traitement des noeuds sur le fond de fissure avec post_k1_k2_k3
 29210 DEGEILH Robin            [RUPT] POST_K1_K2_K3 - Plantage dans un assert
 29896 COURTOIS Mathieu         Option par défaut de 'waf install'
 29897 COURTOIS Mathieu         Vocabulaire TYPE_TABLE
 29816 GEOFFROY Dominique       AFFE_CARA_ELEM/TABLE_CARA
 29869 HAMON François           En version 15.1.18 révision fe6b4e657140 - le cas test validation sdnp002a es...
 29517 COURTOIS Mathieu         Construction et Installation sur le cluster EDF R&D Chine
 29265 COURTOIS Mathieu         En version 15.0.13 et 14.4.9 le cas test validation miss03c s'arrête dans CAL...
 29769 GEOFFROY Dominique       En version 14.5.11-révision 5638b5abd772, le cas test vérification zzzz289a e...
 29267 GEOFFROY Dominique       En version 14.4.9 les cas-tests validation (mpi) perf005i, perf005k, perf018c...
 26925 ABBAS Mickael            D124.19 - Restitution de cas-tests de grande dimension pour le contact
 29157 BÉREUX Natacha           Sur GAIA  - en versions  mpi 14.4.4 ET mpi 15.0.7, hsna100c et wtnv136c sont ...
 29903 BÉREUX Natacha           Problème de libération de la matrice MUMPS avec PETSC + LDLT_SP + ELIM_LAGR='...
 28427 BÉREUX Natacha           Différences de performances aster/asterxx
 29865 GRANGE Fabien            [V&V] Documentation du comportement DIS_GRICRA à quatre pentes
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29879 DU 13/05/2020
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : SDNL141 : Poutre immergée oscillante soumise à un champ de vague
- FONCTIONNALITE :
  | SDNL141 : Poutre immergée oscillante soumise à un champ de vague
  | 
  | Ajout de modélisations pour compléter la validation (a,b,c). Et avec une doc !!
  | 
  | issue23764 : Toutes les doc sont dans la GED, elle peut être "fermée".
  | issue29868 : peut également être "fermée"

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v5.02.141
- VALIDATION : passage cas test
- DEJA RESTITUE DANS : 15.1.20
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 29886 DU 18/05/2020
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Catalogue CALC_PRESSION
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | La réglè actuelle (EXCLUS) du catalogue de CALC_PRESSION permet à l'utilisateur de ne déclarer aucun des 2 mots clés TOUT_ORDRE et INST, pour arriver ensuite sur un plantage en cours d’exécution.
  | 
  | De plus, les arguments CRITERE et PRECISION ne sont pas utilisés.
  | 
  | 
  | Correction
  | ----------
  | 
  | 1 - La règle EXCLUS est remplacée par UN_PARMI.
  | 2 - Les arguments CRITERE et PRECISION sont passés aux opérateurs CREA_CHAMP et CREA_RESU.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28893 DU 03/06/2019
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 14.3.13, couverture de *_MAC3COEUR
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Suite à la restitution de issue28687 et issue26160, les mot-clés suivants ne sont pas couverts:
  | 
  | 
  |    ** le mot clé :MAINTIEN_GRILLE#OUI
  |       n'est pas testé dans :
  |           CALC_MAC3COEUR/DEFORMATION
  |           CALC_MAC3COEUR/ETAT_INITIAL
  | 
  |    ** le mot clé :NB_ASSEMBLAGE_N
  |       n'est pas testé dans :
  |           PERM_MAC3COEUR/__
  | 
  | 
  |    ** le mot clé :NB_ASSEMBLAGE_NP1
  |       n'est pas testé dans :
  |           PERM_MAC3COEUR/__
  | 
  | 
  | Correction
  | ----------
  | Ajout de tests de non-régression. 
  | 
  | MAINTIEN_GRILLE=OUI #DEFORMATION testé dans mac3c11a
  | MAINTIEN_GRILLE=OUI #ETAT_INITIAL testé dans mac3c06a
  | NB_ASSEMBLAGE_N et NB_ASSEMBLAGE_NP1 #PERM_MAC3COEUR testés dans mac3c09b

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas tests
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29235 DU 17/10/2019
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : Erreur développeur LDLT_INC + MATR_DISTRIBUEE
- FONCTIONNALITE :
  | Problème 
  | ========
  | Lorsqu'on utilise le préconditionneur LDLT_INC avec METHODE=PETSC et MATR_DISTRIBUEE='OUI' on est arrêté par un ASSERT.
  | 
  | Diagnostic:
  | ========== 
  | Suivant que l'on utilise un solveur de PETSC ou GCPC, on fait appel à la construction du préconditionneur LDLT_INC de PETSc ou bien
  |  d'aster. En revanche, dans les deux cas on utilise la même routine de renumérotation (Reverse Cuthill Mac Kee) préalable de la
  | matrice (ldlt_matr.F90). Cette routine est séquentielle et ne s'applique pas aux matrices distribuées.
  | Elle contient un ASSERT qui arrête brutalement le calcul si la matrice est distribuée.
  | 
  | Correction:
  | ===========
  | On ajoute un message d'erreur pour indiquer que MATR_DISTRIBUEE='OUI' est incompatible avec 'LDLT_INC' et on corrige la doc.
  | 
  | 
  | Remarque: 
  | =========
  | Si besoin, émettre une fiche d'évolution pour que LDLT_INC utilisé avec METHODE='PETSC' n'appelle pas l'implémentation aster de
  | l'algorithme de renumérotation RCMK mais l'implémentation PETSc du même algorithme.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.50.01
- VALIDATION : cas tests petsc*
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 29580 DU 12/02/2020
================================================================================
- AUTEUR : DEGEILH Robin
- TYPE : anomalie concernant code_aster (VERSION 14.6)
- TITRE : [RUPT] post-traitement des noeuds sur le fond de fissure avec post_k1_k2_k3
- FONCTIONNALITE :
  | *** PROBLEMES *** 
  | Ces problèmes ne concernent que les calculs FEM.
  | a) Les points où sont calculés K dans POST_K1_K2_K3 sont les noeuds sommets uniquement, selon la doc.
  | Or, en pratique le code calcule sur les noeuds sommets et milieux par défaut, d'où un nombre de lignes
  | dans le tableau de sortie supérieur à l'attendu.
  | **Analyse : Il existe dans le code une boucle qui calcule un "pas" qui devrait sauter les noeuds
  | milieu si l'option TOUT n'est pas activée. Or, ce "pas" n'est jamais appelé ailleurs. Le comportement
  | du code ne correspond donc pas à la documentation.
  | 
  | b) L'option TOUT=OUI ne change rien.
  | **Analyse : L'option TOUT='OUI' permet normalement d'activer le calcul aux noeuds milieu également.
  | Or, ici comme le comportement par défaut est déjà celui-ci, cette option ne change rien.
  | 
  | c) Lorsque l'on entre un groupe de noeuds où l'on veut forcer les points de calcul 'GROUP_NO' ou un
  | groupe de noeuds à exclure 'SANS_GROUP_NO' qui contient une minuscule, le code plante car il ne
  | trouve pas ce groupe dans le maillage alors qu'il existe bel et bien.
  | **Analyse : l'entrée de groupe de noeuds utilisateur est convertie de la sorte :
  | grpno.ljust(24).upper() avant d'être comparé aux groupes de noeuds du maillage. La conversion upper
  | convertie tout en majuscule d'où l'erreur.
  | 
  |    *** SOLUTIONS APPORTEES ***
  | a+b) La selection des noeuds de calcul est revue. Le comportement par défaut est remis selon la doc,
  | à savoir un calcul sur les noeuds sommets uniquement. En complément, il était possible selon le code
  | (mais non documenté) de renseigner des noeuds de calcul par des NOEUDS, ce qui est déconseillé,
  | je supprime donc cette possibilité. Cette suppression impacte le test sslv134d qui testait cette option. Je garde le test en ajoutant un DEFI_GROUP pour "convertir" ces NOEUDS en groupes. 
  | 
  | c) Je supprime le .upper() qui ne sert à rien.
  | 
  |    *** VERIFICATION ***
  | a+b) J'ajoute au test zzzz257d des POST_K1_K2_K3 qui activent les différentes possibilités de 
  | renseignement des noeuds de calculs (TOUT=None ou 'OUI'; GROUP_NO=None ou 'XXX' ; GROUP_NO_SANS=None
  | ou 'XXX'). Je fais un test sur le MAX de NUM_PT pour vérifier si le nombre de ligne dans la table
  | est la bonne.
  | Je change une valeur de non-regression dans sslv154a car la valeur max était obtenue sur un noeud milieu (mais on constate de fortes oscillations entre noeuds milieux et noeuds sommets donc cette valeur ne me paraissait pas physique)
  | 
  | c) Dans zzzz257d, j'utilise un groupe de noeuds contenant des majuscules et minuscules.
  | 
  |    *** IMPACT DOC ***
  | Le code a été remis en cohérence avec la documentation donc la documentation U4 actuelle est bonne.
  | Pas d'impact doc V car zzzz.
  |    
  |    *** RISQUES DE RESULTATS FAUX ***
  | Le code calculait en plus de points que ce qui était demandé par l'utilisateur. Le résultat était
  | donc dans tous les cas conservatif.
  | 
  | 
  | Complément : Dans l'exemple envoyé pour illuster le problème, un maillage libre est utilisé alors
  | que l'option TYPE_MAILLAGE est laissé par défaut à "REGLE". L'opérateur détecte donc que les noeuds
  | du maillage libre ne sont pas alignés et exclut donc une partie des points de calcul, d'où un nombre
  | de ligne dans la table de sortie de 155 alors qu'il devrait être de 199. Bien penser à utiliser
  | TYPE_MAILLAGE='LIBRE' pour traiter ces cas.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzz257d, sslv134d, sslv154a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29210 DU 10/10/2019
================================================================================
- AUTEUR : DEGEILH Robin
- TYPE : anomalie concernant code_aster (VERSION 14.4)
- TITRE : [RUPT] POST_K1_K2_K3 - Plantage dans un assert
- FONCTIONNALITE :
  | *** PROBLEME *** 
  | L'utilisateur restreint son maillage à la zone autour de la fissure pour améliorer les performances
  | de son post-traitement avec POST_K1_K2_K3. L'opérateur plante dans : assert len(ChnoVrcNoeu) == NbNoMa
  | **Analyse : Cet assert vérifie que tous les noeuds du maillages ont bien une valeur de VARC. Or,
  | ici ce n'est pas le cas pour la structure de donnée RESULTAT en entrée de POST_K1_K2_K3. Il existe
  | 21784 noeuds dans le maillage et seuls 21310 ont une variable de commande 'TEMP'. L'opérateur plante
  | logiquement. Le problème ne vient donc pas de POST_K1_K2_K3. 
  | Ici, le maillage est composé d'éléments 3D et d'éléments 2D (éléments de bords ET éléments
  | surfaciques représentant la fissure mais non connectés à des éléments volumiques). Ce nombre de
  | 21310 correspond au nombre de noeuds des éléments volumiques. Il semblerait donc que la VARC ne soit
  | affectée qu'aux noeuds des éléments volumiques. Ceci pour une raison que j'ignore.
  | 
  |    *** SOLUTIONS APPORTEES ***
  | On ajoute un message d'erreur à la place de l'assert pour être plus explicite.
  | Je préconise également à l'utilisateur de toujours extraire des zones du maillage dont les éléments
  | surfaciques sont connectés à des éléments volumiques. 
  | 
  |    *** VERIFICATION ***
  | Vérification manuelle sur le cas de l'utilisateur. Le message affiché est le suivant :
  |    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  |    ! <S> Exception utilisateur levee mais pas interceptee.                          !
  |    ! Les bases sont fermees.                                                        !
  |    ! Type de l'exception : error                                                    !
  |    !                                                                                !
  |    ! Le champ de variables de commande 'TEMP' ne contient pas autant de valeurs     !
  |    ! (21310) que le nombre de noeuds (21784) dans la structure de données résultat. !
  |    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  | 
  | 
  |    *** IMPACT DOC ***
  | Pas d'impact doc.
  |    
  |    *** RISQUES DE RESULTATS FAUX ***
  | Sans objet.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Manuelle
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29896 DU 20/05/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Option par défaut de 'waf install'
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | issue29763 a introduit les arguments '--safe' et '--fast' pour 'waf install'.
  | 
  | Certains développeurs n'ont pas bien compris la nuance.
  | '--fast' n'utilisent que la date pour décider si une routine fortran doit être reconstruite ou non.
  | '--safe' cherche si le contenu du fichier et de ses dépendances a changé pour décider.
  | 
  | Quand on se déplace dans l'arbre ('hg update'), que se passe-t-il quand on se place sur une 
  | révision plus ancienne ? est-ce que les fichiers sources peuvent être plus anciens que les fichiers
  | objets existants localement ?
  | 
  | Correction
  | ----------
  | 
  | On bascule donc par défaut sur l'option '--safe' ("comme avant").
  | 
  | Il reste toujours très utile d'utiliser 'waf install --fast' quand on travaille sur un développement.
  | Utiliser 'waf install --safe' à chaque fois qu'on modifie le répertoire de travail avec 'hg update'
  | (ou autre copie manuelle).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : build
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29897 DU 22/05/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Vocabulaire TYPE_TABLE
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Dans issue29079, on a introduit le mot-clé caché TYPE_TABLE dans CALC_TABLE pour usage dans des
  | macro-commandes.
  | Les valeurs possibles sont TABLE, TABLE_FONCTION et TABLE_CONTENEUR.
  | 
  | Ce n'est pas cohérent avec les autres mots-clés similaires, par exemple dans EXTR_TABLE.
  | 
  | 
  | Correction
  | ----------
  | 
  | Il faut utiliser les valeurs TABLE_SDASTER, TABLE_FONCTION et TABLE_CONTAINER.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssll107a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29816 DU 27/04/2020
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : AFFE_CARA_ELEM/TABLE_CARA
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Le code plante sur un assert lorsque l'on utilise AFFE_CARA_ELEM/TABLE_CARA et que l'on donne à NOM_SEC un nom qui n'est pas dans la colonne LIEU.
  | 
  | Correction :
  | ==========
  | 
  | Dans aceapo, on supprime l'assert qui est inutile et qui bloque l'accès à l'émission du message d'erreur qui est présent juste en dessous.
  | 
  | Validation :
  | On a vérifié la bonne émission du message d'erreur en modifiant ssll107f.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssll107f modifié


================================================================================
                RESTITUTION FICHE 29869 DU 11/05/2020
================================================================================
- AUTEUR : HAMON François
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 15.1.18 révision fe6b4e657140 - le cas test validation sdnp002a est en NOOK sur Eole
- FONCTIONNALITE :
  | PROJET: ADELAHYD II
  | 
  | OBJET: corriger le cas test sdnp002a
  | 
  | 
  | PROBLÉMATIQUE :
  | La tolérance est dépassée sur un TEST_RESU. L'erreur est de 6% contre une critère à 5%. Ce problème est similaire à la fiche
  | 29688.Le choix de restreindre le calcul dynamique à 1 seconde au lieu de 15 secondes pour des raison de temps de calcul induit des
  | déplacements très (très) faibles. Une petite variation peut entraîner des écarts importants.
  |   
  | 
  | SOLUTION :
  | Généraliser l'augmentation de la tolérance sur l'ensemble des TEST_RESU à 50%. 
  | 
  | Pour la fiche 29688, seules les tableaux associés au déplacement suivant Y avaient été modifiés. Une tolérance de 50% est aussi
  | choisie pour les déplacements suivants X.
  | 
  | 
  | 
  | DOCUMENTATION: doc V5.04.002

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : v5.04.002
- VALIDATION : Non regression
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 29517 DU 28/01/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : aide utilisation concernant code_aster (VERSION 15.1)
- TITRE : Construction et Installation sur le cluster EDF R&D Chine
- FONCTIONNALITE :
  | Cette fiche trace le travail de construction et d'installation sur le cluster EDF R&D Chine.
  | 
  | L'historique de la fiche détaille les étapes de construction des prérequis, jusqu'au bilan des tests.
  | 
  | On trouve une archive jointe à la fiche contenant les fichiers de configurations utilisés.
  | 
  | De plus, cette fiche est indiquée dans la documentation des serveurs pour référence future.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : bilan de tests sur la machine considérée
- NB_JOURS_TRAV  : 15.0


================================================================================
                RESTITUTION FICHE 29265 DU 04/11/2019
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 15.0.13 et 14.4.9 le cas test validation miss03c s'arrête dans CALC_MISS sur GAIA
- FONCTIONNALITE :
  | Cas-test ok, résolu par issue27242 (correction du nombre de threads utilisés) de la semaine passée.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : miss03c
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29769 DU 14/04/2020
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 14.5)
- TITRE : En version 14.5.11-révision 5638b5abd772, le cas test vérification zzzz289a est en FPE sur clap0f0q
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | En version 14.5.11-révision 5638b5abd772, le cas test vérification zzzz289a est en FPE sur clap0f0q.
  | 
  | Analyse :
  | =======
  | 
  | Je ne suis pas parvenu à reproduire le problème sur clap0f0q, ni sur une autre machine.
  | J'ai donc lancé le test avec valgrind. Une erreur a été détectée dans CALC_MATR_ELEM à la ligne 155 de la routine thmGetGene.
  | 
  | En regardant les sources, on s'aperçoit que le problème est dû à une non_initialisation de ds_the_ds_elem%nb_phase(1:2).
  | J'ai vérifié que l'erreur disparaissant avec cette correction.
  | Cette correction vient d'être faite dans le cadre de issue29749 (en v15 et v14).
  | 
  | Conclusion :
  | ==========
  | 
  | Problème déjà résolu.
  | Rien à restituer

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test ok sur clap0f0q


================================================================================
                RESTITUTION FICHE 29267 DU 04/11/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 14.4)
- TITRE : En version 14.4.9 les cas-tests validation (mpi) perf005i, perf005k, perf018c et ssnv249a sont en ABNORMAL_ABORT sur EOLE
- FONCTIONNALITE :
  | Probleme
  | =============
  | En version 14.4.9 les cas-tests validation (mpi) perf005i, perf005k, perf018c et ssnv249a sont en ABNORMAL_ABORT sur EOLE
  | 
  |    !----------------------------------------------------------------------------------!
  |    ! <F> <APPELMPI_99>                                                                !
  |    !                                                                                  !
  |    !     Au moins un processeur n'est pas en mesure de participer à la communication. !
  |    !     L'exécution est donc interrompue.                                            !
  |    !                                                                                  !
  |    !                                                                                  !
  |    ! Cette erreur est fatale. Le code s'arrête.                                       !
  |    !----------------------------------------------------------------------------------!
  | 
  | 
  | 
  | Solution
  | ============
  | Depuis 14.5.9, les tests sont OK. Il semble donc que la communication MPI soit rétablie sur Eole. On ne sait pas d'où provient 
  | l'erreur, mais on ne sait pas reproduire. On ne fait rien.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests OK
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 26925 DU 21/09/2017
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION 11.4)
- TITRE : D124.19 - Restitution de cas-tests de grande dimension pour le contact
- FONCTIONNALITE :
  | Le test ssnv249c a été introduit par la fiche 28547, révision 9b3e86e4199d.
  | 
  | Je propose donc de fermer cette fiche

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv249b
- NB_JOURS_TRAV  : 6.0


================================================================================
                RESTITUTION FICHE 29157 DU 24/09/2019
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : Sur GAIA  - en versions  mpi 14.4.4 ET mpi 15.0.7, hsna100c et wtnv136c sont en NO_RESU_FILE
- FONCTIONNALITE :
  | Problème :
  | ==========
  | Sur GAIA  - en versions  mpi 14.4.4 ET mpi 15.0.7, hsna100c et wtnv136c sont en NO_RESU_FILE
  | 
  | Constat :
  | =========
  | Les deux tests sont OK avec la 15.1.19 sur GAIA => sans suite

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : hsna100c wtnv136c
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 29903 DU 22/05/2020
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : Problème de libération de la matrice MUMPS avec PETSC + LDLT_SP + ELIM_LAGR='OUI'
- FONCTIONNALITE :
  | Créée par erreur - à fermer sans suite

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Non
- NB_JOURS_TRAV  : 0.0


================================================================================
                RESTITUTION FICHE 28427 DU 16/01/2019
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : anomalie concernant asterxx (VERSION 15.1)
- TITRE : Différences de performances aster/asterxx
- FONCTIONNALITE :
  | Problème:
  | =========
  | En janvier 2019, on observe des différences de temps significatives pour l'exécution d'un même cas test avec asterxx et aster
  | legacy. Cette différence apparaît sur la version optimisée mais pas la version debug
  | 
  | Diagnostic/Conclusion:
  | ======================
  | En mai 2020, avec la version 15.1.19 il n'est plus possible de reproduire le problème. Les temps d'exécution des tests avec la
  | version asterxx et la version legacy sont très voisins. 
  | 
  | 
  | => sans suite

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz255b sdnl113a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29865 DU 11/05/2020
================================================================================
- AUTEUR : GRANGE Fabien
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [V&V] Documentation du comportement DIS_GRICRA à quatre pentes
- FONCTIONNALITE :
  | Modification de la doc R5.03.17 pour tenir compte de la 4eme pente de DIS_GRICRA (#25714)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Documentation
- NB_JOURS_TRAV  : 0.5

