==================================================================
Version 15.7.10 (révision a3a59600fbaa) du 2023-06-30 10:57 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32870 PIGNET Nicolas           Plantage dans POST_ELEM/TRAV_EXT
 32947 BETTONTE Francesco       DEFI_MATERIAU depuis une TABLE n'ajoute pas les paramètres par défaut
 32946 BETTONTE Francesco       DEFI_MATERIAU / THER_NL_ORTH ignore le paramètre RHO_CP
 32920 BETTONTE Francesco       Nom des champs en acoustique
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32870 DU 19/05/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 16.4)
- TITRE : Plantage dans POST_ELEM/TRAV_EXT
- FONCTIONNALITE :
  | Problèmes :
  | =========
  | 
  | 1 - Le calcul du travail des forces extérieures échoue dans un calcul avec contact car il n'y a pas le même nombre de composantes
  | dans les champs DEPL et FORC_NODA.
  | 
  | 2- Le calcul de travail extérieur prend en compte des composantes qui ne devrait pas l'être dans certains cas.
  | Par exemple pour la modélisation 3D_INCO_UPG, les composantes PRES et GONF sont prises en compte.
  | 
  | 
  | Correction :
  | ==========
  | 
  | Pour corriger ces deux problèmes, on crée un nouvelle routine cnsreddepl qui :
  | 
  | - regarde parmi les composantes DX, DY, DZ, DRX, DRY et DRZ lesquelles sont présents dans le champ simple fourni
  | - effectue un cnsred sur les composantes présentes
  | 
  | On appelle cette nouvelle routine dans pexext pour sélectionner (et ordonner) les composantes des champs de déplacements et de forces nodales.
  | 
  | 
  | Validation :
  | ----------
  | 
  | Dans ssnv231a, (modélisation 3D_INCO_UPG) on ajoute le calcul du travail ainsi qu'un test de non-régression.
  | On a vérifié au préalable que le test était NOOK avant correction.
  | On a également vérifié que le test fourni fonctionnait après correction.
  | 
  | Report en v15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv231a
- DEJA RESTITUE DANS : 16.3.24


================================================================================
                RESTITUTION FICHE 32947 DU 16/06/2023
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : DEFI_MATERIAU depuis une TABLE n'ajoute pas les paramètres par défaut
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | L'option TABLE de DEFI_MATERIAU n'ajoute pas les paramètres par défaut.
  | L'erreur a été identifiée lors d'une utilisation de la table produite par CALC_MATE_HOMO.
  | 
  | Correction
  | ----------
  | 
  | Déplacement de l'appel à addDefaultKeywords dans la fonction check_syntax.
  | 
  | 
  | Test
  | ----
  | Ajout d'une poursuite à hplv108a

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test hplv108a
- DEJA RESTITUE DANS : 16.3.25
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32946 DU 16/06/2023
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : DEFI_MATERIAU / THER_NL_ORTH ignore le paramètre RHO_CP
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | DEFI_MATERIAU / THER_NL_ORTH ignore le paramètre RHO_CP.
  | Le plantage arrive dans le fortran.
  | 
  | Correction
  | ----------
  | 
  | Le paramètre RHO_CP pour les options THER_NL et THER_NL_ORTH sert à créer la fonction enthalpie BETA. Ce n'était pas programmé 
  | pour THER_NL_ORTH
  | 
  | Test
  | ----
  | Modification de ttnl304b

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ttnl304b
- DEJA RESTITUE DANS : 16.3.25
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32920 DU 07/06/2023
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Nom des champs en acoustique
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Dans le test ahlv100t, on appelle CALC_MODES avec un modèle acoustique.
  | La structure de données en sortie n'est pas du bon type et n'a pas de champ PRES mais un champ DEPL.
  | 
  | Correction :
  | ==========
  | 
  | Le problème vient du fichier code_aster/MacroCommands/Modal/mode_iter_simult.py. Il y a une erreur dans la méthode create_result.
  | On corrige :
  | 
  |          elif isinstance(vale_rigi, AssemblyMatrixPressureReal):
  | -            self._result = ModeResultComplex()
  | +            self._result = AcousticModeResult()
  | 
  | Tout est bon dans mode_iter_inv.py.
  | 
  | Validation : on ajoute un TEST_RESU sur le champ PRES. Le calcul plante avant correction.
  | 
  | Report en v15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ahlv100t
- DEJA RESTITUE DANS : 16.3.26

