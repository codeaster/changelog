==================================================================
Version 15.4.13 (révision 56442ee74d8b) du 2021-12-15 17:26 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31608 BETTONTE Francesco       Erreurs COOR_ELGA MATE_ELGA
 31440 PIGNET Nicolas           zzzz503c et DEFI_GROUP
 31586 PIGNET Nicolas           MECA_STATIQUE ne prend pas en compte INST_FIN
 31576 PIGNET Nicolas           Erreur DVP_1 dans STAT_NON_LINE avec PRE_EPSI
 31640 COURTOIS Mathieu         Temps du test miss10c
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31608 DU 02/12/2021
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Erreurs COOR_ELGA MATE_ELGA
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Plusieurs erreurs liés à la manipulation de champs aux éléments type COOR_ELGA ou MATE_ELGA. 
  | Par ailleurs, le test szlz107b est NOOK en debug seulement. 
  | 
  | 
  | Corrections
  | -----------
  | 1 # On ne peut pas extraire un champ MATE_R :
  |     CH_MATE_ELGA = CREA_CHAMP(RESULTAT=RESU,
  |                           TYPE_CHAM='ELGA_MATE_R',
  |                           OPERATION='EXTR',
  |                           NOM_CHAM='MATE_ELGA',
  |                           NUME_ORDRE=1)
  | 
  |     car il manque MATE_R dans c_nom_grandeur.py
  | 
  | 2 # L'impression au format MED d'un champ elga restreint sur un groupe de mailles donne un erreur Jeveux.
  |     Depuis issue28494 on cherche la dimension du problème dans le modèle au lieu du maillage, mais le développement 
  |     introduit par issue21972 pour écrire un champ aux éléments au format MED passe par la création d'une 
  |     sd_modele contenant le .MAILLE uniquement. Or il faut une sd_modele complète d'une sd_ligrel pour faire appel à dismoi DIM_GEOM. 
  |     
  |     Pour corriger on essaie d'abord de récupérer le modèle depuis le ligrel restreint (pour éviter la création d'un faux modèle). 
  |     Si il est absent on passe par la création d'une sd_modele fausse mais avec copie du ligrel en plus.
  | 
  | 3 # L'export en champ simple termine en Programming Error.
  |     La vérification sur la taille du champ simple n'est pas correcte. On corrige de façon cohérente avec la doc qui dit que la dimension de CESV 
  |     est la somme sur toutes les mailles ima de nbpt(ima) * nbsp(ima) *nbcmp(ima). 
  | 
  | 4 # Valgrind remonte des variables non initialisées dans acgrdo.F90 mais après initialisation le test szlz107b  
  |     est toujours NOOK en debug seulement. Dans POST_FATIGUE (acgrdo.F90) on fait des appels à teneps avec 
  |     des arguments bidons si seule une sortie est nécessaire --call teneps(jrwork, adrl, rbid, rbid, rbid, epspl)--.
  |     Il se trouve que si on évite de répéter le vecteur rbid --call teneps(jrwork, adrl, sigl, epsl, epsel, epspl )-- il n'y a plus de différence entre debug et
  | release
  | 
  | Avec report en V15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz105a + szlz107b en debug
- DEJA RESTITUE DANS : 16.0.20
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 31440 DU 27/09/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : zzzz503c et DEFI_GROUP
- FONCTIONNALITE :
  | Problème
  | -----------------
  | Dans zzzz503c, lorsque j'ajoute le DEFI_GROUP suivant :
  | 
  | DEFI_GROUP(reuse=pMesh,
  |            MAILLAGE=pMesh,
  |            CREA_GROUP_MA=(_F(NOM='BLABLA',
  |                              OPTION='SPHERE',
  |                              POINT=(0.2, 0.2, 0.2),
  |                              RAYON = 0.2),),)
  | 
  | J'obtiens un groupe de maille sur un des processeurs qui a la maille 0 à l'intérieur. Or dans aster, la maille 0 n'existe pas. La numérotation commence à .
  | 
  | 
  | Correction
  | ----------
  | 
  | Il suffit de ne pas créer les groupes quand il y a zero maille. J'avais enlevé une verif pour ne pas s'arrêter en erreur en hpc
  | 
  | Je rajoute un test dans zzzz503c pour s'assurer que la correction marche
  | 
  | Report en v15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz503c
- DEJA RESTITUE DANS : 16.0.10
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31586 DU 22/11/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : MECA_STATIQUE ne prend pas en compte INST_FIN
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | en faisant d'autres fiches, je trouve que MECA_STATIQUE ne prend pas en compte INST_FIN.
  | Voir l'exemple forma02a en pièce jointe
  | 
  | 
  | Correction
  | ------------------------
  | 
  | Je tronque LIST_INST avec la valeur de INST_FIN si présent dans le python
  | 
  | Vérification dans zzzz504b sur le nombre de NUME_ORDRE
  | 
  | Report v15
  | 
  | Résultat faux
  | -------------
  | 
  | Non, on avait juste trop de champs

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz504b
- DEJA RESTITUE DANS : 16.0.18
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31576 DU 18/11/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 15.4)
- TITRE : Erreur DVP_1 dans STAT_NON_LINE avec PRE_EPSI
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Je tombe sur un  
  | 
  | <EXCEPTION> <DVP_1>                                                                            ║
  |  ║                                                                                                ║
  |  ║ Erreur de programmation.                                                                       ║
  |  ║                                                                                                ║
  |  ║ Condition non respectée:                                                                       ║
  |  ║     .false.                                                                                    ║
  |  ║ Fichier /fscronos/home/G79848/dev/codeaster-v15/src/bibfor/calcul/exchml.F90, ligne 110      
  | 
  | lors de l'utilisation d'un champ de déformation initiale (EPSINI, de type ELNO) via un chargement de type PRE_EPSI.
  | 
  | Visiblement, le champ EPSINI que je donne à manger à PRE_EPSI ne plait pas à STAT_NON_LINE.
  | Pourtant, lors de la création de ce champ, les impressions en INFO=2 ont l'air correctes. 
  | Mais bizarrement, je n'arrive pas à le visualiser dans Paravis.
  | 
  | Le champ EPSINI_N de type NOEU préalablement calculé est lui ok dans Paravis.
  | 
  | Bref, quel est le problème du champ EPSINI ?
  | 
  | 
  | Correction/Développement
  | ------------------------
  | Bon, la doc de PRE_EPSI dit clairement qu'il faut soit une carte soit un champ ELGA.
  | 
  | Je rajoute un message d'erreur dans affe_char_meca.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test fourni
- DEJA RESTITUE DANS : 16.0.18
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31640 DU 13/12/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Temps du test miss10c
- FONCTIONNALITE :
  | miss10a, b, c sont identiques, seul le nombre de threads diffère, respectivement, 1, 6 ou 24.
  | 
  | En default (16.0.19) :
  | 
  | flash/miss10a.o16604880: * CALC_MISS                :    8115.43 :      23.98 :    8139.41 :    8173.98 *
  | flash/miss10a.o16604880: * STAT_NON_LINE            :     304.09 :       4.53 :     308.62 :     309.68 *
  | flash/miss10b.o16605599: * CALC_MISS                :    8213.17 :      15.73 :    8228.90 :    1525.91 *
  | flash/miss10b.o16605599: * STAT_NON_LINE            :     387.61 :       8.06 :     395.67 :     286.78 *
  | flash/miss10c.o16607825: * CALC_MISS                :   11158.93 :      89.77 :   11248.70 :     566.97 *
  | flash/miss10c.o16607825: * STAT_NON_LINE            :   25047.11 :      40.65 :   25087.76 :    1457.21 *
  | 
  | En v15 (15.4.12) :
  | 
  | flash/miss10a.o16606240: * CALC_MISS                :    8033.76 :      14.72 :    8048.48 :    8081.24 *
  | flash/miss10a.o16606240: * STAT_NON_LINE            :     338.00 :       4.56 :     342.56 :     343.83 *
  | flash/miss10b.o16605896: * CALC_MISS                :    8190.04 :      15.50 :    8205.54 :    1521.35 *
  | flash/miss10b.o16605896: * STAT_NON_LINE            :     476.74 :       7.77 :     484.51 :     303.49 *
  | 
  | Pour le c :
  | - MODE_STATIQUE x4 en v15 avec <A> FACTOR_9 (détection de singularité)
  | - MACR_ELEM_DYNA suivants x2 à x4 (mais restent petits)
  | - CALC_MISS identique
  | 
  | 
  | On peut se poser la question : est-ce que le temps elapsed de STAT_NON_LINE largement supérieur pour 'c' n'est pas lié aux autres jobs pouvant tourner sur le
  | noeud ?.
  | On ne peut pas se permettre de lancer tous les tests avec --exclusive.
  | Est-ce que la réservation de ressources ne fonctionne pas bien au delà de quelques threads ?
  | 
  | Quick Fix : Supprimer miss10c en v15 car le problème existe en default même si le test passe.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : suppression du test
- NB_JOURS_TRAV  : 0.5

