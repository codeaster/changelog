==================================================================
Version 15.6.10 (révision 55c7848c82f3) du 2022-12-12 16:46 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32444 ABBAS Mickael            Usage de THER_LINEAIRE avec EVOL_CHAR
 32423 COURTOIS Mathieu         Remplissage repertoire de travail /tmp ou /SCRATCHDIR - ouverture base
 32309 LE CREN Matthieu         [RUPT] Comportement étrange de Gs sur 2 maillages différents
 32126 LE CREN Matthieu         CALC_G et VMIS_ISOT_NL
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32444 DU 30/11/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Usage de THER_LINEAIRE avec EVOL_CHAR
- FONCTIONNALITE :
  | Demande :
  | =======
  | 
  | Interdire les chargements EVOL_CHAR avec THER_LINEAIRE.
  | 
  | 
  | Modifications :
  | =============
  | 
  | Dans ntdoch, on récupère le nom de la commande et on émet un message d'erreur si présence d'un chargement EVOL_CHAR avec THER_LINEAIRE :
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <F> <CHARGES_11>                                                                               ║
  |  ║                                                                                                ║
  |  ║ Les chargements de type EVOL_CHAR sont interdits avec THER_LINEAIRE.                           ║
  |  ║                                                                                                ║
  |  ║ Conseil : il faut effectuer le calcul avec THER_NON_LINE.                                      ║
  |  ║                                                                                                ║
  |  ║                                                                                                ║
  |  ║ Cette erreur est fatale. Le code s'arrête.                                                     ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | On vérifie que le message est émis au lancement de zzzz137b.
  | Puis on remplace THER_LINEAIRE par THER_NON_LINE dans ce test.
  | 
  | RESU_FAUX
  | =========
  | 
  | Quand on utilise des chargements de type ECHANGE dans EVOL_CHAR pour THER_LINEAIRE (correspond aux champs 'COEF_H' et 'T_EXT' dans la SD résultat utilisée dans
  | AFFE_CHAR_THER/EVOL_CHAR), les résultats seront faux.

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 12.3.17
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz137b


================================================================================
                RESTITUTION FICHE 32423 DU 23/11/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Remplissage repertoire de travail /tmp ou /SCRATCHDIR - ouverture base
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Échec à la relecture d'une base avec le message :
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <EXCEPTION> <JEVEUX_40> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  |  ║ . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  |  ║ Erreur écriture de l'enregistrement 159054 sur la base : GLOBALE 10 . . . . . . . . . . . . . .║
  |  ║ . . .code retour : -4 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  |  ║ . . .Erreur probablement provoquée par une taille trop faible du répertoire de travail. . . . .║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | 
  | Correction
  | ----------
  | 
  | L'erreur semble venir du passage de --max_base lors de la POURSUITE.
  | En effet, c'est curieux que Jeveux essaie d'écrire quelque chose dans la glob.10 alors qu'on doit
  | relire 11 fichiers composant la base.
  | Il ne peut pas écrire car ce fichier est déjà plein.
  | Si on supprime l'argument --max_base, la relecture fonctionne (avec les différentes versions
  | testées : 15.4, 16.2, unstable).
  | 
  | On corrige en ignorant l'argument --max_base en POURSUITE.
  | Avant POURSUITE, on aura l'avertissement suivant :
  |    [1,2]<stdout>:WARNING: '--max_base' is ignored with POURSUITE.
  | 
  | Cela me semble compliqué (risqué) d'autoriser l'augmentation de max_base d'une exécution à l'autre
  | car il y a plusieurs paramètres stockés redondants : taille d'un fichier, taille d'un enregistrement,
  | nombre d'enregistrement.
  | 
  | On décide de mettre une valeur très grande par défaut de manière à ne pas devoir utiliser --max_base (2 To).
  | >>> --max_base ne devrait pas être utilisé dorénavant <<<
  | 
  | En POURSUITE, si la base précédente a été créée avec max_base différent de la valeur par défaut,
  | le nombre d'enregistrement (la longueur d'un enregistrement ne change pas en général) est stocké
  | dans la base. On peut donc recalculer la valeur de max_base initialement utilisée.
  | Si elle est différente, on verra le message :
  | 
  | Ajustement de la taille maximale des bases à 0.10 Go.
  | (cas-test zzzz103c où on force une taille très petite dans le .comm initial)
  | 
  | Autre problème levé par hasard : test qui lève une exception, avant que l'exception soit récupérée,
  | changements en mémoire, jeveux qui émet un message d'information.
  | Cela conduit à réinitialiser l'objet qui stocke les informations sur l'exception.
  | Il ne faut pas le faire tant que le premier appel n'est pas clos (en cas d'appel récursif).
  | En cas de message d'erreur, on s'arrêtera avec le message CATAMESS_55 (appels récursifs de messages d'erreur).
  | 
  | 
  | 
  | Report v15.
  | 
  | 
  | Remarque sur l'accès /scratch depuis les deux noeuds sur lesquels ont été répartis les 6 process :
  | 1200 s pour POURSUITE/FIN pour les 3 procs du 1er noeud, 2200 s sur les 3 procs du 2nd. Une seule fois
  | sur plusieurs runs mais ça peut arriver.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz103c
- DEJA RESTITUE DANS : 16.2.18
- NB_JOURS_TRAV  : 1.5


================================================================================
                RESTITUTION FICHE 32309 DU 05/10/2022
================================================================================
- AUTEUR : LE CREN Matthieu
- TYPE : anomalie concernant code_aster (VERSION 16.2)
- TITRE : [RUPT] Comportement étrange de Gs sur 2 maillages différents
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | En version unstable (16.2.6), le comportement de G le long du front de fissure est très différent sur 
  | deux maillages de la même fissure : sur un maillage "FIN",
  | on constate des oscillations relativement importantes de G, contrairement aux résultats sur un 
  | maillage "GROSSIER", où le G est nickel.
  | 
  | Analyse :
  | =======
  | Dans le maillage FIN, le groupe de noeuds FRONT0 n'est constitué que des noeuds sommets, les noeuds de milieu de segment sont absents.
  | La commande MODI_MAILLAGE/NOEUD_QUART/GROUP_NO_FOND s'en trouve affectée. Cette commande agit en deux temps :
  | 1- elle déplace les noeuds milieu présents sur les arêtes issues des noeuds sommets présents dans GROUP_NO_FOND.
  | 2- elle remet ensuite à la position initiale les noeuds milieu présent dans GROUP_NO_FOND
  | Si ces noeuds sont absents de la liste fournie, l'étape 2 ne peut pas être faite et ces noeuds milieu sont déplacés alors qu'ils ne devraient pas.
  | 
  | Correction :
  | ==========
  | 
  | On vérifie que : Nombre de nœuds milieux fournis = nombre de nœuds sommets fournis - 1
  | Si cette condition n'est pas vérifiée, on sort en erreur avec le message suivant :
  | 
  |     73 : _("""
  | Erreur utilisateur :
  |   Dans la liste de noeuds fournie (MODI_MAILLAGE / MODI_MAILLE / OPTION='NOEUD_QUART')
  |   pour définir le fond de fissure, il manque %(i1)d noeud(s) milieu.
  |   Ces noeuds sont nécessaires au bon fonctionnement de cette commande.
  |  """),
  | 
  | Validation :
  | ----------
  | Vérification de la bonne émission du message sur le test fourni.
  | Lancement de tous les tests de src utilisant NOEUD_QUART (pour vérifier qu'on ne casse rien).
  | 
  | 
  | RQ :
  | On a remarqué que l'utilisation de GROUP_MA_FOND et de GROUP_NO_FOND ne produit pas le même maillage.
  | Cela est expliqué dans un précédent message. Voir si une fiche doit être ouverte.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test fourni


================================================================================
                RESTITUTION FICHE 32126 DU 08/06/2022
================================================================================
- AUTEUR : LE CREN Matthieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : CALC_G et VMIS_ISOT_NL
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | L'utilisation de CALC_G avec VMIS_ISOT_NL produit un plantage du code.
  | 
  | Analyse :
  | =======
  | 
  | J'ai modifié le test ssnp312b pour qu'il appelle CALC_G avec VMIS_ISOT_NL.
  | On tombe alors sur un ASSERT dans le routine nmelnl, car on utilise une loi différente de ELAS, ELAS_VMIS_TRAC, ELAS_VMIS_LINE ou ELAS_VMIS_PUIS.
  | 
  | Une première chose à faire serait de remplacer l'ASSERT par un message d'erreur.
  | 
  | En regardant les sources plus en profondeur, j'ai vu qu'il y avait un traitement fait dans cgCreateCompIncr pour changer les lois
  | VMIS_ISOT_XXXX en ELAS_VMIS_XXXX. Pour VMIS_ISOT_NL, cette routine ferait donc passer à la loi ELAS_VMIS_NL ... qui n'existe pas.
  | J'ai donc modifié les choses pour exclure VMIS_ISOT_NL de ce traitement (et adapter le message d'erreur).
  | Mais je tombe toujours sur le même ASSERT.
  | 
  | En fait, cette routine cgCreateCompIncr est appelée par cgReadCompor uniquement si le drapeau l_incr est levé (pour parler comme Mickaël!).
  | La valeur de ce drapeau est fonction du retour de l'appel à dismoi('ELAS_INCR', compor, 'CARTE_COMPOR', repk=repk).
  | Je suis aller voir dans vmis_isot_nl.py et l'attribut propriete est à COMP_ELAS (quand les autres VMIS_ISOT_XXXX ont None).
  | Le dismoi ressort donc avec la valeur ELAS et l_incr reste à .false.
  | 
  | Si je le mets à None ou à COMP_INCR, avec les modifications apportées, le calcul fonctionne, mais Eric précise que ça doit rester à COMP_ELAS. 
  | 
  | Correction :
  | ==========
  | 
  | On modifie le traitement effectué dans cgCreateCompIncr en excluant VMIS_ISOT_NL.
  | On modifie cgReadCompor pour qu'il appelle cgCreateCompIncr systématiquement (sans appel préalable à dismoi('ELAS_INCR'))
  | On modifie le message d'erreur RUPTURE3_8 :
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <F> <RUPTURE3_8>                                                                               ║
  |  ║                                                                                                ║
  |  ║ La relation suivante n'est pas supportée: VMIS_ISOT_NL. Uniquement les lois basées sur         ║
  |  ║ VMIS_ISOT_*** sont prévues pour un calcul élastoplastique,                                     ║
  |  ║ à l'exception de VMIS_ISOT_NL.                                                                 ║
  |  ║                                                                                                ║
  |  ║                                                                                                ║
  |  ║ Cette erreur est fatale. Le code s'arrête.                                                     ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnp312b modifié

