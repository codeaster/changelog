==================================================================
Version 15.0.19 (révision 4ff11dd8f919) du 2019-12-10 12:04 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 28575 COURTOIS Mathieu         Les lois MFront ne sont pas recompilées après modification
 29349 PIGNET Nicolas           DEFI_GROUP: comportements différents en fonction de l'option
 29049 GEOFFROY Dominique       En version 15.0.3, le cas test sdls109b est en <F>_ERROR sur Gaia
 29058 GEOFFROY Dominique       En version 15.0.3, les cas tests fdlv112f, sdnx100a, sdnx100b et sdnx100e son...
 29048 GEOFFROY Dominique       En version 15.0.3, le cas test sdll123d est NOOK sur Gaia
 28728 COURTOIS Mathieu         En version 14.3.6, le cas test sdnl136a est NOOK sur Eole_Valid
 29347 LEFEBVRE Jean-Pierre     tests de validation miss10a/b/c sur Gaia
 29297 GEOFFROY Dominique       En version 15.0.14 le cas-test perf013g est en NO_RESU_FILE sur GAIA (mpi)
 28784 BÉREUX Natacha           Nombre d'itérations solveur GCPC
 29372 GEOFFROY Dominique       [AOM] - Analyse au flambage d'un réservoir sous pression externe
 29227 TARDIEU Nicolas          Bug dans la numérotation des simples Lagrange
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 28575 DU 21/02/2019
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : Les lois MFront ne sont pas recompilées après modification
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Il semblerait que les fichiers .mfront ne soient plus compilés lors du ./waf install car toutes les modifications
  | réalisées dans le fichier BETON_BURGER.mfront ne sont pas prises en compte après la compilation.
  | 
  | La solution de contournement qui consiste à supprimer le dossier mfront dans build/std/release fonctionne.
  | 
  | 
  | Correction
  | ----------
  | 
  | Dans les scripts de construction, on dit :
  | - Construire la bibliothèque libAsterMFrOfficial.so à partir des fichiers `.mfront`.
  | - À partir d'un fichier `.mfront`, on produit des fichiers `.cxx` en appelant l'exécutable 'mfront'.
  | - `.cxx` en `.o`, puis `.so`, on sait faire.
  | 
  | Or quand on modifie un `.mfront` (au moins certaines modifications courantes), les fichiers `.cxx`
  | ne sont pas modifiés. Ou par exemple, en retirant un 'include' fait deux fois, le `.cxx` est modifié
  | mais par le `.o`.
  | Si le `.o` ne change pas (waf vérifie que le contenu n'a pas changé, pas la date), a fortiori si le
  | `.cxx` ne change pas, il n'y a pas de raison de reconstruire la bibliothèque.
  | 
  | En fait, 'mfront' crée 2 fichiers `.cxx` et 4 fichiers `.hxx`. Ce sont ces derniers qui changent lors
  | des modifications courantes.
  | Il faut donc les déclarer comme 'outputs' pour que ce sont une dépendance et ainsi la bibliothèque
  | est bien reconstruite à chaque modification du `.mfront`.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : build
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29349 DU 03/12/2019
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : DEFI_GROUP: comportements différents en fonction de l'option
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Dans le fichier joins, je souhaite créer un group_ma (DEFI_GROUP) à partir d'une liste de noeud.
  | 
  | Si je mets OPTION='APPUI', TYPE_APPUI='SOMMET'. J'ai une alarme disant qu'il ne créait pas le groupe car il est vide
  | 
  |    !----------------------------------------------------------------!
  |    ! <A> <SOUSTRUC_36>                                              !
  |    !                                                                !
  |    !  le GROUP_MA : Arete_Symm_Y est vide. on ne le crée pas.       !
  |    !                                                                !
  |    !                                                                !
  |    ! Ceci est une alarme. Si vous ne comprenez pas le sens de cette !
  |    ! alarme, vous pouvez obtenir des résultats inattendus !         !
  |    !----------------------------------------------------------------!
  | 
  | 
  | Si je mets OPTION='APPUI', TYPE_APPUI='AU_MOINS_UN'. J'ai une exception disant qu'il ne créait pas le groupe car il est vide et
  | s'arrête.
  | 
  |    !--------------------------------------------------------------------------!
  |    ! <EXCEPTION> <SOUSTRUC2_7>                                                !
  |    !                                                                          !
  |    !  -> Le groupe de mailles Arete_Symm_Y est vide. On ne le crée donc pas ! !
  |    !  -> Risque & Conseil:                                                    !
  |    !     Veuillez vous assurer que le type de mailles souhaité soit cohérent  !
  |    !     avec votre maillage.                                                 !
  |    !--------------------------------------------------------------------------!
  | 
  | Cette différence de comportement est embêtante. Je propose d'émettre seulement une alarme dans les deux cas.
  | 
  | Je peux m'en charger
  | 
  | Correction
  | ------------------------
  | 
  | On remplace l’erreur fatale par l’émission de l’alarme
  | 
  |    !----------------------------------------------------------------!
  |    ! <A> <SOUSTRUC_36>                                              !
  |    !                                                                !
  |    !  le GROUP_MA : Arete_Symm_Y est vide. on ne le crée pas.       !
  |    !                                                                !
  |    !                                                                !
  |    ! Ceci est une alarme. Si vous ne comprenez pas le sens de cette !
  |    ! alarme, vous pouvez obtenir des résultats inattendus !         !
  |    !----------------------------------------------------------------!
  | 
  | 
  | Résultat faux
  | -------------
  | RAS

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test joins
- DEJA RESTITUE DANS : 14.4.14
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 29049 DU 20/08/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 15.0.3, le cas test sdls109b est en <F>_ERROR sur Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.0.3, le cas test sdls109b est en <F>_ERROR sur Gaia
  | La commande en jeu est CALC_MODES, le mode à 530.014469Hz a une norme d'erreur très importante.
  | 
  | !--------------------------------------------------------------------------------------------------------------------------!
  |    ! <E> <ALGELINE5_15>                                                                                                       !
  |    !                                                                                                                          !
  |    !    pour le concept  MODE_DKT, le mode numÃ©ro  10                                                                         !
  |    !                                                                                                                          !
  |    !   de frÃ©quence  530.014469                                                                                               !
  |    !                                                                                                                          !
  |    !   a une norme d'erreur de  0.998329  supÃ©rieure au seuil admis  0.000001.                                                !
  |    !                                                                                                                          !
  |    ! Conseils :                                                                                                               !
  |    ! Si vous utilisez METHODE='SORENSEN' ou 'TRI_DIAG' ou 'JACOBI', vous pouvez amÃ©liorer cette norme :                       !
  |    !  - Si la dimension de l'espace rÃ©duit est infÃ©rieure Ã  (nombre de degrÃ©s de libertÃ© actifs - 2), augmenter la valeur de  !
  |    !    COEF_DIM_ESPACE (la valeur par dÃ©faut est 4 pour 'TRI_DIAG' et 2 pour 'SORENSEN' et 'JACOBI').                        !
  |    !  - DÃ©couper le calcul en plusieurs appels de maniÃ¨re Ã  rÃ©duire le nombre de modes propres recherchÃ©s simultanÃ©ment       !
  |    !    (NMAX_FREQ ou taille de la BANDE).                                                                                    !
  |    !                                                                                                                          !
  |    !                                                                                                                          !
  |    ! Cette erreur sera suivie d'une erreur fatale.                                                                            !
  |    !--------------------------------------------------------------------------------------------------------------------------!
  | 
  | 
  | Correction
  | ----------
  | 
  | On suit la recommandation d'augmenter le paramètre COEF_DIM_ESPACE. En passant de 4 (par defaut) à 5, le test passe
  | sur Gaia ainsi que sur les autres machines.
  | 
  | On ajoute une remarque dans le .comm faisant référence à cette fiche.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test OK
- DEJA RESTITUE DANS : 14.4.14


================================================================================
                RESTITUTION FICHE 29058 DU 20/08/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 15.0.3, les cas tests fdlv112f, sdnx100a, sdnx100b et sdnx100e sont  CPU_LIMIT sur Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.0.3, les cas tests fdlv112f, sdnx100a, sdnx100b et sdnx100e sont  CPU_LIMIT sur Gaia
  | 
  | Analyse :
  | -------
  | 
  | On remarque que ce problème n'est pas présent avec lancement interactif. En regardant les temps de calcul
  | il apparait que plusieurs threads sont utilisés en interactif (par la commande CALC_MISS) alors qu'un seul
  | est demandé dans le .export. Ce qui n'est pas le cas en batch.
  | 
  | En imposant plusieurs threads (ncpus) dans l'export, les calculs en batch sont OK.
  | 
  | Le problème est également présent sur eole.
  | 
  | On en conclut que l'appel à Miss3D via CALC_MISS en interactif ne tient pas compte du paramètre ncpus.
  | 
  | 
  | Correction
  | ----------
  | 
  | Pour fdlv112f, on passe à ncpus 16 et on double la mémoire (pour gaia).
  | Pour sdnx100a, b et e on passe à 8 ncpus.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : fdlv112f, sdnx100a,b,e
- DEJA RESTITUE DANS : 14.4.14


================================================================================
                RESTITUTION FICHE 29048 DU 20/08/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 15.0.3, le cas test sdll123d est NOOK sur Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | En version 15.0.3, le cas test sdll123d est NOOK sur Gaia. 
  | 
  | 
  | 
  | Correction
  | ----------
  | Sur les conseils d'Olivier et de Jean-Pierre, on joue avec le paramètre SEUIL_FREQ qui veut 1E-2 par défaut.
  | La valeur la plus petite pour laquelle le test est OK sur Gaia est 1.8E-2.
  | 
  | On met donc SEUIL_FREQ à cette valeur après avoir vérifié que le calcul est OK sur les autres machines.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet


================================================================================
                RESTITUTION FICHE 28728 DU 03/04/2019
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : En version 14.3.6, le cas test sdnl136a est NOOK sur Eole_Valid
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le test de validation sdnl136a est NOOK sur Eole et Gaia.
  | 
  | 
  | Analyse
  | -------
  | 
  | C'est le dernier test sur la table issue de POST_USURE qui est NOOK.
  | Ce sont des tests de validation donc exécutés uniquement sur Eole et Gaia mais on fait le même
  | diagnostic sur Calibre9 ou Scibian9.
  | 
  | La partie aléatoire de GENE_FONC_ALEA n'est pas en cause : on peut faire un IMPR_CO de l'interspectre
  | et constater que l'écart est sur la dernière décimale affichée.
  | 
  | En mode `dbg=.true.` dans `calcul.F90`, on constate qu'on a exactement les mêmes valeurs issues du
  | calcul des matrices élémentaires.
  | 
  | La norme d'erreur du calcul modal est de 2.e-10 ou 3.e-10.
  | 
  | Sur les autres TEST_RESU, les résultats sont identiques sur 9 décimales ou plus.
  | 
  | 
  | Historique de la valeur testée (il y a peut-être eu des modifications de la modélisation):
  | 
  | 6c855f9824e9 (2012-10):
  | -           VALE_CALC=0.067432544,
  | +           VALE_CALC=0.084524209860779,
  | 
  | 69f76fb03163 (2013-11):
  | -           VALE_CALC=0.084524209860779,
  | +           VALE_CALC=0.083150035040987,
  | 
  | 3b0df1400e07 (2014-02):
  | -           VALE_CALC=0.083150035040987,
  | +           VALE_CALC=0.0831507243987,
  | 
  | 027f507ae0e1 (2014-05):
  | -           VALE_CALC=0.0831507243987,
  | +           VALE_CALC=0.0831703998736,
  | 
  | c2d8c28b7226 (2015-10):
  | -           VALE_CALC=0.0831703998736,
  | +           VALE_CALC=0.000523579138009,
  | 
  | c60cb3af9ebe (2016-08):
  | -           VALE_REFE=0.000523579138009,
  | +           VALE_REFE=0.00012422445471,
  | 
  | On obtient aujourd'hui 8.13719e-4 sur Eole, 7.81307e-4 sur Calibre.
  | 
  | 
  | De plus, il y a beaucoup d'alarmes dans ce test, notamment dans PROJ_SPEC_BASE assez inquiétante.
  | Comme cela a été fait dans sdnl112c (en 6.2.7), on ne teste plus la table d'usure.
  | POST_USURE reste vérifiée dans sdnv101a.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdnl136a


================================================================================
                RESTITUTION FICHE 29347 DU 03/12/2019
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : tests de validation miss10a/b/c sur Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | Les tests de validation miss10a/b/c demandent trop de mémoire pour être soumis sur Gaia. Si on fait passer la ressource mémoire de
  | 500 à 300 Go les tests sont arrêtés dans Miss lors de l'allocation du tableau de travail. Lorsque l'on réduit la taille du tableau
  | alloué en jouant sur la valeur de la variable coefs définie dans src/premiss/input.F90, les tests finissent en NOOK. 
  | 
  | Correction
  | ----------
  | En supprimant l'option -xCORE-AVX-I à la compilation et à l'édition des liens, l'exécutable Miss produit avec la suite Intel2019 sur
  | Gaia permet de faire fonctionner correctement les tests miss10a/b/c. Il y a une différence de temps (elaps) d'exécution d'environ +14%
  | 
  | Avec l'option -xCORE-AVX-I et le résultat NOOK : No : 0055   user+syst:     7203.17s (syst:      180.61s, elaps:      365.36s
  | Sans l'option -xCORE-AVX-I et le résultat OK   : No : 0055   user+syst:     8137.54s (syst:      206.27s, elaps:      416.44s
  | 
  | On supprime l'option -xCORE-AVX-I dans le fichier Makefile.inc du dépôt miss3D aster-prerequisites et on modifie
  | src/premiss/input.F90 pour affecter coefs = 3.0.
  | L'usage de l'option -march=skylake pour la construction de Miss conduit aux mêmes resultats NOOK.
  | 
  | On ajuste la mémoire des tests miss10a/b/c à 300 Go au lieu de 500 Go.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : miss10a/b/c
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 29297 DU 13/11/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 15.0.14 le cas-test perf013g est en NO_RESU_FILE sur GAIA (mpi)
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.0.14 le cas-test perf013g est en NO_RESU_FILE sur GAIA (mpi)
  | 
  | Analyse
  | -------
  | 
  | On s'aperçoit que ce test ne se termine pas toujours de la même manière lors de lancements consécutifs.
  | Il est parfois OK, parfois ALARM et parfois ERROR.
  | 
  | 
  | Correction
  | ----------
  | 
  | En passant le paramètre PCENT_PIVOT à 25 (au lieu de 20 par défaut), on constate que le test ce termine toujours OK ou ALARM,
  | après de nombreux lancements.
  | On effectue donc cette modification.
  | 
  | D'autre part, on a constaté que perf013b était identique à perf013a et que perf013d et perf013e sont identiques à perf013c.
  | Selon la doc la modélisation B est identique à la modélisation A mais sur 4 processeurs, ce qui correspond en fait à perf013g.
  | De même selon la doc, les modélisations D et E sont identiques à la modélisation C mais avec respectivement 4 et 16 processeurs,
  | ce qui correspond justement à perf013h et perf013i.
  | 
  | Il me semble que ces duplications aient été faites automatiquement par issue18626.
  | 
  | Finalement, il suffit de faire les modifications suivants :
  | perf013g.export => perf013b.export
  | perf013h.export => perf013d.export
  | perf013i.export => perf013c.export
  | 
  | Avec ça on colle parfaitement à la doc.
  | 
  | Enfin pour le temps max de perf013g (devenu b), je passe à 500 au lieu de 1000 et non 300 comme je proposais car le calcul
  | est plus long sur eole (jusqu'à 7mn selon les tests que j'ai faits).
  | 
  | Pas d'impact doc.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : passage du test


================================================================================
                RESTITUTION FICHE 28784 DU 18/04/2019
================================================================================
- AUTEUR : BÉREUX Natacha
- TYPE : anomalie concernant Documentation (VERSION 15.2)
- TITRE : Nombre d'itérations solveur GCPC
- FONCTIONNALITE :
  | Correction de la doc du mot clé solveur 
  | =======================================
  | NMAX_ITER=0 par défaut.
  | Si l'utilisateur impose une valeur pour le nombre maximal d'itérations, c'est elle qui est utilisée. 
  | Si l'utilisateur n'impose pas de valeur, une valeur est déterminée par aster. Cette valeur dépend du solveur et du préconditionneur
  | choisis dans le mot-clé facteur SOLVEUR:
  | - si  METHODE='PETSC', 
  |      si PRE_COND='LDLT_SP' alors
  |         le nombre maximal d'itérations est fixé à 100.
  |      sinon 
  |         le nombre maximal d'itérations est fixé à 10000. C'est la valeur par défaut imposée par PETSc.
  | - si METHODE='GCPC'
  |      le nombre maximal d'itérations est fixé à nequ/2.
  | 
  | Je corrige la doc u4.50.01  pour qu'elle soit cohérente avec ces choix.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : u4.50.01
- VALIDATION : non
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29372 DU 06/12/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : aide utilisation concernant code_aster (VERSION 13.6)
- TITRE : [AOM] - Analyse au flambage d'un réservoir sous pression externe
- FONCTIONNALITE :
  | Probleme
  | ------------
  | 
  | L'utilisateur souhaite avoir un appui concernant la mise en place d'une analyse de 
  | flambement
  | 
  | 
  | Solution
  | --------------
  | 
  | Georges et moi-même avons vérifié le fichier de commande avec le soutien de Jean-Luc. 
  | Je note que ce n'est pas clair dans la doc, en fonction des mots-clés utilisés, si 
  | les valeurs propres sont signées. Il faudra émettre une fiche pour tout mettre au 
  | clair.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : utilisateur satisfait
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29227 DU 15/10/2019
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Bug dans la numérotation des simples Lagrange
- FONCTIONNALITE :
  | Problème :
  | ----------
  | 
  | Il y a une erreur dans la routine de numérotation des simples Lagrange nueffe_lag1.F90. Etonnement, il ne cause pas 
  | d'erreur mais il faut corriger.
  | 
  | Cette erreur a été introduite par issue28873, copié/collé malheureux de 'nueffe_lag2'.
  | Le tableau nume_ddl//'.DESCLAG' n'est dimensionné qu'à 2 fois le nombre de Lagrange. Donc aller lire à 3 fois le nombre
  | de Lagrange provoque un dépassement de tableau.
  | 
  | 
  | Solution :
  | ----------
  | 
  | diff -r 19e3ad5f4761 bibfor/assembla/nueffe_lag1.F90
  | --- a/bibfor/assembla/nueffe_lag1.F90	Wed May 15 15:16:59 2019 +0200
  | +++ b/bibfor/assembla/nueffe_lag1.F90	Tue Oct 15 11:24:16 2019 +0200
  | @@ -537,7 +537,7 @@
  |  
  |                              ilag = zi(inuno2+ili-1) + nunoel - 1
  |                              ilag = ilag - nb_node
  | -                            zi(iddlag+2* (ilag-1)+1) = -zi(iddlag+3* ( ilag-1)+1 )*nddlb
  | +                            zi(iddlag+2* (ilag-1)+1) = -zi(iddlag+2* ( ilag-1)+1 )*nddlb
  |                          endif
  |                      end do
  |                  endif

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Passage des tests
- NB_JOURS_TRAV  : 0.5

