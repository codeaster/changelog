==================================================================
Version 15.4.10 (révision 2cbab7cfb74a) du 2021-09-16 15:28 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31384 DEVESA Georges           AMOR_MECA pour éléménts joint 2D
 31290 PIGNET Nicolas           En version 16.0.2 et 15.4.2, le cas-test de validation hsnv141a est en ABNORM...
 31404 PIGNET Nicolas           FieldOnNodes incomplet
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31384 DU 09/09/2021
================================================================================
- AUTEUR : DEVESA Georges
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : AMOR_MECA pour éléménts joint 2D
- FONCTIONNALITE :
  | Pb:
  | ---
  | La fiche issue29678 a introduit la possibilité de calculer l'option AMOR_MECA pour les éléments joint 2D (MFPLQU4 et EJHYME_PLQU8). Néanmoins, on observe des
  | soucis avec l'usage de cette option :
  | - d'une part, il faut compléter l'ajout de l'option aussi aux éléments MFPLQU8
  | - d'autre part, en modifiant le cas-test restitué dans issue29678 (sdnv138) en quadratique (que ce soit EJHYME_PLQU8 ou MFPLQU8) on tombe sur un fpe en début de
  | calcul dynamique dans l'option FULL_MECA. En passant par le mode dbg de calcul, on voit que le calcul de l'option AMOR_MECA pose déjà des problèmes.
  | Si on enlève l'option AMOR_MECA, le fpe disparaît.
  | 
  | Réponse:
  | -------
  | En fait le bug vient du calcul de la longueur à partir des coordonnées dans l'élément en quadratique. On cherche les coordonnées des noeuds 1 à 4. Cela marche
  | en linéaire mais en quadratique avec plusieurs classes de noeuds (cf. catalogue) : (1,2,5), puis (4,3), etc. dans l'ordre et donc pour le troisième noeud on va
  | chercher en fait les coordonnées du noeud 5 intermédiaire entre les noeuds 1 et 2, c'est donc faux même si ça ne donne pas toujours de NaN.
  | En fait en 2D, il suffit dans la routine te0214 de déterminer la longueur à partir des deux sommets de la lèvre supérieure, soit les noeuds 1 et 2 comme en 3D
  | on détermine la surface à partir des 3 ou 4 noeuds sommets de la lèvre supérieure. 
  | Cela ne provoque plus de bug, la longueur est bien calculée et ça ne change pas les valeurs testées de sdnv138b en linéaire.
  | Pour l'élément mécanique quadratique meca_plan_joint_qu8 on ajoute l'option AMOR_MECA dans son catalogue.
  | Pour tester, on ajoute les modélisations du test SDNV138 avec amortissement pour les éléments 2D joints quadratiques : C pour PLAN_JOINT et D pour
  | PLAN_JOINT_HYME.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V5.03.138
- VALIDATION : tests sdnv138
- DEJA RESTITUE DANS : 16.0.8
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 31290 DU 19/07/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : En version 16.0.2 et 15.4.2, le cas-test de validation hsnv141a est en ABNORMAL_ABORT
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | En version 16.0.2 (révision 8ffaa564517f) et 15.4.2 (révision 2f3a9cb012d1), le cas-test de validation hsnv141a est en ABNORMAL_ABORT sur gaia et cronos.
  | 
  | Un segmentation fault survient.
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Le problème se produit en poursuite uniquement car l'objet est mal rechargé (il manque un ligrel). Il faut obligatoirement le model pour recréer correctement
  | les ligrels après
  | 
  | Je rajoute lors du pickling la récupération du model pour qu'il soit rechargé correctement dans contact_ext.py. Je supprime également les constructeurs qui
  | n’appelle pas le modèle
  | 
  | Rq: Merci pour la version dégradé du maillage. 5mn à la place de quelques heures c'est mieux pour debugger

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : hsna141a
- DEJA RESTITUE DANS : 16.0.8
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 31404 DU 16/09/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : FieldOnNodes incomplet
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | POST_K_VARC: FieldOnNodes incomplet en sortie
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | En fait, on reconstruit mal en c++ des FieldOnNodes en sortie d'OP
  | 
  | Il manque souvent un build() avec l'ajout de la numérotation ou du maillage avant si on l'a
  | 
  | Voilà les op corrigés: calc_char_cine, calc_char_seisme, asse_vecteur, extr_table, lire_cham, post_k_varc, calc_cham_elem
  | 
  | Report en v15 à faire

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : submit
- DEJA RESTITUE DANS : 16.0.8
- NB_JOURS_TRAV  : 1.0

