==================================================================
Version 15.2.14 (révision 1b6d2f9e51e4) du 2020-10-14 17:37 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 30334 FLEJOU Jean Luc          Sortie SIEF_ELNO dans repère global
 30046 ABBAS Mickael            En version 15.2.1 - révision bba497ff2945 - le cas-test vérification (mpi) fd...
 30317 PIGNET Nicolas           Problème avec les familles vides (med)
 30243 PIGNET Nicolas           interdiction GCPC en parallèle massif
 30324 PIGNET Nicolas           Tests parallèles en erreur
 30323 BETTONTE Francesco       [ MAC3 ] Amplification des efforts TH
 30283 DEVESA Georges           modification d'un concept macro-élément dynamique à partir d'impédances
 30325 COURTOIS Mathieu         DETRUIRE ne détruit plus rien
 30331 COURTOIS Mathieu         Ajout des accès aux tables (sd_l_table) attachées à certaines SD
 30298 ALVES-FERNANDES Vinicius Permettre l'utilisation des VARC pour les éléménts ABSO
 28837 BETTONTE Francesco       MAC3 : erreur dans datg RFA1300
 30021 BETTONTE Francesco       En version 15.2.0  - révision b83a38921917 le cas-test validation mac3c08d es...
 30238 DROUET Guillaume         3M - ROM Evaluation du résidu en non-linéaire
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 30334 DU 09/10/2020
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Sortie SIEF_ELNO dans repère global
- FONCTIONNALITE :
  | Demande :
  | ---------
  | Dans le cadre de l'appui SND VD4 900, il y a le besoin de sortir des SIEF_ELNO dans le repère global.
  | Les éléments concernés sont les POU_D_T.
  | 
  | La solution doit être compatible avec COMB_SISM_MODAL et l'analyse spectrale.
  | 
  | 
  | Proposition : ==> Même méthode que dans issue30228.
  | -------------
  | issue30228, c'était pour EFGE_ELNO. Les composantes des SIEF_ELNO sont les mêmes que les EFGE_ELNO.
  | 
  | ==> Aucun impact sur les catalogues, pas de nouveau mot clef.
  | ==> Modification des blindages pour autoriser SIEF_ELNO
  | ==> Ajout de test sur les SIEF_ELNO dans zzzz124
  | ==> Modif documentations : MODI_REPERE, COMB_SISM_MODAL, cas test

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : u4.74.01, v1.01.124, u4.84.01
- VALIDATION : passage cas tests
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30046 DU 29/06/2020
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : En version 15.2.1 - révision bba497ff2945 - le cas-test vérification (mpi) fdlv112i est en ABNORMAL_ABORT
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.2.1 - révision bba497ff2945 - le cas-test vérification (mpi) fdlv112i est en ABNORMAL_ABORT
  | 
  | 
  | Correction
  | ----------
  | 
  | La routine rcvarc ne trouve pas la famille d'intégration 'FPG1' pour les éléments MEFL_PENTA15 alors qu'elle est bien définie 
  | dans le catalogue.
  | 
  | La routine getFluidPara a défini la famille d'intégration par un K16, ce qui est faux, c'est un K8. Mais rcvarc utilise la 
  | variable fami en character(len=*), c'est très imprudent !
  | En effet, la routine rcvarc est peu robuste: pour trouver l'index de la famille, elle utilise l'utilitaire indik8 qui est en C. 
  | Or, en C, on n'ignore pas les blancs à la fin des chaines, comme en Fortran !
  | 
  | Du coup, il compare "FPG1____________" à "FPG1____" (les underscores sont des blancs) et il ne trouve donc pas la famille.
  | 
  | Deux corrections:
  | - on modifie getFluidPara pour utiliser le bon format (K8)
  | - on modifie rcvarc pour tronquer la chaîne à K8 et ne plus utiliser une chaîne de longueur indéfinie
  | 
  | V14: reporter la correction de rcvarc (getFluidPara n'existe pas en V14)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : fdlv112i
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30317 DU 06/10/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : Problème avec les familles vides (med)
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Si le maillage au format med contient des familles sans éléments le code plante dans lire_maillage.
  | 
  | 
  | Correction
  | ------------------------
  | Cela peut se produire avec le découpeur parallèle. On crée toutes les familles globales en locales et on ajoute des éléments si besoin. Ca permet de gagner du temps mais on crée des familles vides.
  | 
  | Il y avait un bug dans lire_maillage quand toutes les familles sont vides (une seule dans mon cas).
  | 
  | Il suffit de ne pas essayer de créer des groupes si toutes les familles sont vides
  | 
  | Je rajoute le test "mesh002r" pour tester cela et que les groupes produits sont corrects
  | 
  | A reporter en v14 (le fortran mais pas le test).
  | 
  | Résultat faux
  | -------------
  | 
  | Non. Erreur jeveux (objet de longueur nulle)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : mesh002r
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30243 DU 11/09/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : interdiction GCPC en parallèle massif
- FONCTIONNALITE :
  | Problème
  | -----------------
  | Dans une étude de comparaison, on a trouvé que le solveur GCPC en parallèle massif donne des résultats faux. Apparemment il n'est pas compatible avec la parallélisation massive, mais pas d'interdiction, ni message d'alarme.
  | 
  | 
  | Correction
  | ------------------------
  | 
  | On interdit le solveur GCPC si la matrice assemblée est basée sur un ParallelMesh
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Oui depuis la 15.2 avec le solveur GCPC et un ParallelMesh

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 15.2
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : perso
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30324 DU 07/10/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : Tests parallèles en erreur
- FONCTIONNALITE :
  | Problème
  | -----------------
  | suite à issue29528, des tests parallèles sont en erreur. Il faut les corriger
  | 
  | xxFieldsplit001b
  | xxFieldsplit001d
  | xxParallelLinearTransientDynamics001a
  | xxParallelMechanicalLoad001a
  | xxParallelMechanicalLoad001l
  | xxParallelMechanicalLoad002b
  | xxParallelMechanicalLoad004b
  | xxParallelMechanicalLoad007a
  | xxParallelMesh002a
  | xxParallelNonlinearMechanics001a
  | xxParallelNonlinearMechanics002a
  | xxParallelNonlinearMechanics003a
  | xxParallelNonlinearMechanics004a 
  | 
  | 
  | Correction
  | ------------------------
  | 
  | J'ai oublié de mettre à jour la syntaxe avec l'API python quand le maillage est déjà partitionné.
  | 
  | Le changement de syntaxe corrige les tests

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxFieldsplit001b
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30323 DU 06/10/2020
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [ MAC3 ] Amplification des efforts TH
- FONCTIONNALITE :
  | Objectif
  | --------
  | Tester l'impact des efforts thermohydrauliques(TH) calculés par THYC sur les valeurs de lame d'eau par amplification des efforts.
  | 
  | 
  | Développement
  | -------------
  | Ajout de 2 arguments pour la commande CALC_MAC3COEUR dans l’opérateur LAME: COEF_MULT_THV et COEF_MULT_THT respectivement pour l'amplification des efforts verticaux et transversaux.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : u4.90.11
- VALIDATION : cas test
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30283 DU 25/09/2020
================================================================================
- AUTEUR : DEVESA Georges
- TYPE : evolution concernant code_aster (VERSION 15.3)
- TITRE : modification d'un concept macro-élément dynamique à partir d'impédances
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Dans l'utilisation de modèles avec super-mailles s'appuyant sur des macro-éléments dynamiques, on peut avoir à modifier le contenu de macro-éléments sans changer de nom de concept car sinon cela oblige à créer un tas de nouveaux concepts attachés : maillage, modèle, caractéristiques, matériau et champ matériau, charges diverses, numérotation et cela pourrait être très coûteux à refaire par exemple dans une boucle de résolution harmonique.
  | On ne veut changer que le contenu des objets VALE, ce qui est réalisé dans la macro PRE_SEISME_NONL par un appel à putvectjev. Mais on veut le faire proprement dans le source à la manière de la modification de DEFI_BASE_MODALE par RITZ/BASE_MODALE.
  | 
  | Action :
  | --------
  | Dans le catalogue de MACR_ELEM_DYNA, on ajoute le mot clé MACR_ELEM_DYNA avec le concept à modifier qu'on déclare réentrant par reuse. Les modifications sont actives à partir de nouvelles données d'impédances avec les mots-clés MATR_IMPE*.
  | Cela impacte le catalogue de commandes et les routines OP0081 et IMPE81.
  | Pour la validation, on utilise le test sdlx100g qui lance par ailleurs PRE_SEISME_NONL dans ses différentes phases. On y complète une solution statique obtenue avec une impédance statique à fréquence très faible par une autre solution statique obtenue avec une autre impédance temporelle, ce qui nécessite une charge de rééquilibrage supplémentaire F = DK.U et de changer deux fois du contenu de macro-élément constituant la super-maille du modèle. Cette opération correspond à la phase STAT_DYNA de PRE_SEISME_NONL et doit y être intégrée à la place de l'appel à putvectjev, ce qui fait l'objet de la fiche issue30300 traitée ultérieurement. On vérifie que les deux calculs statiques donnent les mêmes résultats en changeant le contenu du macro-élément avec aussi une charge de rééquilibrage.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.65.01
- VALIDATION : test sdnx100g
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 30325 DU 07/10/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : DETRUIRE ne détruit plus rien
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En chemin pour issue29917 et issue27406, on voit que la commande DETRUIRE ne détruit plus les objets.
  | 
  | 
  | Correction
  | ----------
  | 
  | Pour détruire un objet, il peut maintenant faire simplement "del mesh" par exemple.
  | C'est ce que fait DETRUIRE mais elle ne le fait pas dans le bon contexte !
  | Elle ne supprime que sa référence locale.
  | 
  | Il faut le faire dans le contexte de l'appelant de DETRUIRE (fichier de commandes, macro-commande ou autre).
  | 
  | On supprime la possibilité de détruire des objets en utilisant leur nom Jeveux :
  |  DETRUIRE(OBJET=_F(CHAINE="xyz"))
  | à remplacer par :
  |  DETRUIRE(CONCEPT=_F(NOM=xyz))
  | 
  | 
  | 
  | Validation
  | ----------
  | 
  | On crée un maillage.
  | On appelle DETRUIRE et on vérifie que l'objet Jeveux '._TCO' a bien été supprimé.
  | 
  | On crée un maillage et un modèle sur ce maillage.
  | On appelle DETRUIRE du maillage, on vérifie que rien n'a été détruit car référencé par le modèle.
  | On appelle DETRUIRE du modèlé et on vérifie que le modèle et le maillage ont été supprimés.
  | 
  | Idem avec maillage, modèle et champ de matériau.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test001b
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30331 DU 08/10/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Ajout des accès aux tables (sd_l_table) attachées à certaines SD
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les structures de données suivantes possèdent un sous-objet Jeveux sd_l_table :
  | sd_cabl_precont
  | sd_char_meca
  | sd_maillage
  | sd_melasflu
  | sd_modele
  | sd_resultat
  | sd_xfem
  | 
  | Cet objet définit une association "identificateur" : "sd_table".
  | Il n'est pas connu du C++. On n'y a pas accès et donc n'est pas géré lors des suppressions par exemple.
  | 
  | 
  | Développement
  | -------------
  | 
  | On définit un objet `ListOfTables` pour accéder aux 2 vecteurs jeveux concernés (LTNT et LTNS) et
  | qui définit un std::map stockant les pointeurs vers les objets `Table`.
  | 
  | Les objets cités ci-dessus doivent hériter de `ListOfTables` et leur méthode `update()` doit appeler
  | `update_tables` pour être complets.
  | Certains objets n'ont pas de méthode `update()`. Dans ce cas, on l'appelle dans la méthode qui remplit
  | le contenu : `build()` ou `readMeshFile()` pour le maillage par exemple.
  | On verra à l'usage s'il manque une méthode de "mise à jour" après modification...
  | 
  | Ces objets bénéficient d'une méthode `getTable(identifier)` pour récupérer une table.
  | 
  | Exemple : U0.getTable('PARA_CALC')
  | On le vérifie dans ssnp15g avec un `evol_noli` : on compare avec la table fournie après RECU_TABLE.
  | 
  | NB: `PrestressingCable` n'est pas modifié car semble assez différent de sd_cabl_precont. A revoir...
  | 
  | 
  | Objets modifiés (<méthode appelant `update_tables()`>) :
  | - GenericMechanicalLoad <build()>
  | - BaseMesh <readMeshFile()>
  | - FluidStructureModalBasis <objet vide>
  | - Model <build()>
  | - Result <update()>
  | - XfemCrack <build()>

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnp15g
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30298 DU 01/10/2020
================================================================================
- AUTEUR : ALVES-FERNANDES Vinicius
- TYPE : evolution concernant code_aster (VERSION 15.2)
- TITRE : Permettre l'utilisation des VARC pour les éléménts ABSO
- FONCTIONNALITE :
  | Évolution :
  | ---------------
  | 
  | Permettre l'utilisation des VARC avec les éléments ABSO (meca_d_plan_abso et meca_3d_abso). L'objectif est de pouvoir définir des matériaux (E,NU) selon un champ propriétés matériaux, comme pour les éléments isoparamétriques.
  | 
  | Les options de calcul concernées sont :
  | te5553 (D_PLAN),te569 (3D):
  | AMOR_MECA
  | FORC_NODA
  | FULL_MECA
  | RAPH_MECA
  | RIGI_MECA
  | RIGI_MECA_TANG
  | RIGI_MECA_ELAS
  | 
  | te498(D_PLAN), te499 (3D):
  | ONDE_PLAN
  | 
  | Cela concerne un usage avec DYNA_VIBRA en temporel et DYNA_NON_LINE. 
  | 
  | Le cas de CALC_VECT_ELEM sera traité dans issue30314.
  | 
  | Cas-test:
  | ----------
  | On modifie les cas-tests sdlv120a,b et sdlv121a,b en ajoutant une résolution dynamique avec matériau défini par AFFE_VARC.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdlv120a,b sdlv121a,b
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28837 DU 14/05/2019
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : MAC3 : erreur dans datg RFA1300
- FONCTIONNALITE :
  | Problème : dans le datg de l'assemblage RFA1300, il y a une erreur affectant la longueur du retreint des tubes guide
  | 
  | Correction : fiche sans suite, car résolue par issue30264

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30021 DU 22/06/2020
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : En version 15.2.0  - révision b83a38921917 le cas-test validation mac3c08d est en NOOK sur Eole
- FONCTIONNALITE :
  | Problème : En version 15.2.0  - révision b83a38921917 le cas-test validation mac3c08d est en NOOK sur Eole
  | 
  | Fiche sans suite, résolue par issue30264

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30238 DU 09/09/2020
================================================================================
- AUTEUR : DROUET Guillaume
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : 3M - ROM Evaluation du résidu en non-linéaire
- FONCTIONNALITE :
  | On classe sans suite doublon avec issue 30320

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans suite
- NB_JOURS_TRAV  : 0.1

