==================================================================
Version 15.4.14 (révision d4d501646e60) du 2021-11-22 16:33 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31365 ZAFATI Eliass            problème avec "FieldOnNodesComplex "  dans la version 15.4
 31535 YU Ting                  [FAUX] Les chargements EFFE_FOND sont ignorés
 31272 YU Ting                  Mots-clefs simultanément constants et fonctions dans les commandes
 31296 YU Ting                  AFFE_CHAR_MECA / LIAISON_MAIL /  ELIM_MULT
 31585 COURTOIS Mathieu         Le test "ssnl201s" est en erreur de non convergence sur cronos en version 16....
 31480 DROUET Guillaume         Le mot-clé SANS_GROUP_NO de DEFI_CONTACT n'est pas pris en compte
 31282 ZAFATI Eliass            restreindre les méthodes dot et norm à ASTERDOUBLE pour fieldOnNodes
 31496 PIGNET Nicolas           Il manque le maillage dans Result
 31583 COURTOIS Mathieu         Le test "sdls140b" est en ABNORMAL_ABORT sur cronos et gaia depuis la version...
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31365 DU 02/09/2021
================================================================================
- AUTEUR : ZAFATI Eliass
- TYPE : anomalie concernant code_aster (VERSION 15.4)
- TITRE : problème avec "FieldOnNodesComplex "  dans la version 15.4
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Après la création d'un champ complexe du déplacement avec CH=CREA_CHAMP(...), la méthode CH.EXTR_COMP(...) n'est pas disponible 
  | pour la version 15.4 du code_aster. Pourtant, la méthode EXTR_COMP est disponible pour les champs réels.
  | 
  | Ce problème est bloquant pour corriger les cas test cassés de l'outil MT lorsqu'on fait la stabilization de la version 15.4 pour 
  | remplacer V14
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Après analyse EXTR_COMP existe uniquement pour FieldOnNodesReal et pas pour  FieldOnNodesComplex. Il existe plusieurs manières pour le faire mais la plus
  | simple est d'utiliser (comme FieldOnNodesReal) la fonction 'aster_prepcompcham' dans aster_module.c qui à son tour utilise la routine  prcoch.F90. 
  | 
  | J'ajoute une vérification dans zzzz505a

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz505a
- DEJA RESTITUE DANS : 16.0.17
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31535 DU 29/10/2021
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [FAUX] Les chargements EFFE_FOND sont ignorés
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Lorsqu'on a un chargement de type EFFE_FOND, il est systématiquement ignoré 
  | si c'est le seul mot clef défini dans AFFE_CHAR_MECA.
  | 
  | En effet, pour détecter les types de charges dans la liste des chargements, 
  | on se repère par rapport à l'existence de la carte créée, c'est la routine
  | loadGetNeumannType qui s'en charge. Si la carte est repérée, on met à jour 
  | l'objet INFC dans la liste des charges. C'est cet objet qui est ensuite 
  | utilisé pour activer le calcul dans vechme.
  | 
  | S'il y a une autre charge de Neumann bien identifiée dans la commande, le drapeau 
  | est bien levé et permet à la routine de calcul des charges (vechme) d'activer
  | CALCUL correctement.
  | Sinon, on ne voit pas la charge et elle n'est jamais calculée.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Correction facile en V15: imposer dans le catalogue la présence simultanée de 
  | EFFE_FOND et PRES_REP :
  | PRESENT_PRESENT('EFFE_FOND','PRES_REP'), 
  | 
  | Je crée une autre fiche évolution (issue31574) pour pour modifier le repérage 
  | des cartes entre la liste des charges et le calcul, afin d'améliorer 
  | l'architecture dans AFFE_CHAR_MECA
  | 
  | Résultat faux
  | -------------
  | 
  | lorsqu'il y a uniquement EFFE_FOND (depuis toujours)
  | Mais il y a quasiment toujours PRES_REP avec (ce qui est logique, on prend
  | rarement en compte l'effet de fond d'une pression sans prendre la pression en même 
  | temps !)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test perso
- DEJA RESTITUE DANS : 16.0.18
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31272 DU 09/07/2021
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Mots-clefs simultanément constants et fonctions dans les commandes
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le forum nous a fait remonter un bug dans DEFI_MATERIAU
  | 
  | Dans AsterStudy, pour le paramètre ECRO_LINE_FO, il n'est pas possible de mettre 
  | une fonction sur 
  | les paramètres.
  | 
  | Au début, j'ai accusé AsterSTudy mais en éditant le catalogue, je me suis aperçu 
  | qu'on avait ça:
  | 
  |  D_SIGM_EPSI     =SIMP(statut='o',typ=
  | ('R',fonction_sdaster,nappe_sdaster,formule)),
  |  SY              =SIMP(statut='o',typ=
  | ('R',fonction_sdaster,nappe_sdaster,formule)),
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | On supprime le format R dans pour tous les mot-clé de type fonction dans 
  | DEFI_MATERIAU / ECRO_LINE_FO et MAZARS_FO.
  | 
  | Puisque cela peut empêcher l'utisation de ECRO_LINE_FO dans AsterStudy, je propose 
  | de reporter en v15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test perso
- DEJA RESTITUE DANS : 16.0.18
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31296 DU 21/07/2021
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : AFFE_CHAR_MECA / LIAISON_MAIL /  ELIM_MULT
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Jean-Luc a aperçu qu'il y a une incohérence entre la doc et ce qui est programmé
  | pour ELIM_MULT='NON'/'OUI'.
  | 
  | Si ELIM_MULT='NON' on enlève le noeud de la relation s'il a déjà été pris en 
  | compte.
  | ==> On élimine les noeuds "multiples". donc c'est plus logique ELIM_MULT='OUI' 
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | 
  | Dans le code, j'inverse OUI et NON pour ELIM_MULT, et ELIM_MULT=’OUI’ par défaut
  | 
  | 
  | Cas test : zzzz502q, zzzz504z, ELIM_MULT='NON'  => ’OUI’ 
  | Éventuellement tester ELIM_MULT=’NON’ (ancien 'OUI'), pour tester le fait qu'avec
  | DDL_ESCL=’DNOR’ le déplacement du noeud commun est bien dans la bonne direction.
  | Mais s'il n'y a pas de pivot nul (zzzz127a/b), les relations seront respectées.
  | J'ajoute ELIM_MULT=’NON’ dans zzzz127a/b pour la couverture
  | 
  | Je répète l'occurrence du mot clé LIAISON_MAIL pour tester ELIM_MULT=’NON’:
  | - si 2 LIAISON_MAIL répétés avec ELIM_MULT=’OUI’ (par défaut), le test s'arrêt :
  | ╔══════════════════════════════════════════════════════════════════════════════════════
  | ║ <F> <MODELISA8_48>                                                                       
  | ║                                                                                          
  | ║ Erreur utilisateur pour LIAISON_MAIL :                                                   
  | ║    Il n'y a aucun noeud esclave à lier pour l'occurrence 2 du mot clé LIAISON_MAIL.      
  | ║    Peut-être que tous les noeuds esclaves ont déjà été éliminés dans des occurrences     
  | ║    précédentes.                                                                          
  | ║                                                                                          
  | ║                                                                                          
  | ║ Cette erreur est fatale. Le code s'arrête.                                               
  | ╚══════════════════════════════════════════════════════════════════════════════════════
  | - si 2 LIAISON_MAIL répétés avec ELIM_MULT=’NON’ (au moins dans une occurrence),
  | le traitement des DDL_ESCL est bien fait et le test termine normal.
  | 
  | Doc : je reprends le texte proposé par JL
  | ----------------
  | Si l’utilisateur force ELIM_MULT=’NON’, le programme traitera chaque occurrence de
  | LIAISON_MAIL de façon indépendantes. Le nœud B, appartenant à LIGNE_AB et LIGNE_BC
  | sera donc pris en compte dans 2 relations différentes et il est probable
  | que le calcul s’arrêtera lors de la factorisation de la matrice avec le message
  | «Pivot presque nul ...» si les relations linéaires générées par LIAISON_MAILLE 
  | sont redondantes.
  | La plupart du temps, le défaut ELIM_MULT=’OUI’ est le bon choix.
  | Le seul cas où l’utilisateur pourrait utiliser ELIM_MULT=’NON’ est celui de 
  | l’utilisation
  | du mot clé DDL_ESCL=’DNOR’. Si les normales «esclaves» ne sont pas colinéaires, 
  | les relations linéaires générées ne seront pas redondantes.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.44.01
- VALIDATION : zzzz502q, zzzz504z, zzzz127ab
- DEJA RESTITUE DANS : 16.0.18
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31585 DU 22/11/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Le test "ssnl201s" est en erreur de non convergence sur cronos en version 16.0.16
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le test de validation ssnl201s s'arrête sur erreur d'intégration de la loi de comportement.
  | 
  | 
  | Correction
  | ----------
  | 
  | Variable non initialisée dans difoncalc.F90
  | 
  | Comme ça contient n'importe quoi, ça peut justement dire "non convergence".
  | => Initialiser : fsite(:) = 0.d0
  | 
  | Il n'y a pas de mystère, dans une routine de 1000 lignes, impossible d'avoir une vision complète des conditions imbriquées, et donc le tableau n'est pas
  | toujours initialisé selon les cas, on passe dans le "if" suivant qui l'utilise et boum...
  | 
  | Le compilateur relève aussi plusieurs warnings sur de possibles out-of-bounds.
  | En effet, on fait des boucles de i = 1 à 8 et on remplit tab(i) qui est de dimension 4. Comme il y a des conditions sur l'indice de boucle, ça semble correct.
  | Mais si le compilateur émet un warning, c'est qu'il n'est pas sûr de comprendre donc soit il va faire n'importe quoi, soit il ne pourra pas optimiser !
  | => faire une boucle de 1 à 8 et une autre de 1 à 4.
  | 
  | 
  | Validation
  | ----------
  | 
  | Sur les 30 tests, ssnl201 et ssnl202.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnl201 & ssnl202
- DEJA RESTITUE DANS : 16.0.18
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31480 DU 06/10/2021
================================================================================
- AUTEUR : DROUET Guillaume
- TYPE : anomalie concernant code_aster (VERSION 15.4)
- TITRE : Le mot-clé SANS_GROUP_NO de DEFI_CONTACT n'est pas pris en compte
- FONCTIONNALITE :
  | Problème:
  | =========
  | Lors de la vérification de la présence de nœuds commun entre deux surfaces esclave dans DEFI_CONTACT, le mot-clé SANS_GROUP_NO 
  | n'est pas pris en compte ce qui amène à une erreur fatale. Or, si l'on débranche la vérification tout ce passe bien.
  | 
  | Problème observé dans l'étude sur les internes de cuve et dans l'AOM issue31185.
  | 
  | Analyse:
  | ========
  | 
  | Le comportement pour la méthode continue est voulu. Voir issue17827. 
  | J'ai trouvé des cas test simple où les surfaces esclaves avec nœud commun fonctionne et d'autre où cela ne fonctionne pas le 
  | lagrangien de contact est bien sur-contraint. 
  | 
  | Par contre, après une première analyse, si l'on ajoute SANS_GROUP_NO, les nœuds concernés sont éjecté de toutes les zones de 
  | contact.
  | 
  | Solution:
  | =========
  | 
  | On ne fait rien dans les sources pour modifier le comportement, et on émet une fiche d'évolution pour SANS_GOUP_NO.  
  | Le message d'erreur ne fait pas référence à la solution de sélection alternative des surfaces de contact (maître-esclave) 
  | mentionnée dans issue17827:
  | 
  | """Pour pallier ce problème, on adopte la technique suivante :
  | - inversion des surfaces maîtres et esclaves sur la 2ème zone de contact .... """
  | 
  | On modifie le message d'erreur CONTACT2_16

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : submit + test perso
- DEJA RESTITUE DANS : 16.0.19
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 31282 DU 13/07/2021
================================================================================
- AUTEUR : ZAFATI Eliass
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : restreindre les méthodes dot et norm à ASTERDOUBLE pour fieldOnNodes
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le but de cette fiche est de restreindre les méthodes dot et norm à ASTERDOUBLE pour fieldOnNodes. Ca sera encore l'occasion revoir la docstring pour
  | fieldOnNodes et fieldOncells pour certaines méthodes.  
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | On blinde les méthodes "dot" et "norm" pour FieldOnNodes pour que ça soit uniquement fonctionnel pour les réels. A revoir leur implémentations après pour que ça
  | tient compte des complexes.
  | 
  | A cette occasion quelques corrections ont été faites sur la docstring

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : bilan tests cronos
- DEJA RESTITUE DANS : 16.0.19
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31496 DU 13/10/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Il manque le maillage dans Result
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Il manque l'objet maillage dans Result pour 129 tests d'aster
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | C'est toujours dans le post_exec qu'il manque l'ajout du maillage directement ou par une méthode détournée. Le problème c'est que l'on fait du bricolage pour
  | ajouter dans le c++ les données du fortran
  | 
  | Les principaux opérateurs corrigés:
  | crea_resu, crea_champ, proj_champ, mode_iter_*, defi_base_*, rest_gene_phys
  | 
  | J'en ai profité pour ajouter des vérifications automatiques sur la cohérence du maillage utilisé entre les différents objets.
  | 
  | Report en v15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- DEJA RESTITUE DANS : 16.0.12
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 31583 DU 22/11/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Le test "sdls140b" est en ABNORMAL_ABORT sur cronos et gaia depuis la version 16.0.13
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | CREA_RESU, OPERATION='KUCV' échoue dans sdls140b, le maillage n'est pas renseigné dans l'objet Result produit.
  | 
  | 
  | Correction
  | ----------
  | 
  | En effet, KUCV n'est pas testé dans les tests de vérification donc il plante !
  | Quand on le corrige, ça plante encore dans CREA_RESU/CONV_RESU. Là, c'est l'utilisation CREA_RESU/CONV_RESU/MATR_RIGI qui n'est pas testée ailleurs.
  | => il faut des tests de verification : autre fiche à émettre.
  | 
  | La correction est simple, le maillage à utiliser est fourni par RESU_INIT sous les mots-clés facteurs CONV_RESU et KUCV.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdls140b
- DEJA RESTITUE DANS : 16.0.17
- NB_JOURS_TRAV  : 0.5

