==================================================================
Version 15.0.15 (révision 2753938679cb) du 2019-11-15 10:59 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29250 ABBAS Mickael            En version 15.0.12 sur Gaia, le test hsnv101d s'arrête en erreur
 28982 COURTOIS Mathieu         En version 14.3.14, le cas test ssnv147b est NOOK sur clap0f0q
 29078 YU Ting                  Changer le champ ITERAT en carte d'entiers
 29139 LE CREN Matthieu         [RUPT] [Chantier CALC_G] – Remplacement de l'objet .FONDFISS par .ABSCRUV dan...
 29136 LE CREN Matthieu         [RUPT] [Chantier CALC_G] – Suppression de l'objet .FOND.TYPE de la "sd_fissure"
 28648 COURTOIS Mathieu         En 14.3.5, les tests plexu* sont en erreur
 27491 COURTOIS Mathieu         Limiter à 500 lignes dans les sources fortran : à changer pour les modules
 29274 COURTOIS Mathieu         Erreur Python (aslint) sur les catalogues EF
 29008 GUILLOUX Adrien          LIST_FREQ pris partiellement en compte seulement dans CALC_FONCTION/LISS_ENVELOP
 28595 ESCOFFIER Florian        Développement d'un opérateur de calcul de coupure "CALC_COUPURE"
 28978 GUILLOUX Adrien          Depuis la version 14.3.14, le cas test sdnv142a est NOOK sur Eole_Valid
 29106 GEOFFROY Dominique       Le test ssns115b ne converge pas en version NEW15 sur clap0f0q
 22208 MATHIEU Tanguy           META_LEMA_ANI - Sortie des variables internes
 23027 MATHIEU Tanguy           Repère cylindrique local pour META_LEMA_ANI
 26644 COURTOIS Mathieu         Revoir la construction du vocabulaire depuis restructuration des catalogues d...
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29250 DU 28/10/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 15.0.12 sur Gaia, le test hsnv101d s'arrête en erreur
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.0.12 sur Gaia, le test hsnv101d s'arrête en erreur.
  | 
  | 
  | Correction
  | ----------
  | 
  | Les routines nzcifw, nzisfw, nzedga et nzcizi ont toutes le même défaut.
  | Au début de ces routines, les variables suivantes sont initialisées à zéro:
  | vip (variables internes)
  | dsidep (matrice tangente)
  | sigp (contraintes)
  | 
  | Or l'accès à ces vecteurs n'existe pas vraiment lors de la prédiction (qui ne calcule que la matrice) et lors de RAPH_MECA (qui ne
  | calcule que vip/sigp)
  | 
  | Pour se "protéger" d'un risque d'écrasement, on avait l'habitude de transférer un vecteur de réels basé sur l'adresse "1" dans ZR
  | (common JEVEUX). Voir par exemple le te0139 et te0100. C'était bizarre et globalement peu prudent !
  | 
  | Apparemment, sur Gaia, le comportement est différent, un tel accès est interdit. Il faut donc initialiser ces vecteurs seulement
  | dans les cas où c'est nécessaire, sous risque d'un vilain accès mémoire défaillant.
  | 
  | Il est FORTEMENT probable que d'autres lois de comportement soient aussi peu prudentes. En fait, paradoxalement, c'est l'excès
  | d’initialisations qui provoque un problème).
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Cet écrasement mémoire peut produire des résultats faux.
  | 
  | Configuration:
  | Lois META_*_* pour l'acier et le zircaloy

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 15.0.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 14.0.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : hsnv101d
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28982 DU 15/07/2019
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 14.3.14, le cas test ssnv147b est NOOK sur clap0f0q
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le test est actuellement NOOK sur Gaia en v15 et clap0f0q en v14.
  | Dans issue28710, la machine clap0f0q n'avait pas été prise en compte.
  | 
  | 
  | Correction
  | ----------
  | 
  | En passant la précision du calcul (RESI_GLOB_RELA) à 2E-7 et en mettant la valeur d'Eole
  | en référence, le test est OK sur toutes les machines.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv147b


================================================================================
                RESTITUTION FICHE 29078 DU 28/08/2019
================================================================================
- AUTEUR : YU Ting
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Changer le champ ITERAT en carte d'entiers
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | La carte PITERAT qui donne le numéro de l’itération de NEwton est une carte de réels
  | 
  | Correction
  | ------------------------
  | 
  | J'ai changé la carte de réels à entier dans PITERAT, CITERAT (located components correspondant à PITERAT)
  | 
  | 
  | Validation
  | -------------
  | 
  | Ré-installer code aster

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : installation
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29139 DU 17/09/2019
================================================================================
- AUTEUR : LE CREN Matthieu
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [RUPT] [Chantier CALC_G] – Remplacement de l'objet .FONDFISS par .ABSCRUV dans la "sd_fissure"
- FONCTIONNALITE :
  | Dans le cadre du chantier calc_G, on supprime l'objet .FONDFISS qui contient les coordonnées des nœuds du fond de fissure ainsi que
  | l’abscisse curviligne.
  | 
  | On remplace cet objet par .ABSCUR qui contient uniquement l'abscisse curviligne. En effet, les coordonnées des nœuds en fond de
  | fissure sont récupérables avec les numéros des nœuds du fond de fissure dans l'objet FOND.NOEUD et n'ont plus besoin d'être stockées.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test zzzz
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 29136 DU 17/09/2019
================================================================================
- AUTEUR : LE CREN Matthieu
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [RUPT] [Chantier CALC_G] – Suppression de l'objet .FOND.TYPE de la "sd_fissure"
- FONCTIONNALITE :
  | L'objet .FOND.TYPE de la SD fond_fissure est une chaine de caractères qui vaut :
  | -- 'SEG2' si les mailles sont linaires,
  | -- 'SEG3' si les mailles sont quadratiques.
  | 
  | Cette information n'a pas besoin d'être stockée dans la SD fond_fissure car on peut utiliser la SD maillage pour savoir si la maille
  | est linéaire ou quadratique. On supprime donc l'objet .FOND.TYPE de la structure de données.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test zzzz
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28648 DU 20/03/2019
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En 14.3.5, les tests plexu* sont en erreur
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les cas-tests plexu* sont presque tous en erreur ou nook.
  | 
  | 
  | Développement
  | -------------
  | 
  | CALC_EUROPLEXUS se limite maintenant à produire le fichier de commandes pour Europlexus et
  | le maillage dans REPE_OUT.
  | On ne lance plus le calcul Europlexus lui-même et on ne recrée pas un evol_noli après le
  | calcul Europlexus.
  | L'utilisateur peut le faire avec LIRE_EUROPLEXUS (cf. plexu09/plexu10).
  | 
  | 
  | Pour créer plusieurs fichiers de commandes différents (plusieurs CALC_EUROPLEXUS dans le même
  | '.comm'), on ajoute un mot-clé NOM_CAS.
  | Le nom du fichier créé est : `REPE_OUT/commandes_<NOM_CAS>.epx`.
  | 
  | Les cas-tests sont modifiés en conséquence.
  | On vérifie le bon fonctionnement avec TEST_FICHIER sur les fichiers de commandes EPX créés.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u7.03.10
- VALIDATION : plexu*
- NB_JOURS_TRAV  : 1.5


================================================================================
                RESTITUTION FICHE 27491 DU 22/03/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant devtools (VERSION *)
- TITRE : Limiter à 500 lignes dans les sources fortran : à changer pour les modules
- FONCTIONNALITE :
  | La limite à 500 lignes a du sens pour les routines seules.
  | Un module rassemble plusieurs définitions. Il peut donc logiquement être plus long.
  | 
  | Pour les modules, on demande au plus une moyenne de 200 lignes par routines (nombre
  | de lignes du fichier / nombre de routines).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : aslint
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29274 DU 07/11/2019
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Erreur Python (aslint) sur les catalogues EF
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le passage d'aslint sur les catalogues EF provoque une erreur de type C4007 sur 4 fichiers
  | C4007:     Using 'exec()' is discouraged
  | 
  | 
  | Correction
  | ----------
  | 
  | L'usage de 'exec()' est vraiment à décourager.
  | Le fonctionnement du superviseur et INCLUDE, INCLUDE_MATERIAU le nécessite.
  | Dans MAC3, on pourrait certainement faire un import.
  | 
  | Pas d'acharnement sur MACR_RECAL.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : aslint **/*.py
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29008 DU 26/07/2019
================================================================================
- AUTEUR : GUILLOUX Adrien
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : LIST_FREQ pris partiellement en compte seulement dans CALC_FONCTION/LISS_ENVELOP
- FONCTIONNALITE :
  | Le problème portait sur des problèmes de précision, notamment à cause d'aller retour entre les domaines lin et log. Les 
  | modifications nécessaires ont été apportées dans la prise en compte de FREQ_MIN/FREQ_MAX et dans le calcul de l'enveloppe. 
  | La prise en compte des arrondis à travers la gestion d'une précision déjà préalablement présente mais incomplète a été 
  | étendue.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas stagiaire
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28595 DU 27/02/2019
================================================================================
- AUTEUR : ESCOFFIER Florian
- TYPE : evolution concernant code_aster (VERSION 13.4)
- TITRE : Développement d'un opérateur de calcul de coupure "CALC_COUPURE"
- FONCTIONNALITE :
  | Intégration de l'opérateur CALC_COUPURE() :
  | Cet opérateur à pour objectif de fournir les efforts (OPTION = EXTRACTION) et les résultantes (OPTION=RESULTANTE) dans 
  | le repère local d'une ligne de coupe déterminée à partir de deux points.
  | Dans le cas de l'option EXTRACTION, l'opérateur fournit le champ EFGE_NOEU dans le repère de la coupure (l'axe X étant 
  | la coupe elle-même dans la direction A-->B). Dans le cas de l'option RESULTANTE, l'opérateur calcul les résultantes du 
  | champ EFGE_NOEU dans le repère de la coupure. les composantes résultantes sont :
  | - N l'effort normal (intégration de la composante NYY)
  | - VPL le cisaillement membranaire ou cisaillement dans le plan (intégration de la composante NXY)
  | - MHP le moment hors plan (intégration de la composante MYY)
  | - VHP le cisaillement hors plan (intégration de la composante QY)
  | - MPL le moment dans le plan (moment de la composante NYY par rapport au point milieu de la ligne de coupe).
  | 
  | Pour les résultats modaux, l'opérateur fournit aussi les combinaisons modales (CQC signée et Newmark) selon la méthode 
  | spectrale.
  | 
  | Le développement de cet opérateur est réalisé sous forme d'une macro Python faisant appel à MACR_LIGN_COUPE().
  | 
  | Validation :
  | La validation de la macro se fait au travers du test zzzz115 qui comporte trois modélisations :
  | - A : chargements statiques sur plaque modélisée avec des éléments DKT
  | - B : chargements statiques sur plaque orientée de 150 degrés et modélisée avec des éléments DST
  | - C : chargements dynamiques sur plaque modélisée avec des éléments DKT
  | Les valeurs de références sont calculées analytiquement.
  | 
  | Impact doc :
  | Doc U : U4.81.24
  | Doc V : V1.01.115

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.01.115;U4.81.24
- VALIDATION : analytique
- NB_JOURS_TRAV  : 90.0


================================================================================
                RESTITUTION FICHE 28978 DU 15/07/2019
================================================================================
- AUTEUR : GUILLOUX Adrien
- TYPE : anomalie concernant code_aster (VERSION 14.4)
- TITRE : Depuis la version 14.3.14, le cas test sdnv142a est NOOK sur Eole_Valid
- FONCTIONNALITE :
  | On ne restitue pas les modifications décrites dans mon précédent message.
  | issue29273 va régler le problème.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : pas besoin


================================================================================
                RESTITUTION FICHE 29106 DU 06/09/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : Le test ssns115b ne converge pas en version NEW15 sur clap0f0q
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le test ssns115b s'arrête pour non convergence sur clap0f0q en version 15.0.5.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Le problème est résolu suite aux changements de prérequis de la v15.
  | 
  | 
  | 
  | Rien à restituer

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet


================================================================================
                RESTITUTION FICHE 22208 DU 27/02/2014
================================================================================
- AUTEUR : MATHIEU Tanguy
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : META_LEMA_ANI - Sortie des variables internes
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Ajouter 3 variables internes supplémentaires dans le comportement META_LEMA_ANI :  les contraintes équivalentes par phase sigma_vi.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Fait dans le cadre de issue25018
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Sans objet

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : couvert par issue 25018
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 23027 DU 08/09/2014
================================================================================
- AUTEUR : MATHIEU Tanguy
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Repère cylindrique local pour META_LEMA_ANI
- FONCTIONNALITE :
  | Fiche redondante avec issue29221 que l'on conserve.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 26644 DU 27/06/2017
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant devtools (VERSION *)
- TITRE : Revoir la construction du vocabulaire depuis restructuration des catalogues de commandes
- FONCTIONNALITE :
  | Fait par 644b0ff721a5 de devtools en avril 2018.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.1

