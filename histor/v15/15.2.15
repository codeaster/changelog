==================================================================
Version 15.2.15 (révision 496993ccd636) du 2020-10-20 16:43 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 30345 PIGNET Nicolas           HHO et AFFE_CARA_ELEM ne sont pas compatibles
 30346 PIGNET Nicolas           HHO ne sait pas calculer INIT_VARC
 30355 PIGNET Nicolas           HHO et CALC_CHAMP
 28413 ABBAS Mickael            A101.19 - amélioration du changement entre les options dans CALC_MODES
 30339 COURTOIS Mathieu         En version 15.2.13 - révision 29d84d768f92 - les cas-tests vérification (mpi)...
 30277 TARDIEU Nicolas          PETSc : spécifier des options supplémentaires
 30320 DROUET Guillaume         3M - Modifier le calcul du résidu d'équilibre en ROM
 29644 DROUET Guillaume         Calcul taux de décroissance dans DEFI_BASE_REDUITE
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 30345 DU 13/10/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 15.2)
- TITRE : HHO et AFFE_CARA_ELEM ne sont pas compatibles
- FONCTIONNALITE :
  | Objectif
  | -----------------
  | 
  | On ne peut pas affecter de caractéristiques élémentaires de type MASSIF aux éléments HHO.
  | 
  | 
  | Développement
  | ------------------------
  | 
  | Il suffit d'ajouter les éléments autorisés pour MASSIF dans cara_elem_parameter module.
  | 
  | Je rajoute une modélisation l au test mfron03 pour tester cela (loi polycristalline)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v1.03.128
- VALIDATION : mfron03l
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30346 DU 13/10/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 15.3)
- TITRE : HHO ne sait pas calculer INIT_VARC
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | HO ne sait pas calculer l'option INIT_VARC donc on ne peut pas avoir de variables de commande.
  | 
  | C'est embêtant si on veut qu'un matériau dépende de la température.
  | 
  | 
  | Développement
  | ------------------------
  | 
  | J'ai rajouté le calcul de ces quantités:
  | - INIT_VARC (j'ai désactivé le calcul CHAR_MECA_TEMP_R pour le moment car trop compliqué avec HHO et uniquement pour la prédiction)
  | - COOR_ELGA
  | - EPSI_ELGA et EPSI_ELNO (j'ai repris le TE des EF de Lagrange car le champ DEPL en sortie de STAT_NON_LINE contient bien DX, DY, DZ. En toute rigueur, ce calcul devrait être fait avec les inconnues et opérateurs HHO mais ça complique beaucoup pour un gain faible je pense -> A voir à l'usage)
  | - EPSP_ELGA et EPSP_ELNO (j'ai repris le TE des EF de Lagrange pour les mêmes raisons)
  | 
  | J'ajoute les modélisations h,i,j,k à ssnv194 pour tester tout cela

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v6.04.194
- VALIDATION : ssnv194h
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30355 DU 16/10/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 15.3)
- TITRE : HHO et CALC_CHAMP
- FONCTIONNALITE :
  | Objectif
  | -----------------
  | 
  | Les éléments HHO ne savent pas calculer beaucoup de choses dans CALC_CHAMP.
  | 
  | Il faut rajouter à minima:
  | -SIEF_ELNO,
  | -SIEQ_ELGA
  | -VARC_ELGA
  | 
  | 
  | Développement
  | ------------------------
  | 
  | Pour compléter les post-traitements possibles:
  | 
  | Je rajoute les options:
  | -SIEF_ELNO,
  | -SIEQ_ELGA,
  | -VARC_ELGA,
  | -MATE_ELGA, MATE_ELEM
  | 
  | Je modifie les tests ssnp178c,g,h,i; ssnp179d,g,i

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnp178c, ssnp179d, ssnv194h
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28413 DU 14/01/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : A101.19 - amélioration du changement entre les options dans CALC_MODES
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Dans CALC_MODES, si on change l'option, par exemple, de BANDE à PLUS_PETITE, la MATR_RIGI oublie la matrice donnée avant :
  | MATR_RIGI=RIGID,
  | MATR_MASS=MASSE,
  | =>
  | MATR_RIGI=MASSE,
  | MATR_MASS=MASSE,
  | 
  | 
  | Développement
  | -------------
  | 
  | Le problème vient du catalogue de la commande, très touffu, avec une logique de blocs qui est "numérique" et non 
  | "fonctionnelle"
  | 
  | On rebelote tout le catalogue en prenant un point de vue plus fonctionnel:
  | 1/ Choix du type de TYPE_RESU: 'DYNAMIQUE'
  | 2/ Sélection des matrices
  | 3/ Mode de sélection des modes (BANDE, PLUS_PETITE)
  | 4/ Autres options
  | 
  | Ainsi, le changement du mode de sélection des modes n'impacte pas le choix des matrices
  | 
  | Le test sur AsterStudy est concluant
  | Désormais, le changement d'option ne modifie pas les matrices

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tout src + visuel dans AsterStudy
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 30339 DU 13/10/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 15.2.13 - révision 29d84d768f92 - les cas-tests vérification (mpi) zzzz352a, zzzz352c sont en ABNORMAL_ABORT sur calibre, scibian, eole et gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | zzzz352a et zzzz352c en erreur de syntaxe :
  | 
  |   File "./zzzz352a.comm.changed.py", line 27
  |     DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI')))
  |                                                                  ^
  | 
  | Correction
  | ----------
  | 
  | Retirer une parenthèse fermante.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz352a, zzzz352c
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30277 DU 24/09/2020
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : PETSc : spécifier des options supplémentaires
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | La librairie PETSc est conçue avec une grande capacité de configuration à l'exécution. Pour ce faire, des options sous forme de 
  | chaines de caractère peuvent être passées sur la ligne de commandes ou dans un fichier au nom normalisé ~/.petscrc ou 
  | $PWD/.petscrc. Malheureusement cela n'est pas tracé dans fichier message. On propose donc de rendre cette configuration 
  | explicite en renseignant le mot-clé OPTION_PETSC, sous le mot-clé facteur SOLVEUR. 
  | ATTENTION : 
  | ces options ne peuvent changer le solveur et préconditionneur choisis par l'utilisateur ; il s'agit donc d'ajouter des 
  | impressions ou des vérifications à la résolution (comme le calcul des vp de l'opérateur préconditionné). 
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Cette possibilité est déjà offerte pour le préconditionneur FIELDSPLIT. On l'étend à tous les préconditionneurs mais on change 
  | complètement la manière dont cela est fait. 
  | Pour une prise en compte totale des options, on récupère la chaîne OPTION_PETSC et on la passe en tant que argc/argv à la 
  | commande PetscInitialize systématiquement appelée au lancement de PETSc. 
  | Un détail : ces options sont globales à l'instance PETSc ce qui veut dire qu'une fois la librairie initialisée avec un certain 
  | jeu d'options, les autres résolutions en bénéficieront. Donc si on veut réinitialiser ces options, il faut stopper puis 
  | redémarrer PETSc.
  | Pour ce faire, 2 méthodes ont été ajoutées : 
  | - LinearALgebra.petscFinalize()
  | - LinearALgebra.petscInitialize( str myOptions )
  | 
  | On vérifie que ce mécanisme fonctionne bien dans différents tests.
  | 
  | Pour permettre le transfert des options de PETSc, on a créé un nouveau vecteur dans la SD solveur SOLVEUR.SLVO pour les stocker 
  | par morceaux dde K80. On stocke le nombre de K80 dans SOLVEUR.SLVI(9).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.50.01,D4.06.11
- VALIDATION : Passage de tous les tests petsc
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 30320 DU 06/10/2020
================================================================================
- AUTEUR : DROUET Guillaume
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : 3M - Modifier le calcul du résidu d'équilibre en ROM
- FONCTIONNALITE :
  | Problème
  | ========
  | 
  | L'évaluation du résidu en ROM n'est pas optimal. En effet, on calcul l'équilibre à partir de Fext et Fint sans prendre en compte le contact et certaines autres contributions.
  | 
  | Développement:
  | ==============
  | 
  | On propose de travailler sur le vecteur d'équilibre des forces directement cnequi (toutes les contributions seront alors calculées). De plus, on ajoute un pseudo estimateur d'erreur dans le tableau de convergence le résidu HF calculé à partir de la solution réduite (hors hyper réduction)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test ROM et submit
- NB_JOURS_TRAV  : 1.5


================================================================================
                RESTITUTION FICHE 29644 DU 28/02/2020
================================================================================
- AUTEUR : DROUET Guillaume
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Calcul taux de décroissance dans DEFI_BASE_REDUITE
- FONCTIONNALITE :
  | Développement:
  | ==============
  | 
  | On souhaite ajouter un indicateur de qualité de la base réduite générée dans DEFI_BASE_REDUITE par POD. Ce nouvel indicateur doit mesurer la vitesse de décroissance des valeurs singulières. Pour ce faire on ajoute ce calcul dans DEFI_BASE_REDUITE et on sort l'indicateur via un message d'information.
  | 
  | """La pente de décroissance des valeurs singulière de la POD: %(r1)13.6G."""
  | 
  | Ces travaux ont été réalisés dans le cadre de la thèse d'I.Niakh

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : R5.01.05
- VALIDATION : submit + test perso
- NB_JOURS_TRAV  : 2.5

