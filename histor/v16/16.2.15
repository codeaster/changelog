==================================================================
Version 16.2.15 (révision 64cb3bfd775e) du 2022-11-17 11:01 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32383 COURTOIS Mathieu         aslint et vérif parallèle
 32376 ABBAS Mickael            En version 16.2.13 le cas-test verification hsnv139b est en erreur sur gaia e...
 32374 PIGNET Nicolas           HHO et AFFE_CHAR_CINE
 32382 PIGNET Nicolas           Plantage dans getNodeName
 32270 PIGNET Nicolas           En version 16.2.6 dégradation des performances de certains cas-tests de valid...
 32389 PIGNET Nicolas           Rendre possible chamno dans SOUR_CALCULEE
 32174 PIGNET Nicolas           pas de test pour LIAISON_CHAMNO / AFFE_CHAR_THER
 29228 DEGEILH Robin            Ligne de titre avec POST_K1_K2_K3
 31932 GRIMAL Etienne           integration des travaux de Daniela Vo
 32282 COURTOIS Mathieu         Erreur MUMPS sur une installation d'aster sur le CCRT
 29951 GANTIER Maxime           [RUPT] Pertinence de l'option TYPE_RESU='VALE_MAX' de POST_RCCM
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32383 DU 09/11/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION *)
- TITRE : aslint et vérif parallèle
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Il serait pratique que aslint vérifie la cohérence entre testlist et mpi_nbcpu et mpi_nbnoeu
  | 
  | Je me suis fait avoir avec :
  | mpi_nbcpu 3
  | testlist sequential
  | 
  | 
  | Développement
  | -------------
  | 
  | On vérifie déjà que :
  | - les valeurs de 'testlist' sont dans une liste prédéfinie,
  | - mpi_nbcpu n'est pas trop grand.
  | 
  | On ajoute les conditions si 'sequential' :
  | - mpi_nbnoeud * mpi_nbcpu == 1
  | - mpi_nbnoeud <= mpi_nbcpu
  | 
  | Évidemment, il y a 2 export incorrects - cont001d et ssna108b - dans lesquels on met 'parallel' dans 'testlist'.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : invalid export
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32376 DU 07/11/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.2.13 le cas-test verification hsnv139b est en erreur sur gaia et scibian9
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 16.2.13 le cas-test verification hsnv139b est en ABNORMAL_ABORT  sur gaia et scibian9 
  | 
  | Dès la première itération du STAT_NON_LINE 
  | 
  | 
  | Correction
  | ----------
  | 
  | Initialement, on pensait que le problème venait de la révision suivante:
  | [#31643] All DOF can be used with LIAISON_PROJ
  | 
  | Mais il n'y a pas de LIAISON_PROJ dans ce test, c'est que du DDL_IMPO !
  | Le fait que ça remonte sur cette révision est un hasard.
  | 
  | En effet, il y a un FPE dans la loi de comportement (métallurgie avec SIMO_MIEHE), dans la routine nzgdzi.F90 (il suffit de mettre INFO=2 dans STAT_NON_LINE et
  | d'activer le mode debug de calcul pour le voir)
  | 
  | C'est (encore) une conséquence du chantier sur la prédiction.
  | Dans la routine, on met à zéro les variables internes et la matrice alors qu'on est en prédiction et que c'est interdit.
  | Variable non accessible => boom, erreur mémoire
  | 
  | Remarque: de plus, les variables internes sont dimensionnées à une longueur de 6, ce qui est faux.
  | 
  | On corrige comme dans lcgdpm.F90
  | 
  | Le test est OK sur gaia

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : hsnv139b
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32374 DU 04/11/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : HHO et AFFE_CHAR_CINE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Il reste quand même un problème qui est que les inconnues de cellule ne sont pas mises à la bonne valeur depuis que j'ai enlevé la condensation statique.
  | 
  | Dans le cas non-linéaire c'est pas grave car les faces rattrapent le coup mais il faut quand même corriger
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Je rajoute l'imposition des inconnues de cellules dans affe_char_cine grace au convertisseur.
  | 
  | Il y a quelques tests qui bougent maintenant que je bloque partout.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnp179h
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 32382 DU 09/11/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Plantage dans getNodeName
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Dans l'étude jointe, je plante dans la méthode getNodeName applicable sur un maillage avec le message ci-dessous. Je ne comprends pas pourquoi.
  | 
  |  <EXCEPTION> <JEVEUX1_13>                                                                       
  |  ║ Le répertoire de noms 00000001.NOMNOE         $$XNUM contient 21881 points d'entrée, la  requête sur le numéro 0 est invalide.                               
  |                                   
  |  ║ Ce message est un message d'erreur développeur.                                               
  |  ║ Contactez le support technique.
  | 
  | Correction/Développement
  | ------------------------
  | getNodeName() et getCellName() étaient réstées en 1-based. Je les passe en 0-based comme le reste.
  | 
  | Je modifie mesh001a pour vérifier

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : mesh001a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32270 DU 12/09/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.2.6 dégradation des performances de certains cas-tests de validation  appelant MODI_MAILLAGE/AFFE_CHAR_MECA
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | En version 16.2.6 dégradation des performances de certains cas-tests appelant MODI_MAILLAGE/AFFE_CHAR_MECA (cf CR eda 12092022)
  | 
  | Les tests incriminés sont :
  | 
  | ssnv249a,b,c 
  | ssnv258a,b
  | 
  | 
  | Extrait du fichier perf (de cronos)
  | 
  |  Degradations :
  |  95.4% ssnv249b   AFFE_MODELE_15         1.52     2.97
  |  84.9% ssnv249a   MODI_MAILLAGE_3        6.49    12.00
  |  84.9% ssnv249b   MODI_MAILLAGE_3        6.48    11.98
  |  84.3% ssnv249c   MODI_MAILLAGE_3        6.50    11.98
  | 
  |  55.3% ssnv249c   AFFE_CHAR_MECA_57      7.67    11.91
  |  55.1% ssnv249b   AFFE_CHAR_MECA_58      7.72    11.97
  |  53.8% ssnv249a   AFFE_CHAR_MECA_58      7.92    12.18
  | 
  |  50.0% ssnv258a   DEFI_GROUP_8           0.86     1.29
  |  48.9% ssnv258b   DEFI_GROUP_8          41.57    61.90
  |  48.0% ssnv249c   AFFE_CHAR_MECA_F_58     5.33     7.89
  |  47.9% ssnv249b   AFFE_CHAR_MECA_F_59     5.36     7.93
  | 
  |  46.2% ssnv249a   AFFE_CHAR_MECA_F_59     5.54     8.10
  |  39.1% ssnv249c   AFFE_CHAR_MECA_F_87     3.20     4.45
  |  38.3% ssnv249c   AFFE_CHAR_MECA_F_88     3.21     4.44
  |  38.3% ssnv249b   AFFE_CHAR_MECA_F_86     3.24     4.48
  |  38.3% ssnv249b   AFFE_CHAR_MECA_F_85     3.24     4.48
  |  37.9% ssnv249c   AFFE_CHAR_MECA_F_89     3.22     4.44
  |  37.8% ssnv249b   AFFE_CHAR_MECA_F_87     3.25     4.48
  |  37.3% ssnv249a   AFFE_CHAR_MECA_F_87     3.35     4.60
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Pour MODI_MAILLAGE, je ne reconstruit pas la collection des groupes et ça revient comme avant.
  | 
  | Pour AFFE_CHAR_MECA, c'est la construction des ligrels qui est long quand il y a beaucoup de ddl impliqués car on les traite 1 par 1 (et par groupes, ça doit
  | sûrement causer des problèmes de perf quand c'est encore plus grand). J'enlève la reconstruction du ligrel, il faudra le faire à l'utilisation qui n'est pas
  | utilisée pour le moment (hors contact)
  | 
  | On retrouve les mêmes temps qu'avant

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv249c
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 32389 DU 10/11/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Rendre possible chamno dans SOUR_CALCULEE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | SOUR_CALCULEE ne prend en entrée qu'un ELGA. Je propose de le faire pour les autres champs aussi
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Je rajoute la possibilité d'un cham_no en entrée. On fait la conversion du champ vers ELGA dans affe_char_ther.
  | 
  | je modifie tpll101b pour tester le dev. Le résultat est très proche de l'ancien.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tpll101b
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32174 DU 06/07/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : pas de test pour LIAISON_CHAMNO / AFFE_CHAR_THER
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Il n'existe pas de test pour LIAISON_CHAMNO / AFFE_CHAR_THER !
  | 
  | Solution :
  | ========
  | On en ajoute un.
  | 
  | Dans la partie thermique de zzzz127b, on construit des chargements LIAISON_CHAMNO équivalents aux chargements utilisant LIAISON_DDL.
  | On crée un troisième calcul (avec le nouveau chargement) et on vérifie (comme pour le calcul avec LIAISON_MAIL) que l'on obtient les mêmes
  | résultats.
  | 
  | Remarque : j'ai d'abord tenté de faire cela dans le test zzzz508 (test parallèle) et cela ne semble pas fonctionner.
  | 
  | Impact doc : [V1.01.127], on ajoute en intro que le test valide également l'utilisation de AFFE_CHAR_THER/LIAISON_CHAMNO.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.01.127
- VALIDATION : zzzz127b


================================================================================
                RESTITUTION FICHE 29228 DU 15/10/2019
================================================================================
- AUTEUR : DEGEILH Robin
- TYPE : evolution concernant code_aster (VERSION 14.4)
- TITRE : Ligne de titre avec POST_K1_K2_K3
- FONCTIONNALITE :
  | Analyse :
  | =======
  | 
  | On remarque que lorsque l'on ajoute le mot-clé TITRE à CALC_G cela n'a pas d'effet sur la table en sortie qui reste sans titre.
  | 
  | CALC_G renvoie une table_container contenant la table des résultats et un champ de THETA. Mais l'utilisateur obtient malgré tout la table des
  | résultats en retour. Ceci s'explique dans le fichier code_aster/Commands/calc_g.py dans lequel on récupère la table contenue dans la
  | table_container pour la retourner à l'utilisateur. Sauf que cette table n'a pas de TITRE contrairement à la table_container.
  | 
  | Modifications :
  | =============
  | 
  | 1/ Dans code_aster/Commands/calc_g.py, on récupère le titre de la table_container (.getTitle()) 
  | que l'on affecte à la table des résultats (setTitle()).
  | => avec cette modification, les titres donnés par l'utilisateur sont pris en compte, mais si on ne donne pas de titre, on a le titre
  | par défaut comme en version 14.4 (voir message de Sébastien).
  | 
  | 2/ 
  | On ajoute deux arguments optionnels à titre.F90 et titrea.F90 : defTitle (K80) et lDefTitle (I).
  | 
  | Dans titrea.F90, si ces arguments sont présents et qu'il n'y a pas de titre utilisateur, on prends en compte le titre contenu dans defTitle. 
  | Dans bibfor/fracture/cgExportTableG.F90, on appelle titre() avec les deux arguments optionnels :
  | defTitle = "CALCUL DES FACTEURS D'INTENSITE DES CONTRAINTES PAR LA METHODE CALC_G"
  | lDefTitle = 70 (nécessaire pour le fonctionnement des sous-routines de titrea)
  | 
  | Ceci répond à la demande. On obtient le titre souhaité sauf si l'utilisateur à demander un autre titre.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz406b (modifié)


================================================================================
                RESTITUTION FICHE 31932 DU 21/03/2022
================================================================================
- AUTEUR : GRIMAL Etienne
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : integration des travaux de Daniela Vo
- FONCTIONNALITE :
  | Objectif
  | --------
  | Intégration des travaux de thèse de Daniela Vo (LMDC- Toulouse).
  | 
  | Dans le cadre d'une thèse DT-LMDC, Daniela Vo a développé une loi intégrant le comportement du beton armé dans le modèle 
  | de gonflement dejà présent dans aster
  | BETON_RGI.
  | 
  | Il s'agit de restituer ces développements dans Code_Aster sous le nom de comportement RGI_BETON_BA
  | 
  | 
  | Développement :
  | -------------
  | 
  | Nom de la loi RGI_BETON_BA 
  | mot-clef matériau RGI_BETON_BA
  | 
  | Les sources originales étaient en version 14. Elles sont été portées en version de développement (16).
  | Les routines principales de le loi RGI_BETON (cfluendo3d.F90 et fluendo3d.F90) avaient été copiées en cfluendo3d_ba.F90 
  | et fluendo3d_ba.F90.
  | Une première étape a consisté à effectuer un merge des routines "sœurs".
  | Un gros travail de refactoring a été mené afin de dépasser les moins possibles les règles des 500 lignes max et des 20 
  | arguments max 
  | par routine. De nouvelles sous-routines ont été crées.
  | 
  | Les autres remarques émises lors de la RTA ont été prise en compte (noms des nouveaux paramètre, noms des variables 
  | internes).
  | 
  | Validation :
  | ----------
  | Ce développement est validé par deux nouveaux cas test : 
  | ssnv269a : test avec 1 armature
  | ssnv269b : test avec 5 armatures
  | 
  | La fiche issue32375 est ouverte dans le but le restituer un cas test industriel.
  | 
  | Impact doc :
  | ----------
  | 
  | r7.01.45 : Loi de comportement RGI_BETON_BA
  | u4.43.01 : AJout du matériau RGI_BETON_BA
  | u4.51.11 : AJout du comportement RGI_BETON_BA
  | V6.04.269 : doc des nouveaux tests

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : r7.01.45, u4.43.01, u4.51.11, V6.04.269
- VALIDATION : ssnv269a et b


================================================================================
                RESTITUTION FICHE 32282 DU 19/09/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Erreur MUMPS sur une installation d'aster sur le CCRT
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Erreur à l'utilisation de MUMPS sur une machine du CCRT.
  | 
  | 
  | Correction
  | ----------
  | 
  | Cela se produisait dans le contexte MUMPS + METIS.
  | Il manquait les bibliothèques GKLib, prérequis de metis.
  | 
  | Dans le cas de ce type d'erreur (sur une machine fraichement installée), penser à vérifier les tests unitaires de Mumps.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : installation
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29951 DU 05/06/2020
================================================================================
- AUTEUR : GANTIER Maxime
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [RUPT] Pertinence de l'option TYPE_RESU='VALE_MAX' de POST_RCCM
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Lors de l'ouverture de la fiche, l'opérateur POST_RCCM permettait l'impression de la table de résultats sous deux formats : 
  | TYPE_RESU = 'DETAILS' ou 'VALE_MAX'.
  | 
  | Ces deux formats sont décrits dans le § 2.2.2.6 de la notice d'utilisation U2.09.03.
  | En particulier, VALE_MAX permet d'afficher les valeurs maximales de SN, SN*, SP, KE, SALT, NADM et DOMMAGE calculé par 
  | l'opérateur. Toutefois, rien n'impose que ces valeurs soient prises aux mêmes instants, ni pour la même combinaison de 
  | transitoire. Ainsi, la table en retour de l'opérateur peut poser des difficultés d'interprétation. Toutefois, cette option a le 
  | mérite d'être très synthétique dans sa présentation des résultats. Entre temps, la signification de l'option 'VALE_MAX' a aussi 
  | été précisée dans la doc U. 
  | 
  | Par ailleurs, depuis l'ouverture de cette fiche, une option d'affichage supplémentaire a été ajoutée (option 
  | 'SYSTUS',https://aster.retd.edf.fr/rex/issue28849 ). Cette option permet un affichage semi-synthétique, à mi-chemin entre 
  | 'VALE_MAX' et 'DETAILS'. 
  | 
  | La présente fiche peut donc être classée sans suite. 
  | Sans source.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 0.5

