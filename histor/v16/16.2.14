==================================================================
Version 16.2.14 (révision 036d39b62473) du 2022-11-10 07:48 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32268 PIGNET Nicolas           En version 16.2.6 les cas-tests de validation  sdls121e sdnp001b sont en ABNO...
 32323 PIGNET Nicolas           HHO et THER_LINEAIRE
 31621 PIGNET Nicolas           HHO et prédiction élastique
 32344 PIGNET Nicolas           CREA_MAILLAGE/MODI_HHO plante en parallèle
 32362 PIGNET Nicolas           COPIER et références
 32341 PIGNET Nicolas           Mettre au carré THER_LINEAIRE
 32187 BETTONTE Francesco       Extraction des champs d'un résultat
 31043 ABBAS Mickael            Alarme COMPOR4_17 non débrayable et difficile à comprendre
 32182 PIGNET Nicolas           Simplification THER_LINEAIRE
 32342 ABBAS Mickael            Problème avec repères éléments TUYAUX
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32268 DU 12/09/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.2.6 les cas-tests de validation  sdls121e sdnp001b sont en ABNORMAL_ABORT
- FONCTIONNALITE :
  | Problème
  | ---
  | 
  | Suite issue32238, les cas-tests de validation sdls121e et sdnp001b sont en erreur :
  | 
  | libaster.AsterError: ('PROF_CHNO already exists: 0000002d      .NUME', (), (), ())
  | 
  | Analyse
  | ---
  | 
  | Il manque l'ajout des dof_numbering au moment de crea_champ
  | 
  | sdls121e : mode_meca
  | sdnp001b : dyna_trans
  | 
  | Travail effectué
  | ---
  | 
  | * On ajoute les dof_numbering manquant pour crea_resu dans les cas mode_meca et dyna_tran
  | cela corrige les tests sdls121e et sdnp001b
  | 
  | * Modification pour reproduire les mêmes plantages dans src
  | sdll146a (mode_meca) : ajout d'un PROJ_MESU_MODAL + REST_GENE_PHYS au CREA_RESU
  | sdls140a (dyna_trans) : ajout d'un REST_SPEC_TEMP + une poursuite
  | 
  | Validation
  | ---
  | 
  | * tous les tests src dont les tests modifiés sdll146a et sdls140a
  | * test de validation sdls121e et sdnp001b

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdll146a


================================================================================
                RESTITUTION FICHE 32323 DU 14/10/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : HHO et THER_LINEAIRE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le but est d'ajouter HHO à l'opérateur de thermique dans l'objectif de faire des lois non-locales plus tard.
  | 
  | On aura besoin de résoudre un problème de Helmotz de la forme:
  | 
  | A (\grad u, \grad v) + k^2 (u,v) = (f,v)
  | 
  | Avec A, k et f donné. On est très proche de la thermique d'où l’intérêt de commencer par là pour HHO
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Voici ce que j'ai programmé pour HHO en thermique:
  | - RIGI_THER, MASS_THER, RIGI_THER_COEH_*
  | - CHAR_THER_EVOL, CHAR_THER_SOUR_*, CHAR_THER_COEH_*, CHAR_THER_FLUN_*, CHAR_THER_TEXT_*
  | 
  | Les EF HHO: PLAN et en 3D; linéaire et quadratique. Les points de gauss sont les mêmes en mécanique et thermique.
  | 
  | J'ai rajouté une classe HHO en c++ qui permet de faire des préparation de champs comme
  | passer d'un réel à un champ HHO ou d'un champ HHO à Lagrange. Je n'ai pas pu aller plus loin car calcul.F90 ne supporte pas des ELNO en sortie qui n'ont pas les
  | mêmes composantes sur tout les noeuds
  | 
  | En plus du champ TEMP, il y a un champ HHO_TEMP qui est la projection du champ TEMP sur l'espace des EF Lagrange. C'est celui-ci qui est à passer dans AFFE_VARC
  | pour le moment (voir si on peut faire une meilleure interpolation plus tard).
  | 
  | Les tests: ttlv301b, ttlv301c (en 3D), ttlp301c, ttlp301d (en PLAN)
  | 
  | J'ai rajouté le test sslp116e qui fait de la thermo-élasticité en full HHO.
  | 
  | Doc: U4.41.01, V4.23.301, V4.25.301
  | 
  | Je pense que l'on a presque tous les ingrédients pour faire un schéma étagé pour faire de l'endommagement non-local

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.41.01, V4.25.301
- VALIDATION : ttlv301b, sslp116e
- NB_JOURS_TRAV  : 6.0


================================================================================
                RESTITUTION FICHE 31621 DU 08/12/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : HHO et prédiction élastique
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | La prédiction élastique n'est pas dispo pour HHO ce qui est embêtant en décharge
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | C'est très facile maintenant qu'il n'y a plus de condensation statique.
  | 
  | Je programme l'option correspondante et je teste dans ssnpl116e.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sslp116e
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32344 DU 26/10/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : CREA_MAILLAGE/MODI_HHO plante en parallèle
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | CREA_MAILLAGE/MODI_HHO plante sur un ParallelMesh.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Il y avait un problème pour la création des faces pour le TETRA15 (mauvaise connectivité d'une face)
  | 
  | Je vérifie dans ssnp179j.
  | 
  | 
  | Résultat faux
  | -------------
  | Pas de risque de résultats faux car on ne servait que des noeuds sommets du TETRA15 pour l'instant

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnp179j
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32362 DU 31/10/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 16.2)
- TITRE : COPIER et références
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Dans l'étude jointe, je ne comprends pas pourquoi les instructions
  | 
  | print(resuwb[nbwbm].getMaterialField())
  | print(resuwb[nbwbm].getMaterialFields())
  | 
  | renvoient respectivement : None et []
  | 
  | Le concept d'origine resunowb a bien un champ matériau. Mais après le passage par la commande COPIER, le champ matériau est visiblement perdu ou bien les
  | commandes getMaterialField(s) n'arrivent pas à le retrouver.
  | 
  | Cela me pose problème pour réussir à modifier les paramètres de Weibull à la volée, (après lecture d'un résultat), ce qui nous serait très utile pour certains
  | utilisateurs de MMC.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | -getMaterialField() renvoie un pointeur qui peut être nul s'il n'y en a pas (donc None) ou emet une erreur s'il y a plusieurs matériaux
  | -getMaterialFields() renvoie la liste des matériaux qui peut être vide.
  | 
  | Par contre COPIER ne fait pas à jour les pointeurs correctement. Je corrige pour les classes qui héritent de Result
  | 
  | Je rajoute une poursuite à zzzz229a pour tester 
  | 
  | Rmq: Il y a setMaterialField() si tu veux changer le matériau mais attention ça peut créer des problème de cohérence.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz229a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32341 DU 24/10/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Mettre au carré THER_LINEAIRE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Mettre au carré THER_LINEAIRE
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | On rajoute un mot-clé TYPE_CALCUL = TRAN ou STAT pour savoir si le calcul est transitoire ou non. Dans le cas transitoire, ETAT_INIT et INCREMENT deviennent
  | obligatoire.
  | 
  | Je rajoute un compat_syntax pour le changement de syntaxe de STATIONNAIRE en STAT.
  | 
  | Je modifie CALC_MATE_HOMO qui faisait un faut calcul évolutif avant pat un vrai calcul stationnaire avec plusieurs pas de temps.
  | 
  | Tout src est ok
  | 
  | Doc modifiée: U4.54.01.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.54.01
- VALIDATION : src
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 32187 DU 15/07/2022
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Extraction des champs d'un résultat
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Ajouter des arguments supplémentaires à resultat.getField() afin d'extraire un champ sur la base d'un paramètre 
  | d'accès quelconque.
  | 
  | 
  | Développement
  | -------------
  | 
  | Ajout d'une classe SearchList pour effectuer des recherches dans une liste (d'instants par exemple) au sens aster, 
  | avec une précision et un critère absolu ou relatif.
  | 
  | slist = SearchList(times, precision=1.e-6, criterion="RELATIF")
  | idx = slist.index(1.0) #pour restituer l'index correspondant à l'instant 1.0
  | cond = 1.0 in slist # pour déterminer si l'instant est trouvé dans la liste, même en double selon le 
  | critère/précision
  | cond = slist.unique(1.0) # pour déterminer si l'instant est unique dans la liste selon le critère/précision
  | 
  | Lève : 
  |  ValueError si la valeur recherchée n'est pas presente
  |  IndexError si la plusieurs valeurs correspondent à la recherche.
  | 
  | 
  | 
  | Avec cette classe il devient simple d'ajouter la fonction: 
  | _getRankFromParameter(para, value, crit="RELATIF", prec=1.e-6)
  | 
  | qui lève un KeyError si para n'existe pas.
  | 
  | ..et modifier ensuite getField(name, index) en getField(name, value=None, para="NUME_ORDRE, crit="RELATIF", 
  | prec=None)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz505e
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 31043 DU 17/05/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Alarme COMPOR4_17 non débrayable et difficile à comprendre
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Cette alarme est la suivante: 
  | 
  | ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <A> <COMPOR4_17>                                                                               ║
  |  ║                                                                                                ║
  |  ║ Il y a au moins un comportement qui sera traité en mode total or vous avez des variables de    ║
  |  ║ commande. Référez-vous à la documentation pour comprendre la manière dont seront prises en     ║
  |  ║ comptes les variables de commande dans cette situation.                                        ║
  |  ║                                                                                                ║
  |  ║ Ceci est une alarme. Si vous ne comprenez pas le sens de cette                                 ║
  |  ║ alarme, vous pouvez obtenir des résultats inattendus !                                         ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | - elle se déclenche  dès qu'on a AFFE_VARC
  | - on ne comprend pas bien ce qui est dit
  | 
  | 
  | Développement
  | -------------
  | 
  | 
  | Cette alarme signifie en fait: vous avez des variables de commande et vous utilisez une loi de comportement qui ne sait pas gérer les déformations anélastiques
  | (comme la thermique).
  | 
  | Le test se faisait sur la présence de la propriété COMP_ELAS. 
  | Or cette propriété ne correspond plus avec le chantier sur la prédiction.
  | 
  | En fait, le vrai critère, c'est DEFO_LDC
  | - OLD (ancien système). Les déformations anélastiques sont prises en compte dans chaque loi de comportement normalement mais sans aucune garantie en pratique !
  | Évidemment, cette propriété devrait disparaître à terme. Il faut juste vérifier toutes les lois et les passer en MECANIQUE ou TOTALE.
  | - MECANIQUE (nouveau système). La loi de comportement utilise des déformations mécaniques et la prise en compte des déformations anélastiques est
  | automatiquement réalisée par lc0000, sans action du développeur de la loi de comportement
  | - TOTALE  (nouveau système). Les déformations anélastiques ne sont pas prises en compte du tout. En général, c'est le cas des modèles de grandes déformations
  | comme SIMO_MIEHE
  | 
  | Donc, en fait, on ne devrait émettre cette alarme que si DEFO_LDC='TOTALE', pas sur la propriété COMP_ELAS.
  | 
  | On commence par compléter DEFO_LDC='TOTALE' sur toutes les lois de comportement qui en manque. En fait, ça correspond essentiellement:
  | - Aux lois de comportement spécifique à certains éléments finis (comme CABLE, ELAS_POUTRE_GR, etc.)
  | - Aux lois de comportement qui ne prennent absolument pas en compte les déformations anélastiques (j'ai vérifié dans le source) 
  | 
  | M code_aster/Behaviours/czm_elas_mix.py
  | M code_aster/Behaviours/czm_exp_mix.py
  | M code_aster/Behaviours/czm_exp_reg.py
  | M code_aster/Behaviours/czm_fat_mix.py
  | M code_aster/Behaviours/czm_lab_mix.py
  | M code_aster/Behaviours/czm_lin_reg.py
  | M code_aster/Behaviours/czm_ouv_mix.py
  | M code_aster/Behaviours/czm_tac_mix.py
  | M code_aster/Behaviours/czm_tra_mix.py
  | M code_aster/Behaviours/joint_ba.py
  | M code_aster/Behaviours/joint_meca_endo.py
  | M code_aster/Behaviours/joint_meca_frot.py
  | M code_aster/Behaviours/joint_meca_rupt.py
  | 
  | Le message sera désormais plutôt:
  | 
  |    17: _("""Vous utilisez des variables de commande et il y a au moins un comportement qui ne peut pas prendre en compte les déformations anélastiques
  | (thermique par exemple)."""),
  | 
  | Pour COMP_ELAS, l'idée est de dire que les contraintes initiales (état initial) sont ignorées pour ces lois car c'est une abomination du point de vue de la
  | formulation. Ca correspond donc à l'alarme COMPOR&_61
  | 
  |     61: _("""Vous utilisez un comportement non-incrémental (élasticité non-linéaire par exemple) avec un état initial. Cet état initial ne sera pas pris en
  | compte."""    ),

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : visuelle
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32182 DU 12/07/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Simplification THER_LINEAIRE
- FONCTIONNALITE :
  | Sans suite. Voir issue32341

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32342 DU 25/10/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Problème avec repères éléments TUYAUX
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | lI s'agit d'un calcul dynamique sur une poutre bi-encastrée où on applique une pesanteur selon une rampe linéaire.
  | La poutre est orientée suivant l'axe X. La pesanteur est appliqué suivant l'axe Z.
  | 
  | Les résultats en POU_D_T montrent : 
  | un déplacement de la poutre suivant Z
  | un moment Mfy important et un moment Mfz quasi nul
  | 
  | Les résultats en TUYAU_6M montrent : 
  | un déplacement de la poutre suivant Z
  | un moment Mfz important et un moment Mfy quasi nul.
  | 
  | Le résultat en TUYAU_6M n'est pas cohérent. 
  | 
  | 
  | Correction
  | -----------
  | 
  | En ajoutant 
  | 
  |                         ORIENTATION=_F( GROUP_NO = 'ENCA1',
  |                                        CARA = 'GENE_TUYAU',
  |                                        VALE = (0., 0., 1.,)));
  | 
  | On retrouve une cohérence avec un moment résultant autour de l'axe Y. 
  | Dire quel type de validation a été fait (test analytique, de non régression, ...les plus courts possibles).
  | 
  | On propose de faire un imapct doc pour rappeler qu'il faut systématiquement utiliser ORIENTATION avec les TUYAUX

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.42.01
- VALIDATION : rien
- NB_JOURS_TRAV  : 1.0

