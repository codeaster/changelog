==================================================================
Version 16.3.27 (révision d9a755de27d2) du 2023-07-04 13:12 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32645 PIGNET Nicolas           Plantage contact avec GTN non local
 32978 COURTOIS Mathieu         ThermalResultDict KO en poursuite
 32982 COURTOIS Mathieu         Build debian 12
 32980 COURTOIS Mathieu         THER_NON_LINE : precision liste d'instants ignorée
 32960 HABIB Souheil            Tests CALC_FERAILLAGE en erreur en mode debug
 32855 PIGNET Nicolas           ENERGIE=_F() et énergie cinétique négative ???
 32811 LORENTZ Eric             AFFE_CARA_ELEM / MASSIF avec éléments d'interface
 32671 PIGNET Nicolas           Erreur non claire dans CREA_MAILLAGE avec DECOUPE_LAC
 32480 PIGNET Nicolas           En version 16.2.18 - dégradation de l'opérateur DEFI_GROUP
 32969 TARDIEU Nicolas          En version 16.3.25, le cas test sdlv102b est en erreur sur GAIA
 30761 NGUYEN Thuong Anh        Reprise du cas-test sdld33b_ajout reac_noda
 32693 ABBAS Mickael            DYNA_NON_LINE - Souci lors du calcul de l'accélération initiale
 27985 PIGNET Nicolas           Adapter CREA_MAILLAGE pour un parallel_mesh
 25953 PIGNET Nicolas           D124.19 - Améliorations pour DECOUPE_LAC
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32645 DU 23/02/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 16.3)
- TITRE : Plantage contact avec GTN non local
- FONCTIONNALITE :
  | Problème
  | ---
  | 
  | lorsqu'il y a échec lors de la projection à la réactualisation géométrique du contact,
  | le code s'arrête au lieu de redécouper le pas de temps.
  | 
  | Le cas joint illustre le problème avec contact LAC, mais il faut aussi corrigé pour le contact CONTINUE
  | 
  | Travail effectué
  | ---
  | 
  | 1/ stockage de l'erreur
  | 
  | Plutôt que d'émettre directement une erreur <F><APPARIEMENT_13>, on émet une alarme,
  | le code retour est remonté et traité dans la routine plus haut
  | 
  | * apntos pour le contact CONTINUE
  | * apstos pour le contact LAC
  | 
  | si le code retour est positif, on l'enregistre :
  | 
  |     mmbouc(ds_contact, 'Geom', 'Set_Error')
  | 
  | 2/ Traitement du découpage
  | 
  | On ajoute un nouvel évènement "ERRE_APPA" dans nmcrga
  | 
  | Dans cfmmcv, on récupère le code retour :
  | 
  |     mmbouc(ds_contact, 'Geom', 'Is_Error', loop_state_=loop_geom_error)
  | 
  | Si c'est positif, on déclenche l’évènement
  | 
  |     call nmcrel(sderro, 'ERRE_APPA', .true._1)
  | 
  | En cas de découpage actif, on émet un nouveau message MECANONLINE10_14
  | 
  |     <Erreur> Échec de l'appariement de contact lors de l'actualisation géométrique en Newton généralisé.
  | 
  | Sinon, on  s'arrête avec un nouveau message MECANONLINE9_13
  | 
  |     Arrêt suite à l'échec de l'appariement de contact lors de l'actualisation géométrique.
  | 
  | Report en v15
  | 
  | Validation
  | ---
  | 
  | Étude jointe pour contact LAC + un test perso pour contact CONTINUE + tests de la base

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test joint


================================================================================
                RESTITUTION FICHE 32978 DU 28/06/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : ThermalResultDict KO en poursuite
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Dans zzzz154a, si on fait :
  | 
  | coll = ThermalResultDict()
  | coll["12"] = CREA_RESU(...)
  | 
  | la POURSUITE échoue.
  | 
  | 
  | Correction
  | ----------
  | 
  | Cela fonctionne si on fait :
  | 
  | resu = CREA_RESU(...)
  | coll["12"] = resu
  | 
  | Cela vient du fait qu'on sérialise les objets présents dans le context où FIN() est appelé.
  | Donc on écrit 'resu' et 'coll'.
  | Pour tous les types "non spécifiques", on laisse pickle s'en charger, il ne fait donc que stocker
  | la référence (persistent id).
  | 
  | Au depickling, on a la référence qui est stockée au bon endroit dans le dict.
  | Cette référence correspond à l'objet 'resu' stocké par ailleurs donc on peut le restaurer.
  | 
  | 
  | Quand on fait :
  | coll["12"] = CREA_RESU(...)
  | 
  | On stocke bien la référence mais personne n'a stocké ce qu'il faut pour restaurer l'objet.
  | D'où l'erreur : il manque les init_args.
  | 
  | Pour les objets du type WithEmbeddedObjects, il faut faire comme pour les autres conteneurs
  | de DataStructures connus, il faut parcourir les attributs pour stocker les objets.
  | Ils pourront ainsi être reconstruits quand on tombera sur une de leurs références.
  | 
  | 
  | Validation
  | ----------
  | 
  | Dans zzzz154a, on fait un mix :
  | 
  | resu = CREA_RESU(...)
  | ther_dict["a"] = resu
  | 
  | ther_dict["b"] = CREA_RESU(...)
  | 
  | et dans zzzz501d, on supprime la référence du contexte global vers les objets embarqués.
  | 
  | 
  | A priori, report à faire en v15.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz501d, zzzz154a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32982 DU 29/06/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Build debian 12
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Erreur de compilation sur Debian 12 (GCC 12, Python 3.11, numpy 1.24) :
  | 
  | - "forward declaration" de medTypes : manque '#include <array>'
  | 
  | - alias numpy.int, numpy.float, nunmpy.complex deprecated.
  | 
  | 
  | Correction
  | ----------
  | 
  | Ajout de '#include <array>' dans astercxx.h
  | 
  | Suppression des alias numpy dans la vérification des types.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : build debian 12
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32980 DU 29/06/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : THER_NON_LINE : precision liste d'instants ignorée
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | THER_NON_LINE affiche systématiquement l'alarme :
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <A> <LISTINST_2>                                                                               ║
  |  ║                                                                                                ║
  |  ║ Sélection d'instants.                                                                          ║
  |  ║   Vous n'avez pas donné de précision pour votre critère de sélection. On a pris la valeur      ║
  |  ║ 1.000000000000e-06 par défaut.                                                                 ║
  |  ║                                                                                                ║
  |  ║                                                                                                ║
  |  ║ Ceci est une alarme. Si vous ne comprenez pas le sens de cette                                 ║
  |  ║ alarme, vous pouvez obtenir des résultats inattendus !                                         ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | Même quand on spécifie une précision.
  | 
  | 
  | Correction
  | ----------
  | 
  | Ce n'est pas la précision sur la liste d'instants qui est vérifiée mais celle de l'archivage.
  | Toutes les commandes qui utilisent le mot-clé commun C_ARCHIVAGE sont concernées.
  | 
  | CRITERE et PRECISION n'ont de sens que si INST ou LIST_INST sont utilisés.
  | On a ajouté un bloc dans ce sens dans issue32934.
  | Il suffit d'avoir la même logique dans le fortran et de ne pas vérifier PRECISION que quand cela a du sens.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ttnl302d
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32960 DU 22/06/2023
================================================================================
- AUTEUR : HABIB Souheil
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Tests CALC_FERAILLAGE en erreur en mode debug
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | En mode debug, certains cas tests de vérification étaient cassés.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | En regardant de près, il s'agissait d'une erreur causée dans les fichiers cafelsiter et cafeluiter, au niveau d'une équation où on rencontrait une division par
  | 0 :
  | 
  | ... alphaI = aG+(aD-aG)/(1+abs(fD/fG)) ...
  | 
  | cette équation était placée au mauvais niveau ; elle passe désormais au sein d'une boucle avec condition qui écarte ce cas de division par 0 (et où elle aurait
  | du etre placée initialement)
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | NON

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssls146a, ssls135a, ssls134a/b/c, ssll13a/b/c
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32855 DU 12/05/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 16.4)
- TITRE : ENERGIE=_F() et énergie cinétique négative ???
- FONCTIONNALITE :
  | Problèmes :
  | =========
  | 
  | 1/ Dans le cas d'une reprise avec le mot-clé ENERGIE le calcul plante si ENERGIE n'était pas activé dans le calcul initial.
  | 
  | 2/Dans le cas d'un reprise, le bilan énergétique initial n'est pas récupéré.
  | 
  | Analyse :
  | =======
  | 
  | 1/ En effet dans ce cas, la table PARA_CALC est crée seulement avec les paramètres NUME_REUSE et INST. Mais comme elle existe, elle n'est 
  | pas recréée avec tous les paramètres lors de la reprise, ce qui provoque l'erreur.
  | 
  | 2/ On peut en effet remarque en comparant les deux .mess que le bilan des énergies TOTAL est égal à l'incrément d'énergie à l'instant suivant
  | la reprise.
  | 
  | Ce comportement semble connu. En effet, dans le routine nmdoet.F90 il y a l'émission d'un message d'information quand on a un ETAT_INIT et 
  | le mot-clé ENERGIE (ETATINIT_5). 
  | 
  | Cette information est bien présente dans le .mess du test en deux étapes :
  | 
  | "A l'instant initial, tous les termes du bilan d'énergie sont nuls bien qu'un état
  |  initial non vierge soit renseigné. Le bilan d'énergie indique la variation des différents
  |  termes d'énergie entre deux instants de calcul consécutifs ainsi que leur variation
  |  totale entre l'instant courant et l'instant initial.
  | "
  | 
  | Modification :
  | ============
  | 
  | 1/ On fait les modifications nécessaire à ce que la table soit créée avec tous les paramètres que le mot-clé ENERGIE soit présent ou non.
  | Validation : dans sdnv106a, on enchaine un STAT_NON_LINE et 2 DYNA_NON_LINE. On tombe sur cette erreur si on ajoute ENERGIE aux DYNA_NON_LINE.
  | Le test passe avec la correction.
  | 
  | 2/ On crée la routine nonlinDSEnergyInitValues qui récupère les infos dans le résultat fourni par ETAT_INIT. Elle est appelée par nmdoet.
  | Dans le cas où l'état initial n'est pas un résultat, on garde l'émission de l'information dans le .mess.
  | Validation :
  | Le développement a été validé par le test fourni (on ne restitue pas cette modification car il s'agit d'un test forma). 
  | Dans sdnv106a, on ajoute un TEST_TABLE sur ENER_CIN pour le dernier pas de calcul (non-regression). Le test est NOOK sans correction.
  | 
  | Impact doc :
  | ==========
  | 
  | R4.09.01 : Mise à jour du paragraphe sur l'état énergétique initial.
  | 
  | Report en v15
  | 
  | Resultats faux:
  | ==============
  | Les bilans énergétiques totaux sont faux en cas de reprise car ils sont réinitialisés à 0 au lieu de repartir de la valeur au pas précédent. Cependant,
  | l'utilisateur est prévenu par la doc et par le message d'info. donc pas de résultats faux.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : R4.09.01
- VALIDATION : sdnv106a


================================================================================
                RESTITUTION FICHE 32811 DU 22/04/2023
================================================================================
- AUTEUR : LORENTZ Eric
- TYPE : anomalie concernant code_aster (VERSION 16.3)
- TITRE : AFFE_CARA_ELEM / MASSIF avec éléments d'interface
- FONCTIONNALITE :
  | Demande :
  | =======
  | 
  | Les éléments d'interface *_INTERFACE et *_INTERFACE_S ne sont pas reconnus comme des éléments massifs dans AFFE_CARA_ELEM / MASSIF. Quand il n'y a pas d'autres
  | éléments massifs (ou reconnus comme tel), AFFE_CARA_ELEM s'arrête, à tort. En effet, il faut pouvoir affecter la direction normale via AFFE_CARA_ELEM.
  | 
  | Je propose d'ajouter les éléments finis concernés dans la liste de ceux reconnus comme massifs.
  | 
  | Modifications :
  | =============
  | 
  | Etienne a reproduit l'erreur sur sslv01a en retirant les 3D du modèle. Le problème disparaît avec la correction.
  | 
  | Rem :
  | Le problème est finalement plus apparu car les éléments GRAD_VARI_INCO ne sont pas non plus reconnus comme MASSIF. En pratique, on peut d'ailleurs s'étonner que
  | le test "être massif" ne se préoccupe pas des éléments sur lesquels on cherche à affecter un repère mais soit simplement "existe-t-il des éléments massifs dans
  | le modèle". En l'état, il n'est pas certain que le test serve à quelque chose...
  | 
  | Etienne a remarqué au passage qu'on plantait sur un assert (de jevech) si les orientations ne sont pas fournies sur les éléments d'interface.
  | Il propose de remplacer le jevech par un tecach(ONO) et d'émettre le message suivant en cas de retour non nul :
  | 
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <F> <JOINT1_3>                                                                                 ║
  |  ║                                                                                                ║
  |  ║ Les données d'orientation sont indisponibles pour cet élément d'interface.                     ║
  |  ║                                                                                                ║
  |  ║     Vérifiez que vous les avez bien déclarées dans AFFE_CARA_ELEM/MASSIF.                      ║
  | 
  | 
  | Validation : Pas de modification sur sslv01a. Elle consisterait à créer un modèle avec uniquement des éléments d'interface puis de créer un CARA_ELEM sur ce
  | modèle.
  | On ne ferait ensuite rien de ce CARA_ELEM

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : RAS


================================================================================
                RESTITUTION FICHE 32671 DU 07/03/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Erreur non claire dans CREA_MAILLAGE avec DECOUPE_LAC
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | La fonction DECOUPE_LAC plante avec le maillage disponible dans cette fiche.
  | L'erreur renvoyée n'est pas claire :
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <EXCEPTION> <JEVEUX1_13>                                                                       ║
  |  ║                                                                                                ║
  |  ║ Le répertoire de noms &CATA.TM.NOMTM          $$XNUM contient 74 points d'entrée, la requête   ║
  |  ║ sur le numéro 2305843009213693952 est invalide.                                                ║
  |  ║ Ce message est un message d'erreur développeur.                                                ║
  |  ║ Contactez le support technique.                                                                ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | Analyse :
  | =======
  | 
  | Code :
  | ---- 
  | la routine cnmpmc construit la connectivité maille de peau/maille de corps pour l'opération découpe_lac.
  | Si on regarde pour la première maille esclave (surfacique) quel est le numéro de maille volumique trouvé on obtient 0. Ce qui cause l'erreur
  | constatée par l'utilisateur.
  | 
  | On voit dans la routine qu'il y a bien un ASSERT si le nombre de mailles candidates trouvées est nul. Or dans le traitement tel qu'il est
  | fait, on a toujours à ce moment au moins une maille (la maille esclave dont on recherche la maille volumique correspondante).
  | Il faut donc juste passer de 
  | if (ntrou .gt. 0) then
  | 
  | à 
  | if (ntrou .gt. 1) then
  | 
  | Avec ça le test s'arrête dans l'ASSERT.
  | 
  | Maillage :
  | --------
  | 
  | J'ai ensuite cherché à comprendre pourquoi on tombait sur ce problème avec le maillage fourni.
  | En fait (comme on peut le voir sur la photo jointe), les mailles surfaciques sont deux fois plus petites que les mailles volumiques.
  | Il n'y a donc pas correspondance des connectivités.
  | 
  | 
  | Modifications :
  | =============
  | 
  | 1- On modifie la condition sur nbtrou décrite plus haut et on remplace l'assert par le message suivant (Erreur fatale) :
  | 
  | 
  |     2: _("""Opération DECOUPE_LAC : 
  | pour au moins une maille surfacique déclarée dans GROUP_MA_ESCL, 
  | on ne trouve pas de maille volumique dont cette maille serait une des faces."""),
  | 
  | 2- Toujours dans la même routine, on fait un "call utlisi('DIFFE'" pour supprimer de la liste des mailles trouvées la maille esclave dont
  | on recherche la maille surfacique.
  | Or pour faire ce traitement on fait la différence avec toutes les mailles esclaves. C'est à mon avis un excès de prudence qui coûte plus cher
  | que de simplement donné le maille en question.
  | 
  | Report en v15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test fourni


================================================================================
                RESTITUTION FICHE 32480 DU 12/12/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.2.18 - dégradation de l'opérateur DEFI_GROUP
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | En version 16.2.18 - dégradation de l'opérateur DEFI_GROUP.
  | 
  | 
  | Analyse :
  | =======
  | 
  | La révision en cause est la suivante :
  | 
  | 
  | changeset:   16150:37df9753d47d
  | user:        Nicolas Pignet <nicolas.pignet@edf.fr>
  | date:        Thu Dec 01 16:47:21 2022 +0100
  | summary:     [#32440] Replace NOEUD and MAILLE
  | 
  | 
  | J'ai fait différents tests et j'ai pu constater que l'utilisation des routines addGroupElem et addGroup_Node depuis op0104 est responsable de la dégradation.
  | Ce qui est bizarre au premier abord est que cette routine remplace ligne par ligne ce qui était fait dans op0104.
  | 
  | La seule différence est l'utilisation de pointeurs dans addGroupxxxx contre des adresses jeveux dans op0104.
  | En modifiant cela dans addGroupElem et addGroupNode on retrouve les mêmes temps de calcul qu'avant cette révision.
  | 
  | Rmq: J'ai fait un peu de profiling. C'est effectivement beaucoup plus long dans jeveuo de récupérer un pointeur qu'une adresse (car pour le pointeur on commence
  | déjà par récupérer l'adresse). Au moins un facteur 10 sur les collections. Pour un vecteur, la différence est beaucoup plus faible
  | 
  | Modification :
  | ============
  | 
  | On remplace les pointers par des adresses jeveux dans addGroupElem et addGroupNode.
  | Le temps de calcul redeviennent comme avant.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv258b


================================================================================
                RESTITUTION FICHE 32969 DU 26/06/2023
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.3.25, le cas test sdlv102b est en erreur sur GAIA
- FONCTIONNALITE :
  | Problème
  | --------
  | En version 16.3.25, le cas test sdlv102b est en erreur sur GAIA
  | 
  | 
  | Solution
  | --------
  | Je ne parviens pas à reproduire le problème.
  | J'en profite pour faire un peu de ménage dans le test et améliorer la qualité de la programmation dans 
  | dynamic_substructuring.
  | 
  | PS : j'en profite pour corriger un message d'erreur dans EquationNumbering.h

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Passage du test
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30761 DU 08/03/2021
================================================================================
- AUTEUR : NGUYEN Thuong Anh
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Reprise du cas-test sdld33b_ajout reac_noda
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | ajout des calculs de réaction d'appuis dans le cas-test sdld33b
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | dans le fichier sdld33b.comm: ajout des TEST_RESU pour REAC_NODA
  | dans la doc v2.01.033: ajout des formules analytiques pour REAC_NODA ainsi que les valeurs de 
  | références analytiques
  | 
  | 
  | Résultat faux
  | -------------
  | Pas de résultats faux

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : v2.01.033
- VALIDATION : passage du cas-test
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32693 DU 14/03/2023
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : aide utilisation concernant code_aster (VERSION 11.8)
- TITRE : DYNA_NON_LINE - Souci lors du calcul de l'accélération initiale
- FONCTIONNALITE :
  | On transforme la fiche en AOM.
  | 
  | Le problème est celui d'un calcul de dynamique non-linéaire, avec un simple chargement de gravité qu'on applique de deux manières différentes. Les deux
  | chargements ne permettent pas de converger, ce qui semble étrange a priori.
  | 
  | Il y a deux chargements de gravité, appliqués avec une fonction différente:
  | Chargement 1: VALE=[0.0, 0.0, 0.01, 0.0, 0.2, 1.0],
  | Chargement 2: VALE=[0.0, 0.0, 0.2, 1.0],
  | 
  | Premier constat:
  | Dans les deux cas de chargement, on calcule bien une accélération initiale nulle. Comme prévu.
  | 
  | Par contre, au premier instant de calcul (5.E-3), on a un comportement différent entre les deux chargements.
  | 
  | En effet, la première fonction continue à donner zéro, pas la deuxième. Ce qui fait que le premier chargement donne une solution triviale: tout est nul
  | (déplacements, vitesse, accélération). Il n'y aucune force extérieure (pas de gravité, ni de réactions d'appuis).
  | 
  | Or le critère de convergence en dynamique pour RESI_GLOB_RELA est évalué ainsi:
  | 
  | Fext-(Fint+Freac) / (Freac + M . accel)
  | 
  | Chargement 1: RESI_GLOB_MAXI est strictement nul (résultat trivial), on ne peut pas mettre à l'échelle car Freac + M Accel est nul aussi. Pour éviter de diviser
  | par zéro, on met -1. Le calcul ne converge pas
  | 
  | Chargement 2: Ca se passe beaucoup mieux. 
  | 
  | Le résultat est donc normal, il n'y a pas d'anomalie.
  | 
  | Néanmoins, il y a un constat: on ne peut pas faire converger quand la solution est nulle ! Ce qui est très embêtant en dynamique car c'est fréquent d'avoir des
  | phases de "vol libre" (mouvements de corps rigide)
  | 
  | On avait mis en place un mécanisme qui basculait automatiquement de RESI_GLOB_RELA en RESI_GLOB_MAXI, mais on n'a jamais réussi à le faire fonctionner
  | correctement parce qu'il n'y a pas de référence pour le zéro tant qu'on a pas convergé au moins une seule fois.
  | 
  | Une solution  est effectivement de passer par RESI_REFE_RELA.
  | On pourrait aussi faire évoluer ce critère pour y ajouter une vitesse ou une accélération de référence. Si l'accélération du solide est "nulle", alors on
  | converge ?
  | 
  | En tout cas, pas d'anomalies.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 27985 DU 29/08/2018
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Adapter CREA_MAILLAGE pour un parallel_mesh
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Quand on utilise CREA_MAILLAGE sur un ParallelMesh, la commande produit un maillage standard. Il faudrait alors produire un
  | ParallelMesh qui respecte la même distribution que son ancêtre.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | Déjà fait. Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.0


================================================================================
                RESTITUTION FICHE 25953 DU 17/01/2017
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 11.4)
- TITRE : D124.19 - Améliorations pour DECOUPE_LAC
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Suite à Issue23885 sur DECOUPE_LAC, il reste deux points à traiter:
  | 
  | * Gérer le problème de nommage des noeuds et éléments (mot-clef PREF_NOEUD/PREF_NUME)
  | * Créer un GROUP_NO spécifique qui contiendra tous les noeuds ayant un lagrangien de
  | contact (mot-clef à ajouter pour que l'user donne ce nom)
  | 
  | Correction/Développement
  | ------------------------
  | 
  | DECOUPE_LAC est voué à disparaître
  | 
  | On ne fait rien.
  | 
  | 
  | Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.5

