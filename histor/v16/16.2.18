==================================================================
Version 16.2.18 (révision 29eb8acf5489) du 2022-12-01 16:46 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32440 PIGNET Nicolas           Mot-clés non-testés
 32447 PIGNET Nicolas           catalogues de FORMULE et peut-être DEFI_NAPPE
 32306 ZAFATI Eliass            Message d'erreur PREPOST4_97 n'est pas à jour
 32411 GRIMAL Etienne           RELATION_KIT et RGI_BETON_BA
 32421 COURTOIS Mathieu         [HPC] Parallélisme massif IMPR_RESU("MED", FICHIER_UNIQUE="OUI") plante
 32319 COURTOIS Mathieu         CALC_TABLE et parallélisme distribué
 32336 SELLENET Nicolas         Problème avec ssnp14a / MPI / MED fichier unique
 32430 SELLENET Nicolas         [HPC] Impression MED parallélisme distribué avec shared_tmpdir
 32423 COURTOIS Mathieu         Remplissage repertoire de travail /tmp ou /SCRATCHDIR - ouverture base
 32278 COURTOIS Mathieu         En version 16.2.5, dégradations perf de certains tests CALC_MISS
 32326 COURTOIS Mathieu         Méthode addDependency fonctionnelle ?
 32385 COURTOIS Mathieu         Problème CALC_CHAMP/reuse
 32410 PIGNET Nicolas           En version 16.2.15 , dégradation performance ASSEMBLAGE
 32435 PIGNET Nicolas           Plantage gros calculs MPI sur cronos
 32032 ABBAS Mickael            [Suivi_Etude_AQ] Calcul SNS pour dossier CSC/RIS
 31991 ABBAS Mickael            [Suivi_Etude_AQ] Etude de cloquage du liner (projet NGC)
 32454 ABBAS Mickael            Correction à apporter à la fiche 32126
 32439 SELLENET Nicolas         Test sslp116e
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32440 DU 30/11/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Mot-clés non-testés
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Suite au chantier que j'avais fait pour enlever le maximum de mot-clé NOEUD/MAILLE des cas-tests il y a des mot-clés non testés.
  | 
  | Je ne sais pas pourquoi on n'avait rien vu.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Les mots-clé cachés ne sont pas pris en compte dans le script de couverture donc on n'a pas vu que certains ne sont pas testés.
  | 
  | Je supprime ceux que j'ai trouvé (uniquement lié à NOEUD et MAILLE a priori)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32447 DU 01/12/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : catalogues de FORMULE et peut-être DEFI_NAPPE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Dans le catalogue de FORMULE, il serait judicieux d'ajouter validators=NoRepeat() pour NOM_PARA 
  | 
  | NOM_PARA = SIMP(statut='o', typ='TXM', validators=NoRepeat(), max='**')
  | 
  | et pour éviter de passer lorsque NOM_PARA=('toto', ' toto ') d'ajouter dans
  | "code_aster/Commands/formule.py" pour "change_syntax" un truc du genre
  | 
  | keywords['NOM_PARA'] = [ aa.strip() for aa in keywords['NOM_PARA'] ]
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Je rajoute uniquement un validators=NoRepeat() dans NOM_PARA de FORMULE
  | 
  | J'ai du modifier test_compor et sdlv127f (qui je ne sais pas comment marcher avant)
  | 
  | Report v15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32306 DU 03/10/2022
================================================================================
- AUTEUR : ZAFATI Eliass
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Message d'erreur PREPOST4_97 n'est pas à jour
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Le message d'erreur fatale PREPOST4_97 fait allusion aux mots-clé GROUP_MA_VOL en 3D et GROUP_MA_SURF en 2D, or cela a été harmoniser
  | avec le mot-clé GROUP_MA_INTERNE dans la fiche issue29713.
  | 
  | Correction :
  | ==========
  | 
  | Mise à jour du message :
  | 
  |     97 : _("""
  |  La maille de peau : %(k1)s ne peut pas être réorientée.
  |  Car elle est insérée entre 2 mailles "support" placées de part et d'autre : %(k2)s et %(k3)s.
  | 
  | Conseils :
  |  Vous pouvez utiliser le mot-clé GROUP_MA_INTERNE pour choisir une des
  |  deux mailles supports et ainsi choisir la normale permettant la réorientation.
  | """),

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras


================================================================================
                RESTITUTION FICHE 32411 DU 21/11/2022
================================================================================
- AUTEUR : GRIMAL Etienne
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : RELATION_KIT et RGI_BETON_BA
- FONCTIONNALITE :
  | Problème :
  | ========
  | Suite à la restitution de la loi RGI_BETON_BA, un manque de couverture apparaît avec l'utilisation de cette loi en THM.
  | 
  | Correction :
  | ==========
  | 
  | En fait toutes les lois sont par défaut autorisées en THM (dans le catalogue).
  | On décide de n'autoriser que les lois (mécaniques) effectivement testées en THM dans la base src. Ce qui donne :
  | 
  |  into=('BARCELONE',
  |                           'CAM_CLAY',
  |                           'CJS',
  |                           'CZM_EXP_REG',
  |                           'CZM_LIN_REG',
  |                           'DRUCK_PRAGER',
  |                           'DRUCK_PRAG_N_A',
  |                           'ELAS',
  |                           'ENDO_ISOT_BETON',
  |                           'GONF_ELAS',
  |                           'HOEK_BROWN_EFF',
  |                           'HOEK_BROWN_TOT',
  |                           'HUJEUX',
  |                           'Iwan',
  |                           'JOINT_BANDIS',
  |                           'LAIGLE',
  |                           'LETK',
  |                           'LKR',
  |                           'MAZARS',
  |                           'MFRONT',
  |                           'MOHR_COULOMB',
  |                           'RANKINE',
  |                           'VISC_DRUC_PRAG',
  |                           'VISC_MAXWELL', 
  |                           'VISC_MAXWELL_MT')
  | 
  | 
  | Validation :
  | ----------
  | Lancement des tests KIT_"THM" au sens large de la base de src et vérification à l’œil dans validation.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras


================================================================================
                RESTITUTION FICHE 32421 DU 23/11/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [HPC] Parallélisme massif IMPR_RESU("MED", FICHIER_UNIQUE="OUI") plante
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | IMPR_RESU/FICHIER_UNIQUE en erreur.
  | 
  | 
  | Correction
  | ----------
  | 
  | Effectivement, on demande d'imprimer dans un fichier unique sans fournir le chemin vers ce fichier.
  | Donc chaque processeur essaie d'écrire dans "fort.NN"... sauf qu'il y a un fichier "fort.NN" dans chaque répertoire de travail, pas vraiment unique !
  | 
  | On arrête l'exécution en indiquant qu'il faut fournir un chemin absolu (en utilisant DEFI_FICHIER) :
  | 
  | ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  | ║ <F> <MED_11> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ║
  | ║ . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  | ║ On essaie d'imprimer dans 'REPE_OUT/maillage_proj_loin_00000001.med'. . . . . . . . . . . . . .║
  | ║ Pour imprimer des résultats au format MED dans un unique fichier, il est nécessaire . . . . . .║
  | ║ de préciser un chemin absolu vers ce fichier (doit commencer par '/'). . . . . . . . . . . . . ║
  | ║ . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  | ║ Risque & Conseil : Utiliser DEFI_FICHIER pour associer l'unité logique à un chemin absolu. . . ║
  | ║ . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  | ║ . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  | ║ Cette erreur est fatale. Le code s'arrête. . . . . . . . . . . . . . . . . . . . . . . . . . . ║
  | ╚════════════════════════════════════════════════════════════════════════════════════════════════╝

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz502p sans DEFI_FICHIER
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32319 DU 13/10/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : CALC_TABLE et parallélisme distribué
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En parallèle HPC, POST_RELEVE_T produit une table vide sur un des processus lors de l'extraction de valeurs.
  | CALC_TABLE plante ensuite avec une erreur 'Table vide'.
  | 
  | 
  | Correction
  | ----------
  | 
  | Dans ce cas, CALC_TABLE doit retourner une table vide, c'est cohérent.
  | On garde le message "TABLE0_35: Table vide" que l'on transforme en alarme pour attirer l'attention de l'utilisateur.
  | 
  | Puis, il suffit de gérer le cas où la table est vide ou pas dans la suite de l'étude :
  | 
  | tab_ouv = OUVCMOD.EXTR_TABLE().COOR_Z == 0.0
  | if tab_ouv:
  |  . .val_ouv = tab_ouv.CMOD.values()

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test joint
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32336 DU 21/10/2022
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Problème avec ssnp14a / MPI / MED fichier unique
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Je constate (comprendre "je n'explique pas") que si je lance ssnp14a avec run_ctest seul, il passe correctement. Si je le passe avec  un autre test (j'ai pris
  | supv003c), il est systématiquement en échec.
  | 
  | 
  | Analyse :
  | ---------
  | Cela se produit avec les prérequis scibian 10 (pas avec scibian 9).
  | 
  | Le test est séquentiel mais fait un IMPR_RESU FICHIER_UNIQUE='OUI'.
  | 
  | C'est un problème d'environnement (au sens le plus flou du terme).
  | 
  | J'ai vérifié les variables d'environnement, le mode de lancement et les ulimit et tout est pareil.
  | 
  | J'ai trouvé un ticket dans le bugtracker OpenMPI qui traite du sujet. Je pense que c'est un bug OpenMPI avec l'écriture de fichier en MPI :
  | https://github.com/open-mpi/ompi/issues/4336
  | 
  | De plus si je mets FICHIER_UNIQUE='NON', alors le test passe à tous les coups.
  | 
  | 
  | Contournement :
  | ---------------
  | Ne pas prendre en compte le mot-clé FICHIER_UNIQUE dans le cas où le calcul est séquentiel.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnp14a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32430 DU 25/11/2022
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [HPC] Impression MED parallélisme distribué avec shared_tmpdir
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Pour imprimer un résultat de calcul réalisé en parallélisme distribué, l'utilisateur a voulu tenter la piste de la fiche issue31512.
  | 
  | Malheureusement, ça plante en erreur DVP_1.
  | 
  | 
  | Solution :
  | ----------
  | Le plantage a lieu quand on essaie d'imprimer un champ aux points de Gauss. Ce champ n'est défini que sur une partie du maillage et il se trouve que certains
  | processeurs n'ont pas de valeur pour ce champ à imprimer.
  | 
  | Malgré tout avec MED même si un proc n'a rien à imprimer, il faut qu'il passe quand même dans les mêmes routines d'impression que les autres.
  | 
  | Là, on plante au moment où on cherche le mode local du champ pour créer la famille de point de Gauss (qui même si elle est unique doit être donnée à l'identique
  | par tous les procs).
  | 
  | Je rajoute une comm pour que celui qui a trouvé le mode local le partage aux autres.
  | 
  | En faisant cela, l'étude passe sans problème.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : unitaire
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32423 DU 23/11/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Remplissage repertoire de travail /tmp ou /SCRATCHDIR - ouverture base
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Échec à la relecture d'une base avec le message :
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <EXCEPTION> <JEVEUX_40> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  |  ║ . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  |  ║ Erreur écriture de l'enregistrement 159054 sur la base : GLOBALE 10 . . . . . . . . . . . . . .║
  |  ║ . . .code retour : -4 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .║
  |  ║ . . .Erreur probablement provoquée par une taille trop faible du répertoire de travail. . . . .║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | 
  | Correction
  | ----------
  | 
  | L'erreur semble venir du passage de --max_base lors de la POURSUITE.
  | En effet, c'est curieux que Jeveux essaie d'écrire quelque chose dans la glob.10 alors qu'on doit
  | relire 11 fichiers composant la base.
  | Il ne peut pas écrire car ce fichier est déjà plein.
  | Si on supprime l'argument --max_base, la relecture fonctionne (avec les différentes versions
  | testées : 15.4, 16.2, unstable).
  | 
  | On corrige en ignorant l'argument --max_base en POURSUITE.
  | Avant POURSUITE, on aura l'avertissement suivant :
  |    [1,2]<stdout>:WARNING: '--max_base' is ignored with POURSUITE.
  | 
  | Cela me semble compliqué (risqué) d'autoriser l'augmentation de max_base d'une exécution à l'autre
  | car il y a plusieurs paramètres stockés redondants : taille d'un fichier, taille d'un enregistrement,
  | nombre d'enregistrement.
  | 
  | On décide de mettre une valeur très grande par défaut de manière à ne pas devoir utiliser --max_base (2 To).
  | >>> --max_base ne devrait pas être utilisé dorénavant <<<
  | 
  | En POURSUITE, si la base précédente a été créée avec max_base différent de la valeur par défaut,
  | le nombre d'enregistrement (la longueur d'un enregistrement ne change pas en général) est stocké
  | dans la base. On peut donc recalculer la valeur de max_base initialement utilisée.
  | Si elle est différente, on verra le message :
  | 
  | Ajustement de la taille maximale des bases à 0.10 Go.
  | (cas-test zzzz103c où on force une taille très petite dans le .comm initial)
  | 
  | Autre problème levé par hasard : test qui lève une exception, avant que l'exception soit récupérée,
  | changements en mémoire, jeveux qui émet un message d'information.
  | Cela conduit à réinitialiser l'objet qui stocke les informations sur l'exception.
  | Il ne faut pas le faire tant que le premier appel n'est pas clos (en cas d'appel récursif).
  | En cas de message d'erreur, on s'arrêtera avec le message CATAMESS_55 (appels récursifs de messages d'erreur).
  | 
  | 
  | 
  | Report v15.
  | 
  | 
  | Remarque sur l'accès /scratch depuis les deux noeuds sur lesquels ont été répartis les 6 process :
  | 1200 s pour POURSUITE/FIN pour les 3 procs du 1er noeud, 2200 s sur les 3 procs du 2nd. Une seule fois
  | sur plusieurs runs mais ça peut arriver.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz103c
- NB_JOURS_TRAV  : 1.5


================================================================================
                RESTITUTION FICHE 32278 DU 16/09/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.2.5, dégradations perf de certains tests CALC_MISS
- FONCTIONNALITE :
  | Le temps d'exécution de quelques tests Miss a augmenté.
  | 
  | L'origine est l'ajout de l'option '-fno-frontend-optimize'.
  | Sans cette option, les "read/write implicit loop" sont clairement incorrects.
  | 
  | La question pourra se reposer avec les prochaines versions du compilateur.
  | Pour le moment, on garde cette option 'safe'.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests Miss ok
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32326 DU 14/10/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Méthode addDependency fonctionnelle ?
- FONCTIONNALITE :
  | Sans suite, revoir la syntaxe du print...
  | 
  | print("dépendances de resuwb = ".format(rmec.getDependencies()))
  | => il n'y a pas de champ à formater.
  | 
  | 
  | dépendances de resuwb = [<libaster.Model object at 0x7f9d0d55bf80>, <libaster.MaterialField object at 0x7f9d19f428f0>, <libaster.MechanicalDirichletBC object at
  | 0x7f9d0d54e3e8>, <libaster.MechanicalLoadFunction object at 0x7f9d0d54e260>, <libaster.TimeStepper object at 0x7f9d0d556730>, <libaster.Material object at
  | 0x7f9d1a467928>]

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32385 DU 09/11/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 16.2)
- TITRE : Problème CALC_CHAMP/reuse
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | L'utilisation de reuse dans CALC_CHAMP plante après STAT_NON_LINE selon :
  | AttributeError: 'tuple' object has no attribute 'build'
  | 
  | CALC_CHAMP sans reuse fonctionne.
  | 
  | 
  | Analyse
  | -------
  | 
  | statDDS = (
  |     STAT_NON_LINE(
  |         MODELE=MO,
  |         ...
  |     ),
  | )
  | 
  | Donc statDDS est un tuple qui contient `( <evol_noli>, )`
  | 
  | statDDS = STAT_NON_LINE() et ça fonctionne correctement.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : erreur de mise en données
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32410 DU 21/11/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.2.15 , dégradation performance ASSEMBLAGE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | En version 16.2.15 il y a une dégradation des temps CPU sur les cas-tests appelant assemblages (rapport temps CPU cronos - cf 3.1 cr_eda e20221121) :
  | 
  | 
  | 
  | 
  | Degradations / ameliorations des performances des cas-tests entre les dates : 2022-11-18 et 2022-11-20
  | Nombre de commandes comparees : 10019
  |  Degradations :
  |  62.6% elsa01c    ASSEMBLAGE_19          2.94     4.78
  |  59.4% elsa01b    ASSEMBLAGE_17          5.77     9.20
  |  58.5% sdll135d   ASSEMBLAGE_12         42.18    66.87
  |  58.0% sdnl131a   ASSEMBLAGE_181         2.19     3.46
  |  58.0% sdnl131a   ASSEMBLAGE_15          2.19     3.46
  | 
  | Degradations / ameliorations des performances des cas-tests entre les dates : 2022-11-13 et 2022-11-18
  | Nombre de commandes comparees : 10008
  |  Degradations :
  |  62.6% sdll14c    ASSEMBLAGE_10          1.55     2.52
  |  59.9% sdlx02f    ASSEMBLAGE_9           1.57     2.51
  |  59.4% sdll14e    ASSEMBLAGE_9           0.69     1.10
  |  59.4% sdll14a    ASSEMBLAGE_9           0.69     1.10
  |  57.5% sdnl113a   ASSEMBLAGE_23          1.93     3.04
  |  57.5% sdll14b    ASSEMBLAGE_9           2.40     3.78
  |  57.2% sdlx02e    ASSEMBLAGE_8           2.43     3.82
  |  56.5% sdll14d    ASSEMBLAGE_9           0.69     1.08
  |  56.2% sdlx02d    ASSEMBLAGE_8           0.73     1.14
  |  56.1% ssll106c   ASSEMBLAGE_49          1.57     2.45
  | 
  | 
  | Correction/Développement
  | ------------------------
  | J'ai repris le test en local car ça bougre trop sur cronos. Je confirme le diagnostique d'Alexandre. 
  | 
  | Le test était lent en 16.3.15 mais revenu à la normal en 16.3.16. Donc rien à faire
  | 
  | Sans source
  | 
  | Il y a eu un problème qui a été réglé depuis.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdnl131a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32435 DU 28/11/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Plantage gros calculs MPI sur cronos
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | [1,6]<stdout>:|   752        X | 5.28024E-06  X | 7.31228E-01    | 1.21237E+01    |                | 5.99285E-01    |
  | [1,2]<stderr>:[crbm0315:5494 :0:5494] Caught signal 7 (Bus error: nonexistent physical address)
  | [1,2]<stderr>:/scratch/users/C84550/C84550-crbm0315-batch_21416914/global/mpi_script.sh : ligne 37 :  5494 Erreur du bus           (core
  | dumped)/software/restricted/simumeca/aster/install/14.8/mpi/bin/aster /software/restricted/simumeca/aster/install/14.8/mpi/lib64/aster/Execution/E_SUPERV.py
  | -commandes fort.1 --num_job=21416914 --mode=batch --rep_outils=/fscronos/software/restricted/simumeca/aster/generic/asrun-2020.0.1-2/outils
  | --rep_mat=/software/restricted/simumeca/aster/install/14.8/mpi/share/aster/materiau
  | --rep_dex=/software/restricted/simumeca/aster/install/14.8/mpi/share/aster/datg --numthreads=1 --suivi_batch --tpmax=360000.0 --memjeveux=10125.0
  | [1,2]<stdout>:EXECUTION_CODE_ASTER_EXIT_21416914=135
  | [1,0]<stdout>:   
  | [1,0]<stdout>:   !------------------------------------------------------------------------------!
  | [1,0]<stdout>:   ! <EXCEPTION> <APPELMPI_97>                                                    !
  | [1,0]<stdout>:   !                                                                              !
  | [1,0]<stdout>:   !     Le délai d'expiration de la communication est dépassé.                   !
  | [1,0]<stdout>:   !     Cela signifie que le processeur #0 attend depuis plus de 40091 secondes, !
  | [1,0]<stdout>:   !     ce qui n'est pas normal.                                                 !
  | [1,0]<stdout>:   !------------------------------------------------------------------------------!
  | [1,0]<stdout>:   
  | [1,0]<stdout>:   
  | [1,0]<stdout>:   !-----------------------------------------------------------!
  | [1,0]<stdout>:   ! <EXCEPTION> <APPELMPI_96>                                 !
  | [1,0]<stdout>:   !                                                           !
  | [1,0]<stdout>:   !   Le processeur #2 n'a pas répondu dans le délai imparti. !
  | [1,0]<stdout>:   !   Communication de type 'MPI_IRECV' annulée.              !
  | [1,0]<stdout>:   !-----------------------------------------------------------!
  | [1,0]<stdout>:   
  | [1,0]<stdout>:  On demande au processeur #1 de s'arrêter ou de lever une exception.
  | [1,1]<stdout>:  Le processeur #0 demande d'interrompre l'exécution.
  | [1,1]<stdout>:   
  | [1,1]<stdout>:   !------------------------------------------------------------------------!
  | [1,1]<stdout>:   ! <EXCEPTION> <APPELMPI_95>                                              !
  | [1,1]<stdout>:   !                                                                        !
  | [1,1]<stdout>:   !     Tous les processeurs sont synchronisés.                            !
  | [1,1]<stdout>:   !     Suite à une erreur sur un processeur, l'exécution est interrompue. !
  | [1,1]<stdout>:   !------------------------------------------------------------------------!
  | [1,1]<stdout>:   
  | [1,0]<stdout>:  On demande au processeur #3 de s'arrêter ou de lever une exception.
  | [1,0]<stdout>:  On demande au processeur #4 de s'arrêter ou de lever une exception.
  | [1,0]<stdout>:  On demande au processeur #5 de s'arrêter ou de lever une exception.
  | [1,0]<stdout>:  On demande au processeur #6 de s'arrêter ou de lever une exception.
  | [1,0]<stdout>:  On demande au processeur #7 de s'arrêter ou de lever une exception.
  | 
  | 
  | Ou :
  | 
  | [1,5]<stdout>:|   335        X | 3.64225E-05  X | 5.62686E-02    | 4.40816E-01    |                | 5.07925E-01    |
  | [1,0]<stdout>:|   335        X | 3.64225E-05  X | 5.62686E-02    | 4.40816E-01    |                | 5.07925E-01    |
  | [1,2]<stdout>:|   335        X | 3.64225E-05  X | 5.62686E-02    | 4.40816E-01    |                | 5.07925E-01    |
  | [1,4]<stdout>:|   335        X | 3.64225E-05  X | 5.62686E-02    | 4.40816E-01    |                | 5.07925E-01    |
  | [1,7]<stdout>:|   335        X | 3.64225E-05  X | 5.62686E-02    | 4.40816E-01    |                | 5.07925E-01    |
  | sh : ligne 1 : 45071 Erreur du bus           (core dumped)mpirun -np 8 --bind-to none --tag-output
  | /scratch/users/C84550/C84550-crbm0262-batch_21416878/global/mpi_script.sh
  | EXIT_COMMAND_44482_00000012=135
  | <INFO> Exécution Code_Aster terminée, diagnostic : <F>_ABNORMAL_ABORT
  | 
  | Correction/Développement
  | ------------------------
  | 
  | L'étude s'arrête à cause d'une erreur de bus sur cronos. Encore un problème du FileSystem
  | 
  | DSIT a fait une maintenant du FS dans le week-end ce qui a engendré le problème
  | 
  | C'est revenu à la normale. 
  | 
  | Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test Nicolas
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32032 DU 22/04/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : aide utilisation concernant code_aster (VERSION 11.8)
- TITRE : [Suivi_Etude_AQ] Calcul SNS pour dossier CSC/RIS
- FONCTIONNALITE :
  | Fiche chapeau pour les études en cours sur la SNS pour le dossier RIS/CSC
  | 
  | Il s'agit ici de noter différentes informations relatives à aster concernant ces études.
  | 
  | /!\ ces études sont réalisées en v14.8, sous AQ et en mode "urgent", non planifié dans un projet, mais rattaché à SAFER (PRISME)
  | 
  | issue32087 - Création d'une version 14.8 qualifiée pour la SNS
  | => nécessite l'intégration de issue25882
  | 
  | issue31931 - Correction of a memory overwrite in LIRE_RESU 
  | => OK, corrigé
  | 
  | issue25882 - Nouveau modèle pour la restauration d'écrouissage
  | => fonctionnelle, à restituer en version unstanle
  | 
  | issue32031 - PROJ_CHAMP et sa fameuse alarme CALCULEL5_48
  |  -> en lien avec les travaux réalisés par SG pour le transfert des calculs de SNS vers Coriolis
  | => OK, corrigé
  | 
  | Au programme de l'année prochaine:
  | issue32085 - Opérateur de mesure des quantités géométriques
  | issue32086 - Introduction de sources de chaleur de soudage pré-programmées

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 45.0


================================================================================
                RESTITUTION FICHE 31991 DU 07/04/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : aide utilisation concernant code_aster (VERSION 11.8)
- TITRE : [Suivi_Etude_AQ] Etude de cloquage du liner (projet NGC)
- FONCTIONNALITE :
  | Fiche chapeau pour les études en cours sur le cloquage du liner (projet NGC)
  | 
  | Cette étude contient plusieurs aspects, mais l'essentiel des fiches concerne l'élément COQUE_SOLIDE
  | 
  | Anomalies
  | =========
  | 
  | issue31962 - Problèmes de convergence du contact en MPI
  | => OK, corrigé
  | 
  | issue31934 - Améliorations de PRES_REP pour élément COQUE_SOLIDE
  | => OK, corrigé
  | 
  | issue30766 - Problème de perf de CREA_MAILLAGE/COQUE_SOLIDE
  | => OK, corrigé
  | 
  | issue31992 - AFFE_CHAR_MECA/LIAISON_PROJ avec des COQUE_SOLIDE
  | => OK, corrigé
  | 
  | issue31347 - Coque_solide Calcul thermique
  | => OK, corrigé
  | 
  | issue31288 - COQUE_SOLIDE - La dilatation thermique échoue en non-linéaire
  | => OK, corrigé
  | 
  | issue31254 - COQUE_SOLIDE ne sait pas imprimer des champs ELNO au format MED
  | => OK, corrigé
  | 
  | issue32139 - COQUE_SOLIDE : Erreur dans le calcul des distorsion
  | => OK, corrigé
  | 
  | issue31978 - COQUE_SOLIDE ne sait pas utiliser des matrices non-symétriques
  | => OK, corrigé
  | 
  | 
  | issue31328- [NGC] COQUE_SOLIDE & Pesanteur
  | => problème toujours présent
  | 
  | issue31979 - Temps de préparation du contact anormalement long
  | => problème toujours présent
  | 
  | 
  | Évolutions
  | ==========
  | 
  | issue30886 - Ajout de variables d'état externes (TEMP, SECH, HYDR) pour COQUE_SOLIDE
  | => OK, réalisé
  | 
  | Spécifique NGC
  | --------------
  | 
  | issue31362 - Discrets avec une LdC élastique non-linéaire
  | => A faire 2023
  | 
  | Dans PSM/3M
  | -----------
  | 
  | issue31287 - COQUE_SOLIDE - Épaisseur variable
  | => A faire
  | 
  | issue31278 - COQUE_SOLIDE - Calculer les grandeurs généralisées
  | => A faire
  | 
  | issue31213 - COQUE_SOLIDE - V&V - Ajout d'un test sur les quantités EPS*_NOEU
  | => A faire
  | 
  | issue32416 - COQUE_SOLIDE - Développer le calcul de l'option EPSG_ELGA
  | => A faire
  | 
  | issue30858 - COQUE_SOLIDE - Version multicouches
  | => A faire
  | 
  | issue30853 - COQUE_SOLIDE - Sortie de l'énergie de stabilisation
  | => A faire
  | 
  | issue30847 - COQUE_SOLIDE - Améliorer le contrôle des éléments mal-orientés
  | => A faire
  | 
  | issue32284 - Critère de 'Contact Rasant'
  | => A considérer dans 3M. la notion de contact rasant vit peut-être ses derniers instants avec l'introduction des nouveaux modèles

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 20.0


================================================================================
                RESTITUTION FICHE 32454 DU 02/12/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Correction à apporter à la fiche 32126
- FONCTIONNALITE :
  | Sans suite.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32439 DU 30/11/2022
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Test sslp116e
- FONCTIONNALITE :
  | Problème :
  | ----------
  | Le test sslp116e met ma machine à genoux à chaque submit.
  | 
  | Il consomme toute la RAM (32 Go) et l'OS se met à swapper.
  | 
  | 
  | Solution :
  | ----------
  | En fait, j'avais pas mal de trucs dans mon .petscrc. Et ça fait consommer toute la RAM de mon PC !
  | 
  | Fiche sans source.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sslp116e
- NB_JOURS_TRAV  : 1.0

