==================================================================
Version 16.3.15 (révision 033a1cc09f17) du 2023-04-06 16:10 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32666 COURTOIS Mathieu         Maillage de bloc fissure : correct ou pas pour un calcul EF ?
 32723 PIGNET Nicolas           [RUPT] Extension de POST_K1_K2_K3 aux éléments HHO
 32728 PIGNET Nicolas           la routine panbno.F90 ne sait pas gérer les TRIA7
 29418 PIGNET Nicolas           supprimer getvectjev et putvectjev
 32520 SELLENET Nicolas         Nouveau partitionneur
 32384 FERTÉ Guilhem            [Résorption] Modélisations second gradient _2DG
 31565 FERTÉ Guilhem            [Résorption] Chargement INTE_ELEC
 32724 PIGNET Nicolas           Scinder les blocs EXCIT de DYNA_VIBRA/HARM/GENE DYNA_VIBRA/HARM/PHYS
 32663 ALVES-FERNANDES Vinicius [ADELAHYD2025] Vérification de la masse dans POST_NEWMARK
 32703 ALVES-FERNANDES Vinicius [ADELAHYD2025] Calcul du facteur de sécurité statique/dynamique avec option R...
 32744 ABBAS Mickael            Ménage carte des critères de convergence CARCRI
 32710 ABBAS Mickael            [RS2024-CSM] - Nouvelles méthodes d'accès à ls SD Résultat
 32720 ALVES-FERNANDES Vinicius [ADELAHYD2025] Ajout d'un cas-test de comparaison de POST_NEWMARK au logiciel...
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32666 DU 02/03/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 16.2)
- TITRE : Maillage de bloc fissure : correct ou pas pour un calcul EF ?
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | On fait 2 AFFE_MODELE sur le même maillage.
  | Le premier émet des alarmes, plus de 5.
  | Le second est silencieux.
  | 
  | 
  | Correction
  | ----------
  | 
  | Au bout de 5 messages émis, une alarme est désactivée au sein d'une commande.
  | 
  | Le compteur n'était pas réinitialisé.
  | On le fait au début de chaque commande.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test joint
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32723 DU 28/03/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [RUPT] Extension de POST_K1_K2_K3 aux éléments HHO
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Autoriser l'opérateur POST_K1_K2_K3 sur une formulation HHO. 
  | L'utilisation du champ HHO_DEPL suffira.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | On corrige DEFi_FOND_FISS qui ne supportait pas les éléments biquadratiques sur les lèvres.
  | 
  | Dans POST_K1_K2_K3, on utilise le champs HHO_DEPL à la place de DEPL pour HHO. Ceci est une approximation raisonnable si le maillage est assez fin en pointe de
  | fissure.
  | 
  | On rajoute les tests ssnp103e,f qui compare HHO avec D_PLAN + BARSOUM. 0.3% d'écart sur K1 avec la méthode 3
  | 
  | Doc: V6.03.103

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V6.03.103
- VALIDATION : ssnp103e,f
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32728 DU 29/03/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : la routine panbno.F90 ne sait pas gérer les TRIA7
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Sur un maillage constitué de TRIA7 (merci HHO), la routine panbno.F90 renvoie une erreur F
  | 'MODELISA6_21'
  | 
  | C'est parce que cette routine ne gère pas les TRIA7 (ok pour QUAD9 et HEXA27).
  | Le TETRA11 ne semble pas être géré non plus
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Je rajoute le support du TRIA7 et le reste des mailles spécifiques à HHO en 3D
  | 
  | Test dans ssnp103a

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnp103a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29418 DU 17/12/2019
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 15.2)
- TITRE : supprimer getvectjev et putvectjev
- FONCTIONNALITE :
  | Travail effectué
  | ---
  | 
  | Suppression des getvectjev / putvectjev / getcolljev / putcolljev
  | 
  | Il en reste qui seront traiter dans issue32729
  | 
  | I / ObjectExt
  | 
  | * fieldonnode_ext / fieldoncell_ext EXTR_COMP
  | 
  | EXTR_COMP entièrement réécrit en c++. 
  | On supprime aster.prepcompcham et prcoch.F90
  | Rq : pour FieldOnCell on utilise toSimpleFieldOnCell et on manipule un temporaire SimpleFieldOnCell comme c'était fait en Fortran
  | 
  | * meshcoordinatesfield EXTR_COMP :
  | 
  | resorbé
  | 
  | * generalizedmodel :
  | 
  | on remplace par des méthodes c++
  | Rq : correction des noms d'attributs de GeneralizedModel
  | 
  | * table_ext
  | 
  | on réécrit __getitem__ et EXTR_TABLE avec de nouvelles méthodes c++
  |  getParameters() : retourne la liste des paramètres de la table ("X", "TEMP", "ORDRE", etc.)
  |  getNumberOfLines() : retourne le nombre de lignes de la table
  |  getColumn(para) : retourne les valeurs de la colonne pour le paramètre para
  | Rq : mise au propre des méthodes build() pour Table, TableContainer et diverses corrections dans les Commandes créant des Tables
  | 
  | II / Macros
  | 
  | On remplace les appels par la nouvelle API c++. On ajoute au besoin de nouvelles méthodes / attributs avec binding Python
  | 
  | Voici la liste des nouvelles méthodes / attributs :
  | 
  | * post_dyna_alea :
  | 
  | InterspectraMatrix::getNumI(), getNumJ(), getNoeI(), getNoeJ(), getCmpI(), getCmpJ()
  | 
  | * post_k_tran
  | 
  | GeneralizedResult::getDisplacement(), getNumberOfModes(), getAbscissasOfSamples(), getIndicesOfSamples()
  | 
  | * calc_stabilite
  | 
  | BaseDOFNumbering::getMorseStorage(), MatrixStorage::getRows() et getDiagonalPositions()
  | AssemblyMatrix::getUpperValues() et getLowerValues() : pour récupérer les matrices triangulaires supérieure et inférieure
  | 
  | * calc_essai
  | 
  | InterspectraMatrix::getNumberOfFrequencies()
  | 
  | * calc_modes
  | 
  | Model::getPartitionMethod() : retourne le type de partitionnement (mot clé DISTRIBUTION)
  | ajout de la SD partition manquante dans le c++ "class Model::Partition" qui contient ces infos dans des objets jeveux
  | 
  | * veri_matr_tang
  | 
  | ajout de static JeveuxVectorReal _mata et _matc dans Result
  | elles correspondent aux objets "PYTHON.TANGENT.MATA" et "PYTHON.TANGENT.MATC" attachés à aucune SD dans le Fortran
  | 
  | * pre_seisme_nonl
  | 
  | On a besoin de récupérer / modifier la matrice du macroelement associé au maillage 
  | ajout de _dynamic_macro_elements et _static_macro_elements manquants dans le maillage (voir DEFI_MAILLAGE)
  | nouvelles méthodes BaseMesh::addDynamicMacroElement(), addStaticMacroElement(), getDynamicMacroElements() getStaticMacroElements()
  | 
  | * char_grad_impo
  | 
  | ThermalLoadDescription::getThermalLoadDescription() : retourne le constantFieldOnCell ".CHTH.GRAIN"
  | ConstantFieldOnCells::setValueOnCells : impose une valeur sur une liste d'éléments
  | 
  | * dyna_iss_vari
  | 
  | GeneralizedResult::setDisplacement, setVelocity, setAcceleration : impose les champs déplacement, vitesse ou accélération
  | 
  | * pour calc_bt_ops, calc_gp_ops, calc_precont_ops, calc_spectre_ipm_ops, dyna_visco_ops, impr_table_ops, macr_cara_poutre_ops
  | 
  | On remplace par des méthodes existantes
  | 
  | III / Tests
  | 
  | sdll123a, sdll123b, sdll123c, sdll137a, sdll137d,
  | sdnd121a, sslp317b, sslv322a, ssns106a, supv003d,
  | zzzz208b, zzzz314e, zzzz314f
  | 
  | On remplace par des méthodes existantes
  | 
  | IV / Divers
  | 
  | * asojb, changeJeveuxValues résorbé
  | 
  | * sd_util , sdu_verif_nom_gd, sdu_nom_gd, sdu_licmp_gd, sdu_nb_ec
  | 
  | On a besoin du physicalQuatityManager qui contient les &CATA.GD.xxxxx.
  | Pour le binder en Python, on modifie la manière de faire en remplaçant le "Singleton" par des attributs statiques.
  | L'utilisation est plus naturelle :
  | 
  | PhysicalQuantityManager::Class().getPhysicalQuantityName( gdeur );
  | 
  | devient :
  | 
  | PhysicalQuantityManager::getPhysicalQuantityName( gdeur );
  | 
  | V / Validation
  | 
  | Tous les tests de vérification et validation

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src


================================================================================
                RESTITUTION FICHE 32520 DU 03/01/2023
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Nouveau partitionneur
- FONCTIONNALITE :
  | Demande :
  | ---------
  | L'année dernière, Nicolas Tardieu avait émis l'hypothèse d'une réécriture du partitionneur de maillage.
  | 
  | Quand j'ai écrit le partitionneur, on n'avait pas asterxx, asterhpc et rien ne permettait dans aster de faire du découpage de maillage, j'avais donc pris la
  | décision d'écrire le découpeur en python en dehors d'aster avec MEDCoupling.
  | 
  | Maintenant, le découpeur est dans aster. Mais ce n'est pas idéal parce qu'on passe essentiellement par des fichiers MED.
  | 
  | Je pense qu'il serait bien mieux maintenant de réécrire le découpeur en python/C++ dans aster sans passer par MEDCoupling (en tous cas pour la phase de
  | partitionnement et de découpe).
  | 
  | 
  | Réponse :
  | ---------
  | J'ai réécrit un partitionneur qui fait appel à Scotch.
  | 
  | Dans le détail, il relis un fichier MED soit en séquentiel (sur le proc 0) ou bien en parallèle sur tous les procs en utilisant MEDCoupling. Suivant le cas de
  | relecture, on produit soit un MeshPtr ou bien un IncompleteMeshPtr (inutilisable dans les calculs)
  | 
  | Ensuite, en utilisant PtScotch, on peut déterminer avec l'objet PtScotchPartitioner un équilibrage des nœuds en se fondant sur la connectivité entre ces nœuds.
  | 
  | Cet équilibrage peut être fournit à l'objet MeshBalancer pour qu'il produise un ParallelMesh.
  | 
  | Maintenant, on a un "éclaté" de partitionneur qui permettra de prendre en compte des besoins spécifiques beaucoup plus facilement qu'auparavant.
  | 
  | 
  | Validation :
  | ------------
  | Le test zzzz155 et ses 2 modélisations.
  | 
  | La modélisation A qui découpe plusieurs maillages de plusieurs manières différentes : en lisant en séquentiel ou en parallèle. Les maillages sont : un pavé, le
  | tuyau coudé de la formation et une ligne de tuyauterie donnée par Nicolas Tardieu.
  | 
  | L'avantage de ce maillage est qu'il n'était pas découpable avec l'ancien découpeur. Le nouveau s'en tire sans problème.
  | 
  | Dans zzzz155a, on teste en détail les connectivités des mailles obtenues ainsi (et c'est nouveau) que les raccords calculés entre les différents procs. Ce
  | dernier contrôle est réalisé en numérotation globale.
  | 
  | La modélisation B sert de test de perf (dans validation). Elle découpe perf008d (plus de 700 000 nœuds) en se comparant à l'ancien partitionneur.
  | Malheureusement, le nouveau partitionneur est 20% moins performant que l'ancien sur ce test.
  | 
  | Temps Elapse | perf008d 
  | -------------+----------
  |  Ancien      |  11,3 s
  |  Nouveau     |  14,8 s
  | 
  | Il y a des possibilités d'améliorer les performances, notamment par le fait que par commodité j'ai utilisé en certains endroits des comm globales au lieu de
  | point à point. De même, comme l'API MEDCoupling n'est pas encore totalement conforme à mes souhaits, je suis obligé de faire plusieurs relectures du maillage
  | pour obtenir les infos que je cherche.
  | 
  | Je pense que ces modifs pourraient nous rendre équivalent à l'ancien partitionneur. En plus moyennant les modifs de MEDCoupling, ce ne serait pas très compliqué
  | à mettre en œuvre.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz155a et b
- NB_JOURS_TRAV  : 15.0


================================================================================
                RESTITUTION FICHE 32384 DU 09/11/2022
================================================================================
- AUTEUR : FERTÉ Guilhem
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [Résorption] Modélisations second gradient _2DG
- FONCTIONNALITE :
  | Demande :
  | =======
  | 
  | Supprimer les modélisations second gradient (XXXX_2DG).
  | 
  | Modifications :
  | =============
  | 
  | Suppression du test ssll117f (et ssll117h = même fichier de commande que le f mais lancé en //)
  | 
  | Suppression des catalogues d'éléments
  | Suppression du fortran
  | 
  | Suppression de la modélisation F de V3.01.117
  | Suppression du $1.3 de R4.04.03.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : R5.04.03, V3.01.117
- VALIDATION : src


================================================================================
                RESTITUTION FICHE 31565 DU 08/11/2021
================================================================================
- AUTEUR : FERTÉ Guilhem
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [Résorption] Chargement INTE_ELEC
- FONCTIONNALITE :
  | Demande :
  | =======
  | 
  | Résorber le chargement INTE_ELEC d'AFFE_CHAR_MECA.
  | 
  | Travail effectué :
  | ================
  | 
  | - Suppression des test : sdnl101
  | Attention ce test servait également à la validation de DEFI_FONC_ELEC. Cet opérateur est encore utilisé dans sdll102.
  | 
  | - Suppression du mot-clé du catalogue d'AFFE_CHAR_MECA
  | - Ménage profond dans les sources
  | 
  | 
  | U4.44.01 : Suppression du paragraphe sur INTE_ELEC dans AFFE_CHAR_MECA
  | V5.02.101 : doc à supprimer

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V5.02.101, U4.44.01
- VALIDATION : src


================================================================================
                RESTITUTION FICHE 32724 DU 28/03/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Scinder les blocs EXCIT de DYNA_VIBRA/HARM/GENE DYNA_VIBRA/HARM/PHYS
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Le mot-clé EXCIT est défini de manière commune pour HARM/PHYS et HARM/GENE dans le catalogue.
  | De ce fait on peut lancer un calcul HARM/GENE avec un EXCIT/CHARGE. On plante alors sur un message non compréhensible :
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <F> <DVP_1>                                                                                    ║
  |  ║                                                                                                ║
  |  ║ Erreur de programmation.                                                                       ║
  |  ║                                                                                                ║
  |  ║ Condition non respectée:                                                                       ║
  |  ║     model .ne. ' '                                                                             ║
  |  ║ Fichier /home/ec06974s/dev/codeaster/src/bibfor/calculel/memare.F90, ligne 61                  ║
  |  ║                                                                                                ║
  |  ║                                                                                                ║
  |  ║ Cette erreur est fatale. Le code s'arrête.                                                     ║
  |  ║ Il y a probablement une erreur dans la programmation.                                          ║
  |  ║ Veuillez contacter votre assistance technique.                                                 ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | Sachant en plus qu'on ne doit pas donner de VECT_ASSE_GENE dans le cas HARM/PHYS et pas de VECT_ASSE ni de CHARGE dans le cas HARM/GENE,
  | je pense qu'il serait bien de scinder le bloc excit_harm en deux.
  | 
  | 
  | Correction :
  | ==========
  | 
  | On scinde le bloc EXCIT du cas HARM en deux : 
  | 
  | - un pour HARM/GENE dans lequel VECT_ASSE_GENE est obligatoire et CHARGE et VECT_ASSE sont supprimés 
  | 
  | - un pour HARM/PHYS dans lequel VECT_ASSE et CHARGE sont présents et pas VECT_ASSE_GENE

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : passage des tests DYNA_VIBRA


================================================================================
                RESTITUTION FICHE 32663 DU 01/03/2023
================================================================================
- AUTEUR : ALVES-FERNANDES Vinicius
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [ADELAHYD2025] Vérification de la masse dans POST_NEWMARK
- FONCTIONNALITE :
  | Besoin :
  | --------
  | 
  | Pouvoir vérifier l'adéquation du calcul de la masse de la zone qui glisse pour la macro-commande POST_NEWMARK (car calcul basé sur la discrétisation EF via
  | POST_ELEM).
  | 
  | Projet ADELAHYD2025
  | 
  | Développement :
  | ---------------
  | 
  | Vérification réalisée par remaillage avec HOMARD, avec deux options :
  | - si usage par définition d'un cercle de glissement : on remaille uniquement la zone du cercle (option ZONE, TYPE='DISQUE') en prenant un rayon 1,2x la valeur
  | du rayon du cercle d'origine
  | - si usage via un maillage externe MAIL_GLIS pour définition de la zone qui glisse, raffinement uniforme
  | 
  | Possibilité de contrôler la convergence de la vérification via un résidu relatif par itération et nombre maximal d'itérations.
  | 
  | Je profite pour faire un refactoring de la macro : passage de certaines étapes en fonctions (définition et nettoyage des groupes de maille, réorganisation des
  | étapes de calcul de facteur de sécurité et déplacements irréversibles).
  | 
  | 
  | Proposition des ajouts dans le catalogue de la commande :
  | -----------------------------------------------------------------
  | 
  | VERI_MASSE = SIMP(statut="f", typ="TXM", into=("OUI", "NON"), defaut="NON")
  |         b_VERI_MASS=BLOC(
  |             condition="""equal_to("VERIF_MASSE", 'OUI')""",
  |             RESI_RELA = SIMP(statut='f',typ='R',default=0.05, val_min=0.,fr="Tolérance d'arrêt des itérations"),                    
  |             ITER_MAXI=SIMP(statut='f',typ='I',default=2, val_min=1,fr="Nombre maximal d'itérations"),  
  |             CHAM_MATER=SIMP(statut="f", typ=cham_mater),
  |                         ),
  |                     ),
  | 
  | L'utilisateur doit fournir le CHAM_MATER si le calcul dynamique a été réalisé avec DYNA_VIBRA en fournissant directement les matrices de M,C,K. Autrement, la
  | macro récupère le champ directement du résultat dynamique.
  | 
  | Cas-test :
  | -----------
  | Ajout du cas-test zzzz402d (23s) qui compare le résultat d'un modèle avec maillage raffinée avec l'option développée et résultat sur un maillage moins raffiné.
  | On montre que :
  | - l'écart entre la solution pour le maillage raffiné et le maillage moins raffiné avec la vérification de la masse est moins importante que sans cette
  | vérification
  | - le résultat du maillage moins raffiné avec vérification de la masse est conservatif 
  | 
  | Impact source :
  | ----------------
  | src/code_aster/Cata/Commands/post_newmark.py
  | src/code_aster/MacroCommands/post_newmark_ops.py
  | src/code_aster/Messages/post0.py
  | src/astest/zzzz402d.comm
  | src/astest/zzzz402d.export
  | 
  | Impact documentaire : 
  | ----------------------
  | V1.01.402 : ZZZZ402 - Validation de l'utilisation de POST_NEWMARK
  | U4.84.45 Macro commande POST_NEWMARK

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.84.45, V1.01.402
- VALIDATION : zzzz402d
- NB_JOURS_TRAV  : 5.0


================================================================================
                RESTITUTION FICHE 32703 DU 20/03/2023
================================================================================
- AUTEUR : ALVES-FERNANDES Vinicius
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [ADELAHYD2025] Calcul du facteur de sécurité statique/dynamique avec option RAYON
- FONCTIONNALITE :
  | Besoin :
  | ---------
  | 
  | Disposer des fonctionnalités du calcul du facteur de sécurité en statique et dynamique pour l'option 'RAYON', pour laquelle l'utilisateur définit uniquement le
  | centre et rayon du cercle de glissement. Ces options sont déjà disponibles pour un usage avec l'option 'MAILLAGE_GLISS'. En effet, on s'appuie sur les
  | contraintes de peau type SIRO_ELEM obtenues le long de la ligne de glissement, après projection des contraintes aux points de gauss vers le maillage auxiliaire.
  | 
  | Projet ADELAHYD2025
  | 
  | Développement :
  | ----------------
  | Le développement s'appuie sur la construction automatiquement du maillage de cercle via la méthode buildDisk de l'objet Mesh. 
  | En dynamique, le facteur de sécurité est calculé à chaque pas de temps en tenant compte des variations des contraintes normales et tangentielles.
  | Je profite pour réduire la zone de projection des contraintes du maillage de la pente, cela permet de réduire le temps de calcul.
  | 
  | Impact catalogue de la commande :
  | ----------------------------------
  | RAFF_CERCLE : permet de définir le niveau de raffinement du cercle construit par la macro (permet de passer l'option à buildDisk)
  | On enlève le verrou associé à l'interdiction de l'option 'RAYON' pour le calcul du facteur de sécurité (avec usage du mot-clé 'RESULTAT_PESANTEUR')
  | 
  | Cas-test:
  | ----------
  | - sslp119a,b pour analyse statique (comparaison à résultats externes déjà obtenus avec un maillage de cercle)
  | - zzzz40c : on compare le calcul à partir du maillage externe d'un cercle à celui du cercle directement construit dans la macro en dynamique.
  | 
  | Impact source :
  | ----------------
  | src/astest/sslp119a.comm
  | src/astest/sslp119a.med
  | src/astest/sslp119b.comm
  | src/astest/sslp119b.med
  | src/astest/zzzz402c.comm
  | src/code_aster/Cata/Commands/post_newmark.py
  | src/code_aster/MacroCommands/post_newmark_ops.py
  | 
  | Impact documentaire :
  | ----------------------
  | V1.01.402 ZZZZ402 - Validation de l'utilisation de POST_NEWMARK
  | V3.02.119 SSLP119 - Calcul du facteur de sécurité statique d’un remblai homogène avec POST_NEWMARK
  | U4.84.45 Macro commande POST_NEWMARK

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : V3.02.119,V1.01.402,U4.84.45
- VALIDATION : zzzz402c, sslp119a,b
- NB_JOURS_TRAV  : 5.0


================================================================================
                RESTITUTION FICHE 32744 DU 31/03/2023
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Ménage carte des critères de convergence CARCRI
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Lors de la suppression des mots-clefs HHO dans STAT_NON_LINE et leur déplacement dans DEFI_MATERIAU, les slots réservés dans CARCRI sont restés, ce qui
  | surdimensionne cette carte
  | 
  | 
  | Correction
  | ----------
  | 
  | On diminue la taille de la carte

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32710 DU 23/03/2023
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [RS2024-CSM] - Nouvelles méthodes d'accès à ls SD Résultat
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Dans le cadre du chantier COMB_SISM_MODAL, il est nécessaire de disposer de nouvelles méthodes d'accès Python aux SD résultat, en particulier MULT_ELAS
  | 
  | 
  | Développement
  | -------------
  | 
  | Il restait quelques appels par wrapping à des routines FOrtran dans le C++: rsexch et et rsnoch en particulier
  | 
  | On remplace par l'utilisation directe des SD JEVEUX dans le C++
  | La routine setFieldBase fait désormais usage uniquement du C++ par exemple
  | 
  | Dans le Python
  | 
  | La méthode getField permet de récupérer un champ dans une SD résultat avec le numéro d'ordre (NUME_ORDRE). On étend au cas des SD indexés par NOM_CAS.
  | On ajoute la méthode setField sur le même modèle pour le cas où l'on sauve des champs dans la SD résultat. Cette routine utilise des nouvelles fonctions C++
  | createIndexFromParameter permet de créer un emplacement de stockage dans la SD résultat
  | 
  | 
  | Exemple d'usage des méthodes Python:
  | 
  | # Get displacements
  | disp1 = elasMult.getField("DEPL", value="grav1", para="NOM_CAS")
  | disp2 = elasMult.getField("DEPL", value="grav2", para="NOM_CAS")
  | disp3 = elasMult.getField("DEPL", value="grav3", para="NOM_CAS")
  | 
  | # Create new result
  | syntElasMult = code_aster.MultipleElasticResult()
  | syntElasMult.allocate(nbIndexes)
  | 
  | syntElasMult.setField(disp1, "DEPL", value="grav1", para="NOM_CAS")
  | syntElasMult.setField(disp2, "DEPL", value="grav2", para="NOM_CAS")
  | syntElasMult.setField(disp3, "DEPL", value="grav3", para="NOM_CAS")
  | syntElasMult.setField(disp1, "DEPL", value="grav4", para="NOM_CAS")

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz505f
- NB_JOURS_TRAV  : 5.0


================================================================================
                RESTITUTION FICHE 32720 DU 27/03/2023
================================================================================
- AUTEUR : ALVES-FERNANDES Vinicius
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [ADELAHYD2025] Ajout d'un cas-test de comparaison de POST_NEWMARK au logiciel GeoSlope pour calcul facteur de sécurité dynamique
- FONCTIONNALITE :
  | Besoin:
  | --------
  | Apporter des éléments supplémentaires de vérification des fonctionnalités de POST_NEWMARK pour le calcul de stabilité statique et dynamique des remblais au
  | séisme. Pour ce faire, on propose de restituer un cas-test dans la base de validation (~ 3 min) qui compare les résultats pour la méthodologie de référence pour
  | ce type de calcul avec code_aster et GeoSlope.
  | 
  | Cas-test (sdlp200a) :
  | ----------
  | Il s'agit de la modélisation d'une digue homogène sous séisme, pour laquelle on s'intéresse pour un cercle de glissement donné à :
  | - estimer le facteur de sécurité statique,
  | - déterminer l'évolution du facteur de sécurité en dynamique,
  | - vérifier l'évolution de l'accélération moyenne dans la zone de glissement et les déplacement irréversibles.
  | 
  | Les comparaisons entre code_aster et GeoSlope montrent :
  | - une très bonne cohérence pour l'estimation du facteur de sécurité statique,
  | - des écarts dans l'estimation des accélérations et contraintes dans le calcul dynamique, en particulier liées au choix des conditions aux limites (très
  | performantes pour code_aster par rapport à GeoSlope). Ces écarts se traduisent par une surestimation de l'accélération moyenne et in fine du déplacement
  | irréversible dans GeoSlope par rapport à code_aster.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : V2.03.200
- VALIDATION : cas-test
- NB_JOURS_TRAV  : 10.0

