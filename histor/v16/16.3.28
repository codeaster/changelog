==================================================================
Version 16.3.28 (révision 2a9a7512fc2a) du 2023-07-12 10:42 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32318 TARDIEU Nicolas          DOFNumbering et Lagrange
 32665 DEGEILH Robin            Visualisation des champs aux sous-points impossible avec les EF TUYAU
 32438 FERTÉ Guilhem            Problème avec calcul des modes statiques
 28328 NGUYEN Thuong Anh        REST_GENE_PHYS+CORR_STAT+GROUP_MA
 32743 PIGNET Nicolas           DYNA_VIBRA : exclure MATR_AMOR et AMOR_MODAL
 32467 AUDEBERT Sylvie          Mots-clés non-testés: POST_ROCHE
 32950 COURTOIS Mathieu         En version 16.3.23, le cas test pynl02a est en erreur sur Gaia
 32993 COURTOIS Mathieu         `configure --disable-petsc` ne fonctionne pas
 32333 ABBAS Mickael            [SAFER] Mise en donnée DEFI_TRC
 32987 PIGNET Nicolas           Objet joints mal créé
 32532 PIGNET Nicolas           zzzz503m plante en debug en QUADRATIQUE
 32977 PIGNET Nicolas           Valeur non initalisée HHO
 32992 PIGNET Nicolas           Résidu de MECA_NON_LINE
 32970 TARDIEU Nicolas          En version 16.3.25, les cas tests sdlv103a sdlv105a & sdlv105b sont en erreur...
 30315 PIGNET Nicolas           résultats d'anomalie avec MUMPS
 32832 FLEJOU Jean Luc          Combinaison de champs issus de modèles différents
 32747 FLEJOU Jean Luc          U4.44.01 : Par defaut ELIM_MULT='OUI' et pas 'NON' comme le dit la documentation
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32318 DU 12/10/2022
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : DOFNumbering et Lagrange
- FONCTIONNALITE :
  | Problème
  | ---
  | 
  | quand on demande ses composantes au DOFNumbering, les Lagranges sont indifféremment désignés par 'LAGR'.
  | Par contre, si on demande la composante associée à une ligne de la matrice, on obtient une information plus précise : 'LAGR:DX'
  | 
  | Analyse
  | ---
  | 
  | Problème résolu à la refonte avec le nume_equa
  | Les Lagranges sont tous désignés par des "LAGR:xx" pour getComponents, getComponentFromDOF et getRowsAssociatedToComponent
  | 
  | Remarque
  | ---
  | 
  | 1/ getRowsAssociatedToComponent("LAGR") retourne getLagrangeDOFs()
  | 2/ il faut couvrir getRowsAssociatedToComponent
  | 
  | 
  | Travail effectué
  | ---
  | 
  | * dans zzzz503i : couverture du dofNumbering.getRowsAssociatedToComponent (LAGR:DX et LAGR:DY), 
  | * dans zzzz502t : couverture du parallelDofNumbering.getRowsAssociatedToComponent (LAGR:PRES et LAGR:MPC)
  |  + vérif cohérence avec getLagrangeDOFs en 'local' et 'global'
  | * correction pour parallelDofNumbering : parfois un LAGR:xx est présent dans un proc et pas dans l'autre, on retourne [] au 
  | lieu d’émettre keyerror

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Nouveaux tests


================================================================================
                RESTITUTION FICHE 32665 DU 02/03/2023
================================================================================
- AUTEUR : DEGEILH Robin
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Visualisation des champs aux sous-points impossible avec les EF TUYAU
- FONCTIONNALITE :
  | Contexte
  | --------
  | 
  | Pour l'impression MED des champs à sous points, IMPR_RESU procède de la façon
  | suivante:
  |   1. On imprime les coordonnées et le poids de chaque sous-point dans un
  |     champ aux points de Gauss de la fibre (ou du feuillet) neutre dans le fichier MED
  |     Le fichier MED résultat contient donc plusieurs points de Gauss confondus
  |     sur la fibre neutre.
  |     Les coordonnées réelles de ces points dans la section sont donc exportées sous
  |     forme de champ au point de Gauss au sens su fichier MED.
  |     Ceci est fait dans irmpga.F90
  | 
  |   2. On imprime les valeurs du champ cible de la même façon.
  |     Ceci est fait dans ircam1.F90, appel cesexi.F90 pour avoir l'index de rangement dans le champ.
  | 
  |   3. Il y a ensuite un filtre dans Paravis qui reconstruit les valeurs aux points de Gauss
  |     de la section à partir des deux informations précédentes et du maillage de la section
  |     (qui est également exporté par IMPR_RESU dans le fichier MED).
  |     Ce filtre appelle des fonctions BuildMeshXXX
  | 
  | Anomalie / résultats faux
  | -------------------------
  | 
  | Pour les éléments à sous-points qui ne sont ni PMF, ni GRILLE, ni COQUE, ni TUYAU,
  | la numérotation n'est pas cohérente entre 1. et 2.
  | 
  | Pour ces éléments, à la visualisation Paravis, les valeurs sein d'une section ne
  | sont donc pas affichées au bon point de Gauss
  | 
  | Correction
  | ----------
  | On corrige irmpga.F90 pour rendre la numérotation cohérente avec ircam1/cesexi.
  | 
  | Autres anomalies détectées, corrigées ailleurs
  | ==============================================
  | 
  | SEG3
  | ----
  | Visualisation aux sous-points impossible dans le fichier "tuyau_sous_points.tar.xz"
  | joint en utilisant des mailles SEG3
  | 
  | Problème dans Homard: composante Y qui vaut +1848187100000000.000000
  | voir issue32997
  | 
  | SEG4
  | ----
  | 
  | La visualisation des champs à sous-points dans Paravis est impossible avec les éléments TUYAU SEG4
  | 
  | Manque dans MedCoupling: BuildMeshPipeSEG4
  | nouvelle fiche salomé https://forge.pleiade.edf.fr/issues/28170
  | 
  | IMPR_RESU sans CARA_ELEM
  | ---
  | Cf issue31622

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 14.1
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 14.1
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : visu sur les éléments concernés


================================================================================
                RESTITUTION FICHE 32438 DU 28/11/2022
================================================================================
- AUTEUR : FERTÉ Guilhem
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Problème avec calcul des modes statiques
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Dans le calcul joint (8000 modes statiques sur 1M DDL),
  | les modes statiques sont tous nuls.
  | 
  | Le calcul est correct si on demande seulement une partie des modes statiques,
  | en découpant le GROUP_MA en plusieurs sous-groupes, ou si on enlève les barres
  | du calcul
  | 
  | Analyse
  | -------
  | 
  | Comme on impose 1 sur un DDL en bloquant tous les autres, on a une forte disparité
  | d'ordres de grandeur entre le multiplicateur et les composantes de déplacement.*
  | 
  | Valeur de .CONL de la matrice (indicateur conditionnement multiplicateur): ~1.e12
  | 
  | Sur une résolution: multiplicateur à 1.e10
  | 
  | C'est un problème dans MUMPS ou son appel quand on donne les 8000 second membres d'un coup.
  | Il doit y avoir une mise à l'échelle interne qui doit mal marcher: on trouve 1.e25 pour
  | le multiplicateur et 0 (exactement) sur les déplacements.
  | C'est étrange car MUMPS est paramétré pour faire la phase de descente-remontée
  | avec les seconds membres 8 par 8.
  | Il doit y avoir une procédure de dimentionnement au global, qui se passe mal
  | 
  | Correction
  | ----------
  | 
  | Pour l'appel à resoud de modsta.F90, on passe les seconds membres 8 par 8.
  | 
  | Permet de résoudre le problème pour l'étude de Stéfano, mais c'est une rustine.
  | 
  | Il "faudrait" en toute rigueur faire une mise à l'échelle de la matrice
  | (mais ça n'existe qu'en python dans aster, je crois)
  | 
  | Corrections annexes:
  | - on initialise à 0 le second membre zrmod dans op0093 (wkvect n'initialise pas à 0)
  | - on met à 0 les multiplicateurs de la solution trouvée dans modsta.F90 pour 
  | éviter les problèmes d'impression ensuite, sachant que la valeur des multiplicateurs pour
  | les modes statiques n'est jamais utilisée

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src + étude jointe
- NB_JOURS_TRAV  : 1.5


================================================================================
                RESTITUTION FICHE 28328 DU 06/12/2018
================================================================================
- AUTEUR : NGUYEN Thuong Anh
- TYPE : anomalie concernant code_aster (VERSION 13.6)
- TITRE : REST_GENE_PHYS+CORR_STAT+GROUP_MA
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Nous avons des gros soucis pour récupérer les REAC_NODA à la suite d'un DYNA_VIBRA (GENE/TRAN) --> résultats NaN (cf ci-après).
  | 
  | Nous avons trituré le problème dans tous les sens, sans arriver à trouver le bug (dans l'étude ou côté ASTER)
  | 
  | Analyse :
  | =======
  | 
  | Les Nan apparaissent dans les champs DEPL et ACCE en sortie de REST_GENE_PHYS. Donc pas besoin de lancer les CALC_CHAMP
  | (qui prenaient un temps fou).
  | 
  | J'ai voulu comparer le même calcul avec GROUP_NO au lieu de GROUP_MA.
  | J'ai alors été arrêté par une règle du catalogue : EXCLUS (CORR_STAT, GROUP_NO).
  | Quand je supprime les CORR_STAT, j'ai les mêmes résultats dans les 3 cas (TOUT, GROUP_MA et GROUP_NO).
  | 
  | J'ai regardé dans la doc, je n'ai pas trouvé de mention à cette restriction, mais cela ne fonctionne visiblement pas car GROUP_MA
  | fait la même chose que GROUP_NO dans ce cas c-a-d restreindre sur un groupe de nœud.
  | 
  | Modification :
  | ------------
  | 
  | On ajoute MAILLE et GROUP_MA aux règles d'exclusion avec CORR_STAT et MULTI_APPUI.
  | => j'ai fait tourné les 88 tests utilisant REST_GENE_PHYS avec ça et aucun n'est cassé.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src


================================================================================
                RESTITUTION FICHE 32743 DU 31/03/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : DYNA_VIBRA : exclure MATR_AMOR et AMOR_MODAL
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Il me semble à la lecture de la routine dtmprep que MATR_AMOR et AMOR_MODAL ne doivent pas être donnés ensemble.
  | Le catalogue ne l'interdit pas et si on le fait on tombe sur cette erreur (au moins pour HARM/GENE) :
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <F> <JEVEUX_26>                                                                                ║
  |  ║                                                                                                ║
  |  ║ Objet inexistant dans les bases ouvertes : .REFA                                               ║
  |  ║ l'objet n'a pas été créé ou il a été détruit                                                   ║
  |  ║ Ce message est un message d'erreur développeur.                                                ║
  |  ║ Contactez le support technique.                                                                ║
  |  ║                                                                                                ║
  |  ║                                                                                                ║
  |  ║ Cette erreur est fatale. Le code s'arrête.                                                     ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | Modification :
  | ============
  | Pour les cas TRAN/GENE et HARM/PHYS on ajoute une règle d'exclusion de MATR_AMOR et AMOR_MODAL.
  | On en profite pour ajouter une règle d'exclusion entre AMOR_REDUIT et LIST_AMOR de AMOR_MODAL
  | => J'ai vérifié dans le code et seul AMOR_REDUIT était pris en compte si présence des deux.
  | 
  | Merci de passer la fiche à résolu.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : passage des tests concernés


================================================================================
                RESTITUTION FICHE 32467 DU 07/12/2022
================================================================================
- AUTEUR : AUDEBERT Sylvie
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Mots-clés non-testés: POST_ROCHE
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Les mot-clés suivants ne sont pas testés.
  | 
  | POST_ROCHE:
  |   RESU_MECA:
  |     CHAM_GD: []
  |     CRITERE#ABSOLU: []
  |     CRITERE#RELATIF: []
  |     DIRECTION#X: []
  |     DIRECTION#Y: []
  |     DIRECTION#Z: []
  |     INST: []
  |     PRECISION: []
  |   RESU_MECA_TRAN:
  |     CHAM_GD: []
  |     CRITERE#ABSOLU: []
  |     TOUT_ORDRE#OUI: []
  | 
  | Correction :
  | ==========
  | 
  | On complète les tests existants avec les mots-clés manquants.
  | Le seul point posant question est le mot-clé DIRECTION car pas de référence avec X, Y ou 
  | Z.
  | Par contre je remarque que l'on obtient les mêmes résultats en remplaçant : par  
  | 
  |         _F(TYPE_CHAR="SISM_INER_SPEC", RESULTAT=sismique, DIRECTION="COMBI"),
  | 
  | par 
  | 
  |         _F(TYPE_CHAR="SISM_INER_SPEC", RESULTAT=sismique, DIRECTION="X"),
  |         _F(TYPE_CHAR="SISM_INER_SPEC", RESULTAT=sismique, DIRECTION="Y"),
  |         _F(TYPE_CHAR="SISM_INER_SPEC", RESULTAT=sismique, DIRECTION="Z"),
  | 
  | Je propose donc de faire ce changement pour un des appels à POST_ROCHE.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas-test


================================================================================
                RESTITUTION FICHE 32950 DU 19/06/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.3.23, le cas test pynl02a est en erreur sur Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le test pynl02a échoue dans le test du code retour :
  | 
  | if max(result.CODE_RETOUR_INTE.getValuesWithDescription("IRET")[0]) > 0:
  | . . raise IntegrationError("echec d integration de la loi de comportement")
  | 
  | Le champ de code retour par éléments d'entiers quand il est crée par la commande CALCUL.
  | Ensuite, on extrait ce champ avec EXTR_TABLE qui ne traite que les champs de réels.
  | Et c'est là que ça se passe mal.
  | 
  | 
  | Correction
  | ----------
  | 
  | Le champ de code retour n'est jamais utilisé au niveau Python.
  | Dans la table conteneur en sortie de CALCUL, on ne sort plus le champ mais uniquement le
  | maximum de la valeur du code retour, dans une nouvelle colonne VALE_I.
  | Type ENTIER dans la table au lieu de CHAM_ELEM.
  | 
  | EXTR_TABLE permet d'extraire un entier.
  | 
  | Dans pynl02a, on fait donc :
  | 
  | 
  | if result.CODE_RETOUR_INTE > 0:
  | . . raise IntegrationError("echec d integration de la loi de comportement")

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.51.10
- VALIDATION : pynl01a/b, pynl02a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32993 DU 05/07/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : `configure --disable-petsc` ne fonctionne pas
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | L'option pour désactiver un des prérequis n'est pas prise en compte.
  | 
  | $ ./configure --disable-petsc
  | ...
  | Checking for library petsc               : yes 
  | ...
  | 
  | 
  | Correction
  | ----------
  | 
  | Par défaut, on veut que tous les prérequis soient obligatoires si on n'a pas activé --disable-all
  | ou mis ENABLE_ALL=0 dans l'environnement.
  | 
  | Or désactiver un produit avec --disable-xxx ne changeait pas la valeur par défaut précédente.
  | C'est ce qu'il faut corriger.
  | 
  | Si ni --enable-all, ni --disable-all n'a été fourni et qu'un produit a été désactivé alors
  | enable-all devient False, les autres produits sont facultatifs.
  | 
  | Remarque :
  | '--disable-xxx' indique qu'on désactive le produit xxx,
  | '--enable-xxx' indique qu'on impose que le produit xxx soit bien trouvé,
  | sinon on le configure si on le trouve, on l'ignore sans erreur si on ne le trouve pas.
  | Alors que --disable-all indique qu'on ne rend pas obligatoire tous les prérequis
  | mais pas qu'on désactive tous les prérequis.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : configure avec --disable-xxx
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32333 DU 20/10/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [SAFER] Mise en donnée DEFI_TRC
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Lors de l'utilisation des diagrammes TRC en métallurgie, il peut y avoir une incohérence avec les données de DEFI_MATERIAU/META_ACIER et celles venant de
  | DEFI_TRC.
  | 
  | Correction
  | ----------
  | 
  | Les diagrammes TRC (Transformation en Refroidissement Continue) sont des données expérimentales décrivant le passage d'une phase chaude (austénite) en plusieurs
  | phases froides (ferrite, perlite, bainite, la martensite étant traitée à part).
  | Parmi ces données, on a la température de début de transformation au refroidissement (dans le vecteur VALE de DEFI_TRC/HIST_EXP) qui est aussi donnée par le
  | paramètre AR3 dans DEFI_MATERIAU/META_ACIER.
  | Il n'y a aucune vérification de cohérence entre ces données, ce qui peut conduire à des résultats faux.
  | L'idéal serait de revoir complètement la manière d'entrer les diagrammes TRC. Mais, en attendant, on fait une vérification dans le te qui calcule les
  | répartitions de phases dans CALC_META et on émet une alarme s'il y a inconsistance.
  | 
  | 
  | "La différence entre la température de début de transformation des phases froides en austénite dans le diagramme TRC et celle donnée par DEFI_MATERIAU est
  | supérieure de plus de 1.0000E+01°C."
  | 
  | 
  | De manière assez arbitraire, et compte tenu des tests actuels dans aster, j'ai fixé l'écart maximum à 10°C.
  | 
  | Validation
  | ==========
  | 
  | Tous les tests de src passent avec ce critère à 10°C maximum d'amplitude. Par contre, on a un problème sur le test de validation hsna104a qui fait du mixte
  | inox/pas inox. J'ouvre la fiche issue32991 à ce sujet, car ça révèle un problème dans la mise en données actuelle de DEFi_TRC
  | 
  | Report en 15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.43.01;u4.43.04
- VALIDATION : src
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 32987 DU 03/07/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Objet joints mal créé
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le build sur l'objet Joint du ParallelMesh ne fonctionne pas.
  | 
  | A priori NUTIOC ne peut pas être défini sur une collection dispersée et on se base dessus pour reconstruire en c++
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | En fait c'est la création de l'objet Joints un peu partout, maillage, numérotation
  | 
  | Il manquait un jecroc pour incrmémenter la taille de la collection et la reconstruire correctement dans le c++

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz503n
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32532 DU 05/01/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : zzzz503m plante en debug en QUADRATIQUE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | zzzz503m plante avec un beau segfault en mode debug.
  | 
  | C'est le cas QUADRATIQUE qui plante.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Il y avait un écrasement mémoire possible en QUADRATIQUE lors de l'évaluation de AFFE_CHAR_CINE_F.
  | 
  | Il faut passer par un vecteur intermédiaire de la bonne taille avant de faire la recopie dans la solution
  | 
  | Report en v15.
  | 
  | Résultat faux
  | -------------
  | Possible avec des HEXA27 (uniquement), HHO_QUADRATIQUE, et présence de AFFE_CHAR_CINE_F

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 15.1.3
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 15.1.3
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz503m
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 32977 DU 27/06/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Valeur non initalisée HHO
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | [1,2]<stdout>:==754312== Conditional jump or move depends on uninitialised value(s)
  | [1,2]<stdout>:==754312==    at 0xF5571E4: daxpy_ (in /usr/lib/libopenblasp-r0.2.19.so)
  | [1,2]<stdout>:==754312==    by 0x95831CC: __hho_rhs_module_MOD_hhomakerhscellscal (HHO_rhs_module.F90:193)
  | [1,2]<stdout>:==754312==    by 0x9582DFE: __hho_rhs_module_MOD_hhomakerhscellvec (HHO_rhs_module.F90:240)
  | [1,2]<stdout>:==754312==    by 0x954EFA3: __hho_l2proj_module_MOD_hhol2projcellvec (HHO_L2proj_module.F90:259)
  | [1,2]<stdout>:==754312==    by 0x9549728: __hho_dirichlet_module_MOD_hhodirimecaprojfunc (HHO_Dirichlet_module.F90:865)
  | [1,2]<stdout>:==754312==    by 0xA0E7671: te0458_ (te0458.F90:67)
  | [1,2]<stdout>:==754312==    by 0x8EA6A57: te0000_ (te0000.F90:1580)
  | [1,2]<stdout>:==754312==    by 0x8E8D5F2: calcul_ (calcul.F90:384)
  | [1,2]<stdout>:==754312==    by 0x954B304: __hho_dirichlet_module_MOD_hhodirifunccompute (HHO_Dirichlet_module.F90:504)
  | [1,2]<stdout>:==754312==    by 0x99DFF92: nonlinloadcompute_ (nonlinLoadCompute.F90:136)
  | 
  | [1,2]<stdout>:==754312==  Uninitialised value was created by a stack allocation
  | [1,2]<stdout>:==754312==    at 0x9548B89: __hho_dirichlet_module_MOD_hhodirimecaprojfunc (HHO_Dirichlet_module.F90:764)
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Le problème est apparu suite à la suppression de la condensation statique. Il faut aussi projeter les inconnues de cellules.
  | 
  | J'ai oublié de redimentionner le tableau des faces pour ajouter la cellule. J'augmente la taille de un et ça passe.
  | 
  | il n'y avait le problème qu'avec les HEXA27 et AFFE_CHAR_CINE_F

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz503m
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32992 DU 05/07/2023
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Résidu de MECA_NON_LINE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Il manque les termes de contact pour l'évaluation du résidu dans MECA_NON_LINE
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | En regardant nmresi.F90, il faut sauter les équations des LAGR_C et LAGR_F et ajouter le residu de contact
  | 
  | On modifie le convergence_manager pour corriger.
  | 
  | Ca ne change rien aux résultats à la fin car à convergence ces termes sont très petits

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cont002a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32970 DU 26/06/2023
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.3.25, les cas tests sdlv103a sdlv105a & sdlv105b sont en erreur sur CRONOS & GAIA
- FONCTIONNALITE :
  | Problème
  | --------
  | En version 16.3.25, les cas tests sdlv103a sdlv105a & sdlv105b sont en erreur sur CRONOS & GAIA
  | 
  | 
  | 
  | Solution
  | --------
  | Plusieurs pbs différents dans cette fiche :
  | - sdlv103a : modification des valeurs de non-regression
  | - sdlv105a : modification des valeurs de non-regression et protection race condition dans DEFI_FICHIER 
  | (issue32983)
  | - sdlv105b : Ajout de valeurs de référence et protection race condition dans DEFI_FICHIER (issue32983)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Passage des tests
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30315 DU 06/10/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 15.2)
- TITRE : résultats d'anomalie avec MUMPS
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Voici les fichiers de calcul en pièce jointe.
  | 
  | Le calcul est celui d'une onde plane se propageant dans un milieu élastique homogène semi-infini :
  | Des conditions aux limites absorbantes imposées sur 4 côtés et sur la face inférieure (Y=-100m) du cube. La face supérieure du cube (Y=0m) est libre. Une onde
  | plane transversale de type SH (Shear-Horizontal) se propage à partir de la face inférieure suivant la direction Y. Elle est ensuite réfléchie sur la face
  | supérieure (Y=0m). 
  | 
  | Les résultats reçu sont la vitesse au point N sur la face supérieure (geom.png).
  | Différents résultats avec MUMPS en parallèle modéré (à gauche) et en parallèle massif (à droite dans l'image de comparaison).
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | C'était un bug dans l'interface HPC de mumps qui a été corrigé depuis.
  | 
  | Sans suite

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32832 DU 04/05/2023
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : aide utilisation concernant code_aster (VERSION 11.8)
- TITRE : Combinaison de champs issus de modèles différents
- FONCTIONNALITE :
  | Analyse :
  | =========
  | Dans l'étude jointe la succession des commandes est la suivante :
  | .  MODELE=AFFE_MODELE( sur plein de groupes de mailles )
  | .  RESU = Calcul STAT_NON_LINE( ..... )
  | .  SIGSTAT0=CREA_CHAMP('ELGA_SIEF_R', OPERATION='EXTR', RESULTAT=RESU, NOM_CHAM='SIEF_ELGA',);
  | .  ROSET1=CALC_CHAMP('SIRO_ELEM', MODELE=MODELE, RESULTAT=RESU, GROUP_MA)
  | # Post-traitement
  | .  MODPOST=AFFE_MODELE( sur quelques groupes de mailles, déjà présente dans MODELE )
  | .  SIGSTAT=PROJ_CHAMP(CHAM_GD=SIGSTAT0, MODELE_1=MODELE, MODELE_2=MODPOST,);
  | .  CUMSIG=CREA_CHAMP( 'ELGA_SIEF_R',OPERATION='ASSE', MODELE=MODPOST, GROUP_MA=('BARRAGE',), CHAM_GD=SIGSTAT,)
  | .  RESSIG=CREA_RESU( 'AFFE', NOM_CHAM='SIEF_ELGA', CHAM_GD=CUMSIG,)
  | .  ROSET2=CALC_CHAMP('SIRO_ELEM', MODELE=MODPOST, RESULTAT=RESSIG, GROUP_MA)
  | ROSET2 donne des résultats bizarres en comparaison avec ceux de ROSET1 précédemment calculé avec RESU.
  | Normalement cela devrait donner exactement la même chose.
  | 
  | Un objectif est de cumuler sur des groupes de mailles des champs de même nature issus de plusieurs calculs :
  | .  MECA_STATIQUE, STAT_NON_LINE, DYNA_NON_LINE, DYNA_VIBRA, ...
  | Par exemple : SIEF_ELGA, DEPL, ... des champs issus de CALC_CHAMP, ...
  | 
  | Observations :
  | ==============
  | Dans ce cas on utilise PROJ_CHAMP pour "copier" des champs d'un modèle1 vers un modèle2.
  | Les champs sont de type NOEUD pour les déplacements et ELGA pour les contraintes.
  | Les modèles sont basés sur le même maillage.
  | 
  | La commande PROJ_CHAMP est à utiliser lorsque les maillages de départ et d'arrivé sont différents et
  | nécessitent de faire des interpolations MAIS cette commande devrait également fonctionner pour faire
  | une simple copie. Ici cela ne fonctionne pas pour les SIEF_ELGA.
  | 
  | Proposition :
  | =============
  | Comme la solution PROJ_CHAMP est buguée, je propose une solution pour faire une copie des champs
  | sans passer par PROJ_CHAMP.
  | 
  | Le principe c'est de restreindre le résultat complet sur les groupes de mailles que l'on souhaite:
  | - restriction du maillage : MAILR = CREA_MAILLAGE / RESTREINT / GROUP_MA
  | - MODR   : affe_modele sur MAILR
  | - CARAR  : cara_elem sur MODR
  | - MATERR : affe_materiau sur MAILR
  | - extraction du résultat restreint : RESSR = EXTR_RESU / RESULTAT=RESU, RESTREINT=_F(MODR, CARAR, MATERR)
  | - calcul du champ ROSETR = CALC_CHAMP('SIRO_ELEM', RESSR) comme pour ROSET1
  | 
  | Après mise en œuvre de la proposition, il n'y a aucune différence entre ROSET1 et ROSETR,
  | pour les champs SIRO_ELEM (diff sur les impr_resu des champs).
  | 
  | Remarques :
  | -----------
  | + RESSR : on récupère à tous les instants tous les champs qui existent dans RESU restreint aux
  | groupes de mailles demandés, ici : DEPL, VARI_ELGA, SIEF_ELGA.
  | C'est un concept résultat qui peut être utiliser dans d'autres commandes.
  | 
  | Retours des émetteurs de la fiche :
  | -----------------------------------
  | + J'ai testé ta solution sur mon calcul (combinant un résultat statique et dynamique). Elle fonctionne !
  | Je vais transmettre aux collègues car au-delà de la résolution du problème de PROJ_CHAMP avec des éléments
  | plaques, la méthode me parait plus précise (extraction au lieu de projection). Et ne connaissant pas pour
  | l'instant le problème de PROJ_CHAMP, nous devons être prudents dans son utilisation à l'avenir.
  | 
  | + C'est une bonne solution de contournement. Il serait quand même bon de regarder en détail ce qui se passe
  | avec PROJ_CHAMP car la syntaxe avec cette commande est quand même plus light et moins plantogène
  | que l'ensemble des commandes devant être réalisée pour restreindre les modèles.
  | 
  | 
  | Pour réponde aux retours des émetteurs :
  | ----------------------------------------
  | + Je complète la fiche REX issue30108 en faisant référence à cette étude. La fiche issue30108 évoquait le
  | même problème avec PROJ_CHAMP, mais est restée pour l'instant sans réponse. La présente fiche à mis en
  | évidence des résultats FAUX, il ne faut donc pas laisser le code dans cet état.
  | La fiche issue30108 était "évolution" maintenant c'est "anomalie" avec "résultats faux".
  | 
  | + J’émets une fiche d'évolution pour que l'on puisse faire quelque chose du genre
  | .   RESSR = EXTR_RESU / RESULTAT=RESU, RESTREINT=_F(GROUP_MA=('xx','ff','vv'))
  | .   commandes
  |         regles=(
  |             UN_PARMI("MAILLAGE", "MODELE", "GROUP_MA"),
  |             EXCLUS("GROUP_MA","CHAM_MATER","CARA_ELEM"),
  |         ),
  |         MAILLAGE=SIMP(statut="f", typ=maillage_sdaster),
  |         MODELE=SIMP(statut="f", typ=modele_sdaster),
  |         CHAM_MATER=SIMP(statut="f",typ=cham_mater,),
  |         CARA_ELEM=SIMP(statut="f",typ=cara_elem,),
  |         GROUP_MA=SIMP(statut="f",typ=grma, validators=NoRepeat(),)
  | .   Si on donne GROUP_MA alors la commande va extraire de RESU toutes les infos qui vont bien sur les mailles.
  | .   Les informations sont dans le concept RESU qui connaît :
  | .       - le modèle (donc le maillage),
  | .       - le cara_elem (si présent),
  | .       - les matériaux
  | .   Il n'y aurait plus besoin de créer des objets intermédiaires : maillage, modèle, cara_elem, matériaux
  | .   La commande serait plus "light" que PROJ_CHAMP (pas besoin des CALC_CHAMP) et moins "plantogène"
  | .   Dans le cas présent on aurait donc une seule commande :
  | .   .   RESSR = EXTR_RESU(
  | .   .       RESULTAT=RESU,
  | .   .       RESTREINT=_F(GROUP_MA=('BARRAGE','AMONT','AVAL',),),)
  | 
  | En bonus :
  | ----------
  | + Sur les temps CPU (sur cronos en version 16.3 séquentielle, ncpus=8)
  |  * STAT_NON_LINE            :     495.51 :      23.42 :     518.93 :     188.77 *
  | Avec proj_champ
  |  * PROJ_CHAMP               :      81.98 :       0.70 :      82.68 :      82.90 *
  | Restriction
  |  * CREA_MAILLAGE            :       0.03 :       0.00 :       0.03 :       0.03 *
  |  * AFFE_MODELE              :       0.02 :       0.00 :       0.02 :       0.03 *
  |  * AFFE_CARA_ELEM           :       0.01 :       0.00 :       0.01 :       0.00 *
  |  * AFFE_MATERIAU            :       0.00 :       0.00 :       0.00 :       0.01 *
  |  * EXTR_RESU                :       1.55 :       0.16 :       1.71 :       1.71 *

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : passage du cas
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 32747 DU 03/04/2023
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant Documentation (VERSION 11.*)
- TITRE : U4.44.01 : Par defaut ELIM_MULT='OUI' et pas 'NON' comme le dit la documentation
- FONCTIONNALITE :
  | Dans le catalogue de la commande AFFE_CHAR_MECA/LIAISON_MAIL
  | le mot clef ELIM_MULT est 'OUI' par défaut
  | 
  | Dans la description du vocabulaire c'est marqué 'NON' : 5.13 Mot-clé LIAISON_MAIL
  | ◊ ELIM_MULT = / ’NON’,                [DEFAUT]
  | . . . . . . . / ’OUI’,
  | 
  | Correction :
  | ◊ ELIM_MULT = / ’OUI’,                [DEFAUT]
  | . . . . . . . / ’NON’,
  | 
  | 
  | Dans §5.3.13, les explications sont correctes :
  | ◊ ELIM_MULT
  | Si l’utilisateur force ELIM_MULT=’NON’,  .....
  | ...
  | ...
  | La plupart du temps, le défaut ELIM_MULT=’OUI’ est le bon choix. ....
  | ...
  | ...

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.44.01
- VALIDATION : humaine
- NB_JOURS_TRAV  : 1.0

