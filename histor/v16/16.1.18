==================================================================
Version 16.1.18 (révision 46122399dfdf) du 2022-05-12 17:11 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31924 PIGNET Nicolas           Comportement CJS en GDEF_LOG
 31925 PIGNET Nicolas           Comportement LAIGLE en GDEF_LOG
 31964 PIGNET Nicolas           GDEF_LOG non disponible pour XFEM
 30888 DROUET Guillaume         [FAUX] Récupération des caractéristiques élastiques variables en temps
 31634 ABBAS Mickael            Préparation des champs avant CALC_COUPURE
 32050 COURTOIS Mathieu         waf configure échoue si la version de PETSc n'a pas de patch
 32054 TARDIEU Nicolas          Problème de communicateur mpi dans aster_fieldsplit
 32060 TARDIEU Nicolas          ksp_util ne rend pas le ksp construit
 32034 COURTOIS Mathieu         Documentation embarquée sur RTD
 32066 COURTOIS Mathieu         Construction de la version MPI par défaut
 32059 PIGNET Nicolas           Supprimer elementary_computations
 27689 HABIB Souheil            Extension de l'opérateur CALC_FERRAILLAGE aux éléments 1D de type poutres et ...
 31935 HABIB Souheil            Evolution de la macro commande COMBINAISON_FERRAILLAGE
 31815 Yi ZHANG                 Calcul des options MASS_MECA et AMOR_MECA: ecriture en C++
 30925 PIGNET Nicolas           les classes DiscreteProblem et LinearSolver ne fonctionnent qu'en mécanique
 32027 ABBAS Mickael            Compléments doc R4.07.03
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31924 DU 17/03/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Comportement CJS en GDEF_LOG
- FONCTIONNALITE :
  | Problème :
  | ========
  | Dans le cadre de issue29813, on souhaite interdire DEFORMATION='GROT_GDEP'  pour les éléments qui ne sont
  | pas des éléments de structure. Pour cela il était proposé de passer à par DEFORMATION='GDEF_LOG'
  | 
  | Pour le comportement CJS, on ne peut pas faire ce changement car GDEF_LOG n'est pas disponible.
  | 
  | Analyse :
  | =======
  | 
  | Aucune raison de le mettre en GROT_GDEP (et encore moins GDEF_LOG) pour ssnv135a.
  | En effet, il s'agit d'un test en petites déformations pour lequel le dernier calcul (W2) a été dupliqué 2 fois en GROT_GDEP (W3 et W4).
  | D'après issue10369, le but était de comparer les calculs avec GROT_GDEP en forçant ou non la matrice à être symétrique.
  | En comparant les deux appels on remarque qu'il s'agit (dans la version actuelle) du même calcul exactement.
  | 
  | Solution :
  | ========
  | 1- On supprime les calculs W3 et W4 du test ssnv135a (pas de modif de la doc qui n'y faisait pas référence).
  | 2- On interdit GROT_GDEP avec CJS

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv135a


================================================================================
                RESTITUTION FICHE 31925 DU 17/03/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Comportement LAIGLE en GDEF_LOG
- FONCTIONNALITE :
  | Problème :
  | ========
  | Dans le cadre de issue29813, on souhaite interdire DEFORMATION='GROT_GDEP'  pour les éléments qui ne sont
  | pas des éléments de structure. Pour cela il était proposé de passer à DEFORMATION='GDEF_LOG'
  | 
  | Pour le comportement LAIGLE, on ne peut pas faire ce changement car GDEF_LOG n'est pas disponible.
  | 
  | Analyse :
  | =======
  | 
  | GROT_GDEP pour le comportement LAIGLE est présent dans un seul test ssnv158a. 
  | Il s'agit d'un test en petites déformations pour lequel le dernier calcul (U2) a été dupliqué 2 fois en GROT_GDEP (U3 et U4).
  | D'après issue10369, le but était de comparer les calculs avec GROT_GDEP en forçant ou non la matrice à être symétrique.
  | En comparant les deux appels on remarque qu'il s'agit (dans la version actuelle) du même calcul exactement.
  | Ces deux calculs n'ont donc pas de raison d'être conservés.
  | 
  | Solution :
  | ========
  | 1- On supprime les calculs U3 et U4 du test ssnv158a (pas de modif de la doc qui n'y faisait pas référence).
  | 2- On interdit GROT_GDEP avec LAIGLE
  | 
  | Charge prise en compte dans issue31924

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv158a


================================================================================
                RESTITUTION FICHE 31964 DU 31/03/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : GDEF_LOG non disponible pour XFEM
- FONCTIONNALITE :
  | Problème :
  | ========
  | Dans le cadre de issue29813, on souhaite remplacer DEFORMATION='GROT_GDEP' par DEFORMATION='GDEF_LOG' pour les éléments qui ne sont
  | pas des éléments de structure.
  | En faisant ce changement pour les tests C_PLAN XFEM + VMIS_ISOT_LINE, on plante sur un assert car GDEF_LOG n'est pas développé pour les 
  | éléments XFEM (voir te0539.F90).
  | De plus ELAS+GDEF_LOG sur les éléments XFEM se comporte comme PETIT.
  | 
  | Solution :
  | ========
  | 
  | XFEM n'est plus maintenu. On supprime les cas-tests qui posent problème et on supprime GROT_GDEP avec XFEM.
  | 
  | - Vérification dans te0539 que DEFORMATION = PETIT sinon :
  |     50: _("""
  |      La méthode X-FEM n'est pas disponible avec %(k1)s.
  | """),
  | On a ainsi également un message explicite si l'on cherche à utiliser GDEF_LOG par exemple.
  | 
  | - Suppression du tests ssnv507 (src et validation)
  | - Suppression de la doc V6.04.507

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V6.04.507
- VALIDATION : ras


================================================================================
                RESTITUTION FICHE 30888 DU 14/04/2021
================================================================================
- AUTEUR : DROUET Guillaume
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [FAUX] Récupération des caractéristiques élastiques variables en temps
- FONCTIONNALITE :
  | Problème :
  | ========
  | Il existe une routine générique pour récupérer les coefficients élastiques (get_elas_para)
  | Une modification a déjà été faite pour rendre le paramètre "time" optionel, mais ce même paramètre est souvent obligatoire dans 
  | les
  | chaînes d'appel vers get_elas_para. 
  | 
  | Dans certains cas les routines n'ont pas accès à l'information du temps, certains des appelants de ces routines font le choix 
  | suivant:
  | 1/ mettre time=0 => faux temps
  | 2/ mettre time=r8vide => faux temps mais ça nous protège un peu car ça correspond à un temps "infini"
  | 3/ Utiliser une séquence comme celle-ci
  | call tecach('ONO', 'PTEMPSR', 'L', iret, nval=8, itab=itab)
  | ibid = itab(1)
  | if (iret .eq. 0) then
  |   inst = zr(ibid)
  | else
  |    inst = zero
  | endif
  | 
  | Avec ce paramètre temps potentiellement faux:
  | * Si les paramètres élastiques ne dépendent pas du temps => pas de souci
  | * Si les paramètres élastiques dépendent du temps et que le temps est inexact => risque de résultats faux
  | 
  | Correction :
  | ==========
  | 
  | Développement :
  | -------------
  | 
  | On choisit d'étendre l'utilisation de r8vide() pour initialiser "time" dans les arbres d'appels à get_elas_para.
  | On modifie get_elas_para pour ne pas considérer de paramètre INST quand time vaut r8vide().
  | On reprend toutes les chaînes d'appel pour initialiser time à r8vide() quand cela est nécessaire
  | 
  | Validation :
  | ----------
  | 
  | Lancement de la base de tests => Un seul test plante : func01e. 
  | En effet avant correction, time est mis à 0 dans l'option RIGI_MECA (te0011) donc ça passe avec le temps zéro. 
  | Après correction, on plante, ce qui est une bonne nouvelle ! 
  | Je propose de faire les ajouts nécessaires dans meca_3d.py et dans te0011.F90 pour que le test soit OK.
  | 
  | J'attire votre attention sur le fait que l'erreur produite n'est pas très claire pour l'utilisateur :
  | 
  | Traceback (most recent call last):
  |   File "code_of_00000005", line 1, in <module>
  | NameError: name 'INST' is not defined
  | Traceback (most recent call last):
  |   File "./ssnv163a.comm.changed.py", line 199, in <module>
  |     ITER_GLOB_MAXI = 25)
  | 
  | Il me semblait qu'il y avait un message aster du type : "on ne trouve pas le paramètre INST".
  | 
  | 
  | Risques de résultats faux : 
  | =========================
  | 
  | Code_Aster a pu produire des résultats faux si on utilisait ELAS**_FO avec des formules ou des fonctions dépendant du paramètre 
  | INST dans les cas suivants 
  | :
  | 
  | - Calcul de SIEF_ELGA pour XFEM 2D
  | - Résolution mécanique linéaire et non-linéaire avec éléments incompressibles en grandes déformations 3D/D_PLAN/AXIS 
  | - Résolution mécanique linéaire et non-linéaire en hyper-élasticité X-FEM  2D et 3D
  | - Calcul de SIEF_ELGA avec X-FEM en 3D
  | - Commande POST_ELEM/ENER_POT en 3D et 2D
  | - Calcul de SIEF_ELGA éléments iso-paramétriques 3D, 2D et 2D Fourier
  | - Calcul de SIEF_ELGA éléments incompressible sen petites déformations
  | - Résolution mécanique RIGI_MECA FOURIER 2D
  | - Résolution mécanique RIGI_MECA 3D_SI
  | - Résolution mécanique RIGI_MECA 2D et 3D
  | - Résolution mécanique RIGI_MECA_HYST 3D et 2D
  | - Commande DEFI_MATERIAU/ELAS_ORTH_FO
  | - Commande POST_ELEM 3D/2D OPTIONS : 'INDIC_ENER''INDIC_SEUIL'ENEL_ELEM''ENER_TOTALE'
  | - Commande DEFI_MATERIAUAFFE_VAR_C(NOM_VARC='TEMP',)  3D, 2D et éléments FOURIER
  | - Commande AFFE_CHAR_MECA/PRE_EPSI en 2D et 3D
  | - CALC_CHAMP: déformations dues aux variables de commande (ex: dilatation thermique) en 3D

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 15.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 16.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : submit + test perso


================================================================================
                RESTITUTION FICHE 31634 DU 13/12/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Préparation des champs avant CALC_COUPURE
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Suite à la fiche issue31348, on s'aperçoit que CALC_COUPURE se permet de calculer EFGE_ELNO quelque soit le contenu de la SD en entrée. En plus de n'être pas
  | efficace sur des gros modèles (on risque de calculer deux fois EFGE_ELNO dans une étude), ça induit des effets de bord qui nécessiterait une programmation
  | compliquée dans la macro pour vérifier la présence du champ
  | 
  | On propose que CALC_COUPURE oblige l'utilisateur à calculer EFGE_ELNO avant et que CALC_COUPURE ne la fasse pas lui-même.
  | 
  | 
  | Développement
  | -------------
  | 
  | On modifie la macro pour que EFGE_ELNO ne soit plus calculé en interne mais avant son appel.
  | Par contre, on ne peut pas faire le calcul d'EFGE_NOEU avant de faire le MODI_REPERE,ce qui veut dire qu'on est obligé de faire un CALC_CHAMP au moins pour
  | EFGE_NOEU dans la macro.
  | Si on veut supprimer ça, la macro-commande n'a plus beaucoup d'intérêt (ceci dit, dans l'absolu, il s'agit d'un post-traitement trop complexe et qui mériterait
  | d'être conçu de manière plus simple)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.81.24
- VALIDATION : zzzz115
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32050 DU 03/05/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : waf configure échoue si la version de PETSc n'a pas de patch
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Si la version de PETSc n'a pas de patch, le configure échoue.
  | 
  | 
  | Correction
  | ----------
  | 
  | Dans le test, on met PETSC_VERSION_PATCH à 0 s'il n'est pas défini (c'est ce que met PETSc pour la 3.15.3 sans patch).
  | Par précaution, on fait pareil pour PETSC_VERSION_SUBMINOR.
  | 
  | Bonus :
  | - Suite aux problèmes sur Cronos, on utilise 'addmem' quand on compile les catalogues d'éléments (déjà fait en v15).
  | - En cas de lancement impossible (on ne peut pas créer le répertoire de travail dans /tmp !), on s'assure que le code retour est initialisé.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : configure & build
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32054 DU 04/05/2022
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Problème de communicateur mpi dans aster_fieldsplit
- FONCTIONNALITE :
  | Problème
  | --------
  | Quand on utilise le préconditionneur fieldsplit avec des combinaisons "particulières" de composantes, on peut 
  | se 
  | retrouver  dans une situation où des objets ne partagent pas le même communicateur mpi. Cela fait planter le 
  | code.
  | 
  | 
  | Solution
  | --------
  | Pour le préconditionneur fieldsplit, on crée différents fields par paquet de composantes. Quand on utilise 3 
  | fields ou plus, on génère des vecteurs d'indice (index set, IS) qui décrivent l'entrelacement des composantes 
  | des différents fields. Il peut alors arriver que, lors de la création de ces IS, des sous-communicateurs 
  | soient 
  | créés par PETSc. 
  | Par ailleurs on crée des vecteurs distribués selon le communicateur par défaut. Lorsque l'on veut extraire un 
  | sous-vecteur en utilisant un IS d'un field, on peut alors se retrouver dans la situation où ces 2 objets ne 
  | partagent pas le même communicateur.
  | Pour corriger, on oblige l'IS à se redistribuer sur le communicateur par défaut, en utilisant un appel à la 
  | fonction ISOnComm de PETSc.
  | 
  | 
  | Validation
  | ----------
  | Je définis une distribution des composantes "tordues" dans petsc04f qui faisait planter le code mais qui 
  | fonctionne maintenant.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Nouveau test
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32060 DU 06/05/2022
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : ksp_util ne rend pas le ksp construit
- FONCTIONNALITE :
  | Problème
  | --------
  | Une fonctionnalité permettant de définir en python un ksp a été introduite.
  | Malheureusement, le ksp est bien construit mais pas utilisé.
  | 
  | 
  | Solution
  | --------
  | En fait le ksp est bien défini mais la définition est écrasée plus tard dans le code.
  | On corrige.
  | 
  | 
  | Validation
  | ----------
  | Je définis un ksp et un pc différents des défauts de code_aster dans petsc05a.
  | J'ajoute également une modélisation b où j'implémente le Reverse Constrained Augmented Preconditioner de 
  | Franceschini et al. : https://doi.org/10.48550/arXiv.2111.05599

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v1.04.120,u4.50.01
- VALIDATION : Nouveau test
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32034 DU 22/04/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Documentation embarquée sur RTD
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | L'objectif de cette fiche est de publiée automatiquement la documentation embarquée sur ReadTheDocs (RTD).
  | 
  | 
  | Développement
  | -------------
  | 
  | Pour automatiser la publication d'une documentation Sphinx sur RTD, il faut :
  | - un dépôt sur GitHub ou GitLab,
  | - indiquer où se trouve la doc dans le dépôt (même s'il peut trouver tout seul),
  | - préciser l'environnement nécessaire.
  | 
  | Pour les projets en Python pur ou avec des extensions compilées de taille raisonnable (temps de compilation, prérequis facilement installable), c'est très
  | facile.
  | Comme d'habitude pour code_aster, c'est un peu plus compliqué car Sphinx a besoin de faire "import code_aster", donc en particulier "import libaster". Il faut
  | donc tout construire !
  | 
  | pydoc (module standard) inspecte le source pour afficher la doc d'un module en texte ou html.
  | On crée donc un "renderer" (`doc/pydoc_pyrenderer.py`) particulier qui produit du code Python à partir de la signature d'une classe, méthode ou fonction et de
  | ses docstrings.
  | La différence par rapport au "renderer" TextDoc est assez limitée.
  | Pour libaster, seules les classes, méthodes et fonctions nous intéressent (on n'a pas adapté 'docmodule' par exemple).
  | Ce "renderer" est appelé sur tous les objets de 'libaster.so' (`doc/build_pylibaster.py`) pour générer un fichier 'libaster.py' qui contient tous les signatures
  | et docstrings sans le code.
  | => Pour le trouver, on adapte le PYTHONPATH dans le fichier config de Sphinx. On pourra ainsi construire la doc Sphinx sans compiler.
  | 
  | Dans quelques sources Python, on trouve 'import aster' : on les "mock" dans le fichier de config de Sphinx pour les ignorer.
  | 
  | La configuration pour RTD est indiquée dans '.readthedocs.yaml' : image docker Ubuntu standard, Python 3.7 + numpy.
  | Contrairement à un message précédent, la doc se construit bien avec des versions récentes de Sphinx (ici 4.5.0).
  | 
  | Quand on crée le projet sur RTD, on indique l'URL du dépôt sur GitLab, la ou les branches. En validant l'accès au dépôt, un webhook est automatiquement ajouté
  | pour qu'à chaque nouvelle révision, la documentation soit reconstruite et publiée.
  | On ajoute un badge sur notre README pour voir si la doc se construit bien.
  | 
  | On modifie la génération de la doc à partir des sources avec 'waf' :
  | 
  | - Uniquement en MPI (sinon la doc générée est incomplète), erreur sinon :
  |   'libaster.py' must be generated using a parallel build of 'libaster.so'.
  | 
  | - Il faut construire la version (`waf_mpi install`) **avant** de générer la doc, sinon :
  |   FileNotFoundError: [Errno 2] No such file or directory: '/.../src/build/mpi/release/bibc/libaster.so'                                                         
  |                     
  | 
  | - Quand on construit la doc avec `waf_mpi doc` :
  | . 1. on génère 'libaster.py', copié dans `doc/_fake/`,
  | . 2. on génère les fichiers RST "autodoc", copiés dans `doc/devguide/`,
  | . 3. on construit les pages html Sphinx,
  | . 4. on installe la doc Sphinx dans `<install-directory>/share/doc/html/`.
  | 
  | 1. et 2. modifient potentiellement les fichiers dans le dépôt. Ils doivent être inclus dans le développement.
  | La doc est *enfin* installée : firefox ../install/mpi/share/doc/html/index.html
  | 
  | Les dépendances sont mieux définies : Si on modifie une docstring dans pybind, 'libaster.py' est regénéré, 'autodoc' aussi, puis 'html'. Si on ne modifie qu'un
  | fichier rst, 'libaster' inchangé, seul 'html' est refait.
  | 
  | Voir à remplacer le script 'generate_rst.py' par des appels à 'sphinx-autodoc' : Non ou il faudrait faire plein d'appels pour organiser/filtrer logiquement.
  | Si la doc était mal transcrite entre pybind et libaster.py, émettre une fiche.
  | Publier 'pydoc_pyrenderer' sur GitLab, peut servir et être amélioré par d'autres.
  | 
  | Sur RTD, on peut consulter la documentation Sphinx sur https://codeaster.readthedocs.io/

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : build & doc
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 32066 DU 10/05/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Construction de la version MPI par défaut
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Construire par défaut la version MPI.
  | 
  | 
  | Développement
  | -------------
  | 
  | src
  | ~~~
  | 
  | On construit maintenant la version MPI par défaut : 'waf' pointe vers 'waf_mpi'.
  | 
  | En passant les tests séquentiels avec la version parallèle, sslv109f est en erreur (object inexistant).
  | Cela vient des méthodes `isMPIFull()` des matrices et termes élementaires. On attend MPI_COMPLET et non OUI !
  | 
  | devtools
  | ~~~~~~~~
  | 
  | 'submit' impose une construction MPI.
  | 
  | 'run_testcases' ne change pas pour le moment. Si la version est parallèle, il ne lance que les tests parallèles. On peut ajouter l'option '--add-sequential'
  | pour lancer également les tests séquentiels. Si la version est séquentielle, elle ne lance toujours pas les tests parallèles !
  | 
  | 'submit' utilise '--add-sequential'. Il vérifie donc les tests séquentiels et parallèles de la liste 'submit'.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sslv109f + submit seq
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32059 DU 06/05/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Supprimer elementary_computations
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Supprimer elementary_computations
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Je supprime elementary_computation.py et j'utilise DiscreteProblem dans incremental_solver à la place
  | 
  | Bonus: je supprime le allLoadDisct de PhysicalProblem car on n'en a plus besoin et c'était moche
  | 
  | Tout les tests zzzz506* sont ok

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz506a-n
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 27689 DU 24/05/2018
================================================================================
- AUTEUR : HABIB Souheil
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Extension de l'opérateur CALC_FERRAILLAGE aux éléments 1D de type poutres et poteaux
- FONCTIONNALITE :
  | Problème/Objectif/Corrections/Développements:
  | ---------------------------------------------
  | 
  | FINALISATION DES DEVELOPPEMENTS DEBUTES EN 2021.
  | Correction et validation des cas tests sur CRONOS.
  | 
  | --------------------------------------------------
  | 
  | Dans le cadre de l'enrichissement de l'opérateur de calcul de ferraillage de code_aster, CALC_FERRAILLAGE, il s'agit de l'ajout des mots clés suivants :
  | 
  | - TYPE_STRUCTURE [fiche actuelle, 27689] : ce mot clé peut prendre comme valeur '1D' ou '2D', permettant désormais d'étendre le champ d'applications de
  | l'opérateur aux éléments linéiques 1D (POU_D_E), sachant que la version antécédente de CALC_FERRAILLAGE été réservée aux éléments surfaciques 2D (DKT).
  | 
  | - TYPE_COMB = 'ELS_QP' [fiche 31307] : ajout d'un 3e état limite de calcul (ELS_QP = Etat Limite de Service Quasi Permanent), vis-à-vis de la vérification des
  | ouvertures des fissures, de sorte à ce que le mot-clé TYPE_COMB puisse désormais prendre l'une des options suivantes ('ELU','ELS','ELS_QP'). Pour cette fin, de
  | nouveaux mots-clés ont du être ajoutés, à savoir les diamètres des barres d'armatures (PHI_INF_X, PHI_SUP_X, PHI_INF_Y, PHI_SUP_Y, PHI_INF_Z, PHI_SUP_Z), les
  | ouvertures maximales de fissuration autorisées ([WMAX_INF,WMAX_SUP ; si TYPE_STRUCTURE = '2D'] ET [WMAX_INF_Y,WMAX_SUP_Y,WMAX_INF_Z,WMAX_SUP_Z ; si
  | TYPE_STRUCTURE = '1D']), la contrainte de compression limite dans le béton pour la maitrise du fluage non linéaire (SIGC_ELS_QP) et le coefficient de durée de
  | chargement (KT).
  | 
  | - FERR_MIN [fiche 31353] : ajout de la possibilité de prise en compte d'un ferraillage minimum, par le biais du nouveau mot-clé 'FERR_MIN'. Ce mot-clé peut
  | prendre comme attribut l'un des choix suivants : 'CODE' (le calcul du ferraillage minium sera effectué conformément aux spécifications de la CODIFICATION
  | retenue), 'NON' (pas de ferraillage minimum à prendre en compte) et 'OUI' (prise en compte d'un ferraillage minimum avec saisie directe des valeurs des ratios
  | de ferraillage minimum ; pour ce fait deux nouveaux mots-clés permettent de répondre à cette fin, à savoir 'RHO_LONGI_MIN' [ratio minimum pour le ferraillage
  | longitudinal de flexion] et 'RHO_TRNSV_MIN' [ratio minimum pour le ferraillage transversal])
  | 
  | Complémentairement à ces 3 axes d'évolution, les mots-clés/variantes suivants ont également été introduits :
  | 
  | - FERR_SYME = ('OUI','NON') : dimensionnement d'un ferraillage symétrique (entre les nappes SUP et INF)
  | - FERR_COMP = ('OUI','NON') : prise en compte d'un ferraillage de compression éventuel (équilibres en Pivots B et C)
  | - EPURE_CISA = ('OUI','NON') : prise en compte de l'effort de traction supplémentaire du à l'effort tranchant et à la torsion
  | - EYS : saisie manuelle de la valeur du module d'Young de l'acier
  | - TYPE_DIAGRAMME = ('B1','B2') : choix de la forme du diagramme (σ-ε) à utiliser: B1 (Incliné) ou B2 (Horizontal)
  | 
  | Description brève des cas tests associés :
  | -----------------------------------------
  | 
  | ssls134a - Calcul du ferraillage d'une plaque à l'ELU
  | ssls134b - Calcul du ferraillage d'une plaque à l'ELS
  | ssls134c - Calcul du ferraillage d'une plaque à l'ELS_QP
  | ssll13a - Calcul du ferraillage d'une poutre à l'ELU
  | ssll13b - Calcul du ferraillage d'une poutre à l'ELS
  | ssll13c - Calcul du ferraillage d'une poutre à l'ELS_QP
  | 
  | Autres mentions importantes et impact sur COMBINAISON_FERRAILLAGE :
  | -------------------------------------------------------------------
  | 
  | L'introduction du mot-clé 'TYPE_STRUCTURE' induit un impact vis-à-vis de certains mots-clés existants, avec un effet de 'dédoublement' :
  | 
  |   --> Enrobage :
  |   si TYPE_STRUCTURE = '2D', les mots-clés 'C_INF' et 'C_SUP' s'activent et sont obligatoires
  |   si TYPE_STRUCTURE = '1D', les mots-clés 'C_INF_Y', 'C_SUP_Y', 'C_INF_Z' et 'C_SUP_Z' s'activent et sont obligatoires
  | 
  |   --> Contraintes limites à l'ELS :
  |   si TYPE_STRUCTURE = '2D', les mots-clés 'SIGC_INF_ELS' et 'SIGC_SUP_ELS' s'activent, et sont obligatoires si TYPE_COMB = 'ELS'
  |   si TYPE_STRUCTURE = '1D', les mots-clés 'SIGC_INF_Y_ELS', 'SIGC_SUP_Y_ELS', 'SIGC_INF_Z_ELS' et 'SIGC_SUP_Z_ELS' s'activent, et sont obligatoires si TYPE_COMB
  | = 'ELS'
  | 
  | Les documentations u4.81.42 et r7.04.05 sont modifiées de sorte à prendre en compte les évolutions listées.
  | Les cas tests de COMBINAISON_FERRAILLAGE (ssls145a et ssls146a) doivent être altérés et mis à jour afin d’être en cohérence avec la nouvelle version de
  | CALC_FERRAILLAGE. Une restructaration de la macro COMBINAISON_FERRAILLAGE est également à attendre dans les prochains développements.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.81.42, r7.04.05, v3.03.134, v3.03.013
- VALIDATION : ssls134a, ssls134b, ssls134c, ssll13a, ssll13b, ssll13c
- NB_JOURS_TRAV  : 60.0


================================================================================
                RESTITUTION FICHE 31935 DU 22/03/2022
================================================================================
- AUTEUR : HABIB Souheil
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Evolution de la macro commande COMBINAISON_FERRAILLAGE
- FONCTIONNALITE :
  | Problème/Objectif/Corrections/Développements:
  | ---------------------------------------------
  | 
  | Suite aux développements portés par la fiche 27689 vis-à-vis de l'enrichissement et évolution de l'opérateur de calcul de ferraillage (CALC_FERRAILLAGE)
  | [notamment vis-à-vis de l'extension de l'opérateur aux éléments 1D], il s'agit dans la présente fiche de mettre à niveau la macro commande
  | COMBINAISON_FERRAILLAGE, pour pouvoir prendre en compte ces nouvelles capacités de l'opérateur de calcul de ferraillage
  | 
  | Impact Sources :
  | Évolution apportée aux fichiers 
  | /code_aster/Cata/Commands/combinaison_ferraillage.py
  | /code_aster/MacroCommands/combinaison_ferraillage_ops.py
  | 
  | Impact Documentations : 
  | Évolution apportée à la doc u4.81.44, et aux docs v3.03.145 et v3.03.146
  | 
  | Impact Cas Tests :
  | ssls126e / ssls145a / ssls146a

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : u4.81.44,v3.03.145,v3.03.146
- VALIDATION : ssls145a,ssls146a
- NB_JOURS_TRAV  : 20.0


================================================================================
                RESTITUTION FICHE 31815 DU 11/02/2022
================================================================================
- AUTEUR : Yi ZHANG
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Calcul des options MASS_MECA et AMOR_MECA: ecriture en C++
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Exercice (pour les débutants en C++) après issue31503
  | 
  | Écrire en C++ (et binder en Python) les routines permettant le calcul des options MASS_MECA et AMOR_MECA.
  | MASS_MECA (memame.F90): très facile, ressemblera à RIGI_MECA
  | AMOR_MECA (meamme.F90): demande une évolution, il faut permettre des RESU_ELEM comme champs d'entrée dans la classe Calcul. Ce 
  | n'est pas fait actuellement
  | 
  | 
  | Correction/Développement
  | ------------------------
  | On réécrit les options 'MASS_MECA' et 'AMOR_MECA de CACL_MATR_ELEM en C++ dans DiscreteComputation.cxx. Deux nouvelles méthodes 
  | massMatrix et dampingMatrix sont créées en s'inspirant de elasticStiffnessMatrix qui est 'RIGI_MECA'. 
  | 
  | Résultat :
  | 
  | - MASS_MECA est OK. 
  | 
  | - Pour AMOR_MECA il reste encore deux problèmes :
  | 1.erreur suivante pour certains éléments de structure :
  |  ║ Pour le calcul de AMOR_MECA, on ne trouve pas dans les arguments de la routine CALCUL ║
  |  ║ de champ à associer au paramètre : PCOMPOR
  | 
  | Dans DiscreteComputation.cxx, on fait   _calcul->addInputField( "PCOMPOR", currMater->getBehaviourField() ) pour récupérer le 
  | champs, je ne sais pas pourquoi cela ne marche pas pour ces éléments structure. Je créerai une nouvelle fiche.  
  | La solution mise en place temporairement est d'utiliser Fortran quand il s'agit des éléments de structure (présence de 
  | CARA_ELEM).  
  | 
  | 
  | 2.pas de cas test si GROUP_MA est renseigné pour calculer AMOR_MECA sur une partie de maille. J'ai créé un cas test dédié. Il 
  | ne passe pas même avec la source en Fortran. Je créerai une autre fiche...

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas tests
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 30925 DU 26/04/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : les classes DiscreteProblem et LinearSolver ne fonctionnent qu'en mécanique
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Les classes DiscreteProblem et LinearSolver ne fonctionnent que pour des problèmes de mécanique.
  | Il serait sûrement possible de les rendre template pour supporter les autres phénomènes.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | Maintenant c'est possible. 
  | 
  | Sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32027 DU 21/04/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : aide utilisation concernant Documentation (VERSION 11.*)
- TITRE : Compléments doc R4.07.03
- FONCTIONNALITE :
  | Suite à demande YuanSuan, on améliore la doc R4.07.03 « Calcul de matrice de masse ajoutée sur base modale ».

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 1.0

