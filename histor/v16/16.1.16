==================================================================
Version 16.1.16 (révision 26644d13a04d) du 2022-04-20 15:32 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32010 LORENTZ Eric             Introduire ECRO_NL_FO pour dépendance en température
 31986 COURTOIS Mathieu         zzzz295a cassé
 32026 COURTOIS Mathieu         Eviter mpiexec pour exécuter les cas séquentiels
 31334 ABBAS Mickael            PREDICTION='EXTRAPOLE' nécessite une résolution
 32022 ABBAS Mickael            Allongement du temps de calcul d' AFFE_CHAR_MECA_7 en version 16.1.14
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32010 DU 11/04/2022
================================================================================
- AUTEUR : LORENTZ Eric
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Introduire ECRO_NL_FO pour dépendance en température
- FONCTIONNALITE :
  | C'est fait. L'emploi dès le départ de RCVALB rend le travail facile :
  | 1) Ajout du catalogue ECRO_NL_FO dans DEFI_MATERIAU
  | 2) Transfert des valeurs par défaut du catalogue vers le fortran
  | 
  | Lois de comportement concernées : GTN, VISC_GTN, VMIS_ISOT_NL, VISC_ISOT_NL
  | 
  | Complément de validation très modéré car le mécanisme rcvalb est générique et l'évaluation des paramètres est bien demandé en t+ (nouveau formalisme en
  | prédiction)-> on teste uniquement le passage dans le mot-clé facteur ECRO_NL_FO via le cas-test ssnv251a dans lequel on décrit les paramètres comme des
  | fonctions constantes.
  | 
  | Sans impact documentaire (on n'a jamais dit que les paramètres ne pouvaient pas dépendre de la température...).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : INFORMATIQUE
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31986 DU 05/04/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : zzzz295a cassé
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le test zzzz295a échoue de temps en temps avec le message :
  | "On ne peut pas interpoler la fonction à l'instant..."
  | 
  | 
  | Correction
  | ----------
  | 
  | On repère deux anomalies détectées par valgrind :
  | 
  | 1. Conditional jump depends on uninitialised value dans lc0000 : typmod(2)
  | 
  | Il faut l'initialiser dans comcq1 : typmod(2) = ' '
  | 
  | 2. Conditional jump depends on uninitialised value dans folocx : vale(1)
  | 
  | Il s'agit du .VALE d'une fonction qui pointe vers une zone qui a été libérée et qui avait
  | été allouée par `Function.setValues()`.
  | 
  | Dans fointe, il y a un mécanisme de cache pour ne pas appeler jeveuo à chaque fois.
  | Au premier appel, on fait jeveut pour garder les objets en mémoire.
  | 
  | On peut croire que les objets (.PROL, .VALE) sont libérés lors de l'appel à jjldyn.
  | Bug dans jjldyn qui libérerait des objets jeveut ?
  | Non, en fait, c'est jelibz qui est appelé à la fin de la commande précédente (STAT_NON_LINE).
  | 
  | Or, la commande suivante est MECA_STATIQUE qui ne passe pas par un op.
  | Et donc le common du cache de fointe n'est pas réinitialisé !
  | Donc on reprend l'adresse du .VALE précédent qui n'est plus là !
  | 
  | On corrige en déplaçant l'appel à foint0 (réinitialisation) à la fin de l'op au lieu du début.
  | 
  | Il n'y a alors plus de messages valgrind.
  | 
  | De plus, on ajoute un context manager pour appeler la fonction `exec_` des commandes (y compris les commandes
  | pures Python):
  | 
  | with command_execution():
  | . . self.exec_(keywords)
  | 
  | Quand on entre dans le "with", on appelle cmd_ctxt_enter() == call superv_before()
  | Quand on sort du bloc "with", on appelle cmd_ctx_exit() == call superv_after()
  | Le fonctionnement est ainsi identique à ce que fait `execop` autour d'un op fortran.
  | 
  | 
  | Note sur l'usage de valgrind
  | ----------------------------
  | 
  | - Récupérer le fichier de suppression `valgrind-python.supp` sur github de Python dans
  | https://github.com/python/cpython/blob/main/Misc/
  | 
  | - Voir le README.valgrind au même endroit : décommenter les lignes correspondant à PyObject_Free et PyObject_Realloc.
  | 
  | - Ajouter l'option `--suppressions=$HOME/dev/codeaster/valgrind-python.supp` selon l'emplacement du fichier.
  | 
  | - Python fait un peu comme jeveux pour allouer les petits objets efficacement. On peut avoir des 
  | messages plus pertinents en définissant la variable d'environnement :
  | 
  | export PYTHONMALLOC=malloc
  | 
  | On peut configurer la ligne de commande de valgrind dans ~/.config/aster/config.json (tel que vu dans le conteneur)
  | (La doc embarquée, §run_aster.config, donne cet exemple, sans --suppressions).
  | 
  | {
  |     "server": [
  |         {
  |             "name": "*",
  |             "config": {
  |                 "exectool": {
  |                     "valgrind": "valgrind --tool=memcheck --leak-check=full --error-limit=no --track-origins=yes
  | --suppressions=$HOME/dev/codeaster/valgrind-python.supp"
  |                 }
  |             }
  |         }
  |     ]
  | }
  | 
  | On a juste à faire :
  | ../install/mpi/bin/run_aster --exectool=valgrind astest/zzzz295a.export

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz295a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32026 DU 20/04/2022
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Eviter mpiexec pour exécuter les cas séquentiels
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Utiliser la version MPI par défaut ne pose pas de problème sauf que mpiexec prend toujours quelques secondes
  | à démarrer le processus, y compris quand le cas à lancer est séquentiel.
  | Pour 4000 cas-tests, ça fait long !
  | 
  | 
  | Développement
  | -------------
  | 
  | La plupart du temps, on peut lancer un cas séquentiel sans mpiexec.
  | Cela peut dépendre selon qu'on utilise OpenMPI, IntelMPI, de la couche réseau...
  | 
  | On ajoute donc un paramètre de configuration : require_mpiexec (bool).
  | 
  | Lors du configure, on teste si on peut exécuter un programme simple (celui qui mesure la consommation mémoire).
  | Si le test échoue, on met require_mpiexec à True.
  | Sinon, c'est False par défaut.
  | 
  | Si le test passe, on peut quand même imposer de toujours utiliser mpiexec en définissant la variable
  | d'environnement :
  | CONFIG_PARAMETERS_require_mpiexec=1

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : configure + run_ctest
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31334 DU 20/08/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : PREDICTION='EXTRAPOLE' nécessite une résolution
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | 
  | Quand on utilise PREDICTION='EXTRAPOLE' dans *_NON_LINE, aucune résolution n'est comptabilisée pour cette étape (dans la synthèse 
  | produite après les pas de temps). Or il y en bien une.
  | 
  | 
  | Correction
  | -----------
  | 
  | Le compteur n'a pas été ajouté lors de l'étape de projection des conditions limites sur la matrice élastique. C'est aussi le cas aussi de DEPL_CALCULE.
  | On ajoute ce compteur dans nmprca.F90

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : visuelle
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32022 DU 19/04/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Allongement du temps de calcul d' AFFE_CHAR_MECA_7 en version 16.1.14
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Entre les dates : 2022-04-08 et 2022-04-10 : (donc le problème peut être en version 16.1.13 tout compte fait)
  | 
  | 39.3% perf001d   AFFE_CHAR_MECA_7      10.61    14.78
  | 38.0% perf001a   AFFE_CHAR_MECA_7       2.87     3.96
  | 36.8% perf004e   AFFE_CHAR_MECA_7       2.88     3.94
  | 36.7% perf001e   AFFE_CHAR_MECA_7       2.89     3.95
  | 35.9% perf004d   AFFE_CHAR_MECA_7       2.90     3.94
  | 35.8% perf001c   AFFE_CHAR_MECA_7       3.32     4.51
  | 31.0% perf004c   AFFE_CHAR_MECA_7       0.87     1.14
  | 
  | 
  | Correction
  | ----------
  | 
  | Impossible de reproduire cette dégradation des perf sur scibian10, le temps d'AFFE_CHAR_MECA reste égal (à peu près 8s)
  | 
  | 16.1.12
  |  * AFFE_CHAR_MECA           :       8.57 :       0.01 :       8.58 :       8.58 *
  | 
  | 16.1.13
  |  * AFFE_CHAR_MECA           :       8.25 :       0.03 :       8.28 :       8.28 *
  | 
  | 16.1.14
  |  * AFFE_CHAR_MECA           :       8.26 :       0.03 :       8.29 :       8.30 *
  | 
  | Classement sans suite donc

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : perf001d
- NB_JOURS_TRAV  : 0.5

