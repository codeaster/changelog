==================================================================
Version 16.4.19 (révision 1671b731f3d9) du 2023-12-15 16:08 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 33507 COURTOIS Mathieu         adapt_increment plante quand ETAT_INIT est une liste
 33504 LE CORVEC Véronique      En 16.4.18, sdll109a, sdll109b, sdll109c, sdll109d sont NOOK
 33478 MARCHENKO Arina          Test perfe02a en erreur
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 33507 DU 15/12/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : adapt_increment plante quand ETAT_INIT est une liste
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | ETAT_INIT est un mot-clé facteur avec max=1.
  | C'est censé fonctionner si on donne une liste de longueur 1: ETAT_INIT=[_F(...)]
  | mais ça plante dans la fonction de compatibilité pour INST_INIT.
  | 
  | 
  | Correction
  | ----------
  | 
  | On faisait :
  | 
  | init_state = keywords.get("ETAT_INIT", {})
  | 
  | 
  | Il manque une fonction pour récupérer la 1ère occurrence.
  | 
  | On fait :
  | 
  | init_state = keywords.get("ETAT_INIT", {})
  | if init_state and type(init_state) in (list, tuple):
  | . . init_state = init_state[0]
  | 
  | sdnv140e dans validation était dans ce cas.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdnv140e
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33504 DU 15/12/2023
================================================================================
- AUTEUR : LE CORVEC Véronique
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En 16.4.18, sdll109a, sdll109b, sdll109c, sdll109d sont NOOK
- FONCTIONNALITE :
  | Suite à issue33441 Update formula for lumped mass of beams, on met à jour les tolérances pour 
  | les tests sdll109a/ sdll109b/ sdll109c / sdll109d

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdll109a
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 33478 DU 07/12/2023
================================================================================
- AUTEUR : MARCHENKO Arina
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Test perfe02a en erreur
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | *Problème de convergence du test de validation perfe02a (traction sur une éprouvette de ténacité 2D) sur gaia et problème 
  | de cpu_limit sur cronos.
  | *Non conformité du test la base de validation (TEST_TABLE à changer).
  | 
  | 
  | Correction/Développement
  | ------------------------
  | Le dernier instant de calcul a été modifié. On s'arrête à F_MAX = 2623.5 N pour t=45 s.
  | Cela permet de ressoudre le problème de cpu_limit sur CRONOS (409 s) et de convergence sur GAIA (solveur n'est pas 
  | touché).
  | Le syntaxe a été changé, le document associé (V1.02.012) est modifié et déposé sur DocAster.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.02.012
- VALIDATION : perfe02a
- NB_JOURS_TRAV  : 1.0

