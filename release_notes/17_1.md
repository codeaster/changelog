# code_aster version 17.1 Release note

## Mécanique

### Fiche 26870 : sorties des champs RESI_GLOB_RELA via MECA_NON_LINE

Pour la commande MECA_NON_LINE, les champs de résidus aux noeuds sont stockés au cours du calcul et peuvent être imprimés à l'issu.

## Thermique

### Fiche 33079 : THER_NON_LINE - TRANSITOIRE

La commande THER_NON_LINE est maintenant implémentée côté Python. Certains mots-clés ne sont pas encore pris en charge (séchage, réduction de modèle), dans ce cas l'opérateur Fortran est appelé.

## Interaction Fluide-Structure

### Fiche 32094 : Evolution de DEFI_SPEC_TURB (SPEC_CORR_CONV_1-2)/PROJ_SPEC_BASE

Les opérateurs DEFI_SPEC_TURB/ PROJ_SPEC_BASE ne permettaient pas de réaliser un calcul de réponse d'une structure à la turbulence de manière adaptée. Les options disponibles étaient trop restrictives concernant les spectres et longueurs de corrélation. Les évolutions portent sur:

- la possibilité de définir des longueurs de corrélation et vitesses convectives
- la possibilité d'intégrer une longueur de corrélation transversale pour Au-Yang et Corcos
- la possibilité de définir des longueurs de corrélation dépendant de la pulsation
- la suppression de la corrélation d'AU-Yang car fausse

## Lois de comportement

### Fiche 32232 : Introduction d'une loi de comportement quasi-fragile avec plafonnement en compression: ENDO_LOCA_TC

Introduction une nouvelle loi de comportement pour le béton qui couvre la phénoménologie suivante :

- Endommagement en traction / cisaillement et seuil dédié
- Ecrouissage en traction identique à ENDO_LOCA_EXP
- Endommagement en compression et seuil dédié
- Ecrouissage en compression de type plasticité parfaite asymptotique après une première phase linéaire
- Les seuils ont une expression analytique (robustesse, efficacité)
- La contrainte est bornée quelle que soit la direction de chargement
- Régularisation des points anguleux pour la matrice tangente (comme ENDO_LOCA_EXP)
- Régularisation visqueuse de l’endommagement

### Fiche 33516 : Intégration du modèle de comportement Cam-Clay modifié via Mfront

Intégration du modèle de comportement Cam-Clay modifié (nommé MCC) via MFront. Il remplacera à terme la loi CAM_CLAY pour gagner en robustesse et en performance pour réaliser des calculs non-linéaires. Cette mise à jour permettra de décliner progressivement des variations de MCC, permettant ainsi des prédictions de plus plus réalistes par rapport aux essais expérimentaux sur sols.

### Fiche 24065/23957 : Prise en compte des mot-clés et des variables de commandes par Mfront

Les mot-clés RESI_INTE_MAXI et ITER_INTE_MAXI de COMPORTEMENT avaient des valeurs par défaut qui sont désormais supprimés. Ainsi, les paramètres Epsilon et IterMax du fichier Mfront ne sont plus systématiquement écrasés.  Les valeurs du fichier MFront ne sont surchargées que si le mot-clé est présent.

On renomme RESI_INTE_MAXI et RESI_INTE_RELA en RESI_INTE.
Pour la gestion des variables de commandes, on créé une correspondance dans Behaviour_module entre le nom consacré MFront pour chacune des composantes de variable de commande et le nom de composante aster dans le champ de variables externes généré par code_aster. Cela facilite l'ergonomie de l'usage des variables de commande transmises à Mfront via l'interface MGIS.

### Fiche 33342 : AFFE_MATERIAU en plusieurs morceaux

On peut décomposer en deux appels distincts l'affectation de AFFE_MATERIAU, d'une part, l'affectation des matériaux (AFFE), et d'autre part, l'affectation des variables de commande (AFFE_VARC) en faisant appel au champs matériau déjà défini.

### Fiche 33602 : Vérification consistance des paramétres élastiques pour la géomécanique

Dans DEFI_MATERIAU, avec mots-clé ELAS et ELAS_FO on ne pouvait saisir que E et nu. Pour de nombreux matériaux (sols, roches, etc), on "connaît" mieux K et G=mu (modules de compressibilité et de cisaillement), ou les deux célérités des ondes P et S : Vp et Vs.

Sous ELAS (on ne fait pas pour les fonctions pour le moment), on peut désormais fournir :

- soit (E, NU)
- soit (K, MU)
- soit (CELE_P, CELE_S, RHO)

## Dynamique

### Fiche 31151 : Réduction de la consommation mémoire de DYNA_VIBRA

Dans DYNA_VIBRA, on allouait un gros bloc de mémoire en début de calcul. On a amélioré la consommation mémoire en découpant en plusieurs blocs les objets jeveux afin de pouvoir allouer / décharger au fur à mesure du calcul.

### Fiche 33544 : Modification calcul des déplacements irréversibles de la commande POST_NEWMARK

Mise à jour de la manière dont la commande POST_NEWMARK calcule les déplacements irréversibles à partir de l'accélération moyenne. La méthode implémentée (construction d'un signal fictif en accélération écrêté à la valeur d'accélération critique avant double intégration) donnait moins satisfaction et semblait moins robuste que l'obtention du signal en vitesse à partir de l'intégration de l'accélération moyenne et écrêtement pour les vitesses plus petites que zéro avant intégration pour obtention des déplacements.

L'algorithme  est remplacé par une simple intégration en temps de l'accélération  moyenne et écrêtage à zéro si valeurs inférieures à 0 avant intégration en temps pour obtention des déplacements.

### Fiche 33542 : Amélioration performance étape de projection pour calcul facteur de sécurité avec POST_NEWMARK

On restreint le maillage utilisé pour la projection des contraintes à uniquement les éléments finis 2D adjacents à la ligne de glissement (étape nécessaire pour le calcul de contraintes de peau SIRO_ELEM sur la ligne de glissement).
On ajoute la possibilité pour l'utilisateur de passer une projection par COLLOCATION de l'opérateur PROJ_CHAMP (dans ce cas, SIEF_NOEU doit être pré-calculé par l'utilisateur). La projection est réalisée pour le champ NOEU et ensuite on calcul le champ des contraintes ELGA, instant par instant.

## Eléments de structure

### Fiche 33252/33584 : Calcul d'un champ EGRU d'efforts généralisés en repère global

Dans la commande MODI_REPERE, un nouveau mot-clé NOM_CHAM_RESU a été ajouté et ne concerne que le champ EFGE_ELNO. Si "NOM_CHAM_RESU=EGRU_ELNO" est indiqué, alors un nouveau champ nommé "EGRU_ELNO" est créé et contient le champ EFGE_ELNO calculé dans le nouveau repère. La sd resultat du MODI_REPERE contiendra également EFGE_ELNO qui reste inchangé (LE CHAMP INITIAL). Si NOM_CHAM_RESU est omit, alors le comportement initial de MODI_REPERE est conservé et le champ renseigné dans NOM_CHAM se retrouve modifié (écrasé).
Ces champs sont utilisables avec l'opérateur COMB_SISM_MODAL.

### Fiche 33437 : Dilatation d'un discret soumis à un champ de température

On souhaite prendre en compte la dilatation thermique des discrets. Les développements concernent seulement les discrets K_TR_D_L (en remplacement des poutres, mêmes DDL). Les déplacements aux noeuds dus à la température pour les poutres est du type α*(T-Tref)*L, et dépend donc de la longueur de l'élément. Les déplacements aux noeuds pour les discrets seront de la même forme et donc dépendront de la longueur du discret. Le coefficient de dilatation "alpha" aura donc la même signification pour les poutres et les discrets.

### Fiche 33329 : Blocage de la normale

On introduit un nouveau mot-clé DRNOR dans AFFE_CHAR_MECA / FACE_IMPO afin d'imposer des déplacements suivant la normale locale.

### Fiche 33522 : Nouveau Mot-clé FORCE_COQUE_FO dans  AFFE_CHAR_MECA

Ce nouveau mot-clé permet de fournit un chargement dépendants de X, Y, Z , EP (epaisseur), et ou RHO. On évalue ces fonctions dans l'opérateur pour chaque maille. Cette évaluation n'est plus nécessaire à chaque pas de temps comme c'est le cas avec AFFE_CHAR_MECA_F/FORCE_COQUE.

### Fiche 33517 : Vérification planéité des plaques

La fiche propose de déplacer la vérification de la planéité des plaques (QUAD4) dans AFFE_MODELE au lieu de l'émettre à chaque itération.
L'option VERI_PLAN est créée. Elle utilise le même critère de planéité que l'ancien (dans le te).
On ajoute le mot-clef VERI_PLAN dans AFFE_MODELE

### Fiche 33303 : Définition d'un repère cylindrique pour AFFE_CARA_ELEM / GRILLE_MEMBRANE

Pour déclarer des repères locaux dans AFFE_CARA_ELEM / GRILLE, l'utilisation de VECT_1 ou VECT_2 est pratique et simple. Mais il existe des configurations géométriques qui ne permettent l'usage ni de VECT_1, ni de VECT_2 (repère polaire dans un plan horizontal).
Avec le nouveau choix, REPERE='CYLINDRIQUE', ORIGINE et AXE_Z, les vecteurs VECT_1 / VECT_2 peuvent maintenant être exprimés dans le repère cylindrique.

## Rupture

### Fiche 33415 : POST_BEREMIN - Amélioration des performances

Réécriture partielle de la macro-commande pour la manipulation des champs et l'appel aux commandes aster, pour un gain de temps d'un facteur 1.5 à 5.

## Métallurgie

### Fiche 27307 : Introduction d'un nouveau modèle métallurgique (revenu)

Introduction du phénomène de revenu sur les phases froides (bainite et martensite) dans CALC_META.
Les équations reposent sur le modèle de loi de type Johnson-Mehl-Avrami pour la bainite et la martensite.
Deux phases sont ajoutées : bainite revenue et martensite revenue.
Le revenu agit comme un post-traitement au calcul métallurgique dans CALC_META via le mot-clef facteur REVENU.

Des paramètres matériaux dédiés sont définisdans DEFI_MATERIAU/META_ACIER_REVENU : coefficients loi JMA (martensite/bainite), température de revenu, température de maintien

```text
    META_ACIER_REVENU=FACT(
        statut="f",
        BAINITE_B=SIMP(statut="o", typ="R"),
        BAINITE_N=SIMP(statut="o", typ="R"),
        MARTENSITE_B=SIMP(statut="o", typ="R"),
        MARTENSITE_N=SIMP(statut="o", typ="R"),
        TEMP=SIMP(statut="o", typ="R"),
        TEMP_MAINTIEN=SIMP(statut="f", typ="R", defaut=610.0),
    ),
```

### Fiche 33628 : Ajouter DURT_ELNO pour le revenu

On ajoute le calcul de la dureté (DURT_ELNO) pour le cas des phases revenus.

## Contact

### Fiche 33438 : DIS_CONTACT / DIS_CHOC : Contact dans le repère global

Lorsque l'on souhaite faire du contact avec des discrets (matériau DIS_CONTACT, comportement DIS_CHOC), le contact s’établit dans la direction du discret, avec ou pas de frottement dans le plan perpendiculaire. On a donc du contact qui est traité dans le repère LOCAL du discret. L'évolution consiste à résoudre le contact dans le repère global du maillage et non plus dans le repère local du discret.

L'ajout d'un mot clef dans le matériau DIS_CONTACT permet de définir le type de contact que l'on souhaite. Ce mot clef sera facultatif, avec une valeur par défaut qui est le cas du discret avec contact dans le repère LOCAL :

```text
CONTACT=SIMP(statut="f", typ="TXM", into=("1D", "COIN_2D"), enum=(0, 1), defaut="1D")
```

### Fiche 33728 : Suppression option GAP_GEOM

L'option GAP_GEOM n'a pas d'utilité en pratique dans le calcul du contact.
Cette option servait juste à faire des tests de l'appariement dans les cas-tests.
On supprime l'option et toutes ses dépendances.

## Performances

### Fiche 33694 : Performances GDEF_LOG

Beaucoup de temps était passé dans "intégration de la loi de comportement" dans le cas GDEF_LOG, même avec un comportement linéaire,notamment dans le calcul des déformations logarithmiques.

La factorisation du code permet un gain de :

- ~20% dans "intégration de la loi de comportement" (ex perf02a)
- 7% en moyenne sur l'ensemble des tests GDEF_LOG

### Fiche 33639 : Réduire l'empreinte disque des sorties de DEFI_SOL_EQUI

Réduction de l'empreinte sur disque des fichiers de sortie de la macro-commande DEFI_SOL_EQUI en proposant à l'utilisateur d'enregistrer dans la table enregistrée dans l'unité UNITE_RESU_TRAN uniquement :

- accélération au champ-libre
- accélération au rocher affleurant
- accélération au borehole
- accélération sur les noeuds inférieurs des couches de la colonne de sol (définis soit via la colonne 'M' de la table de sol, soit via le mot-clé COUCHE)

Pour ce dernier point, l'utilisateur pourra fournir une liste avec les noms des couches où il souhaite obtenir les accélérations (cela peut être
utile pour des comparaisons avec le mouvement sismique "within").

On ajoute les mot-clés TOUT_ACCE et LIST_COUCHE_ACCE.

### Fiche 33336 : optimisation des performances de REST_GENE_PHYS

Dans les études, les coûts en temps de cet op REST_GENE_PHYS (et de DEFI_BASE_MODALE) étaient disproportionnés par rapport à ceux de CALC_MODES (qui lui bénéficie de 3 niveaux de parallèlisme et des optimisations de longues dates dans le calcul modal et dans MUMPS).
Maintenant que ces derniers sont optimisés. On observe des accélérations pouvant aller jusqu'à un facteur 30.

### Fiche 33763 : Simplification de l'appariement nodal pour LIAISON_GROUP

L'appariement nodal effectué dans LIAISON_GROUP n'était pas optimal. Il y a beaucoup trop de vérifications sans utilité et l'ordre des boucles a été inversé pour gagner en efficacité.

## Manipulation des champs

### Fiche 31636 : Possibilité de restreindre MODI_REPERE par le mot-clé GROUP_MA

Le mot clé "GROUP_MA" a été ajouté sous MODI_CHAM dans MODI_REPERE (par défaut TOUT="OUI") pour restreindre le calcul dans les zones ou le repère a été changé. Uniquement les repères suivants sont impactés: COQUE_*.

## Solveurs

### Fiche 33202 : Industrialisation de MUMPS 5.6.2.2c

Montée de version de MUMPS, de ses prérequis (PARMETIS, METIS, SCOTCH, PT-SCOTCH) ainsi que d'autres dépendances:

- METIS 5.1.0 ==> version identique mais fourni par ParMETIS
- MFront 4.1.0 ==> 4.2.0
- MGIS 2.1-dev ==> 2.2
- Scotch 7.0.1 ==> 7.0.4
- Scalapack 2.1.0 ==> 2.2.0 + correction pour utilisation des scalapack MKL si disponibles
- Mumps 5.5.1_consortium_aster1 ==> 5.6.2.2c
- PETSc 3.17.1_aster ==> PETSc 3.19.5
- mpi4py 3.1.3 ==> 3.1.5
- medcoupling V9_10_0 ==> V9_11_0

Impact sur les études :

- Amélioration des performances de toutes les simulations code_aster (lorsque couplé avec modification connexe des paramètres slurm : entre X1.6 ET X3
- Correction de plusieurs bugs/dysfonctionnements dans MUMPS remontés par les tests.

### Fiche 33452/33510: Utilisation de "simples Lagranges"

Pour limiter le nombre de degrés de liberté, on aimerait pouvoir se contenter de multiplicateurs de Lagrange simples. Cela est réalisé pour les commandes STAT_NON_LINE et CALC_PRECONT. Pour les cas de macro-commande CALC_PRECONT, on introduit le mot clé DOUBLE_LAGRANGE='OUI' ou 'NON' pour s'assurer de l'homogénité des choix avec les chargements.

### Fiche 33690 : Exporter la portion locale d'une matrice ou d'un vecteur vers petsc4py

Dans le cadre des travaux de thèse sur le préconditionnement non-linéaire, on souhaite pouvoir résoudre un problème non-linéaire sur chaque sous-domaine. Pour ce faire, on souhaite exporter la portion locale d'une matrice ou d'un vecteur vers petsc4py.  Sur cette base, on pourra construire un solveur non-linéaire sur chaque sous-domaine.

## Informatique

### Fiche 33752 : STAT_NON_LINE devient une MACRO

Pouvoir simplement lancer les tests qui utilisent STAT_NON_LINE avec MECA_NON_LINE afin d'effectuer les bilans pour mettre à jour le périmètre de validation de MECA_NON_LINE.

STAT_NON_LINE devient une macro-commande qui appelle le "vrai" STAT_NON_LINE rebaptisé STAT_NON_LINE_FORT. L'appel à MECA_NON_LINE est aussi ajouté à cette macro.

Suite à cette intégration, il suffira de changer la valeur d'un booléen dans la macro pour que STAT_NON_LINE appelle MECA_NON_LINE.

### Fiche 33431 : DEBUT/POURSUITE

Il n'y a maintenant plus que la commande DEBUT qui, en mode automatique (MODE="AUTO", qui est le défaut), initialise le calcul s'il n'y a pas de base et relit une base si elle existe.

Avec MODE="DEBUT", on initialise un nouveau calcul même si une base existe.
Avec MODE="POURSUITE", on relit la base si elle existe, sinon on s'arrête en erreur.
Pour la compatibilité, on conserve POURSUITE équivalent à DEBUT(MODE="POURSUITE").

### Fiche 16019: Facilité de manipulation des champs

On peut manipuler les champs avec les opérations élémentaires +, - , *.

Voir par exemple dans zzzz505a :

```text
  f0 = fieldp.copy()
  f = fieldp.copy()
  f += f
  f2 = 2 * f0
  f3 = -f + f2  # == 0.
```

### Fiche 33609 : Ajouter de l'équilibrage dynamique

Dans le cadre des études où des non-linéarités sont très localisées puis se propagent, il est nécessaire d'équilibrer la charge au cours du calcul. On peut imaginer fournir un champ aux nœuds scalaire qui sera utilisé comme poids par le partitionneur.

Ajout du test zzzz155h qui réalise un calcul MECA_STATIQUE et redécoupe le résultat suivant une commande donnée par l'utilisateur. Le calcul utilise un AFFE_CHAR_CINE et un AFFE_CHAR_MECA.

## Modifications de syntaxe

- La commande POURSUITE peut ếtre remplacée par DEBUT(MODE="POURSUITE") ( REX 33431)

- Dans la commande CREA_RESU, le mot clé NOM_CHAM est déplacé dans le mot-clé facteur AFFE (REX 33432)

- Dans la commande DYNA_NON_LINE, on change la valeur par défaut de AMOR_RAYL_RIGI pour la valeur 'ELASTIQUE' (REX 33825)
