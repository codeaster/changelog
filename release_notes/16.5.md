# code_aster version 16.5 Release note

## Mécanique

### Fiche 30507 : C_PLAN + DE BORST : et si ça pouvait mieux converger ?

La convergence est souvent délicate dès lors qu'on utilise la méthode DEBORST pour traiter les contraintes planes.

La programmation de nmcomp (contraintes planes) et redece (découpage local du pas de temps) est reprise. 
On opte dorénavant pour un schéma emboîté, plus robuste et plus simple, pour la résolution de SIZZ=0.

Il est recommandé d'utiliser RESI_CPLAN_MAXI (et non pas RELA), associé avec ITER_CPLAN_MAXI=10 (la nouvelle valeur par défaut). 
En effet, si la contrainte tend vers zéro (décharge, endommagement, ...), un critère relatif n'est plus pertinent.

## Mécanique de la rupture

### Fiche 32718 : [RUPT] POST_BEREMIN : CALC_CHAMP/SIEF_ELEM

Création d'une nouvelle option de calcul qui permet d'obtenir la moyenne de contraintes (SIEF_ELGA) aux points de Gauss d'une maille
et de l'affecter sur tous les points de Gauss de cet élément. Cette nouvelle option doit être disponible pour les modélisations 3D, 3D_SI, AXIS, AXIS_SI, D_PLAN et D_PLAN_SI.

La nouvelle option se nommera SIMY_ELGA (comme SIef MoYenné).

Le test zzzz510, pour valider cette nouvelle option, est ajouté.

### Fiche 32235 : [RUPT] POST_BEREMIN : réécriture de la macro-commande en commandes aster

Création d'une nouvelle commande POST_BEREMIN sous MEDcoupling pour calculer la probabilité à rupture et la contrainte de Weibull avec le modèle probabiliste de Beremin.

La commande est utilisable dans un fichier de commande sous le nom POST_BEREMIN.

La validation est faite sur le cas test ssna108a en comparaison avec POST_ELEM/WEIBULL.

### Fiche 28534 : Refonte de la commande IMPR_OAR suite à l'arrêt de l'interface internet

Le logiciel OAR réalise des calculs de rupture brutale selon les méthodes simplifiées du RSEM. Le logiciel OAR existait en deux versions : EDF, sous la forme
d'une interface internet, et FRAMATOME, sous la forme d'un programme informatique prenant en données d'entrée des fichiers texte. La version internet d'EDF a
été arrêtée. Seule la version FRAMATOME est conservée.

L'opérateur IMPR_OAR précédent produisait une donnée d'entrée utile au logiciel OAR sous sa forme d'interface internet. L'abandon de cette interface conduit à
la refonte de cet opérateur.

## Performances

### Fiche 33254 : Optimisation des performances de MODE_STATIQUE

En cherchant le bug MUMPS révélé par la fiche [32438], il a été décidé de:
- améliorer les performances des appels a MUMPS dans cet opérateur
- améliorer la programmation de l'opérateur (routine modstat.F90), sans recourir à du parallélisme (MPI ou OpenMP) complémentaire de celui de MUMPS. En se limitant à qqes reformulations et astuces algorithmiques. Car cet op peut coûter assez cher dans les études (plusieurs heures).

## Lois de comportement

### Fiche 32789 : Reprogrammation de GONF_ELAS en MFront

La loi GONF_ELAS a été reprogrammée en GonfElas via MFront. Cette loi mécanique ne peut être utilisée qu'en THM non saturée 
(modélisation *HH*). En effet, elle utilise la pression capillaire comme donnée d'entrée. Ce couplage nécessite d'utiliser le 
mécanisme développé dans [#32424]. La reprogrammation de cette loi permet ainsi de valider ce mécanisme de couplage via Mfront.

Un cas test représentant le gonflement d'un matériau que l'on remplit d'eau est ajouté. Le matériau est laissé libre et on vérifie qu'il gonfle bien.

### Fiche 30654 : Pilotage en temps du modèle GTN

Amélioration de la robustesse et les performances des calculs avec les modèles GTN et VISC_GTN.

Développements :

- Introduction d'un nouveau schéma d'intégration pour le traitement de la porosité.
- Décomposition de l'endommagement en trois termes
- Pour éviter les discontinuités de contrainte au voisinage de l'endommagement ultime, on introduit plusieurs mécanismes.
- L'intégration du comportement élastoplastique passe par la résolution de plusieurs équations scalaires.
- Pour limiter les erreurs de mise en donnée, on teste les valeurs des paramètres du modèle transmises par l'utilisateur en amont de l'intégration.
- Pour permettre un meilleur suivi du déroulement du calcul et une meilleure analyse a posteriori des résultats, certaines variables de post-traitement sont introduites parmi la liste des variables internes du modèle :

  - ENDO_EX    Valeur de l'endommagement extrapolé (étape (i) du schéma d'intégration)
  - SIEQ_ERX   Ecart entre endommagement extrapolé (i) et corrigé (iii) en termes de contraintes
  - SIEQ_ECR   Part de la contrainte équivalente associée à l'écrouissage
  - SIEQ_VSC   Part de la contrainte équivalente associée à la viscosité
  - SIEQ_NLC   Part de la contrainte équivalente associées au non local (GRAD_VARI)
  - ARRET      Une variable réservée à l'utilisateur (développeur) pour piloter l'arrêt du calcul (via DELTA_GRANDEUR)

### Fiche 32197 : Prendre en compte un plateau de Luders dans VMIS_ISOT_NL

Introduction du plateau de Luders dans la courbe d'écrouissage décrite de manière paramétrique pour les modèles VMIS_ISOT_NL et VISC_ISOT_NL.

On profite des tests ssnv263a et ssnv263d pour faire une seconde passe de calcul à déformation imposée.

### Fiche 32664 : Interdiction des variables de commande de température en THM

Lorqu'une LdC qui dépend de la température est utilisée, celle-ci est une variable de commande (External State Variable) sauf dans le cas de la THM, puisque dans
ce cas, la température devient une variable du problème.
Un mécanisme est prévu pour rendre compatible les LdC mécaniques avec la THM sans aucune modification (on passe par un COMMON en souterrain dans calcul.F90)
Ce mécanisme va être étendu à d'autres variables.

Du fait qu'on ait besoin de TEMP_REFE y compris en THM, on a autorisé l'usage d'une variable de commande dite "VIDE" dans AFFE_MATERIAU )

Désormais, il est possible d'interdire la définition d'une température comme variable de commande sur une zone du modèle en THM.

### Fiche 32162 : Expression du besoin matrice de décharge dans STAT_NON_LINE

Mise à jour et restitution d'un développement effectué par le LMDC (Toulouse) visant à calculer une matrice de décharge pour la loi
RGI_BETON.

Enrichissement d'un cas test fourni par le LMDC pour présenter la procédure de récupération de cette matrice de décharge.

## Modélisation

### Fiche 33024 : CALC_VECT_ELEM en python

CALC_VECT_ELEM est maintenant une macro.

Réécriture du calcul des chargements en mécanique (on ne passe plus par du fortran).

### Fiche 33147 : Export des champs aux noeuds en medcoupling

Disposer d'une fonction d'export d'un champ aux nœuds au format medcoupling, pour manipulation ou sauvegarde.

Dans un premier temps, rendre l'export opérationnel uniquement pour des objets de type FieldOnNodesReal.

### Fiche 32998 : AFFE_CARA_ELEM : suppression des mots clefs MAILLE/NOEUD

- Suppression de MAILLE et NOEUD_CENTRE du catalogue
- Suppression de NOEUD_CENTRE du fortran 
- Vérification des macros

### Fiche 32746 : [RS] Discret de contact avec un comportement non-linéaire élastique

Le besoin identifié concerne la modélisation de problématique de choc élastique non-linéaire.

Lorsque le contact est établi, le discret suit le comportement non-linéaire élastique. Utilisable dans : 
* STAT_NON_LINE, DYNA_NON_LINE : permet de tester et de valider les résultats avec DYNA_VIBRA. Et aussi de faire des calculs en temporel avec ou sans dynamique.

```python
```DIS_CHOC_ELAS=FACT( statut="f",
                       fr=tr("Loi de choc avec comportement élastique non-linéaire"),
                       FX=SIMP(statut="o", 
                               typ=fonction_sdaster,
                               fr=tr("Comportement axial élastique non-linéaire en fonction du déplacement relatif."), ),
                       DIST_1=SIMP(statut="f", typ="R", defaut=0.0, val_min=0.0),
                       DIST_2=SIMP(statut="f", typ="R", defaut=0.0, val_min=0.0),
)
```
Le comportement associé est : CHOC_ELAS_TRAC 

* DYNA_VIBRA : Pour les non-linéarités localisées, il n'y a ni matériau ni élément support. Le comportement est entre 2 noeuds.

```python
CHOC_ELAS_TRAC(fr=tr("Loi de choc avec comportement élastique non-linéaire"),
               FX=SIMP(statut="o", 
                       typ=fonction_sdaster,
                       fr=tr("Comportement axial élastique non-linéaire en fonction du déplacement relatif."), ),
               DIST_1=SIMP(statut="f", typ="R", defaut=0.0, val_min=0.0),
               DIST_2=SIMP(statut="f", typ="R", defaut=0.0, val_min=0.0),
               GROUP_NO_1=SIMP(statut="f", typ=grno),
               GROUP_NO_2=SIMP(statut="f", typ=grno),
)
```

## Dynamique

### Fiche 33119 : Ajout Lissage et filtrage CALC_FONCTION DSP

Avec l'option DSP, la commande CALC_FONCTION permet de déterminer une DSP à partir la donnée d'un spectre de réponse. Sont proposées ici deux évolutions de la
commande pour le mot-clé DSP :

- Ajout du mot-clé FREQ_FILTRE_ZPA: mot-clé et fonctionnalité déjà existants pour l'opérateur GENE_ACCE_SEISME, qui permet de filtrer le contenu hautes
fréquences (filtre passe bas de Buttterworth) au-delà de la ZPA du spectre de réponse qui peut être présent. Le filtre s'applique à directement la DSP. Il
s'agit d'une fonction commun avec GENE_ACCE_SEISME. Ce développement modifie légèrement la prise en compte du filtre dans GENE_ACCE_SEISME, qui s'applique
maintenant avant iterations.

- Ajout du mot-clé NB_FREQ_LISS: ce nouveau mot-clé permet d'appliquer un lissage à la DSP, l'opération est effectuée à chaque itération le cas échéant. 
Si on cherche à améliorer l'ajustement par des itérations, alors les DSP peuvent avoir des pics locaux. Ce nouveau mot-clé permet d'y remédier. La signification
et l'algorithme sont les mêmes que pour le calcul de la COHERENCE avec CALC_FONCTION, à savoir un filtre de Hamming.

### Fiche 33257 : Résorption du comportement 'YACS' de DYNA_VIBRA

Les sources Fortran concernant yacs sont supprimées.

Les messages d'erreur liés à l'interface aster-yacs (code_aster/Messages/edyos.py) sont également supprimés. 

On modifie les sources Fortran et le catalogue c_comportement_dyna afin de supprimer le comportement non linéaire 'YACS'.

## HPC

### Fiche 33008 : Porter MeshCoordinatesField (NOEU_GEOM_R) en parallélisme HPC

MeshCoordinatesField ne supporte pas le parallélisme HPC. Il faut donc compléter son NUME_EQUA.

La méthode toFieldOnNodes est également ajoutée pour transformer un MeshCoordinatesField en FieldOnNodesReal dans la classe FieldConverter.

## Genie civil

### Fiche 32948 : Extension de Calc_Ferraillage aux éléments POU_D_T

Il s'agit d’étendre le fonctionnement de l'opérateur de calcul de ferraillage aux éléments POU_D_T.

Pour ce faire, le lien est introduit dans le fichier /catalo/cataelem/Eléments/meca_pou_d_t.py en ajoutant te=265 dans l'attribut calculs.

Afin de tester la modélisation, une modélisation D au cas test ssll13 est introduite. La documentation v3.01.013 a été également modifiée par conséquent.

### Fiche 32979 : Extension de POST_RCCM aux POU_D_TGM

L'utilisation de POST_RCCM option "MOMENT_EQUIVALENT" n'est actuellement pas possible avec des éléments POU_D_TGM. 

Ajouts nécessaires aux bon fonctionnement de POST_RCCM/MOMENT_EQUIVALENT pour les éléments :

- POU_D_TG
- POU_D_TGM
- POU_D_EM

Les développements sont validés en ajoutant des appels à POST_RCCM dans ssll119a.

### Fiche 32668 : Refonte de l'opérateur CALC_COUPURE

La commande pouvait donner des résultats faux dans certains cas, ou des plantages dans d'autres.

La commande a été entièrement recodée : on utilise maintenant un enchaînement CALC_CHAMP/POST_RELEVE_T pour déterminer 
le torseur de coupure et du python pour l'éventuel changement de repère et les combinaisons des torseurs modaux (CQC). 
Auparavant, la commande se basait sur MACR_LIGNE_COUPE et l'intégration des efforts sur la ligne. 

## CSC

### Fiche 32785 : [TF-CSC] Implanter la recherche linéaire dans MECA_NON_LINE

Dans [32777], le recherche linéaire a été implantée dans STAT_NON_LINE en mode hpc.
Il faut faire de même dans MECA_NON_LINE.

La recherche linéaire dans MECA_NON_LINE dans une classe séparée line_search.py a été programmée.

Les cas tests zzzz509j (=ssnp178m) et zzzz509k (=ssnp15f) sont ajoutés en std et HPC.

### Fiche 33075 : [TF-CSC] Architecture générale python pour THER_NON_LINE

THER_NON_LINE devient une macro python. Pour l'instant, la partie python n'est utilisée que pour les calculs stationnaires.

NonLinearSolver est réutilisé. La seule différence a été de gérer le cas du calcul de l'état initial stationnaire.

Tout les calculs de résidu et matrice tangente ont été transféré dans discretecomputation_ext.

Plusieurs tests sont passés avec la nouvelle architecture.

### Fiche 32971 : [TF-CSC] Rendre le découpage des champs Med plus utilisable

Production d'une commande tout en un : découpeur de champs et de maillage et assemblage d'une sd_resu.

Une fonction python nommée : splitMedFileToResults est ajoutée.

Cette fonction prend comme argument :
- le nom du fichier MED,
- un dictionnaire faisant correspondre le nom MED des champs à relire au nom Aster (ex : {"00000008DEPL": "DEPL"},
- une classe héritant de Result afin d'instancier le résultat de sortie,
- et éventuellement un modèle (pour les champs aux éléments).

Cette fonction se charge de :
- découper le maillage,
- découper les champs en MedVector parallèle sur les différents pas de temps,
- instancier le résultat de sortie selon ce qu'à fournit l'utilisateur,
- transférer les MedVector vers des champs simples puis des champs et finalement les stocker dans la sd de sortie.

La classe FieldCharacteristics permet d'obtenir à partir d'un nom de champ (DEPL, SIEF_ELGA, ...) :
- sa quantité (DEPL_R, SIEF_R, ...),
- sa localisation,
- son option (),
- son paramètre ().

### Fiche 32842 : [TF-CSC] Post-traitement pour la restauration d'écrouissage

Introduction du calcul (mot-clef POST_INCR/REST_ECRO) dans MECA_NON_LINE sur le modèle de ce qu'il y a dans STAT_NON_LINE.
Cette fonctionnalité consiste à faire la restauration d'écrouissage (le revenu ou annealing en anglais) à la fin de chaque pas de temps.

Validation: zzzz509k qui est un duplicata de hsnv140b avec MECA_NON_LINE

### Fiche 32963 : [TF-CSC] Fonctionnalité type POST_ELEM INTEGRALE et MINMAX en HPC

Dans le cas SNS 2D mtlp200a et mtlp201a, POST_ELEM est utilisé en pré et post traitement. 
Néanmoins, ces fonctionnalités ne sont pas disponibles sur maillage découpé (au moins INTEGRALE, plantage "Objet inexistant dans les bases ouvertes : &&PEEINT.CELL_USER "
). Il faut les ajouter au cadre HPC.

Le cas de INTEGRALE est particulier puisque le travail avait déjà été réalisé pour le cas TOUT="OUI". Mais il est nécessaire de l'adapter au cas GROUP_MA. Le
problème vient du fait que les groupes de mailles ne sont pas toujours présent sur tous les procs.

Il faut donc adapter la programmation pour permettre cela et notamment autoriser les objets de longueur nulle. 

Pour MINMAX, ça ne fonctionnait pas du tout en HPC. Il a fallu ajouter des comm globales pour partager les min et les max trouvés sur les
différents procs.

La modélisation f est ajoutée à petsc02. Elle teste POST_ELEM INTEGRALE et MINMAX pour les champs aux nœuds et aux mailles.

### Fiche 33131 : [TF-CSC] Reprogrammer la thermique

Beaucoup de résultats faux en thermique détectés récemment. Toute la thermique des éléments de Lagrange est reprogrammée. 

Un nouveau formalisme orienté objet est utilisé pour programmer les TE avec des nouveaux module (inspiré de HHO).

L'idée est définir un objet topologie de la maille, une quadrature et un objet fonction de base qui dépend de l'EF après tout est automatique. 
En particulier cela supprime la dépendance type de maille / type EF

Le code est commun pour le 2D et 3D. Une vingtaine de TE est libérée. Le volume de source a été réduit de 70%.

Des catalogues sont mis en commun et certaines options sont renommées pour la cohérence.

La prochaine étape (dans une autre fiche) est de mettre au propre les entrée sortie et enlever le théta-schéma des TE.

### Fiche 25882 : [TF-CSC] Nouveau modèle pour la restauration d'écrouissage

La restauration d'écrouissage est un modèle qui simule le fait que la structure micrographique du métal est modifiée au-dessus d'une certaine température de
telle manière à ce que l'écrouissage diminue.
Dans les codes classiques (type ABAQUS), on utilise un seuil de température au-delà duquel la déformation plastique cumulée est annulée.
Ce modèle très simple est difficile à recaler sur les essais et a une dépendance à la discrétisation temporelle.

La restauration d'écrouissage est activée par le mot-clef 'REST-ECRO' dans POST_INCR de STAT_NON_LINE.
Du point de vue fonctionnel, à chaque pas de temps, après le calcul, on applique la restauration selon ce modèle (qui va donc modifier les variables internes).

Le nouveau modèle a deux intérêts:
- il supprime la dépendance à la discrétisation temporelle du premier modèle
- il a des paramètres qui s'identifient naturellement sur les essais classiques (type Satoh)

La syntaxe STAT_NON_LINE reste inchangée.

Le développement est testé dans plusieurs cas-tests :
hsnv140b-i, zzzz367a et zzzz509k (dans MECA_NON_LINE)

Il est également utilisé dans les études CSC de la task-force ainsi que dans les actions de validation qui y sont attachées (voir issue32697 par exemple).
Un certain nombre de corrections y ont été faites sur cette base (contrôle des arrondis, problème d'explosion de l'exponentielle, etc).

On en profite pour ajouter les éléments AXIS_SI (quadratiques) pouvant calculer REST_ECRO (il  avaient été oubliées).

### Fiche 31995 : [TF-CSC] Extension de MECA_NON_LINE à la dynamique

L'architecture de MECA_NON_LINE a été modifiée pour l'étendre à la dynamique. Seul le schéma de Newmark (formulation en déplacement) est implémenté pour
l'instant. D'autres schémas peuvent facilement être ajoutés plus tard.

Cas-tests restitués : zzzz510d et sdld31a

## Risque Sismique

### Fiche 33073 : Modification choix de condition aux limites modèle 1D-3C pour DEFI_SOL_EQUI

Amélioration de la définition des conditions aux limites pour la modélisation 1D-3C avec DEFI_SOL_EQUI.

Il est proposé de remplacer l'approche par cerclage avec raideur de pénalisation par des conditions de liaison. Pour cela, l'utilisateur doit donc définir :
- GROUP_MA_GAUCHE et GROUP_MA_DROITE : deux groupes de maille surfaciques en vis-à-vis de part et d'autre de la colonne de sol 3D
- GROUP_MA_ARRETE_1 et GROUP_MA_ARRETE_2 : deux groupes de maille linéiques définissant les arrêtes sur toute la longueur de la colonne, appartenant tous les
deux à la même face définit soit par GROUP_MA_GAUCHE, soit par GROUP_MA_DROITE (ce point sera explicité dans la doc).

Les cas-tests utilisant cette méthode : zzzz412a,b,c,d sont modifiés. Les valeurs de non-régression des cas-tests sont impactées.

### Fiche 29779 : [RS2024-CSM] - Restructuration de COMB_SISM_MODAL

Restructuration de l'opérateur COMB_SISM_MODAL avec une réécriture en python.

Les bugs suivants dans l'ancienne version en fortran sont corrigés dans la nouvelle version : 

- Cas du mult-appui corrélé: La combinaison des réponses dynamiques par la règle QUAD (valeur par défaut): 
l'implémentation de la règle QUADRATIQUE est fausse 
- Cas du mult-appui décorrélé avec prise en compte du pseudo-mode: l'implémentation de la prise en compte du pseudo-
mode est incorrecte. La règle de combinaison impose que le cumul intra-group est LINE alors qu'il n'y a pas cumul intra- groupe dans le code. 
Le cumul des appuis sont implémenté comme cumul inter-groupe par la règle QUAD quelque soit la constitution des groupes 
d'appuis via GROUP_APPUI et la règle donnée par l'utilisateur.
- Pour la réponse au DDS, la règle impose le cumul LINE entre appuis corrélés. Mais, notamment entre la ligne 259 et 
263 (code fortran asefen.F90), on lit la règle de combinaison étant QUAD quelque soit la règle saisie par l'utilisateur.
- règle DSC (combinaison des modes par formule de ROSENBLUETH) est incorrectement implémentée dans fortran. --> NOOK 
en régression sdlx301a (écart 1.0e-3%) 
- CORR_FREQ = 'OUI'. CORR_FREQ = « OUI », les valeurs des spectres sont multipliées par √(1 − ξ^2) indifféremment de 
leur nature : accélération/vitesse/déplacement --> il faut plutôt multiplier le spectre en accélération par respectivement 1, √(1 − ξ^2) et 1 
− ξ^2 pour des spectres de nature : accélération/vitesse/déplacement: aucun impact observé sur les cas-tests.
- CORRE_FREQ = 'OUI': L valeur de la pulsation propre n'est pas remplacée par la valeur de la pulsation propre amortie 
dans la correction statique (eg avec astron.F90) --> corriger.

Les bug/risque de résultats faux MAIS SANS CORRECTION dans la nouvelle version:

- Correction statique pour ACCE_ABSOLU en mult-appui : seul accélération absolue à l'appui N°1 (premier appui saisi) 
est prise en compte (pas de cas-test pour ceci)

Les options obsolètes :

- Suppression MASS_INER: il revient à l'utilisateur de veiller la masse cumulée avant la fréquence de coupure.
- Remplacer IMPRESSION par TYPE_RESU

Modification des cas-tests. Au total, 32 cas-tests avec 100 appels COMB_SISM_MODAL et 3000 TEST_RESU.

Résultat faux :

- mult-appui corrélé avec COMb_MULT_APPUI= QUAD.
- mult-appui décorrélé + pseudo-mode
- combinaison modes par règle DSC
- correction pseudo-mode en ACCE_ABSOLU en mult-appui

