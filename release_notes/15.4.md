# code_aster version 15.4 Release note

## Syntaxe

### Fiche 29740 : SEPAR vs SEPARATEUR

L’opérateur LIRE_FONCTION utilise le séparateur SEPAR alors que LIRE_TABLE, IMPR_TABLE, IMPR_FONCTION et DEFI_SOL_EQUI utilisent  SEPARATEUR.
Par cohérence, le mot-clé SEPAR est remplacé par SEPARATEUR.
Un message d’erreur a été ajouté sur l’obsolescence du mot-clé SEPAR dans  LIRE_FONCTION.

### Fiche 29196 : Résorption NUME_LAGR dans AFFE_CHAR_*

Le mot-clé NUME_LAGR est supprimé dans le contexte de rationalisation des « double Lagrange ». Cette résorption découle de sa limite et du fait qu’il ne soit plus utilisé aujourd’hui.

### Fiche 30557 : Simplifier la syntaxe de DETRUIRE

Le mot-clé OBJET étant supprimé de DETRUIRE, il ne reste plus que CONCEPT qui ne contient que NOM.  Afin de simplifier la syntaxe de DETRUIRE, le mot-clé NOM est uniquement introduit (DETRUIRE(NOM=TABRES)).

### Fiche 30558 : Renommage kinematicLoad

On remplace ici le nom de la condition kinematicsLoad par DirichletBC pour une cohérence.

## Suppression de fonctionnalités

### Fiche 30640 : Suppression des mot-clefs cachés dans AFFE_MATERIAU/AFFE_VARC

Des mots-clés cachés dans AFFE_VARC ont été supprimés. Les cas-tests ssnv244b, ssnv244f, zzzz122a, zzzz284a sont alors modifiés.

## Mécanique de la rupture

### Fiche 29817 : POST_FM - calcul de facteur de marge

L’automatisation du calcul de facteurs de marge est mise en place par la macro-commande python POST_FM. Les applications visées sont les analyses de nocivité des "Défauts Sous le Revêtement" (DSR) et des "Défauts Dans le Revêtement" (DDR) hypothétiques ou avérés dans les cuves de la base installée.
A chaque instant "INST" en un point B du front de fissure à partir des taux de restitutions d'énergie obtenus en élasticité linéaire et en élastoplasticité, de la température et du champ matériau (module de Young, coefficient de Poisson, ténacité) :

- La tenacité "KIC" au point B
- Les facteurs d'intensité des contraintes correspondant en élasticité linéaire "KELAS" et en élastoplasticité "KPLAS"
- Le facteur d'intensité des contraintes "corrigé" plastiquement "KCP"
- Le facteur de marge réglementaire "FM_ASN"
- Le facteur de marge réaliste "FM_PLAS".

Nouveaux cas-tests : zzzz125a, zzzz125b, ssna126a, ssna126b, ssna127a, ssnv114a

### Fiche 30503 : [RUPT] [Chantier CALC_G] : mettre à jour la sortie du champs theta dans CALC_H

Dans le cadre du chantier CALC_G, l’opérateur  CALC_H détermine désormais un champ de nœuds contenant 6 composantes en 2D et 3D, la sortie CHAMP_THETA (CHAM_NO) est alors mis à jour :

1. valeur de la fonction theta0(r) pour le nœud (évaluée en fonction de position du nœud par rapport aux couronnes d'intégration choisies par
l'utilisateur), où r est la distance du nœud au front de fissure
2. première composante du vecteur direction de propagation du nœud issue de BASLOC de la sd_fond_fissure
3. deuxième composante du vecteur direction de propagation du nœud issue de BASLOC de la sd_fond_fissure
4. troisième composante du vecteur direction de propagation du nœud issue de BASLOC de la sd_fond_fissure (0 en 2D)
5. abscisse curviligne du projeté du nœud sur le front de fissure (0 en 2D)
6. longueur de la fissure (0 en 2D)

Une grandeur (THETA_R) et un champ spéficique (ELNO) sont également créés.

### Fiche 30863 : [RUPT] [Chantier CALC_G] – rendre POST_FM compatible avec CALC_H.

Dans le cadre du chantier CALC_G, l’opérateur CALC_G a été remplacé par CALC_H. Les développements de cette fiche permettent de rendre POST_FM compatible avec CALC_H. Une division étant déjà faite par CALC_H directement, il est nécessaire de la supprimer de POST_FM.

### Fiche 30673 : [RUPT] [Chantier CALC_G] : Remplacement des opérateurs historiques

Les opérateurs historiques CALC_G et DEFI_FOND_FISS sont remplacés par les opérateurs de transition CALC_H et DEFI_FISSURE .
Dans les cas-tests XFEM, l’opérateur CALC_G est remplacé par XALC_G_XFEM.
Dans les cas-tests FEM, l’opérateur CALC_G est remplacé par CALC_H.

### Fiche 30288 : [Chantier CALC_G] : traitement du mot clé NB_POINT_FOND

Le mot-clé NB_POINT_FOND de l’opérateur CALC_G est utilisé en 3D pour un calcul de G utilisant un lissage de Lagrange. Le chantier ayant supprimé des structures de données liées à NB_FOND_FOND, il est nécessaire de faire des modifications.
Les nouveaux opérateurs créés prévoient une projection des nœuds de maillage sur le front de fissure dans DEFI_FISSURE. IL faut donc pour NB_POINT_FOND de recréer un front de fissure et refaire les projections.

### Fiche 30525 : [VIPER] Permettre de changer la valeur d'une condition de Dirichlet

Pour le calcul le relèvement sur toute la structure de plusieurs modes d'interface, il est possible de vouloir faire plusieurs résolutions avec différentes valeurs des conditions de Dirichlet.
Cette fonctionnalité est mis à disposition sur un vecteur assemblé (champs de nœuds).

Nouveaux cas-tests : xxParallelMechanicalLoad001p

## Calculs parallèles

### Fiche 30451 : Enrichir le debug MPI

La variable DEBUG_MPI est introduit dans les fichiers include/{asterf_debug.h,asterc_debug.h} afin d’activer des impressions lors de l'appel des encapsulations de fonctions MPI . Les informations de débug sont corrigées (car fausses dans certains cas) et généralisées.
Cela permet, par exemple, d’analyser l’intensité des communication dans le code.

### Fiche 30626 : Message intenpestif en hpc

Dans l’opérateur CALC_CHAMP, un message intempestif apparaît en HPC. Il est donc supprimé.
De plus, le parallélisme en temps étant incompatible, il est interdit.

### Fiche 30597 : Permettre l'utilisation de ParallelLoad dans l'API Python

Une méthode addParallelMechanicalLoad est ajoutée dans l’API Python. Cela permet d’utiliser des conditions cinématiques reposant sur des Lagrange parallèles.

### Fiche 30362 : Ajouter mpi4py dans les prérequis

Une compilation de mpi4py est faite pour qu’il soit compatible avec python3.6.5 d’aster. Dans les prérequis sont ajoutés  mpi4py 3.0.3 et medcoupling V9_6_asterxx_0.

## Performances

### Fiche 30504 : Suivi des perfs bridé pour 1000 processus

Le suivi des statistiques ne peut pas être activé au-delà de 1000 processeurs avec la commande suivante : DEBUT(MESURE_TEMPS=_F(NIVE_DETAIL=2, MOYENNE='OUI',)).
Un assert limitant le nombre de processeurs maximum dans UTTCPI est remplacé une allocation dynamique.

## Lois de comportement

### Fiche 30523 : Introduction de la température dans la loi de pression capillaire

On introduit ici une dépendance des isothermes de sorption S(Pc) avec la température en mettant en place la possibilité de renseigner plusieurs isothermes sous forme de nappe pour plusieurs températures.
Les lois LIQU_GAZ, LIQU_AD_GAZ, LIQU_VAPE_GAZ,  LIQU_AD_VAPE_GAZ, LIQU_GAZ_ATM, LIQU_VAPE sont alors modifiées.
Nouveaux cas-tests : wtnc107a, wtnc107b

### Fiche 30857 : DYNA_NON_LINE, multi-appui et lois de choc avec flambage

Deux lois de choc avec flambage (CHOC_ENDO et CHOC_ENDO_PENA) ont été introduits pour les calculs DYNA_NON_LINE en multi-appui.
Le but est donc de débloquer l’utilisation des lois DIS_CHOC_ENDO en concomitance avec l'option "MULTI_APPUI" dans l'opérateur DYNA_NON_LINE.

## Matériaux

### Fiche 30124 : Ajout d'un seuil pour la fixation des aluminiums lors de la phase d'échauffement menant à la RSI

On ajoute un seuil pour la fixation des aluminiums lors de la phase d’échauffement menant  à la Réaction Sulfatique Interne (RSI).
Si la température seuil de dissolution des phases primaires est atteinte, des aluminiums et des sulfates deviennent libres et créent un potentiel de formation de produit gonflant de RSI pour la suite de la durée de vie du béton.
Afin d’éviter la fixation des aluminiums en hydro-grenats, on ajoute deux nouveaux paramètres au matériau RGI_BETON :

- NRJF : énergie de fixation des alus en HG (RSI)
- TTKF : température seuil de fixation des alus en HG (RSI)
Nouveaux cas-tests : ssnv107a

### Fiche 30704 : Permettre de définir les variables de commande sur un CHAM_MATER

Une nouvelle méthode addExternalStateVariables est ajouté au concept de type MaterialField afin d’enrichir les variables de commandes après l’utilisation de AFFE_MATERIAU.
Pour ce faire, un catalogue commun est créé pour le mot-clé facteur AFFE_VARC et la notion de mot-clés cachés est supprimé de AFFE_MATERIAU.

## Mac3Coeur

### Fiche 30427: [MAC3 ] Calculs de dilatation simple pour la neutronique

Afin de faire des calculs de dilatation simple du reacteur (sans prendre en compte les contacts/frottements), le mot-clé TEMP_IMPO est ajouté. Celui-ci permet d’imposer une température  constante (°C) sur le réacteur.

### Fiche 24064 : Evolution de la macro POSTMAC3COEUR pour extraction de resultats numériques

L’opérateur POSTMAC3COEUR permet d’extraire complètement les résultats sous forme de table mais aussi d’extraire des valeurs d’analyse des résultats. Cela permet donc d’avoir deux formats de sortie de MAC3.

### Fiche 30915 : [MAC3DYN] - Ajout de post traitements à l'IHM

Dans l’IHM MAC3_Dynamique, on ajoute des modules de post-traitements et une fenêtre associée. Cela permet de gérer et lancer plusieurs cas de calculs.
Dans l’IHM est intégrée :

- un indicateur de suivi des calculs
- une fenêtre de post-traitement permettant de visualiser les résultats
- des cas tests pour le module de post traitement et pour différentes configurations d'études

### Fiche 29787 : [MAC3] Refactoring de POST_MAC3COEUR

Des changements sont effectués dans POST_MAC3COEUR :

- Suppression des sorties au format grace
- Suppression de l'impression des tables
- Suppression de l'arrondi sur les post-traitements
- Suppression des mots-clés facteur DEFORMATION, LAME, FORCE_CONTACT
- Ajout des mots-clés simples :
  - TYPE_CALCUL = into('LAME', 'DEFORMATION', 'FORCE_CONTACT')
  - OPERATION  = into('EXTRACTION', 'ANALYSE')
- La sortie de l'opérateur est maintenant une table

### Fiche 21961 : MAC3COEUR : raideur des liaisons grille/tubes

Dans la macro CALC_MAC3COEUR la raideur de la liaison entre grille et tube-guide doit être redéfinie. La raideur en rotation (kr) donnée en paramètre est divisée par 4 au lieu de 8.

## Modélisation

### Fiche 27353 : D114.20 - Implantation des éléments solide-coques (thèse M.Dia) - Modélisation COQUE_SOLIDE

La thèse de Mouhamadou Dia propose l’implémentation d’un élément à cinématique de plaque décrite sur un support géométrique "brique 3D", en prenant en compte le pincement fonctionnant en linéaire, non-linéaire (matériau, cinématique et contact), statique et dynamique.
L’élément utilise une brique à neuf nœuds (HEXA9) et une numérotation locale spécifique pour identifier les faces supérieures et inférieures (l'épaisseur de la coque).

Nouveaux cas-tests : ssls100n, ssls108c, ssls129c, ssnv303a, ssnv303b, ssnv303c

### Fiche 30893 : [3M] Raccord entre COQUE_SOLIDE et 3D par LIAISON_MAILLE

Cette fiche permet de faire fonctionner PROJ_CHAMP (appelé par AFFE_CHAR_MECA/LIAISON_MAILLE) avec les HEXA9.

Nouveaux cas-tests : ssnv303d

### Fiche 30873 : [3M] Ajout des chargements volumiques et de RESI_REFE_RELA pour COQUE_SOLIDE

Les options CHAR_MECA_PESA_R, CHAR_MECA_FR3D3D et CHAR_MECA_FF3D3D correspondant aux chargements PESANTEUR et FORCE_INTERNE (réel ou fonction) sont ajoutées pour les éléments COQUE_SOLIDE. De plus, un critère de convergence par résidu (RESI_REFE_RELA) a été ajouté.

Nouveaux cas-tests : ssls100n

### Fiche 30886 : [NGC] Ajout de variables d'état externes (TEMP, SECH, HYDR) pour COQUE_SOLIDE

Dans cette fiche, on traite la possibilité de prendre en compte les déformations thermiques pour des éléments COQUE_SOLIDE. Cela permet donc de faire des calculs de dilatation thermique.
Différentes options CHAR_MECA_TEMP_R, CHAR_MECA_HYDR_R sont ajoutées dans le catalogue meca_coque_solide et adaptées en 3D.

Nouveaux cas-tests : sslp116c

### Fiche 30297 : D114.20 - Modificateurs du maillage pour éléments solides-coques

Cette fiche concerne la restitution partielle des éléments coque-solide de la thèse de M. Dia.
Le mot-clé COQUE-SOLIDE est ajouté à CREA_MAILLAGE. Il va permettre de transformer les HEXA8 en HEXA9 et les PENTA6 en PENTA7.
Dans le cas des HEXA8, il est nécessaire d’orienter les maillage, c’est-à-dire modifier le numérotation afin de détecter l’épaisseur des nœuds. Pour ce faire, l’utilisateur doit ajouter le mot-clé GROUP_MA_SURF qui va identifier la face du haut ou du bas. Lors de l’application de chargement, il faudra choisir la face en cohérence avec AFFE_CHAR_MECA/PRES_REP.

De plus, les opérateurs CREA_MAILLAGE et MODI_MAILLAGE ont été repris pour plus de clarté.
Pour ces transformations, le module CREA_MAILLAGE développé pour MODI_HHO est utilisé en modifiant la méthode numbering_nodes.

Les tests purement informatiques suivants sont ajoutés afin de tester le contenu du maillage en non-régression :

- zzzz416a, b, c

### Fiche 30249 : Restitution d'un modèle hydrique adapté aux forts séchages

Suite aux travaux de Ginger El Tabbal, Prise en compte d'une nouvelle pression hydraulique dans l'écriture des contraintes tenant compte d'une nouvelle formulation thermodynamique. Cette formulation est adaptée aux faibles humidités relatives.
Les nouveaux paramètres matériaux suivants sont donc créés :

- Surface spécifique
- Paramètres BJH
- Paramètres Shuttleworth

En conséquence, les mots-clés A0, SHUTTLE, EPAI, W_BJH et S_BJH sont introduits sous le mot clé THM_DIFFU de DEFI_MATERIAU.
De plus, un nouveau comportement, HYDR_TABBAL, a été créé.

### Fiche 30500 : [3M] Séparer le TE pour AMOR_MECA et RIGI_MECA_HYST

Le fichier te0500 regroupe les options AMOR_MECA et MACA_HYST pour certains types d’éléments 3D et 2D. Dans cette fiche, on s’occupe de séparer ces deux options en deux fichiers différents.

### Fiche 30874 : POU_D_T : Calcul Von Mises pour une poutre circulaire

Le fichier "forma02d.com1" est ajouté afin de pouvoir calculer le maximum des critères de Von Mises dans la section droite d’une poutre circulaire.

Nouveaux cas-tests : forma02d

### Fiche 30746 : [3M] ajouter des cas tests de ROM

Dans le cadre de la thèse de Yichang, de nouveaux cas-tests sont introduits pour la réduction de modèle avec une approche DNF (Direct Normal Form).
Dans le cas de la poutre encastrée aux deux extrémités, deux modélisations sont ajoutées.

### Fiche 30878 : [NG] Introduction d'une formulation U-PSI en IFS

Pour les interactions fluide-structure (FSI), une formulation U-PSI est introduite.
Cette formulation à deux champs est composée de U pour la partie structure et de PSI pour la partir fluide.

Nouveaux cas-tests : ahlv100v, ahlv101g, ahlv101h, ahlv101i, ahlv302h, ahlv302i

### Fiche 30879 : [NG] Modification du chargement VITE_FACE

Naval a besoin d'imposer une vitesse non-normale (onde de choc). Pour ce faire, les catalogues AFFE_CHAR_MECA, AFFE_CHAR_MECA_F et AFFA_CHAR_ACOU sont modifiés . VITE_FACE est alors utilisée afin d’imposer une vitesse  normale à une surface.

### Fiche 30883 : [NG] Ajouter "GROUP_MA" à CALC_MATR_ELEM

Pour les options RIGI_MECA, MASS_MECA et AMOR_MECA, on ajoute la possibilité de spécifier le GROUP_MA. Cela permet de limiter le calcul des matrices dans CALC_MATR_ELEM à un sous-groupe de mailles.

Nouveaux cas-tests : zzzz289a

### Fiche 30314 : CALC_VECT_ELEM et les variables de commande

Le développement de variables de commande pour les éléments meca_d_plan_abso et meca_3d_abso a mis en évidence que CALC_VECT_ELEM ne prend pas en compte la dépendance  des paramètres matériaux à ces variables. Les variables de commandes sont alors ajoutées en entrée du calcul d’ONDE_PLAN dans CALC_VECT_ELEM.
Nouveaux cas-tests : sdlv121b

### Fiche 26978 : Modèle GC sous séisme : jonction voile-plancher en béton armé

Un modèle de comportement non linéaire de jonction voile-plancher en béton armé sous séisme a été proposé pour représenter la dégradation des jonctions en flexiondans le cadre d’une thèse sur les jonctions voiles planchers et de la campagne expérimentale correspondante.

Nouveaux cas-tests : ssnd114a

### Fiche 30897 : [3M] Option MASS_INER pour COQUE_SOLIDE

L’option MASS_INER est créée pour l’élément COQUE_SOLIDE.
Le catalogue meca_coque_solide.py est modifié.

## Dynamique

### Fiche 30417 : Evolution de l'option MODE_VIBR pour fonctionner avec des matrices de raideurs tangentes non symétriques

Dans le cadre d’une analyse modale au cours d’un calcul non linéaire (avec la matrice raideur tangente et non élastique) fait par le CIH, une erreur survient.
Lors de l’utilisation de l’option DYNA_VIBRA de DYNA_NON_LINE, la matrice de raideur tangente n’est pas symétrique et un message d’erreur est renvoyé.
A cette étape est ajoutée une étape pour rendre la matrice symétrique.

### Fiche 31087 : Formation Dynamique - MAJ TD

Cette fiche propose de mettre à jour les énoncés et les fichiers de TD de la Formation Dynamique.

## Post-traitements

### Fiche 29920 : Extraction de mêmes résultats avec Post_releve_T

Lors de l'utilisation de OPERATION='EXTRACTION' dans POST_RELEVE_T pour des groupes de noeuds avec des noueds communs, un résultat faux est engendré.
Lors de l’étape d’élimination des nœuds doublés, seuls les nœuds voisins sont comparés. Cette étape entraîne alors un résultat faux.
Cette dernière étape est donc corrigée.

### Fiche 20813 : A203.xx - Nouveau schéma d'integration des pyramides

Les schémas d’intégration des pyramides quadratiques sont modifiés pour que les points de Gauss à l’extérieur de l’élément ne gênent pas le post-traitement.
Le schéma d’ordre 4 nécessitant 10 points de l’article KubatkoYeagerMaggi2013 a été retenu. On ne traite ici que le cas des isoparamétrique 3D.

### Fiche 30630 : création d'une macro-commande pour des calculs des combinaisons de la mécanique et de la thermique en génie civil

Une macro-commande est créée pour faciliter la mise en donnée et le post-traitement pour les éléments de structures utilisés dans les études d’ingénierie du génie civil, des chargement courants de la mécanique ou de la thermique.

### Fiche 30555 : NGC - Post-traitement Epsilon barre

La norme epsilon barre est définie dans le code RCC-CW. La formule doit correspondre à EPEQ_ELGA et INVA_2.

### Fiche 30849 :  Export du maillage vers medcoupling en mémoire

Une fonction createMedCouplingMesh est ajoutée afin de convertir en mémoire un maillage aster en maillage medcoupling. Le maillage de sortie est alors de type MEDCouplingUMesh (unstructured mesh).

Nouveaux cas-tests : mesh001d, mac3c01b, mac3c06b

## HPC

### Fiche 30580 : Ajouter une méthode print aux MATR_ASSE

Ce développement permet d’ajouter une méthode simple pour imprimer des MATR_ASSE à l’objet AssemblyMatrix. La liste des degrés de liberté ainsi qu’une information sur les matrices HPC (fantomes ou locales) sont imprimées.

## Environnement

### Fiche 30501 : Créer proprement le répertoire temporaire dans CALC_MISS et MACR_RECAL

Lors des tests Miss, l’affichage de l’écho des commandes était perturbé.
Un répertoire temporaire est créé à partir des fichiers de configuration du serveur. Si REPERTOIRE est fourni, il ne faut pas créer un répertoire temporaire.

### Fiche 30539 : aster ne compile pas avec petsc en entier long

On permet la construction d’une version de code_aster en s’appuyant sur une bibliothèque Petsc compilée avec un adressage 64 bits. De nouvelles variables sont introduites afin de détecter la désactivation de préconditionneurs  ML et HYPRE et du solveur SuperLU et de conditionner la taille de PetscInt :

- HAVE_PETSC_ML
- HAVE_PETSC_HYPRE
- HAVE_PETSC_SUPERLU
- HAVE_PETSC_MUMPS
- HAVE_PETSC_64BIT_INDICES

### Fiche 30575 : Pouvoir interroger les "define" depuis Python

Dans cette fiche, on permet l’interrogation la valeur des variables de précompilation depuis Python.

### Fiche 30677 : Ajouter le support de Scibian10 pour code_aster

Les fichiers d’environnement " env.d/scibian10_{std,mpi}.sh " ainsi que la détection de plateforme avec "./waf " sont ajoutés pour la construction de code_aster sur scibian10.

### Fiche 30668 : [Mfront] Montée en 3.4

Une nouvelle version de Mfront (3.4.0) est utilisée dans le cadre des projets MOMA2 et GT VAES.  Les prérequis sont construits sur Cronos, Gaia et Scibian9.

### Fiche 31097 : Utiliser une version de MED avec entiers longs

SALOME 9.7 utilisant med compilé avec des entiers longs, il est nécessaire de faire ce changement dans code_aster.
Les prérequis sont alors mis à jour.

## Programmation

### Fiche 30535 : Rendre le modèle obligatoire pour KinematicMechanicalLoad

Comme le modèle est obligatoire dans AFFE_CHAR_CINE, le modèle est rendu obligatoire pour le constructeur c++/python de KinematicMechanicalLoad et le constructeur par défaut est interdit pour le constructeur par défaut. Le même problème est traité pour la thermique et l’acoustique.

### Fiche 30561 : Warning fortran sur cronos

Des corrections sont apportées aux warning dans le fortran sur cronos pour une meilleure lisibilité :

- tab(1) devient tab(*)
- les matrices ne sont plus manipulées comme des vecteurs

### Fiche 30676 : Ajouter des méthodes à StudyDescription

Un constructeur et les accès au modèles, maillage, caraelem et à la docstring sont ajoutés afin d’ajouter des méthodes à StudyDescription.

### Fiche 30608: Petites incohérences dans le nommage auto des objets JEVEUX

Pour le nommage des objets JEVEUX dans la partie C++, il existe des incohérences.
Les modifications suivantes sont faites :

- ResultName et TemporaryDataStructures sont de type unsigned long int
- Une variable statique est définie pour le nombre d’objets maximum
- Les fonctions sont déplacées dans .cxx
- Le fichier TemporaryDataStructureName.cxx et la classe TemporaryDataStructure sont renommés en  TemporaryDataStructureNaming
- La longueur du nom des objets jeveux en paramètre lors de la constructution automatique des noms d’objets jeveux passe en const int avec une longueur inférieure à 24
